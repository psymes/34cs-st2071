package org.smpte._2071._2012.mdc.client;

public class ClientException extends Exception
{
    private static final long serialVersionUID = 201105011303L;
    
    
    public ClientException()
    {
    }
    

    public ClientException(String message)
    {
        super(message);
    }
    

    public ClientException(Throwable cause)
    {
        super(cause);
    }
    

    public ClientException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
