package org.smpte._2071._2012.mdc.client;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import sun.tools.java.ClassFile;
import sun.tools.java.ClassPath;
import sun.tools.java.Identifier;
import sun.tools.java.Package;


/**
 * The PackageReflector exposes one static method that returns a List of all the
 * classes that are within the specified package.  Requires the java tools.jar.
 *
 * @author Steve Posick
 */
public class PackageReflector
{
    static class SimpleFileFilter implements FilenameFilter
    {
        String extension;

        /**
         * @param ext
         */
        public SimpleFileFilter(String ext)
        {
            extension = ext;
        }

        /**
         * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
         */
        public boolean accept(File dir, String name)
        {
            if (name.toLowerCase().endsWith(extension))
            {
                return true;
            } else
                return false;
        }
    }

    
    /**
     * Returns a list of names for all the classes within the specified package.
     * Searches the java.class.path
     * 
     * @param packageName The name of the Package
     * @return List of the class names.
     * @throws IOException
     */
    public static List<String> getClassNames(String packageName)
    throws IOException
    {
        return getClassNames(packageName);
    }
    
    
    /**
     * Returns a list of names for all the classes within the specified package.
     * Searches the java.class.path plus the specified path
     * 
     * @param packageName The name of the Package
     * @param path Additional path to search
     * @return List of the class names.
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static List<String> getClassNames(String packageName, String path)
    throws IOException
    {
        LinkedList<String> classes = new LinkedList<String>();
        String pathStr = System.getProperty("java.class.path");
        
        if (path != null)
        {
            pathStr += System.getProperty("path.separator") + path;
        }

        ClassPath classpath = new ClassPath(pathStr);
        Package pack = new Package(classpath, Identifier.lookup(packageName));
        
        Enumeration<ClassFile> enumerator = pack.getBinaryFiles();
        while (enumerator.hasMoreElements())
        {
            classes.add(((ClassFile) enumerator.nextElement()).getName());
        }
        
        return classes;
    }
    
    
    /**
     * Returns a list of the classes within the specified package.  That 
     * implement the named interface.
     * Searches the java.class.path plus the specified path
     * 
     * @param packageName The package name
     * @param interfaceName The interface name
     * @return The List of class objects
     */
    public static List<Class<?>> getClassesImplementing(String packageName, Class<?> interfaceClass)
    {
        return getClassesImplementing(packageName, interfaceClass, null);
    }
    
    
    /**
     * Returns a list of the classes within the specified package.  That 
     * implement the named interface.
     * 
     * @param packageName The package name
     * @param interfaceName The interface name
     * @param classPath The path to search
     * @return The List of class objects
     */
    public static List<Class<?>> getClassesImplementing(String packageName, Class<?> interfaceClass, String classPath)
    {
        String packageDir = packageName.replace('.', '/');
        List<Class<?>> classes = new ArrayList<Class<?>>();
        String usableClasspath = classPath != null ? classPath + File.pathSeparator + System.getProperty("java.class.path") : System.getProperty("java.class.path");
        File pathFile;
        ZipFile zipFile;
        Enumeration<? extends ZipEntry> zipEntries;
        ZipEntry zipEntry;
        String files[];
        String token;
        Class<?> entry;
        boolean found;
        
        if (!packageDir.endsWith("/"))
        {    
            packageDir += '/';
        }
        
        try
        {
            Manifest manifest = new Manifest(PackageReflector.class.getResourceAsStream("/META-INF/MANIFEST.MF"));
            Attributes manifestAttributes = manifest.getMainAttributes();
            String manifestClasspath = manifestAttributes.getValue("Class-Path");
            if (manifestClasspath != null)
            {
                if (usableClasspath != null && usableClasspath.length() > 0 && !usableClasspath.endsWith(File.pathSeparator))
                {
                    usableClasspath += File.pathSeparator;
                }
                
                manifestClasspath = manifestClasspath.replace(' ', File.pathSeparatorChar);
                manifestClasspath = manifestClasspath.replace('\t', File.pathSeparatorChar);
                manifestClasspath = manifestClasspath.replace('\n', File.pathSeparatorChar);
                manifestClasspath = manifestClasspath.replace('\r', File.pathSeparatorChar);
                
                usableClasspath += manifestClasspath;
            }
        } catch (Exception e)
        {
            // ignore
        }

        StringTokenizer path = new StringTokenizer(usableClasspath, File.pathSeparator);
        
        while (path.hasMoreTokens())
        {
            token = path.nextToken();

            pathFile = new File(token);
            if (pathFile.isDirectory())
            {
                pathFile = new File(pathFile, packageDir.replace('/', File.separatorChar));
                files = pathFile.list(new SimpleFileFilter(".class"));
                if (files != null)
                {
                    for (int index = 0; index < files.length; index++)
                    {
                        try
                        {
                            entry = Class.forName(packageName + "." + files[index].substring(0, files[index].length() - 6));

                            if (interfaceClass != null)
                            {
                                if (interfaceClass.isAssignableFrom(entry))
                                    found = true;
                                else
                                    found = false;
                            } else
                                found = true;
                            if (found)
                                classes.add(entry);
                        }
                        catch (ClassNotFoundException e)
                        {
                        }
                    }
                }
            } else if (pathFile.isFile())
            {
                try
                {
                    zipFile = new ZipFile(pathFile);
                    zipEntries = zipFile.entries();
                    while (zipEntries.hasMoreElements())
                    {
                        zipEntry = (ZipEntry) zipEntries.nextElement();
                        if (zipEntry.getName().startsWith(packageDir) && zipEntry.getName().toLowerCase().endsWith(".class") && packageDir.lastIndexOf('/') == zipEntry.getName().lastIndexOf('/'))
                        {
                            try
                            {
                                entry = Class.forName(zipEntry.getName().replace('/', '.').substring(0, zipEntry.getName().length() - 6));
                                if (interfaceClass != null)
                                {
                                    if (interfaceClass.isAssignableFrom(entry))
                                    {
                                        found = true;
                                        classes.add(entry);
                                    }
                                }
                            }
                            catch (ClassNotFoundException e)
                            {
                            }
                        }
                    }
                }
                catch (IOException e)
                {
                }
            }
        }

        return classes;
    }


    /**
     * Returns a list of all the classes in the specified
     * 
     * @param packageName
     * @return The List of class objects
     */
    public static List<Class<?>> getClasses(String packageName)
    {
        return getClassesImplementing(packageName, null);
    }
}
