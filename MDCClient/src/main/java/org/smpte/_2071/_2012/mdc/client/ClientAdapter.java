package org.smpte._2071._2012.mdc.client;

import java.io.IOException;

public abstract class ClientAdapter
{
    public enum PRIORITY
    {
        HIGHEST, HIGH, NORMAL, LOW, LOWEST
    }


    private String url;
    
    
    protected ClientAdapter(String url)
    throws ClientException
    {
        this.url = url;
    }


    public String getURL()
    {
        return url;
    }
    
    
    public abstract <T> T connect(Class<T> clazz)
    throws IOException;
    
    
    public static boolean accept(String url)
    {
        return false;
    }
    
    
    public static PRIORITY getPriority()
    {
        return PRIORITY.NORMAL;
    }
}
