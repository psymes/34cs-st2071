package org.smpte._2071._2012.mdc.client.adapter;

import java.io.IOException;
import java.net.URL;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.smpte._2071._2012.mdc.client.ClientAdapter;
import org.smpte._2071._2012.mdc.client.ClientException;

/**
 * JMX over RMI MDC Client Protocol Adapter.
 * 
 * @author Steve Posick
 */
public class JMXClientAdapter extends ClientAdapter
{
    private MBeanServerConnection mbsc;
    
    private JMXConnector connector;
    
    private JMXServiceURL jmxUrl;
    
    private ObjectName objectName;
    
    
    /**
     * @throws IOException 
     * @throws NullPointerException 
     * @throws MalformedObjectNameException 
     */
    public JMXClientAdapter(String url) 
    throws IOException, MalformedObjectNameException, NullPointerException, ClientException
    {
        super(url);
        
        int index;
        if ((index = url.lastIndexOf("/")) >= 0 && url.length() >= index)
        {
            objectName = new ObjectName(url.substring(index + 1));
            jmxUrl = new JMXServiceURL(url.substring(0, index));
            connector = JMXConnectorFactory.connect(jmxUrl);
            mbsc = connector.getMBeanServerConnection();
        } else
        {
            throw new MalformedObjectNameException("ObjectName NOT specified in connection URL \"" + url + "\" format is \"service:jmx:<jmx connector path and protocol>/<object name>\".");
        }
    }
    

    /**
     * @see org.smpte.tc34cs.mdcp.client.ClientAdapter#connect(java.lang.Class)
     */
    @Override
    public <T>T connect(Class<T> clazz)
    throws IOException
    {
        return JMX.newMBeanProxy(mbsc, objectName, clazz);
    }
    

    /**
     * URL in the format of "jmx://<host>:<port>/<object name>
     */
    public static boolean accept(String url)
    {
        return url != null && url.startsWith("service:jmx:");
    }
    

    public static PRIORITY getPriority()
    {
        return PRIORITY.HIGHEST;
    }
}
