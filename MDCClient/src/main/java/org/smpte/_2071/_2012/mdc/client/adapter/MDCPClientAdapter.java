package org.smpte._2071._2012.mdc.client.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.smpte._2071._2012.mdc.client.ClientAdapter;
import org.smpte._2071._2012.mdc.client.ClientException;

public class MDCPClientAdapter extends ClientAdapter
{
    private static final String PROTOCOL_MAPPING_PROPERTIES = "protocol_mapping.properties";
    
    private static final Map<String, String> PROTOCOL_MAPPING;
    
    private URL endpointURL;
    
    static
    {
        URL resourceURL = MDCPClientAdapter.class.getResource(PROTOCOL_MAPPING_PROPERTIES);
        if (resourceURL == null)
        {
            resourceURL = MDCPClientAdapter.class.getResource("/" + PROTOCOL_MAPPING_PROPERTIES);
            if (resourceURL == null)
            {
                resourceURL = ClassLoader.getSystemResource(PROTOCOL_MAPPING_PROPERTIES);
                if (resourceURL == null)
                {
                    resourceURL = ClassLoader.getSystemResource("/" + PROTOCOL_MAPPING_PROPERTIES);
                }
            }
        }
        
        Properties p = new Properties();
        try
        {
            if (resourceURL != null)
            {
                InputStream in = resourceURL.openStream();
                p.load(in);
                
                try
                {
                    in.close();
                } catch (Exception e)
                {
                    // ignore, no one cares if you can close the stream, your done with it!
                }
            }
        } catch (IOException e)
        {
            p.put("mdcp", "http");
            p.put("soap_bp11", "http");
            p.put("soap_bp12", "http");
            p.put("soap_bp20", "http");
            p.put("rest_http", "http");
        }
        
        PROTOCOL_MAPPING = new LinkedHashMap<String, String>();
        for (Object key : p.keySet())
        {
            String value = (String) p.get(key);
            PROTOCOL_MAPPING.put((String) key, value);
        }
    }
    
    
    protected MDCPClientAdapter(String url)
    throws ClientException
    {
        super(url);
        
        int pos = url.indexOf(':');
        if (pos > 0)
        {
            String protocol = url.substring(0, pos);
            String urlProtocol = PROTOCOL_MAPPING.get(protocol);
            
            try
            {
                endpointURL = new URL(urlProtocol + url.substring(pos + 1));
            } catch (MalformedURLException e)
            {
                ClientException ce = new ClientException(e);
                ce.setStackTrace(e.getStackTrace());
                throw ce;
            }
        } else
        {
            throw new ClientException("URL cannot be null!");
        }
    }
    
    
    @Override
    public <T>T connect(Class<T> clazz)
    throws IOException
    {
        WebService ws = clazz.getAnnotation(WebService.class);
        
        if (ws != null)
        {
            Service service = Service.create(new QName(ws.targetNamespace(), ws.name()));
            T reference = service.getPort(clazz);
            ((BindingProvider) reference).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL.toString());

            return reference;
        } else
        {
            throw new IOException(clazz.getName() + " is not Web Service annotated!");
        }
    }
    

    /**
     * URL in the format of "jmx://<host>:<port>/<object name>
     */
    public static boolean accept(String url)
    {
        int pos = url.indexOf(':');
        if (pos > 0)
        {
            String protocol = url.substring(0, pos);
            return url != null && PROTOCOL_MAPPING.containsKey(protocol);
        } else
        {
            return false;
        }
    }
    

    public static PRIORITY getPriority()
    {
        return PRIORITY.HIGHEST;
    }
}
