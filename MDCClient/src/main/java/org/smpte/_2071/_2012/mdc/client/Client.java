package org.smpte._2071._2012.mdc.client;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.smpte._2071._2012.mdc.client.ClientAdapter.PRIORITY;

public class Client
{
    public static final String CLIENT_ADAPTER_PACKAGE = "org.smpte.tc34cs.cxf.client.adapter"; 
    
    protected static final Logger log = Logger.getLogger(Client.class.getName());
    
    protected static HashMap<String, Object> clientsByURL = new HashMap<String, Object>();
    
    protected static ReentrantLock lock = new ReentrantLock();

    private static String clientAdapterClasspath = null;

    private String url;
    
    private Class<? extends ClientAdapter> adapterClass;
    
    private ClientAdapter clientAdapter;
    
    
    public Client(String url)
    throws ClientException, IOException
    {
        this(new String[]{url}, (String) null);
    }
    
    
    public Client(String[] urls)
    throws ClientException, IOException
    {
        this(urls, (String) null);
    }
    
    
    public Client(String url, String classpath)
    throws ClientException, IOException
    {
        this(new String[]{url}, classpath);
    }
    
    
    public Client(String[] urls, String classpath)
    throws ClientException, IOException
    {
        // Find ClientAdpater for the protocol specified in the URL.
        ArrayList<Class<? extends ClientAdapter>> clientAdapters = new ArrayList<Class<? extends ClientAdapter>>();
        
        List<Class<?>> classes = PackageReflector.getClassesImplementing(CLIENT_ADAPTER_PACKAGE, ClientAdapter.class, classpath);
        
        for (Class<?> clazz : classes)
        {
            if (!Modifier.isAbstract(clazz.getModifiers()))
            {
                clientAdapters.add(clazz.asSubclass(ClientAdapter.class));
            }
        }
        
        if (clientAdapters.size() == 0)
        {
            throw new ClientException("Could not locate any ClientAdapters in package \"" + CLIENT_ADAPTER_PACKAGE + "\".");
        } else if (clientAdapters.size() > 1)
        {
            Collections.sort(clientAdapters, new Comparator<Class<? extends ClientAdapter>>()
            {
                @Override
                public int compare(Class<? extends ClientAdapter> thisClass, Class<? extends ClientAdapter> thatClass)
                {
                    PRIORITY thisPriority = PRIORITY.NORMAL;
                    PRIORITY thatPriority = PRIORITY.NORMAL;
                    
                    try
                    {
                        Method method = thisClass.getMethod("getPriority", new Class<?>[0]);
                        Object value = method.invoke(null, new Object[0]);
                        
                        if (value instanceof PRIORITY)
                        {
                            thisPriority = (PRIORITY) value;
                        }
                    } catch (Exception e)
                    {
                        log.log(Level.SEVERE, "Error determining ClientAdpapter priority for class \"" + thisClass.getName() + "\" - " + e.getMessage(), e);
                    }
                    
                    try
                    {
                        Method method = thatClass.getMethod("getPriority", new Class<?>[0]);
                        Object value = method.invoke(null, new Object[0]);
                        
                        if (value instanceof PRIORITY)
                        {
                            thatPriority = (PRIORITY) value;
                        }
                    } catch (Exception e)
                    {
                        log.log(Level.SEVERE, "Error determining ClientAdpapter priority for class \"" + thatClass.getName() + "\" - " + e.getMessage(), e);
                    }
                    
                    return thisPriority.compareTo(thatPriority);
                }
            });
        }
        
        outer:
        for (Class<? extends ClientAdapter> clientAdapter : clientAdapters)
        {
            try
            {
                Method method = clientAdapter.getMethod("accept", new Class<?>[]{String.class});
                for (String testURL : urls)
                {
                    try
                    {
                        Object value = method.invoke(null, new Object[]{testURL});
                        
                        if (value instanceof Boolean && (Boolean) value)
                        {
                            this.adapterClass = clientAdapter;
                            this.url = testURL;
                            break outer;
                        }
                    } catch (Exception e)
                    {
                        log.log(Level.WARNING, "Error determining if ClientAdpapter accepts URL \"" + url + "\" for class \"" + clientAdapter.getName() + "\" - " + e.getMessage(), e);
                    }
                }
            } catch (Exception e)
            {
                log.log(Level.SEVERE, "Error determining if ClientAdpapter accepts any of the URLs \"" + Arrays.toString(urls) + "\" for class \"" + clientAdapter.getName() + "\" - " + e.getMessage(), e);
            }
        }
        
        if (this.adapterClass != null)
        {
            try
            {
                Constructor<? extends ClientAdapter> ctor = this.adapterClass.getConstructor(new Class<?>[] {String.class});
                this.clientAdapter = (ClientAdapter) ctor.newInstance(new Object[]{this.url});
            } catch (InvocationTargetException e)
            {
                throw new ClientException("Error instantiating ClientAdapter \"" + this.adapterClass + "\"!", e.getTargetException());
            } catch (Exception e)
            {
                throw new ClientException("Error instantiating ClientAdapter \"" + this.adapterClass + "\"!", e);
            }
        } else
        {
            throw new ClientException("Could not find a ClientAdapter for any of the URLs \"" + Arrays.toString(urls) + "\"!");
        }
        
        if (this.clientAdapter == null)
        {
            throw new IOException("No ClientAdapter loaded for \"" + getURL() + "\"!");
        }
    }
    

    public String getURL()
    {
        return url;
    }
    

    public Class<? extends ClientAdapter> getAdapterClass()
    {
        return adapterClass;
    }
    

    public <T>T connect(Class<T> clazz)
    throws IOException
    {
        if (clientAdapter != null)
        {
            return clientAdapter.connect(clazz);
        } else
        {
            throw new IOException("Could not connect to \"" + getURL() + "\", no ClientAdapter loaded!");
        }
    }
    
    
    public static void appendToClientAdapterClasspath(String classpath)
    {
        if (classpath == null || classpath.length() == 0)
        {
            return;
        }
        
        if (clientAdapterClasspath == null)
        {
            clientAdapterClasspath = classpath;
        } else
        {
            String[] basePathElements = clientAdapterClasspath.split(File.pathSeparator);
            String[] appendPathElements = classpath.split(File.pathSeparator);
            
            List<String> path = Arrays.asList(basePathElements);
            for (String element : appendPathElements)
            {
                if (!path.contains(element))
                {
                    path.add(element);
                }
            }
            
            StringBuilder newPath = new StringBuilder();
            for (String element : path)
            {
                newPath.append(element).append(File.pathSeparator);
            }
            
            if (newPath.length() >= File.pathSeparator.length())
            {
                newPath.setLength(newPath.length() - File.pathSeparator.length());
            }
            
            clientAdapterClasspath = newPath.toString();
        }
    }
    
    
    public static String getClientAdapterClasspath()
    {
        return clientAdapterClasspath;
    }
    
    
    public static <T> T reference(String url, Class<T> clazz)
    throws ClientException, IOException
    {
        return reference(new String[]{url}, null, clazz);
    }
    
    
    public static <T> T  reference(String url, String classpath, Class<T> clazz)
    throws ClientException, IOException
    {
        return reference(new String[]{url}, classpath, clazz);
    }
    
    
    public static <T> T  reference(String[] urls, Class<T> clazz)
    throws ClientException, IOException
    {
        return reference(urls, null, clazz);
    }
    
    
    @SuppressWarnings("unchecked")
    public static <T> T  reference(String[] urls, String classpath, Class<T> clazz)
    throws ClientException, IOException
    {
        if (urls != null)
        {
            try
            {
                lock.lock();
                Object object = null;
                
                for (String url : urls)
                {
                    object = clientsByURL.get(url);
                    if (object != null)
                    {
                        break;
                    }
                }
                
                if (object == null || !clazz.isAssignableFrom(object.getClass()))
                {
                    Client client = new Client(urls, clientAdapterClasspath);
                    if ((object = client.connect(clazz)) == null)
                    {
                        throw new ClientException("Could not get a client connection using URLs " + Arrays.toString(urls) + ".");
                    } else
                    {
                        try
                        {
                            clientsByURL.put(client.getURL(), object);
                        } catch (Exception e)
                        {
                            log.log(Level.WARNING, "Error registering Client for \"" + client.getURL() + "\" - " + e.getMessage(), e);
                        }
                    }
                }
                
                if (clazz.isAssignableFrom(object.getClass()))
                {
                    return (T) object;
                } else
                {
                    throw new ClientException("Could not get a client connection using URLs " + Arrays.toString(urls) + " that implements \"" + clazz.getName() + "\"!");
                }
            } finally
            {
                lock.unlock();
            }
        } else
        {
            throw new ClientException("Cannot get a client connection for a null URL!");
        }
    }
}
