package org.smpte._2071._2012.mdc.query.syntax;

import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator
{
    private static final String TEST_QUERY = "Field1 == 0 and Field2[1].sub[0].sub > 3 or Field3 > 'C' page(1,20) sort by Field1 desc, Field2['w'].sub asc";
    
    private BundleContext context;
    
    private ServiceRegistration registration;
    
    private static QueryParser parser;
    
    
    @Override
    public void start(BundleContext context)
    throws Exception
    {
        System.out.println("Starting MDC Query Syntax Parser");
        
        this.context = context;
        parser = newQueryParser();
        Properties properties = new Properties();
        properties.put("parser", "antlr");
        properties.put("language", "MDC Query Expression");
        registration = context.registerService(QueryParser.class.getName(), parser, properties);
        
//        System.out.println("<<<<< " + parser.parseToXML(TEST_QUERY) + " >>>>>");
    }
    
    
    @Override
    public void stop(BundleContext context)
    throws Exception
    {
        System.out.println("Stopping MDC Query Syntax Parser");
        if (registration != null)
        {
            registration.unregister();
            registration = null;
        }
    }
    
    
    public static synchronized QueryParser newQueryParser()
    {
        if (parser == null)
        {
            parser = new org.smpte._2071._2012.mdc.query.syntax.impl.QueryParser();
        }
        return parser;
    }
}
