package org.smpte._2071._2012.mdc.query.syntax;

public class QueryParserException extends Exception
{
    /**
     * 
     */
    private static final long serialVersionUID = 5370417301525495208L;


    public QueryParserException()
    {
    }
    
    
    public QueryParserException(String message)
    {
        super(message);
    }
    
    
    public QueryParserException(Throwable cause)
    {
        super(cause);
    }
    
    
    public QueryParserException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
