package org.smpte._2071._2012.mdc.query.syntax.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.smpte._2071._2012.mdc.query.syntax.MDCQuerySyntaxLexer;
import org.smpte._2071._2012.mdc.query.syntax.MDCQuerySyntaxParser;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

/**
 * Test driver program for the ANTLR3 Maven Architype demo
 * 
 * @author Steve Posick
 */
class Main
{
    private static boolean makeDot = false;
    
    static MDCQuerySyntaxLexer lexer;
    
    
    /**
     * Just a simple test driver for the ASP parser to show how to call it.
     */
    public static void main(String[] args)
    {
        try
        {
            lexer = new MDCQuerySyntaxLexer();
            
            if (args.length > 0)
            {
                int s = 0;
                
                if (args[0].startsWith("-dot"))
                {
                    makeDot = true;
                    s = 1;
                }
                
                for (int i = s; i < args.length; i++)
                {
                    parse(new File(args[i]));
                }
            } else
            {
                parse("Field1     == 0 and Field2[1].sub[0].sub > 3 or Field3 > 'C' page(1,20) sort by Field1 desc, Field2['w'].sub asc");
            }
        } catch (Exception ex)
        {
            System.err.println("ANTLR demo parser threw exception:");
            ex.printStackTrace();
        }
    }
    
    
    public static void parse(File source)
    throws Exception
    {
        try
        {
            if (source.isDirectory())
            {
                System.out.println("Directory: " + source.getAbsolutePath());
                String files[] = source.list();
                
                for (int i = 0; i < files.length; i++)
                {
                    parse(new File(source, files[i]));
                }
            } else
            {
                String sourceFile = source.getName();
                
                if (sourceFile.length() > 3)
                {
                    String suffix = sourceFile.substring(sourceFile.length() - 4).toLowerCase();
                    
                    if (suffix.compareTo(".dmo") == 0)
                    {
                        parseSource(source.getAbsolutePath());
                    }
                }
            }
        } catch (Exception ex)
        {
            System.err.println("ANTLR demo parser caught error on file open:");
            ex.printStackTrace();
        }
    }
    
    
    public static void parseSource(String source)
    throws Exception
    {
        // Parse an ANTLR demo file
        try
        {
            // First create a file stream using the povided file/path
            // and tell the lexer that that is the character source.
            // You can also use text that you have already read of course
            // by using the string stream.
            lexer.setCharStream(new ANTLRFileStream(source, "UTF8"));
            
            // Using the lexer as the token source, we create a token
            // stream to be consumed by the parser
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            
            // Now we need an instance of our parser
            MDCQuerySyntaxParser parser = new MDCQuerySyntaxParser(tokens);
            
            System.out.println("file: " + source);
            
            // Provide some user feedback
            System.out.println("    Lexer Start");
            long start = System.currentTimeMillis();
            
            // Force token load and lex (don't do this normally,
            // it is just for timing the lexer)
            tokens.LT(1);
            long lexerStop = System.currentTimeMillis();
            System.out.println("      lexed in " + (lexerStop - start) + "ms.");
            
            // And now we merely invoke the start rule for the parser
            System.out.println("    Parser Start");
            long pStart = System.currentTimeMillis();
            parser.query();
            long stop = System.currentTimeMillis();
            System.out.println("      Parsed in " + (stop - pStart) + "ms.");
        } catch (FileNotFoundException ex)
        {
            // The file we tried to parse does not exist
            System.err.println("\n  !!The file " + source + " does not exist!!\n");
        } catch (Exception ex)
        {
            // Something went wrong in the parser, report this
            System.err.println("Parser threw an exception:\n\n");
            ex.printStackTrace();
        }
    }
    
    
    public static void parse(String string)
    throws Exception
    {
        // Parse an ANTLR demo file
        try
        {
            // First create a file stream using the povided file/path
            // and tell the lexer that that is the character source.
            // You can also use text that you have already read of course
            // by using the string stream.
            lexer.setCharStream(new ANTLRStringStream(string));
            
            // Using the lexer as the token source, we create a token
            // stream to be consumed by the parser
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            
            // Now we need an instance of our parser
            MDCQuerySyntaxParser parser = new MDCQuerySyntaxParser(tokens);
            
            System.out.println("string: " + string);
            
            // Provide some user feedback
            System.out.println("    Lexer Start");
            long start = System.currentTimeMillis();
            
            // Force token load and lex (don't do this normally,
            // it is just for timing the lexer)
            tokens.LT(1);
            long lexerStop = System.currentTimeMillis();
            System.out.println("      lexed in " + (lexerStop - start) + "ms.");
            
            // And now we merely invoke the start rule for the parser
            System.out.println("    Parser Start");
            long pStart = System.currentTimeMillis();
            JAXBElement<? extends QueryExpression> query = parser.query();
            long stop = System.currentTimeMillis();
            System.out.println("      Parsed in " + (stop - pStart) + "ms.");
            
            if (query != null)
            {
                StringWriter writer = new StringWriter();
                JAXBContext ctx = JAXBContext.newInstance("org.smpte_ra.schemas._2071._2012.mdcf.device:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.device.control:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.device.mode:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.device.event:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.identity:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.media:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.query:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.security:" +
                		"org.smpte_ra.schemas._2071._2012.mdcf.types");
                Marshaller marshaller = ctx.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.marshal(query, writer);
                System.out.println("Expression:\n" + writer.toString());
            } else
            {
                throw new Exception("Parser failed to compile Query Statement!");
            }
        } catch (Exception ex)
        {
            // Something went wrong in the parser, report this
            System.err.println("Parser threw an exception:\n");
            ex.printStackTrace();
        }
    }
}
