package org.smpte._2071._2012.mdc.query.syntax.impl;

import java.io.StringWriter;
import java.text.ParseException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.smpte._2071._2012.mdc.query.syntax.MDCQuerySyntaxLexer;
import org.smpte._2071._2012.mdc.query.syntax.MDCQuerySyntaxParser;
import org.smpte._2071._2012.mdc.query.syntax.QueryParserException;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public class QueryParser implements org.smpte._2071._2012.mdc.query.syntax.QueryParser
{
    private static final Class<?> DEVICE_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.ObjectFactory.class;
    
    private static final Class<?> DEVICE_CONTROL_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.control.ObjectFactory.class;
    
    private static final Class<?> DEVICE_MODE_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ObjectFactory.class;
    
    private static final Class<?> DEVICE_EVENT_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.event.ObjectFactory.class;
    
    private static final Class<?> IDENTITY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.identity.ObjectFactory.class;
    
    private static final Class<?> MEDIA_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory.class;
    
    private static final Class<?> QUERY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory.class;
    
    private static final Class<?> SECURITY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.security.ObjectFactory.class;
    
    private static final Class<?> TYPES_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.types.ObjectFactory.class;
    
    
    private final MDCQuerySyntaxLexer lexer = new MDCQuerySyntaxLexer();
    
    private JAXBContext defaultContext;


    private JAXBContext initializeContext() 
    throws JAXBException
    {
        if (defaultContext == null)
        {
            try
            {
                defaultContext = JAXBContext.newInstance(DEVICE_FACTORY.getPackage().getName() + ":" +
                                                         DEVICE_MODE_FACTORY.getPackage().getName() + ":" +
                                                         DEVICE_EVENT_FACTORY.getPackage().getName() + ":" +
                                                         DEVICE_CONTROL_FACTORY.getPackage().getName() + ":" +
                                                         IDENTITY_FACTORY.getPackage().getName() + ":" +
                                                         MEDIA_FACTORY.getPackage().getName() + ":" +
                                                         QUERY_FACTORY.getPackage().getName() + ":" +
                                                         SECURITY_FACTORY.getPackage().getName() + ":" +
                                                         TYPES_FACTORY.getPackage().getName(), QueryParser.class.getClassLoader());
            } catch (JAXBException e)
            {
                e.printStackTrace(System.err);
                throw e;
            }
        }
        
        return defaultContext;
    }
    
    
    @Override
    public JAXBElement<? extends QueryExpression> parse(String query)
    throws QueryParserException
    {
        try
        {
            JAXBElement<? extends QueryExpression> expression = null;
            
            // First create a string stream using the provided query and tell the lexer that that is the character source.
            lexer.setCharStream(new ANTLRStringStream(query));
            
            // Using the lexer as the token source, we create a token stream to be consumed by the parser
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            
            // Now we need an instance of our parser
            MDCQuerySyntaxParser parser = new MDCQuerySyntaxParser(tokens);
            
            System.out.println("Parsing MDC Query \"" + query + "\"");
            
            // Invoke the start rule for the parser
            long start = System.currentTimeMillis();
            expression = parser.query();
            long stop = System.currentTimeMillis();
            System.out.println("Parsed MDC Query \"" + query + "\" - took " + (stop - start) + "millis.");
            
            if (expression != null)
            {
                return expression;
            } else
            {
                throw new ParseException("No query expression found.", 0);
            }
        } catch (Throwable e)
        {
            e.printStackTrace(System.err);
            QueryParserException p = new QueryParserException(e.getClass().getSimpleName() + ": " + e.getMessage(), e);
            p.setStackTrace(e.getStackTrace());
            throw p;
        }
    }
    
    
    @Override
    public String parseToXML(String query)
    throws QueryParserException
    {
        try
        {
            JAXBContext ctx = initializeContext();
            
            StringWriter writer = new StringWriter();
            Marshaller marshaller = ctx.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(parse(query), writer);
            String xml = writer.toString();
            
            System.out.println("Query: \"" + query + "\"\nParses to Expression:\n" + xml);
            
            return xml;
        } catch (JAXBException e)
        {
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            
            QueryParserException p = new QueryParserException(e.getMessage(), e);
            p.setStackTrace(e.getStackTrace());
            throw p;
        }
    }
}
