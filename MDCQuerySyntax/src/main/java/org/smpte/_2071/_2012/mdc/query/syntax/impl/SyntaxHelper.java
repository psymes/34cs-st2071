package org.smpte._2071._2012.mdc.query.syntax.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBElement;

import org.antlr.runtime.Token;
import org.smpte._2071._2012.mdc.query.syntax.MDCQuerySyntaxParser;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatSegment;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaSegment;
import org.smpte_ra.schemas._2071._2012.mdcf.query.AND;
import org.smpte_ra.schemas._2071._2012.mdcf.query.CONTAINS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.EQUALS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.GREATERTHAN;
import org.smpte_ra.schemas._2071._2012.mdcf.query.IMPLEMENTS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.LESSTHAN;
import org.smpte_ra.schemas._2071._2012.mdcf.query.MATCHES;
import org.smpte_ra.schemas._2071._2012.mdcf.query.NOT;
import org.smpte_ra.schemas._2071._2012.mdcf.query.OR;
import org.smpte_ra.schemas._2071._2012.mdcf.query.PAGE;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.query.SORTBY;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public class SyntaxHelper
{
    private static final String TIMECODE_PATTERN = "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(([:;][0-9]{1,3})|([\\.][0-9]+))?";

    private static org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory queryFactory = new org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory();
    
    private static org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory mediaFactory = new org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory();
    
    private static HashMap<String, Pattern> patterns = new HashMap<String, Pattern>();
    
    
    public static final PAGE newPAGE(Token offset, Token pageSize)
    {
        BigInteger _offset = BigInteger.valueOf(Long.parseLong(offset.getText()));
        BigInteger _pageSize = BigInteger.valueOf(Long.parseLong(pageSize.getText()));
        
        PAGE page = new PAGE();
        page.setOffset(_offset);
        page.setPageSize(_pageSize);
        
        return page;
    }
    
    
    public static final SORTBY newSORTBY(String field, Token direction)
    {
        SORTBY sortBy = new SORTBY();
        
        sortBy.setField(field);
        
        if (direction != null)
        {
            if (direction.getType() == MDCQuerySyntaxParser.ASCENDING)
            {
                sortBy.setDescending(Boolean.FALSE);
            } else
            {
                sortBy.setDescending(Boolean.TRUE);
            }
        } else
        {
            sortBy.setDescending(Boolean.TRUE);
        }
        
        return sortBy;
    }


    public static JAXBElement<? extends QueryExpression> onBooleanExpression(JAXBElement<? extends QueryExpression> op, JAXBElement<? extends QueryExpression> expr1, JAXBElement<? extends QueryExpression> expr2)
    {
        QueryExpression expr = op.getValue();
        
        if (expr instanceof AND)
        {
            ((AND) expr).getQueryExpression().add(expr1);
            ((AND) expr).getQueryExpression().add(expr2);
        } else if (expr instanceof OR)
        {
            ((OR) expr).getQueryExpression().add(expr1);
            ((OR) expr).getQueryExpression().add(expr2);
        } else if (expr instanceof NOT)
        {
            ((NOT) expr).setQueryExpression(expr1);
        }
        return op;
    }

    
    public static JAXBElement<? extends QueryExpression> onExpression(QueryExpression op, String field, Object val) 
    {
        if (val != null && val instanceof String)
        {
            String value = (String) val;
            if (value.length() > 1)
            {
                if (value.charAt(0) == '\"' && value.charAt(value.length() - 1) == '\"')
                {
                    val = value.substring(1, value.length() - 1);
                }
            }
        }
        
        if (op instanceof MATCHES)
        {
            MATCHES expr = (MATCHES) op;

            expr.setField(field);
            expr.setRegExp((String) val);
            return queryFactory.createMATCHES(expr);
        } else if (op instanceof CONTAINS)
        {
            CONTAINS expr = (CONTAINS) op;
            
            if (val != null)
            {
                if (val instanceof JAXBElement && ((JAXBElement) val).getValue() instanceof MediaPointer)
                {
                    expr.setMediaPointer((JAXBElement<? extends MediaPointer>) val);
                } else
                {
                    throw new IllegalArgumentException("Value for Contains must be a MediaPointer.");
                }

                return queryFactory.createCONTAINS(expr);
            } else
            {
                throw new IllegalArgumentException("onExpression wrong value for op=" + op.getClass().getName() + ", value=" + (val != null ? val.getClass().getName() : "null"));
            }
        } else if (op instanceof IMPLEMENTS)
        {
            if (val != null && val instanceof Capabilities)
            {
                IMPLEMENTS expr = (IMPLEMENTS) op;
                
                expr.setInterfaces((Capabilities) val);
                return queryFactory.createIMPLEMENTS(expr);
            } else
            {
                throw new IllegalArgumentException("onExpression wrong value for op=" + op.getClass().getName() + ", value=" + (val != null ? val.getClass().getName() : "null"));
            }
        } else if (op instanceof EQUALS)
        {
            EQUALS expr = (EQUALS) op;
            
            if (val instanceof BigDecimal)
            {
                expr.setField(field);
                expr.setNumber((BigDecimal) val);
                return queryFactory.createEQUALS(expr);
            } else if (val instanceof Date)
            {
                throw new UnsupportedOperationException("onExpression value is a date.  Not supported yet.");
            } else
            {
                expr.setField(field);
                expr.setObject(val);
                return queryFactory.createEQUALS(expr);
            }
        } else if (op instanceof LESSTHAN)
        {
            LESSTHAN expr = (LESSTHAN) op;
            
            if (val instanceof BigDecimal)
            {
                expr.setField(field);
                expr.setNumber((BigDecimal) val);
                return queryFactory.createLESSTHAN(expr);
            } else if (val instanceof Date)
            {
                throw new UnsupportedOperationException("onExpression value is a date.  Not supported yet.");
            } else
            {
                expr.setField(field);
                expr.setObject(val);
                return queryFactory.createLESSTHAN(expr);
            }
        } else if (op instanceof GREATERTHAN)
        {
            GREATERTHAN expr = (GREATERTHAN) op;
            
            if (val instanceof BigDecimal)
            {
                expr.setField(field);
                expr.setNumber((BigDecimal) val);
                return queryFactory.createGREATERTHAN(expr);
            } else if (val instanceof Date)
            {
                throw new UnsupportedOperationException("onExpression value is a date.  Not supported yet.");
            } else
            {
                expr.setField(field);
                expr.setObject(val);
                return queryFactory.createGREATERTHAN(expr);
            }
        }
        
        throw new IllegalArgumentException("onExpression unknown parameters (op=" + op.getClass().getName() + ", field=" + (field != null ? field : "null") + ", value=" + val != null ? val.getClass().getName() : "null");
    }


    public static JAXBElement<? extends QueryExpression> onNot(JAXBElement<? extends QueryExpression> expr)
    {
        NOT not = new NOT();
        not.setQueryExpression(expr);
        return queryFactory.createNOT(not);
    }


    public static JAXBElement<? extends MediaPointer> onMediaPointer(String umn, String format, String inpoint, String outpoint, String offsetType)
    {
        BigInteger inpointOffset = null;
        try
        {
            inpointOffset = new BigInteger(inpoint);
        } catch (Exception e)
        {
        }
        
        BigInteger outpointOffset = null;
        try
        {
            outpointOffset = new BigInteger(outpoint);
        } catch (Exception e)
        {
        }
        
        DateTime inpointTime = null;
        try
        {
            inpointTime = parseDateTime(inpoint);
        } catch (Exception e)
        {
        }
        DateTime outpointTime = null;
        try
        {
            outpointTime = parseDateTime(outpoint);
        } catch (Exception e)
        {
        }
        
        if (inpointTime != null && outpointTime != null)
        {
            if (format == null)
            {
                MediaSegment segment = new MediaSegment();
                segment.setSource(umn);
                segment.setInpoint(inpointTime);
                segment.setInpointOffset(inpointOffset);
                segment.setOutpoint(inpointTime);
                segment.setOutpointOffset(outpointOffset);
                return mediaFactory.createMediaSegment(segment);
            } else
            {
                MediaFormatSegment segment = new MediaFormatSegment();
                segment.setSource(umn);
                segment.setFormat(format);
                segment.setInpoint(inpointTime);
                segment.setInpointOffset(inpointOffset);
                segment.setOutpoint(inpointTime);
                segment.setOutpointOffset(outpointOffset);
                return mediaFactory.createMediaFormatSegment(segment);
            }
        } else
        {
            if (format == null)
            {
                MediaPointer pointer = new MediaPointer();
                pointer.setSource(umn);
                pointer.setInpointOffset(inpointOffset);
                pointer.setOutpointOffset(outpointOffset);
                return mediaFactory.createMediaPointer(pointer);
            } else
            {
                MediaFormatPointer pointer = new MediaFormatPointer();
                pointer.setSource(umn);
                pointer.setFormat(format);
                pointer.setInpointOffset(inpointOffset);
                pointer.setOutpointOffset(outpointOffset);
                return mediaFactory.createMediaFormatPointer(pointer);
            }
        }
    }


    private static DateTime parseDateTime(String text)
    {
        if (text != null && text.length() > 0)
        {
            Pattern pattern = getPattern(TIMECODE_PATTERN); //([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})(([:;][0-9]{1,3})|([\\.][0-9]+))?
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches())
            {
                int groups = matcher.groupCount();
                
                String hourStr = null;
                String minuteStr = null;
                String secondStr = null;
                String subSecondStr = null;
                String frameStr = null;
                String microSecStr = null;
                
                if (groups > 0)
                {
                    hourStr = matcher.group(1);
                }
                
                if (groups > 1)
                {
                    minuteStr = matcher.group(2);
                }
                
                if (groups > 2)
                {
                    secondStr = matcher.group(3);
                }
                
                if (groups > 3)
                {
                    subSecondStr = matcher.group(4);
                }
                
                if (groups > 4)
                {
                    frameStr = matcher.group(5);
                }
                
                if (groups > 5)
                {
                    microSecStr = matcher.group(6);
                }
                
                DateTime time;
                if (frameStr == null)
                {
                    time = new DateTime();
                } else
                {
                    time = new FramedTime();
                    ((FramedTime) time).setFrame(Long.parseLong(frameStr));
                }
                
                if (hourStr != null)
                {
                    time.setHour(Integer.parseInt(hourStr));
                }
                if (minuteStr != null)
                {
                    time.setMinute(Integer.parseInt(minuteStr));
                }
                if (secondStr != null)
                {
                    time.setSecond(Integer.parseInt(secondStr));
                }
                
                if (microSecStr != null)
                {
                    time.setMicrosecond(Integer.parseInt(microSecStr));
                }
                
                if (frameStr != null)
                    System.out.printf("Parsed Media Pointer Timecode: %s:%s:%s:%s\n", hourStr, minuteStr, secondStr, frameStr);
                else
                    System.out.printf("Parsed Media Pointer Timecode: %s:%s:%s%s\n", hourStr, minuteStr, secondStr, subSecondStr);
                
                return time;
            } else
            {
                Exception e = new Exception("Could not find a Pattern that matches pattern \"" + pattern + "\".");
                e.printStackTrace(System.err);
            }
        }
        
        return null;
    }

    
    private static Pattern getPattern(String expr)
    {
        Pattern pattern = patterns.get(expr);
        if (pattern == null)
        {
            pattern = Pattern.compile(expr);
            patterns.put(expr, pattern);
        }
        
        return pattern;
    }    
}
