package org.smpte._2071._2012.mdc.query.syntax;

import javax.xml.bind.JAXBElement;

import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public interface QueryParser
{
    public JAXBElement<? extends QueryExpression> parse(String query)
    throws QueryParserException;
    
    
    public String parseToXML(String query)
    throws QueryParserException;
}
