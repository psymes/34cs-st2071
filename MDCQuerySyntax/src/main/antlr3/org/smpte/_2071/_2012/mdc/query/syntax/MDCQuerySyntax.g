grammar MDCQuerySyntax;

options 
{
    // Default but name it anyway
    language   = Java;

    // Produce an AST
//    output    = AST;

    // Use ANTLR built-in CommonToken for tree nodes
//    ASTLabelType = CommonToken;
}

tokens 
{
    AND='and';
    OR='or';
    NOT='not';
    EQUALS='=';
    LESS_THAN='<';
    GREATER_THAN='>';
    CONTAINS='contains';
    IMPLEMENTS='implements';
    MATCHES='matches';
    ASCENDING='asc';
    DECENDING='desc';
    SORT='sort';
    BY='by';
    PAGE='page';
    POINTER='pointer';
    SEGMENT='segment';
    FRAMES='frames';
    SECONDS='seconds';
    MICROSECONDS='micros';
    BYTES='bytes';
}

// What package should the generated source exist in?
@header 
{
    package org.smpte._2071._2012.mdc.query.syntax;
    
    import org.smpte._2071._2012.mdc.query.syntax.impl.*;
    import org.smpte_ra.schemas._2071._2012.mdcf.query.*;
    import org.smpte_ra.schemas._2071._2012.mdcf.media.*;
    import java.math.BigInteger;
    import java.math.BigDecimal;
    import javax.xml.bind.JAXBElement;
}

@lexer::header 
{
    package org.smpte._2071._2012.mdc.query.syntax;
}

@members
{
    org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory queryFactory = new org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory();
}

query returns [JAXBElement<? extends QueryExpression> expression]
scope 
{ 
    List<SORTBY> sort;
    PAGE page;
    JAXBElement<? extends QueryExpression> expr;
} 
@init 
{ 
    $query::sort = new ArrayList<SORTBY>(); 
}
    : WS? query_expression /*WS*/ page? /*WS*/ sortBy? 
    {
        if ($query::expr != null)
        {
          $query::expr.getValue().setPAGE($query::page);
	        $query::expr.getValue().getSORTBY().addAll($query::sort); 
	        $expression = $query::expr;
        } 
    }
    ;

query_expression
    : (( NOT WS? '(' WS? boolean_expression WS? ')' ) { $query::expr = SyntaxHelper.onNot($query::expr); }
    | ( boolean_expression ))
    ;

boolean_expression 
    : ( expr1=expression ( /*WS*/ op=boolean_operator /*WS*/ expr2=expression 
    {
         if ($query::expr == null)
         {
           $query::expr = SyntaxHelper.onBooleanExpression(op, expr1, expr2);
         } else
         {
           $query::expr = SyntaxHelper.onBooleanExpression(op, $query::expr, expr2);
         }
    }
    )* )
    {
       if ($query::expr == null)
       {
          $query::expr = expr1;
       }
    }
    ;

expression returns [JAXBElement<? extends QueryExpression> result]
scope
{
    List<String> ifaces;
    QueryExpression operation;
    Object value;
}
@init
{
    $expression::ifaces = new ArrayList<String>();
}
    : ( ( n=NOT /*WS*/ )?
      ( fld=field /*WS?*/ ( EQUALS { $expression::operation = queryFactory.createEQUALS(); }
              | LESS_THAN { $expression::operation = queryFactory.createLESSTHAN(); }
              | GREATER_THAN { $expression::operation = queryFactory.createGREATERTHAN(); }
              ) /*WS?*/
              ( i=INT { $expression::value = new BigDecimal(new BigInteger(i.getText())); }
              | f=FLOAT { $expression::value = new BigDecimal(f.getText()); }
              | s=STRING { $expression::value = s.getText(); }
              | c=CHAR { $expression::value = c.getText(); }
              | h=HEX_STRING { $expression::value = new BigDecimal(new BigInteger(h.getText(), 16)); }
              )
      )
      | ( CONTAINS { $expression::operation = queryFactory.createCONTAINS(); } /*WS*/ mp=mediaPointer { $expression::value = mp; } )
      | ( IMPLEMENTS { $expression::operation = queryFactory.createIMPLEMENTS(); }  /*WS*/ s1=STRING { $expression::ifaces.add($s1.text); } ( /*WS?*/ ',' /*WS?*/ s2=STRING { $expression::ifaces.add($s2.text); } )* )
      | ( fld=field /*WS*/ MATCHES { $expression::operation = queryFactory.createMATCHES(); } /*WS*/ s1=STRING { $expression::value = $s1.text; } )
    )
    { if (n != null) 
      {
        $result = SyntaxHelper.onNot(SyntaxHelper.onExpression($expression::operation, $fld.text, $expression::value)); 
      } else 
      { 
        $result = SyntaxHelper.onExpression($expression::operation, $fld.text, $expression::value); 
      }
    }
    ;

page 
    : PAGE '(' offset=INT ',' pageSize=INT ')' {$query::page = SyntaxHelper.newPAGE(offset, pageSize); }
    ;
    
mediaPointer returns [JAXBElement<? extends MediaPointer> result]
    : ( ( offset_type=( FRAMES | SECONDS | MICROSECONDS ) /*WS*/ ( ( inpoint=INOUTPOINT /*WS?*/ '-' /*WS?*/ outpoint=INOUTPOINT ) | ( inpoint=INT /*WS?*/ '-' /*WS?*/ outpoint=INT ) ) ( /*WS*/ 'of format' /*WS*/ format=STRING )? /*WS*/ 'from' /*WS*/ umn=STRING ) { $result = SyntaxHelper.onMediaPointer($umn.text, $format.text, $inpoint.text, $outpoint.text, $offset_type.text); }
    | ( offset_type=BYTES /*WS*/ inpoint=INT /*WS?*/ '-' /*WS?*/ outpoint=INT ( /*WS*/ 'of format' /*WS*/ format=STRING )? /*WS*/ 'from' /*WS*/ umn=STRING ) { $result = SyntaxHelper.onMediaPointer($umn.text, $format.text, $inpoint.text, $outpoint.text, $offset_type.text); }
    )
    ;
  
sortBy 
    : SORT /*WS*/ BY ( ( /*WS?*/ '(' /*WS?*/ sort_field ( /*WS?*/ ',' /*WS?*/ sort_field )* /*WS?*/ ')') | (/*WS*/ sort_field ( /*WS?*/ ',' /*WS?*/ sort_field )* ) ) 
    ;
  
sort_field 
    : fld=field ( /*WS*/ direction=( ASCENDING | DECENDING ) )? { $query::sort.add(SyntaxHelper.newSORTBY($fld.text, $direction)); } 
    ;

boolean_operator returns [JAXBElement<? extends QueryExpression> result]
    : ( AND { $result = queryFactory.createAND(new AND()); }
    | OR { $result = queryFactory.createOR(new OR()); }
    ) 
    ;

field returns [String result]
@init
{
    StringBuilder fld = new StringBuilder();
}
    : myField=( ident=IDENT { fld.append($ident.text); } ( array=ARRAY { fld.append($array.text); })? (tkn='.' { fld.append($tkn.text); } ident=IDENT { fld.append($ident.text); } ( array=ARRAY { fld.append($array.text); } )?)* ) { $result = fld.toString(); }
    ;

//OFFSET_TYPE : ( FRAMES | SECONDS | MICROSECONDS | BYTES ) ;

IDENT  :  ( LETTER | UNDERSCORE ) ( LETTER | DIGIT | UNDERSCORE )* ;

INT :	DIGIT+ ;

FLOAT 
    : DIGIT+ '.' DIGIT* EXPONENT?
    | '.' DIGIT+ EXPONENT?
    | DIGIT+ EXPONENT
    ;

WS  
    :   ( ' '
        | '\t'
        | '\f'
        | '\r'
        | '\n' )+ {skip();}
    ;
    
HEX_STRING 
    : ('0x' HEX_DIGIT+)
    | (HEX_DIGIT+ ('h' | 'H'))
    ;
    
INOUTPOINT : ( DIGIT DIGIT ':' DIGIT DIGIT ':' DIGIT DIGIT ( ':' | ';' ) DIGIT DIGIT ) ;

STRING :  DQUOTE CHAR_SEQUENCE DQUOTE ;

CHAR :  SQUOTE CHARACTER SQUOTE ;

ARRAY : ( '[' (INT | STRING | CHAR) ']' ) ;

COMMENT 
    :   '//' ~('\n'|'\r')* '\r'? '\n' {skip();}
    |   '/*' ( options {greedy=false;} : . )* '*/' {skip();}
    ;

fragment  
CHAR_SEQUENCE : ( ESC_SEQ | CHARACTER )+ ;

//fragment
//URI_VALUE : ( ~('%' | '\r' | '\n' | '\r\n' | ',' | ';' | ' ' | '\t' | DQUOTE )+ ) | ( DQUOTE ~('%' | '\r' | '\n' | '\r\n' | ',' | ';' | ' ' | '\t' | DQUOTE )+ DQUOTE ) ;

fragment
SQUOTE : '\'' ;
  
fragment
DQUOTE : '"' ;

fragment
UNDERSCORE : '_' ;
  
fragment
LETTER : ( 'a'..'z' | 'A'..'Z' ) ;

fragment
DIGIT : ( '0'..'9' ) ; 

fragment
HEX_DIGIT : (DIGIT | 'a'..'f' | 'A'..'F') ;

fragment
EXPONENT : ( 'e' | 'E' ) ( '+' | '-' )? ( DIGIT )+ ;

fragment
ESC_SEQ  : ( '%' HEX_DIGIT HEX_DIGIT ) ;

fragment
CHARACTER : ~('%' | '\r' | '\n' | '\r\n' | DQUOTE ) ;
