package org.smpte._2071._2012.mdc.search.media;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MediaList implements Serializable
{
    private static final long serialVersionUID = 201207251549L;
    
    private List<Media> media;
    
    private int pageOffset;
    
    private int pageSize;
    

    public MediaList()
    {
    }
    
    
    public MediaList(List<Media> media, int pageOffset, int pageSize)
    {
        this.media = new ArrayList<Media>(media);
        this.pageOffset = pageOffset;
        this.pageSize = pageSize;
    }


    public List<Media> getMedia()
    {
        if (media == null)
        {
            media = new ArrayList<Media>();
        }
        
        return media;
    }


    public void setMedia(List<Media> media)
    {
        this.media = new ArrayList<Media>(media);
    }


    public int getPageOffset()
    {
        return pageOffset;
    }


    public void setPageOffset(int pageOffset)
    {
        this.pageOffset = pageOffset;
    }


    public int getPageSize()
    {
        return pageSize;
    }


    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }
}
