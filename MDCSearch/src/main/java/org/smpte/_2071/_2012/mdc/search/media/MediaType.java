package org.smpte._2071._2012.mdc.search.media;

public enum MediaType
{
    MEDIA, MEDIA_ASSET, MEDIA_FILE, MEDIA_INSTANCE, MEDIA_CONTAINER, MEDIA_BUNDLE
}
