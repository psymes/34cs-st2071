package org.smpte._2071._2012.mdc.search.media;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaAsset;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaBundle;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaContainer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFile;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaInstance;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

public class Media implements Serializable
{
    private static final long serialVersionUID = 201207211050L;
    
    private static final String MEDIA_TAG_PREFIX = "Media";
    
    private static final int MEDIA_TAG_PREFIX_LENGTH = MEDIA_TAG_PREFIX.length();
    
    private MediaType type;
    
    private String udn;
    
    private String umn;
    
    private String location;
    
    private String name;
    
    private int duration;
    
    private String mimeType;
    
    private long size;
    
    private Date created;
    
    private Date modified;
    
    private Map<String, String> metadata;
    
    
    public Media()
    {
        
    }
    
    
    public Media(MediaType type, String umn, String name, String location, Date created, Date modified, Map<String, String> metadata, String udn, int duration, String mimeType, long size)
    {
        super();
        this.type = type;
        this.udn = udn;
        this.umn = umn;
        this.location = location;
        this.name = name;
        this.duration = duration;
        this.mimeType = mimeType;
        this.size = size;
        this.created = created;
        this.modified = modified;
        this.metadata = metadata;
    }
    
    
    public Media(CharSequence xml)
    {
        // TODO: Parse XML String to get Media.
    }


    public String getUDN()
    {
        return udn;
    }
    
    
    public void setUDN(String udn)
    {
        this.udn = udn;
    }
    
    
    public String getUMN()
    {
        return umn;
    }
    
    
    public void setUMN(String umn)
    {
        this.umn = umn;
    }
    
    
    public MediaType getType()
    {
        return type;
    }
    
    
    public void setType(MediaType type)
    {
        this.type = type;
    }
    
    
    public String getLocation()
    {
        return location;
    }
    
    
    public void setLocation(String location)
    {
        this.location = location;
    }
    
    
    public String getName()
    {
        return name;
    }
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    
    public int getDuration()
    {
        return duration;
    }
    
    
    public void setDuration(int duration)
    {
        this.duration = duration;
    }
    
    
    public String getMimeType()
    {
        return mimeType;
    }
    
    
    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }
    
    
    public long getSize()
    {
        return size;
    }
    
    
    public void setSize(long size)
    {
        this.size = size;
    }
    
    
    public Date getCreated()
    {
        return created;
    }
    
    
    public void setCreated(Date created)
    {
        this.created = created;
    }
    
    
    public Date getModified()
    {
        return modified;
    }
    
    
    public void setModified(Date modified)
    {
        this.modified = modified;
    }
    
    
    public Map<String, String> getMetadata()
    {
        return metadata;
    }
    
    
    public void setMetadata(Map<String, String> metadata)
    {
        this.metadata = metadata;
    }
    
    
    private static boolean compareSequence(CharSequence source, int sequenceStart, CharSequence test)
    {
        int sequenceLength = test.length();
        int sequenceEnd = sequenceStart + sequenceLength;
        
        if (sequenceEnd <= source.length())
        {
            for (int index = sequenceStart; index < sequenceEnd; index++)
            {
                char srcChar = source.charAt(sequenceStart + index);
                char testChar = test.charAt(index);
                if (!(srcChar == testChar || Character.toLowerCase(srcChar) == Character.toLowerCase(testChar)))
                {
                    return false;
                }
            }
            
            return true;
        } else
        {
            return false;
        }
    }
    
    
    private static FastCharSequence findMediaTag(int startPos, CharSequence source)
    {
        char c = 0;
        int firstCharPos = -1;
        int testLength = source.length() - MEDIA_TAG_PREFIX_LENGTH;
        int mediaStartPos = -1;
        int mediaEndPos = -1;
        FastCharSequence mediaTag = null;
        
        for (int index = startPos; index < testLength; index++)
        {
            c = source.charAt(index);
            
            if (c == '<')
            {
                // Skip namespace
                int indexSave = index;
                for (; index < testLength; index++)
                {
                    switch (source.charAt(index))
                    {
                        case ':' :
                            index++;
                            break;
                        case '>' :
                            index = indexSave;
                            break;
                    } 
                }
                
                firstCharPos = index + 1;
                index = firstCharPos + MEDIA_TAG_PREFIX_LENGTH;

                if (compareSequence(source, firstCharPos, MEDIA_TAG_PREFIX))
                {
                    mediaStartPos = firstCharPos;
                    for (; index < testLength; index++)
                    {
                        c = source.charAt(index);
                        
                        if (c == ' ' || c == '\t' || c == '\n' || c == '\r')
                        {
                            mediaEndPos = index;
                            break;
                        }
                    }
                    
                    mediaTag = new FastCharSequence(source, mediaStartPos, mediaEndPos);
                }
            }
        }
        
        return mediaTag;
    }
    
    
    private static int findTagEnd(CharSequence source, int startPos, CharSequence tag)
    {
        char c = 0;
        char lastc = 0;
        int firstCharPos = -1;
        int testLength = source.length() - tag.length();
        
        for (int index = startPos; index < testLength; index++)
        {
            c = source.charAt(index);
            
            if (lastc == '<' && c == '/')
            {
                // Skip namespace
                int indexSave = index;
                for (; index < testLength; index++)
                {
                    switch (source.charAt(index))
                    {
                        case ':' :
                            index++;
                            break;
                        case '>' :
                            index = indexSave;
                            break;
                    } 
                }
                
                firstCharPos = index + 1;
                index = firstCharPos + MEDIA_TAG_PREFIX_LENGTH;

                if (compareSequence(source, firstCharPos, tag))
                {
                    for (; index < testLength; index++)
                    {
                        c = source.charAt(index);
                        
                        if (c == '>')
                        {
                            return index + 1;
                        }
                    }
                }
            }
            
            lastc = c;
        }
        
        return -1;
    }
    
    
    public static List<Media> extractMedia(CharSequence xml)
    {
        ArrayList<Media> list = new ArrayList<Media>();
        FastCharSequence mediaTag = null;
        int startPos = 0;
        int endPos = 0;
        
        if (xml != null && xml.length() > 0)
        {
            while ((mediaTag = findMediaTag(startPos, xml)) != null)
            {
                startPos = mediaTag.getStartPosInSource();
                endPos = findTagEnd(xml, mediaTag.getEndPosInSource(), mediaTag);
                if (endPos >= 0)
                {
                    startPos = endPos;
                    list.add(new Media(new FastCharSequence(xml, startPos, endPos)));
                }
            }
        }
        
        return list;
    }
}
