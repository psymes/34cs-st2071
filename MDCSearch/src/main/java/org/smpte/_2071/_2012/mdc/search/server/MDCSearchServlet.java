package org.smpte._2071._2012.mdc.search.server;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.smpte._2071._2012.mdc.query.syntax.QueryParser;
import org.smpte._2071._2012.mdc.search.client.MDCSearchService;
import org.smpte._2071._2012.mdc.search.media.Media;
import org.smpte._2071._2012.mdc.search.media.MediaType;
import org.smpte._2071._2012.mdc.search.media.RN;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaAsset;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaBundle;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaContainer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaDirectory;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFile;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaInstance;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaList;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaListResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaNotFoundFault;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.media.UMNFilter;
import org.smpte_ra.schemas._2071._2012.mdcf.media.UMNParameter;
import org.smpte_ra.schemas._2071._2012.mdcf.query.PAGE;
import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityExceptionFault;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;

@SuppressWarnings("restriction")
public class MDCSearchServlet extends RemoteServiceServlet implements MDCSearchService
{
    private static final long serialVersionUID = 3814788063467680454L;
    
    private static final Class<?> DEVICE_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.ObjectFactory.class;
    
    private static final Class<?> DEVICE_CONTROL_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.control.ObjectFactory.class;
    
    private static final Class<?> DEVICE_MODE_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ObjectFactory.class;
    
    private static final Class<?> DEVICE_EVENT_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.device.event.ObjectFactory.class;
    
    private static final Class<?> IDENTITY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.identity.ObjectFactory.class;
    
    private static final Class<?> MEDIA_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory.class;
    
    private static final Class<?> QUERY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory.class;
    
    private static final Class<?> SECURITY_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.security.ObjectFactory.class;
    
    private static final Class<?> TYPES_FACTORY = org.smpte_ra.schemas._2071._2012.mdcf.types.ObjectFactory.class;
    
    private MediaDirectory md = null;
    

    public PagingLoadResult<Media> search(String expression)
    throws Exception
    {
        Map<String, MediaContainer> locationCache = new HashMap<String, MediaContainer>();
        
        UMNFilter filter = new UMNFilter();
        QueryParser parser = Activator.getQueryParser();
        try
        {
            filter.setQueryExpression(parser.parse(expression));
        } catch (Exception e)
        {
            e.printStackTrace();
            Exception t = new Exception(e.getMessage());
            t.setStackTrace(e.getStackTrace());
            throw t;
        } finally
        {
            Activator.releaseQueryParser();
        }
        
        PAGE page = filter != null && filter.getQueryExpression() != null ? filter.getQueryExpression().getValue().getPAGE() : null;

        HttpServletRequest request = perThreadRequest.get();
        HttpServletResponse response = perThreadResponse.get();
        try
        {
            System.out.println("QueryParser = " + parser);
            System.out.println(parser.parseToXML(expression));
            ArrayList<Media> results = new ArrayList<Media>();
            
            int totalSize = -1;
            BigInteger offset;
            BigInteger pageSize;
            BigInteger index = BigInteger.ZERO;
            if (page != null)
            {
                offset = page.getOffset();
                pageSize = page.getPageSize();
            } else
            {
                offset = BigInteger.ZERO;
                pageSize = BigInteger.ZERO;
            }

            // TODO: Search BundleContext for Root DeviceDirectory to find root MediaDirectory, Call search on Root MediaDirectory.
            MediaDirectory md = mediaDirectory();
            
            List<JAXBElement<? extends org.smpte_ra.schemas._2071._2012.mdcf.media.Media>> elements;
            if (md != null)
            {
                MediaListResponse searchResponse = md.search(filter);
                MediaList list = searchResponse.getMediaList();
                elements = list.getMedia();
            } else
            {
                elements = loadDemoMedia();
            }
            
            if (elements != null)
            {
                totalSize = elements.size();
                
                for (JAXBElement<? extends org.smpte_ra.schemas._2071._2012.mdcf.media.Media> element : elements)
                {
                    index.add(BigInteger.ONE);
                    
                    if (page == null || (offset.compareTo(index) <= 0 && offset.add(pageSize).compareTo(index) <= 0))
                    {
                        Media media = convertMedia(element.getValue());
                        
                        if (!locationCache.containsKey(media.getLocation()))
                        {
                            try
                            {
                                UMNParameter lookup = new UMNParameter();
                                lookup.setUMN(media.getLocation());
                                MediaResponse lookupResponse;
                                lookupResponse = md.lookup(lookup);
                                JAXBElement<? extends org.smpte_ra.schemas._2071._2012.mdcf.media.Media> mediaElement = lookupResponse.getMedia();
                                MediaContainer location = (MediaContainer) mediaElement.getValue();
                                if (location != null)
                                {
                                    media.setLocation(location.getName());
                                    locationCache.put(media.getLocation(), location);
                                }
                            } catch (MediaNotFoundFault e)
                            {
                                e.printStackTrace();
                            } catch (SecurityExceptionFault e)
                            {
                                e.printStackTrace();
                            }
                        } else
                        {
                            media.setLocation(locationCache.get(media.getLocation()).getName());
                        }
                        
                        results.add(media);
                    }
                }
            }
            
            return new PagingLoadResultBean<Media>(results, totalSize, offset.intValue());
        } catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }
    
    
    private List<JAXBElement<? extends org.smpte_ra.schemas._2071._2012.mdcf.media.Media>> loadDemoMedia()
    throws JAXBException, ParseException
    {
        MediaList list;
        ArrayList<Media> results = new ArrayList<Media>();
        
        InputStream mediaIn = getServletContext().getResourceAsStream("Media.xml");
        if (mediaIn == null)
        {
            mediaIn = getServletContext().getResourceAsStream("WEB-INF/Media.xml");
        }
        
        try
        {
            JAXBContext ctx = JAXBContext.newInstance(DEVICE_FACTORY.getPackage().getName() + ":" +
                                                      DEVICE_MODE_FACTORY.getPackage().getName() + ":" +
                                                      DEVICE_EVENT_FACTORY.getPackage().getName() + ":" +
                                                      DEVICE_CONTROL_FACTORY.getPackage().getName() + ":" +
                                                      IDENTITY_FACTORY.getPackage().getName() + ":" +
                                                      MEDIA_FACTORY.getPackage().getName() + ":" +
                                                      QUERY_FACTORY.getPackage().getName() + ":" +
                                                      SECURITY_FACTORY.getPackage().getName() + ":" +
                                                      TYPES_FACTORY.getPackage().getName(), MediaList.class.getClassLoader());
            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            
            @SuppressWarnings("unchecked")
            JAXBElement<MediaList> element = (JAXBElement<MediaList>) unmarshaller.unmarshal(mediaIn);
            return element.getValue().getMedia();
        } catch (JAXBException e)
        {
            e.printStackTrace(System.err);
            throw e;
        }
    }


    private final MediaDirectory mediaDirectory()
    throws IOException
    {
        Properties servletProperties = new Properties();
        InputStream propsIn = getServletContext().getResourceAsStream("WEB-INF/search.properties");
        
        if (propsIn != null)
        {
            servletProperties.load(propsIn);
            URL wsdlURL = new URL((String) servletProperties.get("media_directory_wsdl_url"));
            Service service = Service.create(wsdlURL, new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/media", "DeviceService"));
            
            if (md == null)
            {
                try
                {
                    md = service.getPort(MediaDirectory.class, new javax.xml.ws.soap.AddressingFeature(true, false), new javax.xml.ws.soap.MTOMFeature(true, 65535 * 4));
                } catch (Exception e)
                {
                    System.err.println("Cannot create Web Service Port with Addressing and MTOM support.");
                    
                    md = service.getPort(MediaDirectory.class, new javax.xml.ws.soap.AddressingFeature(true, false));
                }
            }
            
            ((BindingProvider) md).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, (String) servletProperties.get("media_directory_endpoint_url"));
        }
        
        return md;
    }
    
    
    private final Media convertMedia(org.smpte_ra.schemas._2071._2012.mdcf.media.Media inMedia)
    throws ParseException
    {
        Media media = new Media();
        
        RN umn = new RN(inMedia.getUMN());
        
        media.setUMN(umn.toString());
        media.setLocation(inMedia.getLocation());
        media.setType(MediaType.valueOf(umn.getAttribute("type").toUpperCase()));
        media.setName(inMedia.getName());
        try
        {
            media.setCreated(inMedia.getCreated().getDate().toGregorianCalendar().getTime());
        } catch (Exception e)
        {
            media.setCreated(new Date());
        }
        
        try
        {
            media.setModified(inMedia.getModified().getDate().toGregorianCalendar().getTime());
        } catch (Exception e)
        {
        }
        
        DateTime duration;
        switch (media.getType())
        {
            case MEDIA_ASSET :
                duration = ((MediaAsset) inMedia).getDuration();
                if (duration != null)
                {
                    media.setDuration((duration.getHour() * 3600000) + (duration.getMinute() * 60000) + (duration.getSecond() * 1000) + (duration.getMicrosecond() / 1000));
                }
                break;
            case MEDIA_CONTAINER :
                media.setUDN(((MediaContainer) inMedia).getUDN());
                break;
            case MEDIA_BUNDLE :
                media.setUDN(((MediaBundle) inMedia).getUDN());
            case MEDIA_FILE :
                media.setMimeType(((MediaFile) inMedia).getMIMEType());
                media.setSize(((MediaFile) inMedia).getSize().longValue());
            case MEDIA_INSTANCE :
                duration = ((MediaInstance) inMedia).getDuration();
                if (duration != null)
                {
                    media.setDuration((duration.getHour() * 3600000) + (duration.getMinute() * 60000) + (duration.getSecond() * 1000) + (duration.getMicrosecond() / 1000));
                }
                break;
            default :
                break;
        }
        
        return media;
    }
}
