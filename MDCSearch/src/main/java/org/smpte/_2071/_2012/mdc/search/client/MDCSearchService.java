package org.smpte._2071._2012.mdc.search.client;

import org.smpte._2071._2012.mdc.search.media.Media;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("media")
public interface MDCSearchService extends RemoteService
{
    public PagingLoadResult<Media> search(String expression) 
    throws Exception;
}
