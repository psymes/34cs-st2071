package org.smpte._2071._2012.mdc.search.server;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.smpte._2071._2012.mdc.query.syntax.QueryParser;

public class Activator implements BundleActivator
{
    static BundleContext bundleContext;
    
    static ServiceReference reference;
    
    
    public void start(BundleContext bundleContext)
    throws Exception
    {
        System.out.println("Starting MDCSearch WebApp");
        this.bundleContext = bundleContext;
        System.out.println("BundleContext = " + bundleContext);
        reference = bundleContext.getServiceReference(QueryParser.class.getName());
    }
    
    
    public void stop(BundleContext bundleContext)
    throws Exception
    {
        System.out.println("Stopped MDCSearch WebApp");
        bundleContext.ungetService(reference);
        bundleContext = null;
        reference = null;
    }
    
    
    public static QueryParser getQueryParser()
    {
        QueryParser parser = null;
        
        if (bundleContext != null && reference != null)
        {
            System.out.println("Looking up QueryParser OSGi Service.");
            parser = (QueryParser) bundleContext.getService(reference);
        }
        
        if (parser == null)
        {
            System.out.println("Could Not Find  OSGi Service.  Creating New QueryParser.");
            parser = new org.smpte._2071._2012.mdc.query.syntax.impl.QueryParser();
        }
        
        return parser;
    }
    
    
    public static void releaseQueryParser()
    {
        if (bundleContext != null && reference != null)
        {
            System.out.println("Releasing QueryParser OSGi Service.");
            bundleContext.ungetService(reference);
        }
    }
}
