package org.smpte._2071._2012.mdc.search.client;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.smpte._2071._2012.mdc.search.media.Media;
import org.smpte._2071._2012.mdc.search.media.RN;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.ButtonCell.IconAlign;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.XTemplates;
import com.sencha.gxt.core.client.util.IconHelper;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.BeforeLoadEvent;
import com.sencha.gxt.data.shared.loader.LoadEvent;
import com.sencha.gxt.data.shared.loader.LoadExceptionEvent;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.LoaderHandler;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.Container;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.SimpleContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.container.Viewport;
import com.sencha.gxt.widget.core.client.event.RowClickEvent;
import com.sencha.gxt.widget.core.client.event.RowClickEvent.RowClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import com.sencha.gxt.widget.core.client.grid.SummaryColumnConfig;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.tips.QuickTip;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import fr.hd3d.html5.video.client.VideoSource;
import fr.hd3d.html5.video.client.VideoSource.VideoType;
import fr.hd3d.html5.video.client.VideoWidget;

/**
 * @author posicks
 */
public class MediaSearch extends Composite implements IsWidget, EntryPoint
{
    public static interface MediaProperties extends PropertyAccess<Media>
    {
        ValueProvider<Media, String> name();
        
        ValueProvider<Media, String> location();
        
        ValueProvider<Media, Date> created();
        
        ValueProvider<Media, Date> modified();
        
        // Metadata Attributes
        
    }
    
    private static MediaSearchUiBinder uiBinder = GWT.create(MediaSearchUiBinder.class);
    
    
    @UiTemplate("MediaSearch.ui.xml")
    interface MediaSearchUiBinder extends UiBinder<Widget, MediaSearch>
    {
    }
    
    
    interface MediaQuickTipTemplates extends XTemplates
    {
        @XTemplate("<span style='{styles}' qtitle='Change' qtip='{qtip}'>{value}</span>")
        SafeHtml template(SafeStyles styles, String qtip, String value);
    }
    
    private MediaProperties mediaProperties;
    
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Media>> mediaLoader;

    private SummaryColumnConfig<Media, String> mediaAutoExpandColumn;
    
    @UiField
    ListStore<Media> store;
    
    @UiField
    ColumnModel<Media> cm;
    
    @UiField
    Margins outerMargins;
    
    @UiField
    Margins centerMargins;
    
    @UiField
    Margins northMargins;
    
    @UiField
    Margins southMargins;
    
    @UiField
    Margins eastMargins;
    
    @UiField
    Margins westMargins;
    
    @UiField
    MarginData outerData;
    
    @UiField
    MarginData centerData;
    
    @UiField
    BorderLayoutData eastData;
    
    @UiField
    BorderLayoutData northData;
    
    @UiField
    BorderLayoutData southData;
    
    @UiField
    VerticalLayoutData southVData;
    
    @UiField
    BorderLayoutData westData;
    
    @UiField
    Container mediaSearchContainer;
    
    @UiField
    BorderLayoutContainer layoutContainer;
    
    @UiField
    Container centerContainer;
    
    @UiField
    SimpleContainer southContainer;
    
    @UiField
    VideoWidget videoPlayer;
    
    @UiField
    ContentPanel mediaDetailsContainer;
    
    @UiField
    FramedPanel panel;
    
    @UiField
    Grid<Media> grid;
    
    @UiField
    GridView<Media> view;
    
    @UiField
    PagingToolBar pagingToolBar;
    
    @UiField
    TextField searchMediaField;
    
    @UiField
    TextButton searchMediaButton;
    
    MDCSearchServiceAsync searchService;
    
    QuickTip mediaGridTips;
    
    
    @UiFactory
    ColumnModel<Media> createColumnModel()
    {
        SummaryColumnConfig<Media, String> name = new SummaryColumnConfig<Media, String>(mediaProperties.name(), 400, "Name");
        SummaryColumnConfig<Media, String> location = new SummaryColumnConfig<Media, String>(mediaProperties.location(), 200, "Location");
        SummaryColumnConfig<Media, Date> created = new SummaryColumnConfig<Media, Date>(mediaProperties.created(), 200, "Created");
        SummaryColumnConfig<Media, Date> modified = new SummaryColumnConfig<Media, Date>(mediaProperties.modified(), 200, "Modified");
        
        List<ColumnConfig<Media, ?>> cfgs = new ArrayList<ColumnConfig<Media, ?>>();
        cfgs.add(name);
        cfgs.add(location);
        cfgs.add(created);
        cfgs.add(modified);
        
        this.mediaAutoExpandColumn = name;
        return new ColumnModel<Media>(cfgs);
    }
    
    
    @UiFactory
    ListStore<Media> createListStore()
    {
        return new ListStore<Media>(new ModelKeyProvider<Media>()
        {
            public String getKey(Media media)
            {
                return media.getUMN();
            }
        });
    }
    
    
    @UiHandler("searchMediaButton")
    public void onSearchMedia(SelectEvent event)
    {
        pagingToolBar.refresh();
    }
    
    
    @UiHandler("searchMediaField")
    public void onSearchFieldChanged(ChangeEvent event)
    {
        pagingToolBar.refresh();
    }
    
    
    public void onModuleLoad()
    {
        searchService = GWT.create(MDCSearchService.class);
        Viewport viewport = new Viewport();
        Widget widget = asWidget();
        viewport.add(widget);
        RootPanel.get().add(viewport, 0, 0);
    }
    
    
    public Widget asWidget()
    {
        RpcProxy<PagingLoadConfig, PagingLoadResult<Media>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Media>>()
        {
            @Override
            public void load(final PagingLoadConfig loadConfig, final AsyncCallback<PagingLoadResult<Media>> callback)
            {
                String searchStr = searchMediaField.getValue();
                Info.display("Media Search", "Searching for \"" + searchStr + "\"!");
                
                if (searchStr.indexOf("page") < 0)
                {
                    searchStr += " page(" + loadConfig.getOffset() + "," + loadConfig.getLimit() + ")";
                }
                
                if (searchStr.indexOf("sort") < 0)
                {
                    List<? extends SortInfo> sort = loadConfig.getSortInfo();
                    if (sort != null && sort.size() > 0)
                    {
                        searchStr += "sort by ";
                        for (SortInfo info : sort)
                        {
                            SortDir dir = info.getSortDir();
                            String field = info.getSortField();
                            searchStr += field + " " + (SortDir.ASC == dir ? "asc" : (SortDir.DESC == dir ? "desc" : ""));
                        }
                    }
                }
                
                searchService.search(searchStr, callback);
            }
        };
        
        mediaLoader = new PagingLoader<PagingLoadConfig, PagingLoadResult<Media>>(proxy);
        mediaLoader.setRemoteSort(true);
        mediaLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Media, PagingLoadResult<Media>>(store));
        
        mediaProperties = GWT.create(MediaProperties.class);
        
        // Can access @UiField after calling createAndBindUi
        Widget widget = uiBinder.createAndBindUi(this);
        
        grid.setLoader(mediaLoader);
        grid.getView().setEmptyText("No Media Found.");
        
        grid.addRowClickHandler(new RowClickHandler()
        {
            public void onRowClick(RowClickEvent event)
            {
                int row = event.getRowIndex();
                Media media = store.get(row);
                if (media != null)
                {
                    mediaDetailsContainer.setExpanded(true);
                    mediaDetailsContainer.setSize("350px", "100%");
                    String UMID = null;
                    try
                    {
                        RN umn = new RN(media.getUMN());
                        String mid = umn.getAttribute("mid");
                        Info.display("Media Identifier", mid);
                        if (mid.startsWith("0x") || mid.startsWith("urn:smpte:umid"))
                        {
                            UMID = mid;
                        } else
                        {
                            // TODO: Generate UMID from clone zone and clone id
                        }
                        
                        if (UMID != null)
                        {
                            List<VideoSource> sources = new ArrayList<VideoSource>();
                            sources.add(new VideoSource("http://bassnas.bst.espn.pvt/Cache/proxy/cache/mp4/" + UMID + ".mp4", VideoType.MP4));
                            videoPlayer.setSources(sources);
                            videoPlayer.setVisible(true);
                            mediaDetailsContainer.setExpanded(true);
                        }
                    } catch (ParseException e)
                    {
                        Info.display("Error Parsing UMN", e.getMessage());
                    }
                }
            }
        });
        
        mediaLoader.addLoaderHandler(new LoaderHandler<PagingLoadConfig, PagingLoadResult<Media>>()
        {
            public void onBeforeLoad(BeforeLoadEvent<PagingLoadConfig> event)
            {
                grid.getView().setEmptyText("Searching...");
            }

            public void onLoadException(LoadExceptionEvent<PagingLoadConfig> event)
            {
                System.out.println("onLoadException(" + event.toDebugString() + ")");
                Throwable t = event.getException();
                String a = "a";
                String errorName = t.getClass().getName();
                switch (errorName.charAt(0))
                {
                    case 'a' :
                    case 'A' :
                    case 'e' :
                    case 'E' :
                    case 'i' :
                    case 'I' :
                    case 'o' :
                    case 'O' :
                    case 'u' :
                    case 'U' :
                        a = "an";
                        break;
                }
                grid.getView().setEmptyText("<p>The Remote Server raised " + a + " " + errorName + " error while searching for Media.</p>" + " " + t.getMessage());
                grid.getView().refresh(false);
            }

            public void onLoad(LoadEvent<PagingLoadConfig, PagingLoadResult<Media>> event)
            {
                grid.getView().setEmptyText("No Media Found.");
                mediaDetailsContainer.setExpanded(true);
                mediaDetailsContainer.setSize("350px", "100%");
                store.clear();
                if (event != null)
                {
                    if (event.getLoadResult().getTotalLength() == 0)
                    {
                        grid.getView().setEmptyText("No Media Found.");
                    } else
                    {
                        store.addAll(event.getLoadResult().getData());
                    }
                } else
                {
                    grid.getView().setEmptyText("Event null?!");
                }
            }
        });
        
        ImageResource searchIcon = IconHelper.getImageResource(UriUtils.fromSafeConstant("img/search_icon3.png"), 18, 15);
        searchMediaButton.setIcon(searchIcon);
        searchMediaButton.setIconAlign(IconAlign.LEFT);
        
        grid.getView().setAutoExpandColumn(mediaAutoExpandColumn);
        grid.setBorders(false);
        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
     
        // needed to enable quicktips (qtitle for the heading and qtip for the
        // content) that are setup in the change GridCellRenderer
        mediaGridTips = new QuickTip(grid);
        
        pagingToolBar.bind(mediaLoader);
        
        layoutContainer.setBorders(true);
        layoutContainer.setResize(true);
        
        northData.setSize(45);
        northData.setSplit(true);
        northData.setCollapsible(true);
        northData.setCollapseMini(true);
        southData.setFloatable(true);
        
        eastData.setSize(videoPlayer.getOffsetWidth() + eastMargins.getLeft() + eastMargins.getRight());
//        eastData.setSplit(true);
//        eastData.setCollapsible(true);
//        eastData.setCollapseMini(true);
//        eastData.setCollapseHidden(false);
//        eastData.setFloatable(true);
        
        southData.setSize(24);
        
        return widget;
    }
}
