package org.smpte._2071._2012.mdc.search.client;

import org.smpte._2071._2012.mdc.search.media.Media;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("media")
public interface MDCSearchServiceAsync
{
    public void search(String expression, AsyncCallback<PagingLoadResult<Media>> callback);
}
