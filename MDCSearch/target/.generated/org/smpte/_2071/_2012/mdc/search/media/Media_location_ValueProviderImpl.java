package org.smpte._2071._2012.mdc.search.media;

public class Media_location_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<org.smpte._2071._2012.mdc.search.media.Media, java.lang.String> {
  public static final Media_location_ValueProviderImpl INSTANCE = new Media_location_ValueProviderImpl();
  public java.lang.String getValue(org.smpte._2071._2012.mdc.search.media.Media object) {
    return object.getLocation();
  }
  public void setValue(org.smpte._2071._2012.mdc.search.media.Media object, java.lang.String value) {
    object.setLocation(value);
  }
  public String getPath() {
    return "location";
  }
}
