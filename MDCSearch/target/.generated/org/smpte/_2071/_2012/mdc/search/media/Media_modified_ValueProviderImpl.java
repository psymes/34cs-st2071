package org.smpte._2071._2012.mdc.search.media;

public class Media_modified_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<org.smpte._2071._2012.mdc.search.media.Media, java.util.Date> {
  public static final Media_modified_ValueProviderImpl INSTANCE = new Media_modified_ValueProviderImpl();
  public java.util.Date getValue(org.smpte._2071._2012.mdc.search.media.Media object) {
    return object.getModified();
  }
  public void setValue(org.smpte._2071._2012.mdc.search.media.Media object, java.util.Date value) {
    object.setModified(value);
  }
  public String getPath() {
    return "modified";
  }
}
