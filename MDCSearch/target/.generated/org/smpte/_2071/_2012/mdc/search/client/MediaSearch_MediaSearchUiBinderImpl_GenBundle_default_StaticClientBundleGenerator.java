package org.smpte._2071._2012.mdc.search.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class MediaSearch_MediaSearchUiBinderImpl_GenBundle_default_StaticClientBundleGenerator implements org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenBundle {
  private static MediaSearch_MediaSearchUiBinderImpl_GenBundle_default_StaticClientBundleGenerator _instance0 = new MediaSearch_MediaSearchUiBinderImpl_GenBundle_default_StaticClientBundleGenerator();
  private void styleInitializer() {
    style = new org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GH5EYDXDPLC{background-color:" + ("white")  + ";}.GH5EYDXDBMC{font-weight:" + ("bold")  + ";}.GH5EYDXDCMC,.GH5EYDXDAMC{margin:" + ("0")  + ";}.GH5EYDXDDMC{background-image:" + ("transparent"+ " " +"url(img/search_icon.png)"+ " " +"no-repeat"+ " " +"middle"+ " " +"right")  + ";}");
      }
      public java.lang.String background(){
        return "GH5EYDXDPLC";
      }
      public java.lang.String centerMargin(){
        return "GH5EYDXDAMC";
      }
      public java.lang.String important(){
        return "GH5EYDXDBMC";
      }
      public java.lang.String outerMargin(){
        return "GH5EYDXDCMC";
      }
      public java.lang.String searchField(){
        return "GH5EYDXDDMC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style get() {
      return style;
    }
  }
  public org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenBundle::style()();
    }
    return null;
  }-*/;
}
