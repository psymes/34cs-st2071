package org.smpte._2071._2012.mdc.search.media;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Media_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getCreated(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::created;
  }-*/;
  
  private static native void setCreated(org.smpte._2071._2012.mdc.search.media.Media instance, java.util.Date value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::created = value;
  }-*/;
  
  private static native int getDuration(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::duration;
  }-*/;
  
  private static native void setDuration(org.smpte._2071._2012.mdc.search.media.Media instance, int value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::duration = value;
  }-*/;
  
  private static native java.lang.String getLocation(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::location;
  }-*/;
  
  private static native void setLocation(org.smpte._2071._2012.mdc.search.media.Media instance, java.lang.String value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::location = value;
  }-*/;
  
  private static native java.util.Map getMetadata(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::metadata;
  }-*/;
  
  private static native void setMetadata(org.smpte._2071._2012.mdc.search.media.Media instance, java.util.Map value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::metadata = value;
  }-*/;
  
  private static native java.lang.String getMimeType(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::mimeType;
  }-*/;
  
  private static native void setMimeType(org.smpte._2071._2012.mdc.search.media.Media instance, java.lang.String value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::mimeType = value;
  }-*/;
  
  private static native java.util.Date getModified(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::modified;
  }-*/;
  
  private static native void setModified(org.smpte._2071._2012.mdc.search.media.Media instance, java.util.Date value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::modified = value;
  }-*/;
  
  private static native java.lang.String getName(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::name;
  }-*/;
  
  private static native void setName(org.smpte._2071._2012.mdc.search.media.Media instance, java.lang.String value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::name = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getSize(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::size;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setSize(org.smpte._2071._2012.mdc.search.media.Media instance, long value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::size = value;
  }-*/;
  
  private static native org.smpte._2071._2012.mdc.search.media.MediaType getType(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::type;
  }-*/;
  
  private static native void setType(org.smpte._2071._2012.mdc.search.media.Media instance, org.smpte._2071._2012.mdc.search.media.MediaType value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::type = value;
  }-*/;
  
  private static native java.lang.String getUdn(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::udn;
  }-*/;
  
  private static native void setUdn(org.smpte._2071._2012.mdc.search.media.Media instance, java.lang.String value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::udn = value;
  }-*/;
  
  private static native java.lang.String getUmn(org.smpte._2071._2012.mdc.search.media.Media instance) /*-{
    return instance.@org.smpte._2071._2012.mdc.search.media.Media::umn;
  }-*/;
  
  private static native void setUmn(org.smpte._2071._2012.mdc.search.media.Media instance, java.lang.String value) 
  /*-{
    instance.@org.smpte._2071._2012.mdc.search.media.Media::umn = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, org.smpte._2071._2012.mdc.search.media.Media instance) throws SerializationException {
    setCreated(instance, (java.util.Date) streamReader.readObject());
    setDuration(instance, streamReader.readInt());
    setLocation(instance, streamReader.readString());
    setMetadata(instance, (java.util.Map) streamReader.readObject());
    setMimeType(instance, streamReader.readString());
    setModified(instance, (java.util.Date) streamReader.readObject());
    setName(instance, streamReader.readString());
    setSize(instance, streamReader.readLong());
    setType(instance, (org.smpte._2071._2012.mdc.search.media.MediaType) streamReader.readObject());
    setUdn(instance, streamReader.readString());
    setUmn(instance, streamReader.readString());
    
  }
  
  public static org.smpte._2071._2012.mdc.search.media.Media instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new org.smpte._2071._2012.mdc.search.media.Media();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, org.smpte._2071._2012.mdc.search.media.Media instance) throws SerializationException {
    streamWriter.writeObject(getCreated(instance));
    streamWriter.writeInt(getDuration(instance));
    streamWriter.writeString(getLocation(instance));
    streamWriter.writeObject(getMetadata(instance));
    streamWriter.writeString(getMimeType(instance));
    streamWriter.writeObject(getModified(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeLong(getSize(instance));
    streamWriter.writeObject(getType(instance));
    streamWriter.writeString(getUdn(instance));
    streamWriter.writeString(getUmn(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return org.smpte._2071._2012.mdc.search.media.Media_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    org.smpte._2071._2012.mdc.search.media.Media_FieldSerializer.deserialize(reader, (org.smpte._2071._2012.mdc.search.media.Media)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    org.smpte._2071._2012.mdc.search.media.Media_FieldSerializer.serialize(writer, (org.smpte._2071._2012.mdc.search.media.Media)object);
  }
  
}
