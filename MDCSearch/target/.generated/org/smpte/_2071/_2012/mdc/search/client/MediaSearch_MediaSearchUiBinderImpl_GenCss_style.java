package org.smpte._2071._2012.mdc.search.client;

import com.google.gwt.resources.client.CssResource;

public interface MediaSearch_MediaSearchUiBinderImpl_GenCss_style extends CssResource {
  String important();
  String background();
  String centerMargin();
  String searchField();
  String outerMargin();
}
