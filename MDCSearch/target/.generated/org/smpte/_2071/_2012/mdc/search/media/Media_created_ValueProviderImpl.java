package org.smpte._2071._2012.mdc.search.media;

public class Media_created_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<org.smpte._2071._2012.mdc.search.media.Media, java.util.Date> {
  public static final Media_created_ValueProviderImpl INSTANCE = new Media_created_ValueProviderImpl();
  public java.util.Date getValue(org.smpte._2071._2012.mdc.search.media.Media object) {
    return object.getCreated();
  }
  public void setValue(org.smpte._2071._2012.mdc.search.media.Media object, java.util.Date value) {
    object.setCreated(value);
  }
  public String getPath() {
    return "created";
  }
}
