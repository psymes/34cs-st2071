package org.smpte._2071._2012.mdc.search.media;

public class Media_name_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<org.smpte._2071._2012.mdc.search.media.Media, java.lang.String> {
  public static final Media_name_ValueProviderImpl INSTANCE = new Media_name_ValueProviderImpl();
  public java.lang.String getValue(org.smpte._2071._2012.mdc.search.media.Media object) {
    return object.getName();
  }
  public void setValue(org.smpte._2071._2012.mdc.search.media.Media object, java.lang.String value) {
    object.setName(value);
  }
  public String getPath() {
    return "name";
  }
}
