package org.smpte._2071._2012.mdc.search.client;

public class MediaSearch_MediaPropertiesImpl implements org.smpte._2071._2012.mdc.search.client.MediaSearch.MediaProperties {
  public com.sencha.gxt.core.client.ValueProvider created() {
    return org.smpte._2071._2012.mdc.search.media.Media_created_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider location() {
    return org.smpte._2071._2012.mdc.search.media.Media_location_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider modified() {
    return org.smpte._2071._2012.mdc.search.media.Media_modified_ValueProviderImpl.INSTANCE;
  }
  public com.sencha.gxt.core.client.ValueProvider name() {
    return org.smpte._2071._2012.mdc.search.media.Media_name_ValueProviderImpl.INSTANCE;
  }
}
