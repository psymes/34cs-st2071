package org.smpte._2071._2012.mdc.search.media;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MediaType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, org.smpte._2071._2012.mdc.search.media.MediaType instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static org.smpte._2071._2012.mdc.search.media.MediaType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    org.smpte._2071._2012.mdc.search.media.MediaType[] values = org.smpte._2071._2012.mdc.search.media.MediaType.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, org.smpte._2071._2012.mdc.search.media.MediaType instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return org.smpte._2071._2012.mdc.search.media.MediaType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    org.smpte._2071._2012.mdc.search.media.MediaType_FieldSerializer.deserialize(reader, (org.smpte._2071._2012.mdc.search.media.MediaType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    org.smpte._2071._2012.mdc.search.media.MediaType_FieldSerializer.serialize(writer, (org.smpte._2071._2012.mdc.search.media.MediaType)object);
  }
  
}
