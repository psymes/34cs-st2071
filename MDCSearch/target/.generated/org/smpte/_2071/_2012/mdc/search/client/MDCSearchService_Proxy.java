package org.smpte._2071._2012.mdc.search.client;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class MDCSearchService_Proxy extends RemoteServiceProxy implements org.smpte._2071._2012.mdc.search.client.MDCSearchServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "org.smpte._2071._2012.mdc.search.client.MDCSearchService";
  private static final String SERIALIZATION_POLICY ="B7CDA45361C04E2D5244533E468A9046";
  private static final org.smpte._2071._2012.mdc.search.client.MDCSearchService_TypeSerializer SERIALIZER = new org.smpte._2071._2012.mdc.search.client.MDCSearchService_TypeSerializer();
  
  public MDCSearchService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "media", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void search(java.lang.String expression, com.google.gwt.user.client.rpc.AsyncCallback callback) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("MDCSearchService_Proxy", "search");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString(expression);
      helper.finish(callback, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      callback.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
