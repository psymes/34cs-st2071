package org.smpte._2071._2012.mdc.search.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class MediaSearch_MediaSearchUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, org.smpte._2071._2012.mdc.search.client.MediaSearch>, org.smpte._2071._2012.mdc.search.client.MediaSearch.MediaSearchUiBinder {

  public com.google.gwt.user.client.ui.Widget createAndBindUi(final org.smpte._2071._2012.mdc.search.client.MediaSearch owner) {

    org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenBundle) GWT.create(org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenBundle.class);
    org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style style = clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay.style();
    com.sencha.gxt.widget.core.client.grid.ColumnModel cm = owner.createColumnModel();
    com.sencha.gxt.data.shared.ListStore store = owner.createListStore();
    com.sencha.gxt.core.client.util.Margins outerMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.core.client.util.Margins northMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.core.client.util.Margins westMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.core.client.util.Margins centerMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.core.client.util.Margins eastMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.core.client.util.Margins southMargins = (com.sencha.gxt.core.client.util.Margins) GWT.create(com.sencha.gxt.core.client.util.Margins.class);
    com.sencha.gxt.widget.core.client.container.MarginData outerData = (com.sencha.gxt.widget.core.client.container.MarginData) GWT.create(com.sencha.gxt.widget.core.client.container.MarginData.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData northData = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData westData = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData.class);
    com.sencha.gxt.widget.core.client.container.MarginData centerData = (com.sencha.gxt.widget.core.client.container.MarginData) GWT.create(com.sencha.gxt.widget.core.client.container.MarginData.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData eastData = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData southData = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData.class);
    com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData southVData = (com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData) GWT.create(com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData.class);
    com.sencha.gxt.widget.core.client.grid.GridView view = (com.sencha.gxt.widget.core.client.grid.GridView) GWT.create(com.sencha.gxt.widget.core.client.grid.GridView.class);
    com.sencha.gxt.widget.core.client.form.TextField searchMediaField = (com.sencha.gxt.widget.core.client.form.TextField) GWT.create(com.sencha.gxt.widget.core.client.form.TextField.class);
    com.sencha.gxt.widget.core.client.button.TextButton searchMediaButton = (com.sencha.gxt.widget.core.client.button.TextButton) GWT.create(com.sencha.gxt.widget.core.client.button.TextButton.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer f_BorderLayoutContainer1 = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.class);
    com.sencha.gxt.widget.core.client.ContentPanel mediaSearchContainer = (com.sencha.gxt.widget.core.client.ContentPanel) GWT.create(com.sencha.gxt.widget.core.client.ContentPanel.class);
    com.sencha.gxt.widget.core.client.grid.Grid grid = new com.sencha.gxt.widget.core.client.grid.Grid(store, cm, view);
    com.sencha.gxt.widget.core.client.container.SimpleContainer centerContainer = (com.sencha.gxt.widget.core.client.container.SimpleContainer) GWT.create(com.sencha.gxt.widget.core.client.container.SimpleContainer.class);
    fr.hd3d.html5.video.client.VideoWidget videoPlayer = (fr.hd3d.html5.video.client.VideoWidget) GWT.create(fr.hd3d.html5.video.client.VideoWidget.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer f_BorderLayoutContainer2 = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.class);
    com.sencha.gxt.widget.core.client.ContentPanel mediaDetailsContainer = (com.sencha.gxt.widget.core.client.ContentPanel) GWT.create(com.sencha.gxt.widget.core.client.ContentPanel.class);
    com.sencha.gxt.widget.core.client.toolbar.PagingToolBar pagingToolBar = new com.sencha.gxt.widget.core.client.toolbar.PagingToolBar(50);
    com.sencha.gxt.widget.core.client.container.SimpleContainer southContainer = (com.sencha.gxt.widget.core.client.container.SimpleContainer) GWT.create(com.sencha.gxt.widget.core.client.container.SimpleContainer.class);
    com.sencha.gxt.widget.core.client.container.BorderLayoutContainer layoutContainer = (com.sencha.gxt.widget.core.client.container.BorderLayoutContainer) GWT.create(com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.class);
    com.sencha.gxt.widget.core.client.FramedPanel panel = (com.sencha.gxt.widget.core.client.FramedPanel) GWT.create(com.sencha.gxt.widget.core.client.FramedPanel.class);

    searchMediaField.setWidth("80%");
    searchMediaField.setEmptyText("Type media search expression here and press <enter> or the search button ...");
    f_BorderLayoutContainer1.setCenterWidget(searchMediaField, null);
    searchMediaButton.setText("Search");
    f_BorderLayoutContainer1.setEastWidget(searchMediaButton, null);
    mediaSearchContainer.add(f_BorderLayoutContainer1);
    mediaSearchContainer.setHeadingText("Media Search");
    layoutContainer.setNorthWidget(mediaSearchContainer, northData);
    grid.setColumnReordering(true);
    grid.setBorders(false);
    grid.setLoadMask(true);
    centerContainer.add(grid, null);
    layoutContainer.setCenterWidget(centerContainer, centerData);
    videoPlayer.setStylePrimaryName("video.player");
    videoPlayer.setControls(true);
    videoPlayer.setPixelSize(320, 240);
    videoPlayer.setWidth("50%");
    videoPlayer.setAutoPlay(false);
    videoPlayer.setDefaultPlaybackRate(1.0);
    f_BorderLayoutContainer2.setCenterWidget(videoPlayer, null);
    mediaDetailsContainer.add(f_BorderLayoutContainer2);
    mediaDetailsContainer.setHeadingText("Media Details");
    layoutContainer.setEastWidget(mediaDetailsContainer, eastData);
    southContainer.add(pagingToolBar, null);
    southContainer.setLayoutData(southData);
    layoutContainer.setSouthWidget(southContainer, southData);
    layoutContainer.addStyleName("" + style.centerMargin() + "");
    panel.add(layoutContainer);
    panel.addStyleName("" + style.outerMargin() + "");
    panel.addStyleName("" + style.background() + "");
    panel.setHeadingText("SMPTE ST2071 Search Application");
    panel.setCollapsible(false);
    panel.setAnimCollapse(false);



    final com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames1 = new com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler() {
      public void onSelect(com.sencha.gxt.widget.core.client.event.SelectEvent event) {
        owner.onSearchMedia(event);
      }
    };
    searchMediaButton.addSelectHandler(handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames1);

    final com.google.gwt.event.dom.client.ChangeHandler handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames2 = new com.google.gwt.event.dom.client.ChangeHandler() {
      public void onChange(com.google.gwt.event.dom.client.ChangeEvent event) {
        owner.onSearchFieldChanged(event);
      }
    };
    searchMediaField.addChangeHandler(handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames2);

    owner.centerContainer = centerContainer;
    owner.centerData = centerData;
    owner.centerMargins = centerMargins;
    owner.cm = cm;
    owner.eastData = eastData;
    owner.eastMargins = eastMargins;
    owner.grid = grid;
    owner.layoutContainer = layoutContainer;
    owner.mediaDetailsContainer = mediaDetailsContainer;
    owner.mediaSearchContainer = mediaSearchContainer;
    owner.northData = northData;
    owner.northMargins = northMargins;
    owner.outerData = outerData;
    owner.outerMargins = outerMargins;
    owner.pagingToolBar = pagingToolBar;
    owner.panel = panel;
    owner.searchMediaButton = searchMediaButton;
    owner.searchMediaField = searchMediaField;
    owner.southContainer = southContainer;
    owner.southData = southData;
    owner.southMargins = southMargins;
    owner.southVData = southVData;
    owner.store = store;
    owner.videoPlayer = videoPlayer;
    owner.view = view;
    owner.westData = westData;
    owner.westMargins = westMargins;
    clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay.style().ensureInjected();

    return panel;
  }
}
