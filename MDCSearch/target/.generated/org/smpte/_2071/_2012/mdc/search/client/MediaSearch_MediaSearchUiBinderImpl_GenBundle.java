package org.smpte._2071._2012.mdc.search.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.CssResource.Import;

public interface MediaSearch_MediaSearchUiBinderImpl_GenBundle extends ClientBundle {
  @Source("uibinder:org.smpte._2071._2012.mdc.search.client.MediaSearch_MediaSearchUiBinderImpl_GenCss_style.css")
  MediaSearch_MediaSearchUiBinderImpl_GenCss_style style();

}
