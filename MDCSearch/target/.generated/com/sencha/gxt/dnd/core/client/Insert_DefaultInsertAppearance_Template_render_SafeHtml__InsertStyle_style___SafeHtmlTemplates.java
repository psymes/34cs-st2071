package com.sencha.gxt.dnd.core.client;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;

public interface Insert_DefaultInsertAppearance_Template_render_SafeHtml__InsertStyle_style___SafeHtmlTemplates extends com.google.gwt.safehtml.client.SafeHtmlTemplates {
  @Template("<table class=\"{0}\" height=\"6\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td height=\"6\" class=\"{1}\"><div style=\"width: 3px\"></div></td><td class=\"{2}\" width=\"100%\">&nbsp;</td><td class=\"{3}\"><div style=\"width: 3px\"></div></td></tr></tbody></table>")
  SafeHtml render0(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3);
}
