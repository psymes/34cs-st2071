package com.sencha.gxt.dnd.core.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class Insert_DefaultInsertAppearance_TemplateImpl implements com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.Template {
  public com.google.gwt.safehtml.shared.SafeHtml render(com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle style){
    SafeHtml outer;
    
    /**
     * Root of template
     */
    
    /**
     * safehtml content:
       * <table class="{0}" height="6" cellspacing="0" cellpadding="0"><tbody><tr><td height="6" class="{1}"><div style="width: 3px"></div></td><td class="{2}" width="100%">&nbsp;</td><td class="{3}"><div style="width: 3px"></div></td></tr></tbody></table>
     * params:
       * com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_bar_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_left_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_mid_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_right_ValueProviderImpl.INSTANCE.getValue(style)
     */
    outer = GWT.<com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_Template_render_SafeHtml__InsertStyle_style___SafeHtmlTemplates>create(com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_Template_render_SafeHtml__InsertStyle_style___SafeHtmlTemplates.class).render0(com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_bar_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_left_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_mid_ValueProviderImpl.INSTANCE.getValue(style), com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_InsertStyle_right_ValueProviderImpl.INSTANCE.getValue(style));
    return outer;
  }
}
