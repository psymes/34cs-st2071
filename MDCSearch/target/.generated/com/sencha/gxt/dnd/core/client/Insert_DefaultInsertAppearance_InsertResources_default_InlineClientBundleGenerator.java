package com.sencha.gxt.dnd.core.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator implements com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources {
  private static Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator _instance0 = new Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator();
  private void leftInitializer() {
    left = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "left",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 3, 6, false, false
    );
  }
  private static class leftInitializer {
    static {
      _instance0.leftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return left;
    }
  }
  public com.google.gwt.resources.client.ImageResource left() {
    return leftInitializer.get();
  }
  private void midInitializer() {
    mid = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "mid",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 3, 6, false, false
    );
  }
  private static class midInitializer {
    static {
      _instance0.midInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return mid;
    }
  }
  public com.google.gwt.resources.client.ImageResource mid() {
    return midInitializer.get();
  }
  private void rightInitializer() {
    right = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "right",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 3, 6, false, false
    );
  }
  private static class rightInitializer {
    static {
      _instance0.rightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return right;
    }
  }
  public com.google.gwt.resources.client.ImageResource right() {
    return rightInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GH5EYDXDHM{position:" + ("absolute")  + ";z-index:" + ("99999")  + ";}.GH5EYDXDHM td{line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDIM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.left()).getHeight() + "px")  + ";width:" + ((Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.left()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.left()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.left()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.left()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDJM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.mid()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.mid()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.mid()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.mid()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDKM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.right()).getHeight() + "px")  + ";width:" + ((Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.right()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.right()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.right()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_InlineClientBundleGenerator.this.right()).getTop() + "px  no-repeat")  + ";}");
      }
      public java.lang.String bar(){
        return "GH5EYDXDHM";
      }
      public java.lang.String left(){
        return "GH5EYDXDIM";
      }
      public java.lang.String mid(){
        return "GH5EYDXDJM";
      }
      public java.lang.String right(){
        return "GH5EYDXDKM";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAGCAYAAAAG5SQMAAAAHUlEQVR42mNwqDzynwEGQBy4AIwDFsDKgSuD6QcAo8od0TmD2WkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAGCAYAAAAG5SQMAAAAFUlEQVR42mNgwAkcKo/8h9MoHJwAAH7dCfHZZdRDAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAGCAYAAAAG5SQMAAAAG0lEQVR42mNggAKHyiP/4QwwB8bA5KAoQzYAAFRKHdEZWbuZAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource left;
  private static com.google.gwt.resources.client.ImageResource mid;
  private static com.google.gwt.resources.client.ImageResource right;
  private static com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      left(), 
      mid(), 
      right(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("left", left());
        resourceMap.put("mid", mid());
        resourceMap.put("right", right());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'left': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::left()();
      case 'mid': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::mid()();
      case 'right': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::right()();
      case 'style': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::style()();
    }
    return null;
  }-*/;
}
