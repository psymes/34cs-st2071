package com.sencha.gxt.dnd.core.client;

public class Insert_DefaultInsertAppearance_Template_render_SafeHtml__InsertStyle_style___SafeHtmlTemplatesImpl implements com.sencha.gxt.dnd.core.client.Insert_DefaultInsertAppearance_Template_render_SafeHtml__InsertStyle_style___SafeHtmlTemplates {
  
  public com.google.gwt.safehtml.shared.SafeHtml render0(java.lang.String arg0,java.lang.String arg1,java.lang.String arg2,java.lang.String arg3) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<table class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("\" height=\"6\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr><td height=\"6\" class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("\"><div style=\"width: 3px\"></div></td><td class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg2));
    sb.append("\" width=\"100%\">&nbsp;</td><td class=\"");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg3));
    sb.append("\"><div style=\"width: 3px\"></div></td></tr></tbody></table>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
