package com.sencha.gxt.dnd.core.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator implements com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources {
  private static Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator _instance0 = new Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator();
  private void leftInitializer() {
    left = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "left",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      3, 0, 3, 6, false, false
    );
  }
  private static class leftInitializer {
    static {
      _instance0.leftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return left;
    }
  }
  public com.google.gwt.resources.client.ImageResource left() {
    return leftInitializer.get();
  }
  private void midInitializer() {
    mid = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "mid",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 3, 6, false, false
    );
  }
  private static class midInitializer {
    static {
      _instance0.midInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return mid;
    }
  }
  public com.google.gwt.resources.client.ImageResource mid() {
    return midInitializer.get();
  }
  private void rightInitializer() {
    right = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "right",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 3, 6, false, false
    );
  }
  private static class rightInitializer {
    static {
      _instance0.rightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return right;
    }
  }
  public com.google.gwt.resources.client.ImageResource right() {
    return rightInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GH5EYDXDHM{position:" + ("absolute")  + ";z-index:" + ("99999")  + ";}.GH5EYDXDHM td{line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDIM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.left()).getHeight() + "px")  + ";width:" + ((Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.left()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.left()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.left()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.left()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDJM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.mid()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.mid()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.mid()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.mid()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDKM{height:" + ((Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.right()).getHeight() + "px")  + ";width:" + ((Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.right()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.right()).getSafeUri().asString() + "\") -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.right()).getLeft() + "px -" + (Insert_DefaultInsertAppearance_InsertResources_default_StaticClientBundleGenerator.this.right()).getTop() + "px  no-repeat")  + ";}");
      }
      public java.lang.String bar(){
        return "GH5EYDXDHM";
      }
      public java.lang.String left(){
        return "GH5EYDXDIM";
      }
      public java.lang.String mid(){
        return "GH5EYDXDJM";
      }
      public java.lang.String right(){
        return "GH5EYDXDKM";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "2B2AC0533805839857B65E1A8AAEB266.cache.png";
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "50DE67980C35D7829399D0458AD23638.cache.png";
  private static com.google.gwt.resources.client.ImageResource left;
  private static com.google.gwt.resources.client.ImageResource mid;
  private static com.google.gwt.resources.client.ImageResource right;
  private static com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      left(), 
      mid(), 
      right(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("left", left());
        resourceMap.put("mid", mid());
        resourceMap.put("right", right());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'left': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::left()();
      case 'mid': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::mid()();
      case 'right': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::right()();
      case 'style': return this.@com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertResources::style()();
    }
    return null;
  }-*/;
}
