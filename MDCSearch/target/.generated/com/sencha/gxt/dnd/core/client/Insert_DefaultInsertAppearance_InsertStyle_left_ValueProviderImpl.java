package com.sencha.gxt.dnd.core.client;

public class Insert_DefaultInsertAppearance_InsertStyle_left_ValueProviderImpl implements com.sencha.gxt.core.client.ValueProvider<com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle, java.lang.String> {
  public static final Insert_DefaultInsertAppearance_InsertStyle_left_ValueProviderImpl INSTANCE = new Insert_DefaultInsertAppearance_InsertStyle_left_ValueProviderImpl();
  public java.lang.String getValue(com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle object) {
    return object.left();
  }
  public void setValue(com.sencha.gxt.dnd.core.client.Insert.DefaultInsertAppearance.InsertStyle object, java.lang.String value) {
    com.google.gwt.core.client.GWT.log("Setter was called on ValueProvider, but no setter exists.", new RuntimeException());
  }
  public String getPath() {
    return "left";
  }
}
