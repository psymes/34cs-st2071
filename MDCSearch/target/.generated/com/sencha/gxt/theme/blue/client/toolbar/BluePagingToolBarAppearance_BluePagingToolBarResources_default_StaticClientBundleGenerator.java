package com.sencha.gxt.theme.blue.client.toolbar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BluePagingToolBarAppearance_BluePagingToolBarResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources {
  private static BluePagingToolBarAppearance_BluePagingToolBarResources_default_StaticClientBundleGenerator _instance0 = new BluePagingToolBarAppearance_BluePagingToolBarResources_default_StaticClientBundleGenerator();
  private void firstInitializer() {
    first = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "first",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      64, 0, 16, 16, false, false
    );
  }
  private static class firstInitializer {
    static {
      _instance0.firstInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return first;
    }
  }
  public com.google.gwt.resources.client.ImageResource first() {
    return firstInitializer.get();
  }
  private void lastInitializer() {
    last = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "last",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      48, 0, 16, 16, false, false
    );
  }
  private static class lastInitializer {
    static {
      _instance0.lastInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return last;
    }
  }
  public com.google.gwt.resources.client.ImageResource last() {
    return lastInitializer.get();
  }
  private void loadingInitializer() {
    loading = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "loading",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 18, 18, true, false
    );
  }
  private static class loadingInitializer {
    static {
      _instance0.loadingInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return loading;
    }
  }
  public com.google.gwt.resources.client.ImageResource loading() {
    return loadingInitializer.get();
  }
  private void nextInitializer() {
    next = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "next",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      32, 0, 16, 16, false, false
    );
  }
  private static class nextInitializer {
    static {
      _instance0.nextInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return next;
    }
  }
  public com.google.gwt.resources.client.ImageResource next() {
    return nextInitializer.get();
  }
  private void prevInitializer() {
    prev = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "prev",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      16, 0, 16, 16, false, false
    );
  }
  private static class prevInitializer {
    static {
      _instance0.prevInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return prev;
    }
  }
  public com.google.gwt.resources.client.ImageResource prev() {
    return prevInitializer.get();
  }
  private void refreshInitializer() {
    refresh = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "refresh",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 16, 16, false, false
    );
  }
  private static class refreshInitializer {
    static {
      _instance0.refreshInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return refresh;
    }
  }
  public com.google.gwt.resources.client.ImageResource refresh() {
    return refreshInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "C610CADD6FA165BCF3838D4AC9113D68.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "C3D97B8246AAA193BB6C0B266E2EA8C7.cache.gif";
  private static com.google.gwt.resources.client.ImageResource first;
  private static com.google.gwt.resources.client.ImageResource last;
  private static com.google.gwt.resources.client.ImageResource loading;
  private static com.google.gwt.resources.client.ImageResource next;
  private static com.google.gwt.resources.client.ImageResource prev;
  private static com.google.gwt.resources.client.ImageResource refresh;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      first(), 
      last(), 
      loading(), 
      next(), 
      prev(), 
      refresh(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("first", first());
        resourceMap.put("last", last());
        resourceMap.put("loading", loading());
        resourceMap.put("next", next());
        resourceMap.put("prev", prev());
        resourceMap.put("refresh", refresh());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'first': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::first()();
      case 'last': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::last()();
      case 'loading': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::loading()();
      case 'next': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::next()();
      case 'prev': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::prev()();
      case 'refresh': return this.@com.sencha.gxt.theme.blue.client.toolbar.BluePagingToolBarAppearance.BluePagingToolBarResources::refresh()();
    }
    return null;
  }-*/;
}
