package com.sencha.gxt.theme.blue.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridResources {
  private static BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator _instance0 = new BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDITB{position:" + ("relative")  + ";overflow:" + ("hidden")  + ";outline:" + ("0"+ " " +"none")  + ";}.GH5EYDXDDUB{overflow:" + ("auto")  + ";zoom:" + ("1")  + ";position:" + ("relative")  + ";background-color:" + ("white")  + ";}TH{border-bottom:" + ("none")  + " !important;}.GH5EYDXDKIC{table-layout:" + ("fixed")  + ";border-collapse:" + ("separate")  + ";border-spacing:") + (("0")  + ";}.GH5EYDXDMIC{cursor:" + ("default")  + ";padding:" + ("0"+ " " +"1px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDBJC{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GH5EYDXDNIC .GH5EYDXDEIC{background-color:" + ("#fafafa")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC{background-color:" + ("#efefef")  + ";border-width:" + ("1px"+ " " +"0") ) + (";border-style:" + ("solid")  + ";}.GH5EYDXDEIC{background-color:" + ("white")  + ";border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px"+ " " +"0")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDGIC{overflow:" + ("hidden")  + ";padding:" + ("4px"+ " " +"5px"+ " " +"3px"+ " " +"3px")  + ";line-height:") + (("13px")  + ";white-space:" + ("nowrap")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";}.GH5EYDXDFIC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GH5EYDXDHIC{background:" + ("repeat-x"+ " " +"bottom")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/invalid_line.gif"))  + ";}.GH5EYDXDFIC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GH5EYDXDIIC{background-color:" + ("#b8cfee") ) + (" !important;color:" + ("#000")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC,.GH5EYDXDCJC .GH5EYDXDEJC{border-color:" + ("#ddd")  + ";}.GH5EYDXDJIC .GH5EYDXDEIC{padding-left:" + ("0")  + ";border-left:" + ("1px"+ " " +"solid")  + ";}.GH5EYDXDGTB{padding:" + ("10px")  + ";color:" + ("gray")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDOIC{font:" + ("11px"+ "/"+ " " +"13px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";padding:" + ("4px")  + ";}.GH5EYDXDEJC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:") + (("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC,.GH5EYDXDDJC .GH5EYDXDEJC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GH5EYDXDEJC .GH5EYDXDEIC,.GH5EYDXDEJC .GH5EYDXDGIC{border:" + ("none")  + ";}.x-grid-td-numberer,.GH5EYDXDDJC .x-grid-td-numberer{background:" + ("transparent"+ " " +"repeat-y"+ " " +"left")  + ";}.x-grid-td-numberer .GH5EYDXDGIC{padding:" + ("3px"+ " " +"0"+ " " +"0"+ " " +"5px")  + " !important;text-align:" + ("center") ) + (";color:" + ("#444")  + ";}.GH5EYDXDMIC .x-grid-td-numberer{width:" + ((BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getSafeUri().asString() + "\") -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getLeft() + "px -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDDJC .x-grid-td-numberer{width:" + ((BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getSafeUri().asString() + "\") -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getLeft() + "px -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:") + (("auto")  + ";}.GH5EYDXDHTB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GH5EYDXDHTB .GH5EYDXDEIC{background:" + ("none")  + ";}.x-treegrid .x-treegrid-column .GH5EYDXDGIC{padding:" + ("0")  + " !important;}.GH5EYDXDBJC{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GH5EYDXDNIC .GH5EYDXDEIC{background-color:" + ("#fafafa") ) + (";}.GH5EYDXDCJC .GH5EYDXDEIC{background-color:" + ("#efefef")  + ";}.GH5EYDXDEIC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";}.GH5EYDXDIIC{background-color:" + ("#b8cfee")  + " !important;color:" + ("#000")  + ";}.GH5EYDXDJIC .GH5EYDXDEIC{border-left-color:" + ("#ededed")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC,.GH5EYDXDCJC .GH5EYDXDEJC{border-color:" + ("#ddd")  + ";}.GH5EYDXDEJC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-left:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:") + (("1px")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC,.GH5EYDXDDJC .GH5EYDXDEJC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";}.GH5EYDXDHTB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}")) : ((".GH5EYDXDITB{position:" + ("relative")  + ";overflow:" + ("hidden")  + ";outline:" + ("0"+ " " +"none")  + ";}.GH5EYDXDDUB{overflow:" + ("auto")  + ";zoom:" + ("1")  + ";position:" + ("relative")  + ";background-color:" + ("white")  + ";}TH{border-bottom:" + ("none")  + " !important;}.GH5EYDXDKIC{table-layout:" + ("fixed")  + ";border-collapse:" + ("separate")  + ";border-spacing:") + (("0")  + ";}.GH5EYDXDMIC{cursor:" + ("default")  + ";padding:" + ("0"+ " " +"1px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDBJC{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GH5EYDXDNIC .GH5EYDXDEIC{background-color:" + ("#fafafa")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC{background-color:" + ("#efefef")  + ";border-width:" + ("1px"+ " " +"0") ) + (";border-style:" + ("solid")  + ";}.GH5EYDXDEIC{background-color:" + ("white")  + ";border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px"+ " " +"0")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDGIC{overflow:" + ("hidden")  + ";padding:" + ("4px"+ " " +"3px"+ " " +"3px"+ " " +"5px")  + ";line-height:") + (("13px")  + ";white-space:" + ("nowrap")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";}.GH5EYDXDFIC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GH5EYDXDHIC{background:" + ("repeat-x"+ " " +"bottom")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/invalid_line.gif"))  + ";}.GH5EYDXDFIC{background:" + ("transparent"+ " " +"no-repeat"+ " " +"0"+ " " +"0")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/dirty.gif"))  + ";}.GH5EYDXDIIC{background-color:" + ("#b8cfee") ) + (" !important;color:" + ("#000")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC,.GH5EYDXDCJC .GH5EYDXDEJC{border-color:" + ("#ddd")  + ";}.GH5EYDXDJIC .GH5EYDXDEIC{padding-right:" + ("0")  + ";border-right:" + ("1px"+ " " +"solid")  + ";}.GH5EYDXDGTB{padding:" + ("10px")  + ";color:" + ("gray")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDOIC{font:" + ("11px"+ "/"+ " " +"13px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";padding:" + ("4px")  + ";}.GH5EYDXDEJC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:") + (("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:" + ("1px")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC,.GH5EYDXDDJC .GH5EYDXDEJC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";border-style:" + ("dotted")  + ";}.GH5EYDXDEJC .GH5EYDXDEIC,.GH5EYDXDEJC .GH5EYDXDGIC{border:" + ("none")  + ";}.x-grid-td-numberer,.GH5EYDXDDJC .x-grid-td-numberer{background:" + ("transparent"+ " " +"repeat-y"+ " " +"right")  + ";}.x-grid-td-numberer .GH5EYDXDGIC{padding:" + ("3px"+ " " +"5px"+ " " +"0"+ " " +"0")  + " !important;text-align:" + ("center") ) + (";color:" + ("#444")  + ";}.GH5EYDXDMIC .x-grid-td-numberer{width:" + ((BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getSafeUri().asString() + "\") -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getLeft() + "px -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumn()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDDJC .x-grid-td-numberer{width:" + ((BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getSafeUri().asString() + "\") -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getLeft() + "px -" + (BlueGridAppearance_BlueGridResources_default_InlineClientBundleGenerator.this.specialColumnSelected()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:") + (("auto")  + ";}.GH5EYDXDHTB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GH5EYDXDHTB .GH5EYDXDEIC{background:" + ("none")  + ";}.x-treegrid .x-treegrid-column .GH5EYDXDGIC{padding:" + ("0")  + " !important;}.GH5EYDXDBJC{border:" + ("1px"+ " " +"dotted"+ " " +"#545352")  + ";}.GH5EYDXDNIC .GH5EYDXDEIC{background-color:" + ("#fafafa") ) + (";}.GH5EYDXDCJC .GH5EYDXDEIC{background-color:" + ("#efefef")  + ";}.GH5EYDXDEIC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";}.GH5EYDXDIIC{background-color:" + ("#b8cfee")  + " !important;color:" + ("#000")  + ";}.GH5EYDXDJIC .GH5EYDXDEIC{border-right-color:" + ("#ededed")  + ";}.GH5EYDXDCJC .GH5EYDXDEIC,.GH5EYDXDCJC .GH5EYDXDEJC{border-color:" + ("#ddd")  + ";}.GH5EYDXDEJC{border-color:" + ("#fafafa"+ " " +"#ededed"+ " " +"#ededed")  + ";border-right:" + ("0"+ " " +"solid"+ " " +"#ededed")  + ";border-style:" + ("solid")  + ";border-width:") + (("1px")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDJC .GH5EYDXDEIC,.GH5EYDXDDJC .GH5EYDXDEJC{background-color:" + ("#dfe8f6")  + " !important;border-color:" + ("#a3bae9")  + ";}.GH5EYDXDHTB{background:" + ("#f7f7f7"+ " " +"none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"#ddd")  + ";display:" + ("block")  + ";overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}"));
      }
      public java.lang.String body(){
        return "GH5EYDXDOSB";
      }
      public java.lang.String cell(){
        return "GH5EYDXDEIC";
      }
      public java.lang.String cellDirty(){
        return "GH5EYDXDFIC";
      }
      public java.lang.String cellInner(){
        return "GH5EYDXDGIC";
      }
      public java.lang.String cellInvalid(){
        return "GH5EYDXDHIC";
      }
      public java.lang.String cellSelected(){
        return "GH5EYDXDIIC";
      }
      public java.lang.String columnLines(){
        return "GH5EYDXDJIC";
      }
      public java.lang.String dataTable(){
        return "GH5EYDXDKIC";
      }
      public java.lang.String empty(){
        return "GH5EYDXDGTB";
      }
      public java.lang.String footer(){
        return "GH5EYDXDHTB";
      }
      public java.lang.String grid(){
        return "GH5EYDXDITB";
      }
      public java.lang.String headerRow(){
        return "GH5EYDXDLIC";
      }
      public java.lang.String row(){
        return "GH5EYDXDMIC";
      }
      public java.lang.String rowAlt(){
        return "GH5EYDXDNIC";
      }
      public java.lang.String rowBody(){
        return "GH5EYDXDOIC";
      }
      public java.lang.String rowBodyRow(){
        return "GH5EYDXDPIC";
      }
      public java.lang.String rowDirty(){
        return "GH5EYDXDAJC";
      }
      public java.lang.String rowHighlight(){
        return "GH5EYDXDBJC";
      }
      public java.lang.String rowOver(){
        return "GH5EYDXDCJC";
      }
      public java.lang.String rowSelected(){
        return "GH5EYDXDDJC";
      }
      public java.lang.String rowWrap(){
        return "GH5EYDXDEJC";
      }
      public java.lang.String scroller(){
        return "GH5EYDXDDUB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle css() {
    return cssInitializer.get();
  }
  private void specialColumnInitializer() {
    specialColumn = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumn",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnInitializer {
    static {
      _instance0.specialColumnInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumn;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumn() {
    return specialColumnInitializer.get();
  }
  private void specialColumnSelectedInitializer() {
    specialColumnSelected = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumnSelected",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnSelectedInitializer {
    static {
      _instance0.specialColumnSelectedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumnSelected;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumnSelected() {
    return specialColumnSelectedInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridStyle css;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAANklEQVR42rXNqREAIAwEQPqvLn2QH0xUmDmBxiBW76iqfhG5wWNd5glq0aIOLNaTFYiox+/gACe+uBEe1wRCAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAAXUlEQVR42rXNvQ6CMBSA0b7/o/AsDgSiRmIwKNTW8iOaMPTe5qOzu8OZjwnfxC//Sbg1Yd/KsCh9dp+VblJuo9IG5foSGi9cnHB+CqfsaIV6iFR9pHxEisOG+XewA9F9tJvfRlttAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource specialColumn;
  private static com.google.gwt.resources.client.ImageResource specialColumnSelected;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      specialColumn(), 
      specialColumnSelected(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("specialColumn", specialColumn());
        resourceMap.put("specialColumnSelected", specialColumnSelected());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.blue.client.grid.BlueGridAppearance.BlueGridResources::css()();
      case 'specialColumn': return this.@com.sencha.gxt.theme.base.client.grid.GridBaseAppearance.GridResources::specialColumn()();
      case 'specialColumnSelected': return this.@com.sencha.gxt.theme.base.client.grid.GridBaseAppearance.GridResources::specialColumnSelected()();
    }
    return null;
  }-*/;
}
