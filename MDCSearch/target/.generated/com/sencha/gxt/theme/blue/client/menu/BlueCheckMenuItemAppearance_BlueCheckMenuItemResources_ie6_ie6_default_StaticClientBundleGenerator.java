package com.sencha.gxt.theme.blue.client.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueCheckMenuItemAppearance_BlueCheckMenuItemResources_ie6_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemResources {
  private static BlueCheckMenuItemAppearance_BlueCheckMenuItemResources_ie6_ie6_default_StaticClientBundleGenerator _instance0 = new BlueCheckMenuItemAppearance_BlueCheckMenuItemResources_ie6_ie6_default_StaticClientBundleGenerator();
  private void checkStyleInitializer() {
    checkStyle = new com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "checkStyle";
      }
      public String getText() {
        return (".GH5EYDXDEUB{border:" + ("1px"+ " " +"dotted")  + " !important;padding:" + ("0")  + ";border-color:" + ("#a3bae9")  + " !important;background-color:" + ("#def8f6")  + ";}");
      }
      public java.lang.String menuItemChecked(){
        return "GH5EYDXDEUB";
      }
    }
    ;
  }
  private static class checkStyleInitializer {
    static {
      _instance0.checkStyleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemStyle get() {
      return checkStyle;
    }
  }
  public com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemStyle checkStyle() {
    return checkStyleInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (("a.GH5EYDXDIVB{cursor:" + ("pointer")  + ";display:" + ("block")  + ";line-height:" + ("16px")  + ";outline-style:" + ("none")  + ";outline-width:" + ("0")  + ";padding:" + ("3px"+ " " +"27px"+ " " +"3px"+ " " +"21px")  + ";position:" + ("relative")  + ";text-decoration:" + ("none")  + ";white-space:" + ("nowrap")  + ";color:" + ("#222")  + ";}.GH5EYDXDLVB{white-space:") + (("nowrap")  + ";display:" + ("block")  + ";padding:" + ("1px")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDKVB{border:" + ("0"+ " " +"none")  + ";height:" + ("16px")  + ";padding:" + ("0")  + ";vertical-align:" + ("top")  + ";width:" + ("16px")  + ";position:" + ("absolute")  + ";right:" + ("3px") ) + (";top:" + ("3px")  + ";margin:" + ("0")  + ";background-position:" + ("center")  + ";right:" + ("-24px")  + ";vertical-align:" + ("middle")  + ";}.GH5EYDXDJVB{background-image:" + (com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemAppearanceHelper.getMenuParent())  + ";background-position:" + ("left")  + ";background-repeat:" + ("no-repeat")  + ";}")) : (("a.GH5EYDXDIVB{cursor:" + ("pointer")  + ";display:" + ("block")  + ";line-height:" + ("16px")  + ";outline-style:" + ("none")  + ";outline-width:" + ("0")  + ";padding:" + ("3px"+ " " +"21px"+ " " +"3px"+ " " +"27px")  + ";position:" + ("relative")  + ";text-decoration:" + ("none")  + ";white-space:" + ("nowrap")  + ";color:" + ("#222")  + ";}.GH5EYDXDLVB{white-space:") + (("nowrap")  + ";display:" + ("block")  + ";padding:" + ("1px")  + ";font:" + ("normal"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDKVB{border:" + ("0"+ " " +"none")  + ";height:" + ("16px")  + ";padding:" + ("0")  + ";vertical-align:" + ("top")  + ";width:" + ("16px")  + ";position:" + ("absolute")  + ";left:" + ("3px") ) + (";top:" + ("3px")  + ";margin:" + ("0")  + ";background-position:" + ("center")  + ";left:" + ("-24px")  + ";vertical-align:" + ("middle")  + ";}.GH5EYDXDJVB{background-image:" + (com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemAppearanceHelper.getMenuParent())  + ";background-position:" + ("right")  + ";background-repeat:" + ("no-repeat")  + ";}"));
      }
      public java.lang.String menuItem(){
        return "GH5EYDXDIVB";
      }
      public java.lang.String menuItemArrow(){
        return "GH5EYDXDJVB";
      }
      public java.lang.String menuItemIcon(){
        return "GH5EYDXDKVB";
      }
      public java.lang.String menuListItem(){
        return "GH5EYDXDLVB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemStyle style() {
    return styleInitializer.get();
  }
  private void checkedInitializer() {
    checked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "checked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      32, 0, 16, 16, false, false
    );
  }
  private static class checkedInitializer {
    static {
      _instance0.checkedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return checked;
    }
  }
  public com.google.gwt.resources.client.ImageResource checked() {
    return checkedInitializer.get();
  }
  private void groupCheckedInitializer() {
    groupChecked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "groupChecked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      16, 0, 16, 16, false, false
    );
  }
  private static class groupCheckedInitializer {
    static {
      _instance0.groupCheckedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return groupChecked;
    }
  }
  public com.google.gwt.resources.client.ImageResource groupChecked() {
    return groupCheckedInitializer.get();
  }
  private void menuParentInitializer() {
    menuParent = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "menuParent",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      48, 0, 12, 9, false, false
    );
  }
  private static class menuParentInitializer {
    static {
      _instance0.menuParentInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return menuParent;
    }
  }
  public com.google.gwt.resources.client.ImageResource menuParent() {
    return menuParentInitializer.get();
  }
  private void uncheckedInitializer() {
    unchecked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "unchecked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 16, 16, false, false
    );
  }
  private static class uncheckedInitializer {
    static {
      _instance0.uncheckedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return unchecked;
    }
  }
  public com.google.gwt.resources.client.ImageResource unchecked() {
    return uncheckedInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemStyle checkStyle;
  private static com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemStyle style;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "B2F3C76C4A54E53A4E4FC6C714A07333.cache.png";
  private static com.google.gwt.resources.client.ImageResource checked;
  private static com.google.gwt.resources.client.ImageResource groupChecked;
  private static com.google.gwt.resources.client.ImageResource menuParent;
  private static com.google.gwt.resources.client.ImageResource unchecked;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      checkStyle(), 
      style(), 
      checked(), 
      groupChecked(), 
      menuParent(), 
      unchecked(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("checkStyle", checkStyle());
        resourceMap.put("style", style());
        resourceMap.put("checked", checked());
        resourceMap.put("groupChecked", groupChecked());
        resourceMap.put("menuParent", menuParent());
        resourceMap.put("unchecked", unchecked());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'checkStyle': return this.@com.sencha.gxt.theme.blue.client.menu.BlueCheckMenuItemAppearance.BlueCheckMenuItemResources::checkStyle()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemResources::style()();
      case 'checked': return this.@com.sencha.gxt.theme.base.client.menu.CheckMenuItemBaseAppearance.CheckMenuItemResources::checked()();
      case 'groupChecked': return this.@com.sencha.gxt.theme.base.client.menu.CheckMenuItemBaseAppearance.CheckMenuItemResources::groupChecked()();
      case 'menuParent': return this.@com.sencha.gxt.theme.blue.client.menu.BlueMenuItemAppearance.BlueMenuItemResources::menuParent()();
      case 'unchecked': return this.@com.sencha.gxt.theme.base.client.menu.CheckMenuItemBaseAppearance.CheckMenuItemResources::unchecked()();
    }
    return null;
  }-*/;
}
