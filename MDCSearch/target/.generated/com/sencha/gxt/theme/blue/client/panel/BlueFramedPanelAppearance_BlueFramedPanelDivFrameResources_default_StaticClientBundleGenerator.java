package com.sencha.gxt.theme.blue.client.panel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources {
  private static BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator _instance0 = new BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 1, 1, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 150, 1, 6, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both0),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both1),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      0, 0, 6, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both2),
      0, 0, 6, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 1, 150, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 6, 150, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both3),
      0, 0, 6, 150, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramePanelNestedDivFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDDXB{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDCXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";padding-top:" + ("6px")  + ";}.GH5EYDXDJXB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-right:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDIXB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDKXB{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-left:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDAXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:") + (("0"+ " " +"bottom")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDPWB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto") ) + (";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDBXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"bottom")  + ";padding-left:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDEXB{width:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto") ) + (";width:" + ("auto")  + ";}.GH5EYDXDHXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("left"+ " " +"0")  + ";padding-left:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDJXB{height:" + ("auto")  + ";border-bottom:") + (("1px"+ " " +"solid")  + ";border-color:" + ("#99bbe8")  + ";}.GH5EYDXDIXB,.GH5EYDXDKXB{height:" + ("auto")  + ";}.noheader .GH5EYDXDJXB,.noheader .GH5EYDXDIXB{height:" + ("3px")  + ";border:" + ("none")  + ";}.GH5EYDXDEXB{background-color:" + ("#dfe8f6")  + ";padding-right:" + ("6px")  + ";}.GH5EYDXDHXB{background-color:" + ("#dfe8f6")  + ";}.GH5EYDXDPWB,.GH5EYDXDAXB,.GH5EYDXDBXB{height:" + ("6px")  + ";}")) : ((".GH5EYDXDDXB{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDCXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";padding-top:" + ("6px")  + ";}.GH5EYDXDJXB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-left:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDIXB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDKXB{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-right:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDAXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:") + (("0"+ " " +"bottom")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDPWB{height:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto") ) + (";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDBXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"bottom")  + ";padding-right:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:") + (("auto")  + ";height:" + ("auto")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDEXB{width:" + ((BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto") ) + (";width:" + ("auto")  + ";}.GH5EYDXDHXB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueFramedPanelAppearance_BlueFramedPanelDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("right"+ " " +"0")  + ";padding-right:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDJXB{height:" + ("auto")  + ";border-bottom:") + (("1px"+ " " +"solid")  + ";border-color:" + ("#99bbe8")  + ";}.GH5EYDXDIXB,.GH5EYDXDKXB{height:" + ("auto")  + ";}.noheader .GH5EYDXDJXB,.noheader .GH5EYDXDIXB{height:" + ("3px")  + ";border:" + ("none")  + ";}.GH5EYDXDEXB{background-color:" + ("#dfe8f6")  + ";padding-left:" + ("6px")  + ";}.GH5EYDXDHXB{background-color:" + ("#dfe8f6")  + ";}.GH5EYDXDPWB,.GH5EYDXDAXB,.GH5EYDXDBXB{height:" + ("6px")  + ";}"));
      }
      public java.lang.String bodyWrap(){
        return "GH5EYDXDOWB";
      }
      public java.lang.String bottom(){
        return "GH5EYDXDPWB";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDAXB";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDBXB";
      }
      public java.lang.String content(){
        return "GH5EYDXDCXB";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDDXB";
      }
      public java.lang.String left(){
        return "GH5EYDXDEXB";
      }
      public java.lang.String over(){
        return "GH5EYDXDFXB";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDGXB";
      }
      public java.lang.String right(){
        return "GH5EYDXDHXB";
      }
      public java.lang.String top(){
        return "GH5EYDXDIXB";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDJXB";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDKXB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramePanelNestedDivFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramePanelNestedDivFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "5866A6F1619A597CDFD4953F6493175F.cache.png";
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "F48DE06ECA4A1D2DE922FE9E52DFCC22.cache.png";
  private static final java.lang.String bundledImage_Both0 = GWT.getModuleBaseURL() + "05BEBC6BA25556A54B54B19E40798BF5.cache.png";
  private static final java.lang.String bundledImage_Both1 = GWT.getModuleBaseURL() + "AB1F2F3F03FAD6B05C43C9F1908C53A0.cache.png";
  private static final java.lang.String bundledImage_Vertical = GWT.getModuleBaseURL() + "EB3FBDC3BF46FA1B9878EF3A4EB25404.cache.png";
  private static final java.lang.String bundledImage_Both2 = GWT.getModuleBaseURL() + "464A726B03E53892DF1A2626086E421A.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "819A960A6B5E0BCEEAF4FC40106697F6.cache.png";
  private static final java.lang.String bundledImage_Both3 = GWT.getModuleBaseURL() + "58293956E8F7C342B253A7843A733D50.cache.png";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramePanelNestedDivFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.panel.BlueFramedPanelAppearance.BlueFramedPanelDivFrameResources::style()();
    }
    return null;
  }-*/;
}
