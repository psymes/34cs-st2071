package com.sencha.gxt.theme.blue.client.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources {
  private static BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator _instance0 = new BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 3, 3, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void backgroundOverBorderInitializer() {
    backgroundOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundOverBorderInitializer {
    static {
      _instance0.backgroundOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundOverBorder() {
    return backgroundOverBorderInitializer.get();
  }
  private void backgroundPressedBorderInitializer() {
    backgroundPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundPressedBorderInitializer {
    static {
      _instance0.backgroundPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundPressedBorder() {
    return backgroundPressedBorderInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 1, 6, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 6, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void leftOverBorderInitializer() {
    leftOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftOverBorderInitializer {
    static {
      _instance0.leftOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftOverBorder() {
    return leftOverBorderInitializer.get();
  }
  private void leftPressedBorderInitializer() {
    leftPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftPressedBorderInitializer {
    static {
      _instance0.leftPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftPressedBorder() {
    return leftPressedBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage8),
      0, 0, 6, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage9),
      0, 0, 1, 50, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage10),
      0, 0, 6, 50, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topNoHeadBorderInitializer() {
    topNoHeadBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topNoHeadBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage11),
      0, 0, 1, 50, false, false
    );
  }
  private static class topNoHeadBorderInitializer {
    static {
      _instance0.topNoHeadBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topNoHeadBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topNoHeadBorder() {
    return topNoHeadBorderInitializer.get();
  }
  private void topOverBorderInitializer() {
    topOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage12),
      0, 0, 62, 3, false, false
    );
  }
  private static class topOverBorderInitializer {
    static {
      _instance0.topOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topOverBorder() {
    return topOverBorderInitializer.get();
  }
  private void topPressedBorderInitializer() {
    topPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage13),
      0, 0, 52, 3, false, false
    );
  }
  private static class topPressedBorderInitializer {
    static {
      _instance0.topPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topPressedBorder() {
    return topPressedBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage14),
      0, 0, 6, 50, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".x-has-width .GH5EYDXDHO{width:" + ("100%")  + ";}.GH5EYDXDFO{border-bottom:" + ("none")  + " !important;}.GH5EYDXDEO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDOO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";padding:" + ("2px")  + ";text-align:" + ("center")  + ";}.GH5EYDXDPO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getHeight() + "px")  + ";width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDBO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getHeight() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDDO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDMO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDOO,.GH5EYDXDNO{height:") + (("auto")  + ";}.GH5EYDXDPO{height:" + ("auto")  + ";background-position:" + ("top"+ " " +"left")  + ";}.GH5EYDXDMO{background-repeat:" + ("repeat-y")  + ";background-position:" + ("center"+ " " +"left")  + ";}.GH5EYDXDBO{width:" + ("auto")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("center"+ " " +"bottom")  + ";height:" + ("3px")  + ";}.GH5EYDXDOO,.GH5EYDXDPO,.GH5EYDXDIO,.GH5EYDXDMO{width:" + ("3px")  + ";}.GH5EYDXDCO{height:" + ("3px") ) + (";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDDO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left")  + ";}.GH5EYDXDJO .GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getTop() + "px  repeat-x")  + ";height:" + ("2px")  + ";width:") + (("auto")  + ";}")) : ((".x-has-width .GH5EYDXDHO{width:" + ("100%")  + ";}.GH5EYDXDFO{border-bottom:" + ("none")  + " !important;}.GH5EYDXDEO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDOO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";padding:" + ("2px")  + ";text-align:" + ("center")  + ";}.GH5EYDXDPO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getHeight() + "px")  + ";width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDBO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getHeight() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDDO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDMO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDOO,.GH5EYDXDNO{height:") + (("auto")  + ";}.GH5EYDXDPO{height:" + ("auto")  + ";background-position:" + ("top"+ " " +"right")  + ";}.GH5EYDXDMO{background-repeat:" + ("repeat-y")  + ";background-position:" + ("center"+ " " +"right")  + ";}.GH5EYDXDBO{width:" + ("auto")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("center"+ " " +"bottom")  + ";height:" + ("3px")  + ";}.GH5EYDXDOO,.GH5EYDXDPO,.GH5EYDXDIO,.GH5EYDXDMO{width:" + ("3px")  + ";}.GH5EYDXDCO{height:" + ("3px") ) + (";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDDO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right")  + ";}.GH5EYDXDJO .GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_opera_default_InlineClientBundleGenerator.this.topNoHeadBorder()).getTop() + "px  repeat-x")  + ";height:" + ("2px")  + ";width:") + (("auto")  + ";}"));
      }
      public java.lang.String bottom(){
        return "GH5EYDXDBO";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDCO";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDDO";
      }
      public java.lang.String content(){
        return "GH5EYDXDEO";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDFO";
      }
      public java.lang.String focus(){
        return "GH5EYDXDGO";
      }
      public java.lang.String frame(){
        return "GH5EYDXDHO";
      }
      public java.lang.String left(){
        return "GH5EYDXDIO";
      }
      public java.lang.String noheader(){
        return "GH5EYDXDJO";
      }
      public java.lang.String over(){
        return "GH5EYDXDKO";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDLO";
      }
      public java.lang.String right(){
        return "GH5EYDXDMO";
      }
      public java.lang.String top(){
        return "GH5EYDXDNO";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDOO";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDPO";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/gif;base64,R0lGODlhAwADAIAAAP///wAAACH5BAEAAAAALAAAAAADAAMAAAIDhH8FADs=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAARMCAYAAABYnPkoAAAAhUlEQVR42u3MNQ5CURQE0Nn/snD9SIB8wyE4JHiCDPe9goIVUExzyoP0QOKHZG/EXyLPzggdmfBIZKMTkfPEZyKfXIhCeiWKnv6NKA3uRHn4ICqO6uhpjF9EbfIm6o5gal8wMxpzo+loLYz20rEyOo7u2uhtHFshhBBCCCGEEEIIIYT4fz47RRuz1PdkvwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAARMCAYAAABYnPkoAAAAkElEQVR42u3L2woBUQCG0f3+JSlJSpKUJCUxRgyGwpwHOTwA3oDL7eMt1H+z7pYZB3drfjghTCJw44c10wRmX+YpeNnTmkUOBe9tTXEBpSWUV1Dxobp+WVPbQH0LjR0099AKoB1CJ4JuDL2E0U9hkMEwh9EBnCO4J5idwbvA8gr+TQghhBBCCCGEEEIIIf6ED0rGTpFBWmozAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAGCAYAAAACEPQxAAAAFUlEQVR42mNgwAIev/76n2H7iev/ASP/BxijSRT0AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage3 = GWT.getModuleBaseURL() + "50E70590F3183DAEE210D0DD54F2F370.cache.gif";
  private static final java.lang.String externalImage4 = GWT.getModuleBaseURL() + "BC5B11EC6AD181449D4DCA9D9AAEF282.cache.gif";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAABCAYAAAD9yd/wAAAAFElEQVR42mPYfuL6/8evv/5nQAMAj/QHGKqtGIYAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAPoCAYAAAAbQ0RjAAABXElEQVR42u3QZy+DYRQG4Pd/EzNGzIQghKJoFJEQMWIl9urW6l7eavt26o6O4zxPJfwEH+6P14dz7nMfZXzXR8UWUSBPpIxue6jA8OcYI1tuKjR/MLTp+sWAwUmfDF+W0bfhkPAK9K7ZKN/4QY/eKuER6Fo1t5FhdOhMlKsTuf/iyl0gpXPZLHHtKfKM3kJZxo1AN2/LfjG8JQ7lnAzj1lcmpX/dLnHnrzAMDol7gUG+Ol1jBKpcwfgm8RCskTJsdEs8hurt2qkq0ZPA2I5X4jncIGWCv6MJRJqkTO75JV6iLYEAaRUik8DUfpCSDPM795k+CLWhMmYOw5QoE1lijNmjiIRVYO44KmGLM+ZPVIoz7AnGwmmM4iVGkrF49iHxKrB0niCV4dQYuoukhCvFWLnUSC0y0gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8T3587aX+3eFtSAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAPoCAYAAAAbQ0RjAAABUUlEQVR42u3Qx06CQRSG4XPNdg3YSywEK3YJVkRiYowFg4AUCx3s7oE7mLM8/n5X4eJbvMk8yWRmzshYuGjXxRaSkd0Xuym1kPh3nu223ELi236yRKWNZGizYHfVNpLBjbwlvcVf0h/K2X2tjaR3LWupegdJz0rG0o0Okq7ltGWaHQQEUoqk29sWTCuSntUHW8ookj7vtNUHRbgnlFUkA+t528g55D2nYFt5h/DQ3SeHxLf1aOFnh8TvzRN5cUiGvUkPig7hD45KDsnoXtGiZYdkPFyyWMUhmYiULV5VJJP7FTuvKZKpg6pd1BXJ9GHNLhuKZOaobldNRTJ73LCbV0Uyd9KwxJsimY82LfmhSBZOXy31qUgCsTfLfCmS4Nm7Zb8VyWL8wwo/igiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIP4jfgEoEObAPHI8/QAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAABCAYAAAD9yd/wAAAAFElEQVR42mNgQAOPX3/9v/3E9f8AIDgHGAQcEQIAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAyCAYAAACd+7GKAAAAGUlEQVR42mPYfuL6f4bHr7/+Zzh048NIIgBEbLDIQ+jGSgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage10 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAyCAYAAAB/J6rzAAAANklEQVR42mNgAIILd178337iOgpmOHfrOZjx/N13FMyALHjoxgc4hksgC45KjEqMSoxKDJwEALT3JDVH2YxQAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage11 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAyCAYAAACd+7GKAAAAFklEQVR42mPYfuL6f4bHr7/+ZxhhAADIpAcY2VhaVQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage12 = "data:image/gif;base64,R0lGODlhPgADAJEAAPf//6nH8uTz/wAAACH5BAAAAAAALAAAAAA+AAMAAAIVjI+py+0Lopy02ouzFbz7D4biSIYFADs=";
  private static final java.lang.String externalImage13 = "data:image/gif;base64,R0lGODlhNAADAJEAALbK5Z654qW/3gAAACH5BAAAAAAALAAAAAA0AAMAAAITjI+py70Co5y02luB3rz7D4ZfAQA7";
  private static final java.lang.String externalImage14 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAyCAYAAAB/J6rzAAAAOklEQVR42mPYfuL6f2R84c6L/wwg8Pzd9//IGCR57tbz/wyHbnz4D8PIkigSMMlRiVGJUYlRiYGTAAAshiQ14LaAqwAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource backgroundOverBorder;
  private static com.google.gwt.resources.client.ImageResource backgroundPressedBorder;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource leftOverBorder;
  private static com.google.gwt.resources.client.ImageResource leftPressedBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topNoHeadBorder;
  private static com.google.gwt.resources.client.ImageResource topOverBorder;
  private static com.google.gwt.resources.client.ImageResource topPressedBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      backgroundOverBorder(), 
      backgroundPressedBorder(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      leftOverBorder(), 
      leftPressedBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topNoHeadBorder(), 
      topOverBorder(), 
      topPressedBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("backgroundOverBorder", backgroundOverBorder());
        resourceMap.put("backgroundPressedBorder", backgroundPressedBorder());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("leftOverBorder", leftOverBorder());
        resourceMap.put("leftPressedBorder", leftPressedBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topNoHeadBorder", topNoHeadBorder());
        resourceMap.put("topOverBorder", topOverBorder());
        resourceMap.put("topPressedBorder", topPressedBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::background()();
      case 'backgroundOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::backgroundOverBorder()();
      case 'backgroundPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::backgroundPressedBorder()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::leftBorder()();
      case 'leftOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::leftOverBorder()();
      case 'leftPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::leftPressedBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topLeftBorder()();
      case 'topNoHeadBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topNoHeadBorder()();
      case 'topOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::topOverBorder()();
      case 'topPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::topPressedBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::style()();
    }
    return null;
  }-*/;
}
