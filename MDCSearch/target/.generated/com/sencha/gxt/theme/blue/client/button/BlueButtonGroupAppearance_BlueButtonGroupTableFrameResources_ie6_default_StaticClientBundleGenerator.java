package com.sencha.gxt.theme.blue.client.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources {
  private static BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator _instance0 = new BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 3, 3, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void backgroundOverBorderInitializer() {
    backgroundOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundOverBorderInitializer {
    static {
      _instance0.backgroundOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundOverBorder() {
    return backgroundOverBorderInitializer.get();
  }
  private void backgroundPressedBorderInitializer() {
    backgroundPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundPressedBorderInitializer {
    static {
      _instance0.backgroundPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundPressedBorder() {
    return backgroundPressedBorderInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 103, 1, 6, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      12, 0, 6, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void leftOverBorderInitializer() {
    leftOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftOverBorderInitializer {
    static {
      _instance0.leftOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftOverBorder() {
    return leftOverBorderInitializer.get();
  }
  private void leftPressedBorderInitializer() {
    leftPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftPressedBorderInitializer {
    static {
      _instance0.leftPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftPressedBorder() {
    return leftPressedBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      6, 0, 6, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 50, 1, 50, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      0, 0, 6, 50, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topNoHeadBorderInitializer() {
    topNoHeadBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topNoHeadBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 1, 50, false, false
    );
  }
  private static class topNoHeadBorderInitializer {
    static {
      _instance0.topNoHeadBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topNoHeadBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topNoHeadBorder() {
    return topNoHeadBorderInitializer.get();
  }
  private void topOverBorderInitializer() {
    topOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 109, 62, 3, false, false
    );
  }
  private static class topOverBorderInitializer {
    static {
      _instance0.topOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topOverBorder() {
    return topOverBorderInitializer.get();
  }
  private void topPressedBorderInitializer() {
    topPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 100, 52, 3, false, false
    );
  }
  private static class topPressedBorderInitializer {
    static {
      _instance0.topPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topPressedBorder() {
    return topPressedBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 6, 50, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".x-has-width .GH5EYDXDHO{width:" + ("100%")  + ";}.GH5EYDXDFO{border-bottom:" + ("none")  + " !important;}.GH5EYDXDEO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDOO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";padding:" + ("2px")  + ";text-align:" + ("center")  + ";}.GH5EYDXDPO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getHeight() + "px")  + ";width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDBO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getHeight() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDDO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDMO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDEO{background-image:") + ((com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/clear.gif"))  + ";}.GH5EYDXDOO,.GH5EYDXDNO{height:" + ("auto")  + ";}.GH5EYDXDPO{height:" + ("auto")  + ";background-position:" + ("top"+ " " +"left")  + ";}.GH5EYDXDMO{background-repeat:" + ("repeat-y")  + ";background-position:" + ("center"+ " " +"left")  + ";}.GH5EYDXDBO{width:" + ("auto")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("center"+ " " +"bottom")  + ";height:" + ("3px")  + ";}.GH5EYDXDOO,.GH5EYDXDPO,.GH5EYDXDIO,.GH5EYDXDMO{width:" + ("3px") ) + (";}.GH5EYDXDCO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDDO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left")  + ";}.GH5EYDXDJO .GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getTop() + "px  repeat-x")  + ";height:") + (("2px")  + ";width:" + ("auto")  + ";}")) : ((".x-has-width .GH5EYDXDHO{width:" + ("100%")  + ";}.GH5EYDXDFO{border-bottom:" + ("none")  + " !important;}.GH5EYDXDEO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDOO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";padding:" + ("2px")  + ";text-align:" + ("center")  + ";}.GH5EYDXDPO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getHeight() + "px")  + ";width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDBO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getHeight() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDDO{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDMO{width:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDEO{background-image:") + ((com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/clear.gif"))  + ";}.GH5EYDXDOO,.GH5EYDXDNO{height:" + ("auto")  + ";}.GH5EYDXDPO{height:" + ("auto")  + ";background-position:" + ("top"+ " " +"right")  + ";}.GH5EYDXDMO{background-repeat:" + ("repeat-y")  + ";background-position:" + ("center"+ " " +"right")  + ";}.GH5EYDXDBO{width:" + ("auto")  + ";background-repeat:" + ("repeat-x")  + ";background-position:" + ("center"+ " " +"bottom")  + ";height:" + ("3px")  + ";}.GH5EYDXDOO,.GH5EYDXDPO,.GH5EYDXDIO,.GH5EYDXDMO{width:" + ("3px") ) + (";}.GH5EYDXDCO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDDO{height:" + ("3px")  + ";width:" + ("3px")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right")  + ";}.GH5EYDXDJO .GH5EYDXDNO{height:" + ((BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getSafeUri().asString() + "\") -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getLeft() + "px -" + (BlueButtonGroupAppearance_BlueButtonGroupTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topNoHeadBorder()).getTop() + "px  repeat-x")  + ";height:") + (("2px")  + ";width:" + ("auto")  + ";}"));
      }
      public java.lang.String bottom(){
        return "GH5EYDXDBO";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDCO";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDDO";
      }
      public java.lang.String content(){
        return "GH5EYDXDEO";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDFO";
      }
      public java.lang.String focus(){
        return "GH5EYDXDGO";
      }
      public java.lang.String frame(){
        return "GH5EYDXDHO";
      }
      public java.lang.String left(){
        return "GH5EYDXDIO";
      }
      public java.lang.String noheader(){
        return "GH5EYDXDJO";
      }
      public java.lang.String over(){
        return "GH5EYDXDKO";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDLO";
      }
      public java.lang.String right(){
        return "GH5EYDXDMO";
      }
      public java.lang.String top(){
        return "GH5EYDXDNO";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDOO";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDPO";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "26370D87296E533C9E4B2750E15B154E.cache.png";
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "8A174676742869A2669842C52B5CE90E.cache.png";
  private static final java.lang.String bundledImage_Both0 = GWT.getModuleBaseURL() + "B61DC9E92ADE1EA8473778A930D041E5.cache.png";
  private static final java.lang.String bundledImage_Both1 = GWT.getModuleBaseURL() + "B168A93B83B52B6BF736F7CD49924666.cache.png";
  private static final java.lang.String bundledImage_Vertical = GWT.getModuleBaseURL() + "FCA840A6F0C96A344251F525E2E84F2E.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "A7A43B81928BD0276E9F4D3144BB442E.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "ADB30B874E19BEF6B1FF3C04CF59A40A.cache.png";
  private static final java.lang.String externalImage0 = GWT.getModuleBaseURL() + "DC424DD572C0437842A2FC17955825A9.cache.png";
  private static final java.lang.String externalImage1 = GWT.getModuleBaseURL() + "50E70590F3183DAEE210D0DD54F2F370.cache.gif";
  private static final java.lang.String externalImage2 = GWT.getModuleBaseURL() + "BC5B11EC6AD181449D4DCA9D9AAEF282.cache.gif";
  private static final java.lang.String externalImage3 = GWT.getModuleBaseURL() + "18E4405964EE105D17E3818DB59279FE.cache.png";
  private static final java.lang.String externalImage4 = GWT.getModuleBaseURL() + "A4CB34068AD9928C9FA711FEA4248339.cache.png";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource backgroundOverBorder;
  private static com.google.gwt.resources.client.ImageResource backgroundPressedBorder;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource leftOverBorder;
  private static com.google.gwt.resources.client.ImageResource leftPressedBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topNoHeadBorder;
  private static com.google.gwt.resources.client.ImageResource topOverBorder;
  private static com.google.gwt.resources.client.ImageResource topPressedBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.button.ButtonGroupBaseAppearance.ButtonGroupTableFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      backgroundOverBorder(), 
      backgroundPressedBorder(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      leftOverBorder(), 
      leftPressedBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topNoHeadBorder(), 
      topOverBorder(), 
      topPressedBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("backgroundOverBorder", backgroundOverBorder());
        resourceMap.put("backgroundPressedBorder", backgroundPressedBorder());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("leftOverBorder", leftOverBorder());
        resourceMap.put("leftPressedBorder", leftPressedBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topNoHeadBorder", topNoHeadBorder());
        resourceMap.put("topOverBorder", topOverBorder());
        resourceMap.put("topPressedBorder", topPressedBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::background()();
      case 'backgroundOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::backgroundOverBorder()();
      case 'backgroundPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::backgroundPressedBorder()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::leftBorder()();
      case 'leftOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::leftOverBorder()();
      case 'leftPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::leftPressedBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topLeftBorder()();
      case 'topNoHeadBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topNoHeadBorder()();
      case 'topOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::topOverBorder()();
      case 'topPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonGroupBaseTableFrameResources::topPressedBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.button.BlueButtonGroupAppearance.BlueButtonGroupTableFrameResources::style()();
    }
    return null;
  }-*/;
}
