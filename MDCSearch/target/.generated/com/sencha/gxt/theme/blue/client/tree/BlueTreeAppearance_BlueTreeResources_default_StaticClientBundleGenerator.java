package com.sencha.gxt.theme.blue.client.tree;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueTreeAppearance_BlueTreeResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.tree.BlueTreeAppearance.BlueTreeResources {
  private static BlueTreeAppearance_BlueTreeResources_default_StaticClientBundleGenerator _instance0 = new BlueTreeAppearance_BlueTreeResources_default_StaticClientBundleGenerator();
  private void checkedInitializer() {
    checked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "checked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      96, 0, 16, 16, false, false
    );
  }
  private static class checkedInitializer {
    static {
      _instance0.checkedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return checked;
    }
  }
  public com.google.gwt.resources.client.ImageResource checked() {
    return checkedInitializer.get();
  }
  private void folderClosedInitializer() {
    folderClosed = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "folderClosed",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      80, 0, 16, 16, false, false
    );
  }
  private static class folderClosedInitializer {
    static {
      _instance0.folderClosedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return folderClosed;
    }
  }
  public com.google.gwt.resources.client.ImageResource folderClosed() {
    return folderClosedInitializer.get();
  }
  private void folderOpenedInitializer() {
    folderOpened = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "folderOpened",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      64, 0, 16, 16, false, false
    );
  }
  private static class folderOpenedInitializer {
    static {
      _instance0.folderOpenedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return folderOpened;
    }
  }
  public com.google.gwt.resources.client.ImageResource folderOpened() {
    return folderOpenedInitializer.get();
  }
  private void jointCollapsedIconInitializer() {
    jointCollapsedIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "jointCollapsedIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      48, 0, 16, 16, false, false
    );
  }
  private static class jointCollapsedIconInitializer {
    static {
      _instance0.jointCollapsedIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return jointCollapsedIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource jointCollapsedIcon() {
    return jointCollapsedIconInitializer.get();
  }
  private void jointExpandedIconInitializer() {
    jointExpandedIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "jointExpandedIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      32, 0, 16, 16, false, false
    );
  }
  private static class jointExpandedIconInitializer {
    static {
      _instance0.jointExpandedIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return jointExpandedIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource jointExpandedIcon() {
    return jointExpandedIconInitializer.get();
  }
  private void partialCheckedInitializer() {
    partialChecked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "partialChecked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      16, 0, 16, 16, false, false
    );
  }
  private static class partialCheckedInitializer {
    static {
      _instance0.partialCheckedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return partialChecked;
    }
  }
  public com.google.gwt.resources.client.ImageResource partialChecked() {
    return partialCheckedInitializer.get();
  }
  private void uncheckedInitializer() {
    unchecked = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "unchecked",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 16, 16, false, false
    );
  }
  private static class uncheckedInitializer {
    static {
      _instance0.uncheckedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return unchecked;
    }
  }
  public com.google.gwt.resources.client.ImageResource unchecked() {
    return uncheckedInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeBaseStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDIOB{cursor:" + ("default")  + ";-moz-outline:" + ("none")  + ";-moz-user-focus:" + ("none")  + ";outline:" + ("0"+ " " +"none")  + ";position:" + ("relative")  + ";padding-bottom:" + ("1px")  + ";}.GH5EYDXDEOB{cursor:" + ("default")  + ";}.GH5EYDXDBOB{white-space:" + ("nowrap")  + ";height:" + ("21px")  + ";}.GH5EYDXDONB{display:" + ("none")  + ";}.GH5EYDXDHOB{white-space:") + (("nowrap")  + ";line-height:" + ("11px")  + ";text-decoration:" + ("none")  + ";padding:" + ("0"+ " " +"3px"+ " " +"0"+ " " +"0")  + ";display:" + ("inline-block")  + ";vertical-align:" + ("top")  + ";margin-top:" + ("3px")  + ";font:" + ("normal"+ " " +"11px"+ " " +"arial"+ ","+ " " +"tahoma"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("black")  + ";}.GH5EYDXDPNB{background-image:" + ("none")  + " !important;background-color:" + ("#defadc") ) + (" !important;}.GH5EYDXDFOB{background-color:" + ("#eee")  + ";}.GH5EYDXDAOB{background-color:" + ("#defadc")  + ";}.GH5EYDXDGOB{background-color:" + ("#d9e8fb")  + " !important;}")) : ((".GH5EYDXDIOB{cursor:" + ("default")  + ";-moz-outline:" + ("none")  + ";-moz-user-focus:" + ("none")  + ";outline:" + ("0"+ " " +"none")  + ";position:" + ("relative")  + ";padding-bottom:" + ("1px")  + ";}.GH5EYDXDEOB{cursor:" + ("default")  + ";}.GH5EYDXDBOB{white-space:" + ("nowrap")  + ";height:" + ("21px")  + ";}.GH5EYDXDONB{display:" + ("none")  + ";}.GH5EYDXDHOB{white-space:") + (("nowrap")  + ";line-height:" + ("11px")  + ";text-decoration:" + ("none")  + ";padding:" + ("0"+ " " +"0"+ " " +"0"+ " " +"3px")  + ";display:" + ("inline-block")  + ";vertical-align:" + ("top")  + ";margin-top:" + ("3px")  + ";font:" + ("normal"+ " " +"11px"+ " " +"arial"+ ","+ " " +"tahoma"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("black")  + ";}.GH5EYDXDPNB{background-image:" + ("none")  + " !important;background-color:" + ("#defadc") ) + (" !important;}.GH5EYDXDFOB{background-color:" + ("#eee")  + ";}.GH5EYDXDAOB{background-color:" + ("#defadc")  + ";}.GH5EYDXDGOB{background-color:" + ("#d9e8fb")  + " !important;}"));
      }
      public java.lang.String check(){
        return "GH5EYDXDNNB";
      }
      public java.lang.String container(){
        return "GH5EYDXDONB";
      }
      public java.lang.String dragOver(){
        return "GH5EYDXDPNB";
      }
      public java.lang.String drop(){
        return "GH5EYDXDAOB";
      }
      public java.lang.String element(){
        return "GH5EYDXDBOB";
      }
      public java.lang.String icon(){
        return "GH5EYDXDCOB";
      }
      public java.lang.String joint(){
        return "GH5EYDXDDOB";
      }
      public java.lang.String node(){
        return "GH5EYDXDEOB";
      }
      public java.lang.String over(){
        return "GH5EYDXDFOB";
      }
      public java.lang.String selected(){
        return "GH5EYDXDGOB";
      }
      public java.lang.String text(){
        return "GH5EYDXDHOB";
      }
      public java.lang.String tree(){
        return "GH5EYDXDIOB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeBaseStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeBaseStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "CAA7F4CA0918D44551D9EBFEE75CF8EF.cache.png";
  private static com.google.gwt.resources.client.ImageResource checked;
  private static com.google.gwt.resources.client.ImageResource folderClosed;
  private static com.google.gwt.resources.client.ImageResource folderOpened;
  private static com.google.gwt.resources.client.ImageResource jointCollapsedIcon;
  private static com.google.gwt.resources.client.ImageResource jointExpandedIcon;
  private static com.google.gwt.resources.client.ImageResource partialChecked;
  private static com.google.gwt.resources.client.ImageResource unchecked;
  private static com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeBaseStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      checked(), 
      folderClosed(), 
      folderOpened(), 
      jointCollapsedIcon(), 
      jointExpandedIcon(), 
      partialChecked(), 
      unchecked(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("checked", checked());
        resourceMap.put("folderClosed", folderClosed());
        resourceMap.put("folderOpened", folderOpened());
        resourceMap.put("jointCollapsedIcon", jointCollapsedIcon());
        resourceMap.put("jointExpandedIcon", jointExpandedIcon());
        resourceMap.put("partialChecked", partialChecked());
        resourceMap.put("unchecked", unchecked());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'checked': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::checked()();
      case 'folderClosed': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::folderClosed()();
      case 'folderOpened': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::folderOpened()();
      case 'jointCollapsedIcon': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::jointCollapsedIcon()();
      case 'jointExpandedIcon': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::jointExpandedIcon()();
      case 'partialChecked': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::partialChecked()();
      case 'unchecked': return this.@com.sencha.gxt.theme.base.client.tree.TreeBaseAppearance.TreeResources::unchecked()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.tree.BlueTreeAppearance.BlueTreeResources::style()();
    }
    return null;
  }-*/;
}
