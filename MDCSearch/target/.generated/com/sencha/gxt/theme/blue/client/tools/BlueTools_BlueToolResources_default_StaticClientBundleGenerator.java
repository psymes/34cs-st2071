package com.sencha.gxt.theme.blue.client.tools;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class BlueTools_BlueToolResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources {
  private static BlueTools_BlueToolResources_default_StaticClientBundleGenerator _instance0 = new BlueTools_BlueToolResources_default_StaticClientBundleGenerator();
  private void closeIconInitializer() {
    closeIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "closeIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      705, 0, 15, 15, false, false
    );
  }
  private static class closeIconInitializer {
    static {
      _instance0.closeIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return closeIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource closeIcon() {
    return closeIconInitializer.get();
  }
  private void closeOverIconInitializer() {
    closeOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "closeOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      690, 0, 15, 15, false, false
    );
  }
  private static class closeOverIconInitializer {
    static {
      _instance0.closeOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return closeOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource closeOverIcon() {
    return closeOverIconInitializer.get();
  }
  private void collapseIconInitializer() {
    collapseIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "collapseIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      675, 0, 15, 15, false, false
    );
  }
  private static class collapseIconInitializer {
    static {
      _instance0.collapseIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return collapseIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource collapseIcon() {
    return collapseIconInitializer.get();
  }
  private void collapseOverIconInitializer() {
    collapseOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "collapseOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      660, 0, 15, 15, false, false
    );
  }
  private static class collapseOverIconInitializer {
    static {
      _instance0.collapseOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return collapseOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource collapseOverIcon() {
    return collapseOverIconInitializer.get();
  }
  private void doubleDownIconInitializer() {
    doubleDownIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleDownIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      645, 0, 15, 15, false, false
    );
  }
  private static class doubleDownIconInitializer {
    static {
      _instance0.doubleDownIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleDownIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleDownIcon() {
    return doubleDownIconInitializer.get();
  }
  private void doubleDownOverIconInitializer() {
    doubleDownOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleDownOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      630, 0, 15, 15, false, false
    );
  }
  private static class doubleDownOverIconInitializer {
    static {
      _instance0.doubleDownOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleDownOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleDownOverIcon() {
    return doubleDownOverIconInitializer.get();
  }
  private void doubleLeftIconInitializer() {
    doubleLeftIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleLeftIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      615, 0, 15, 15, false, false
    );
  }
  private static class doubleLeftIconInitializer {
    static {
      _instance0.doubleLeftIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleLeftIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleLeftIcon() {
    return doubleLeftIconInitializer.get();
  }
  private void doubleLeftOverIconInitializer() {
    doubleLeftOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleLeftOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      600, 0, 15, 15, false, false
    );
  }
  private static class doubleLeftOverIconInitializer {
    static {
      _instance0.doubleLeftOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleLeftOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleLeftOverIcon() {
    return doubleLeftOverIconInitializer.get();
  }
  private void doubleRightIconInitializer() {
    doubleRightIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleRightIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      585, 0, 15, 15, false, false
    );
  }
  private static class doubleRightIconInitializer {
    static {
      _instance0.doubleRightIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleRightIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleRightIcon() {
    return doubleRightIconInitializer.get();
  }
  private void doubleRightOverIconInitializer() {
    doubleRightOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleRightOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      570, 0, 15, 15, false, false
    );
  }
  private static class doubleRightOverIconInitializer {
    static {
      _instance0.doubleRightOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleRightOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleRightOverIcon() {
    return doubleRightOverIconInitializer.get();
  }
  private void doubleUpIconInitializer() {
    doubleUpIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleUpIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      555, 0, 15, 15, false, false
    );
  }
  private static class doubleUpIconInitializer {
    static {
      _instance0.doubleUpIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleUpIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleUpIcon() {
    return doubleUpIconInitializer.get();
  }
  private void doubleUpOverIconInitializer() {
    doubleUpOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "doubleUpOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      540, 0, 15, 15, false, false
    );
  }
  private static class doubleUpOverIconInitializer {
    static {
      _instance0.doubleUpOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return doubleUpOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource doubleUpOverIcon() {
    return doubleUpOverIconInitializer.get();
  }
  private void downIconInitializer() {
    downIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "downIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      525, 0, 15, 15, false, false
    );
  }
  private static class downIconInitializer {
    static {
      _instance0.downIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return downIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource downIcon() {
    return downIconInitializer.get();
  }
  private void downOverIconInitializer() {
    downOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "downOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      510, 0, 15, 15, false, false
    );
  }
  private static class downOverIconInitializer {
    static {
      _instance0.downOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return downOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource downOverIcon() {
    return downOverIconInitializer.get();
  }
  private void expandIconInitializer() {
    expandIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "expandIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      495, 0, 15, 15, false, false
    );
  }
  private static class expandIconInitializer {
    static {
      _instance0.expandIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return expandIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource expandIcon() {
    return expandIconInitializer.get();
  }
  private void expandOverIconInitializer() {
    expandOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "expandOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      480, 0, 15, 15, false, false
    );
  }
  private static class expandOverIconInitializer {
    static {
      _instance0.expandOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return expandOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource expandOverIcon() {
    return expandOverIconInitializer.get();
  }
  private void gearIconInitializer() {
    gearIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "gearIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      465, 0, 15, 15, false, false
    );
  }
  private static class gearIconInitializer {
    static {
      _instance0.gearIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return gearIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource gearIcon() {
    return gearIconInitializer.get();
  }
  private void gearOverIconInitializer() {
    gearOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "gearOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      450, 0, 15, 15, false, false
    );
  }
  private static class gearOverIconInitializer {
    static {
      _instance0.gearOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return gearOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource gearOverIcon() {
    return gearOverIconInitializer.get();
  }
  private void leftIconInitializer() {
    leftIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      435, 0, 15, 15, false, false
    );
  }
  private static class leftIconInitializer {
    static {
      _instance0.leftIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftIcon() {
    return leftIconInitializer.get();
  }
  private void leftOverIconInitializer() {
    leftOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      420, 0, 15, 15, false, false
    );
  }
  private static class leftOverIconInitializer {
    static {
      _instance0.leftOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftOverIcon() {
    return leftOverIconInitializer.get();
  }
  private void maximizeIconInitializer() {
    maximizeIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "maximizeIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      405, 0, 15, 15, false, false
    );
  }
  private static class maximizeIconInitializer {
    static {
      _instance0.maximizeIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return maximizeIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource maximizeIcon() {
    return maximizeIconInitializer.get();
  }
  private void maximizeOverIconInitializer() {
    maximizeOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "maximizeOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      390, 0, 15, 15, false, false
    );
  }
  private static class maximizeOverIconInitializer {
    static {
      _instance0.maximizeOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return maximizeOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource maximizeOverIcon() {
    return maximizeOverIconInitializer.get();
  }
  private void minimizeIconInitializer() {
    minimizeIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "minimizeIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      375, 0, 15, 15, false, false
    );
  }
  private static class minimizeIconInitializer {
    static {
      _instance0.minimizeIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return minimizeIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource minimizeIcon() {
    return minimizeIconInitializer.get();
  }
  private void minimizeOverIconInitializer() {
    minimizeOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "minimizeOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      360, 0, 15, 15, false, false
    );
  }
  private static class minimizeOverIconInitializer {
    static {
      _instance0.minimizeOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return minimizeOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource minimizeOverIcon() {
    return minimizeOverIconInitializer.get();
  }
  private void minusIconInitializer() {
    minusIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "minusIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      345, 0, 15, 15, false, false
    );
  }
  private static class minusIconInitializer {
    static {
      _instance0.minusIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return minusIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource minusIcon() {
    return minusIconInitializer.get();
  }
  private void minusOverIconInitializer() {
    minusOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "minusOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      330, 0, 15, 15, false, false
    );
  }
  private static class minusOverIconInitializer {
    static {
      _instance0.minusOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return minusOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource minusOverIcon() {
    return minusOverIconInitializer.get();
  }
  private void pinIconInitializer() {
    pinIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "pinIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      315, 0, 15, 15, false, false
    );
  }
  private static class pinIconInitializer {
    static {
      _instance0.pinIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return pinIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource pinIcon() {
    return pinIconInitializer.get();
  }
  private void pinOverIconInitializer() {
    pinOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "pinOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      300, 0, 15, 15, false, false
    );
  }
  private static class pinOverIconInitializer {
    static {
      _instance0.pinOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return pinOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource pinOverIcon() {
    return pinOverIconInitializer.get();
  }
  private void plusIconInitializer() {
    plusIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "plusIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      285, 0, 15, 15, false, false
    );
  }
  private static class plusIconInitializer {
    static {
      _instance0.plusIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return plusIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource plusIcon() {
    return plusIconInitializer.get();
  }
  private void plusOverIconInitializer() {
    plusOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "plusOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      270, 0, 15, 15, false, false
    );
  }
  private static class plusOverIconInitializer {
    static {
      _instance0.plusOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return plusOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource plusOverIcon() {
    return plusOverIconInitializer.get();
  }
  private void printIconInitializer() {
    printIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "printIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      255, 0, 15, 15, false, false
    );
  }
  private static class printIconInitializer {
    static {
      _instance0.printIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return printIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource printIcon() {
    return printIconInitializer.get();
  }
  private void printOverIconInitializer() {
    printOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "printOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      240, 0, 15, 15, false, false
    );
  }
  private static class printOverIconInitializer {
    static {
      _instance0.printOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return printOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource printOverIcon() {
    return printOverIconInitializer.get();
  }
  private void questionIconInitializer() {
    questionIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "questionIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      225, 0, 15, 15, false, false
    );
  }
  private static class questionIconInitializer {
    static {
      _instance0.questionIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return questionIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource questionIcon() {
    return questionIconInitializer.get();
  }
  private void questionOverIconInitializer() {
    questionOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "questionOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      210, 0, 15, 15, false, false
    );
  }
  private static class questionOverIconInitializer {
    static {
      _instance0.questionOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return questionOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource questionOverIcon() {
    return questionOverIconInitializer.get();
  }
  private void refreshIconInitializer() {
    refreshIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "refreshIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      195, 0, 15, 15, false, false
    );
  }
  private static class refreshIconInitializer {
    static {
      _instance0.refreshIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return refreshIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource refreshIcon() {
    return refreshIconInitializer.get();
  }
  private void refreshOverIconInitializer() {
    refreshOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "refreshOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      180, 0, 15, 15, false, false
    );
  }
  private static class refreshOverIconInitializer {
    static {
      _instance0.refreshOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return refreshOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource refreshOverIcon() {
    return refreshOverIconInitializer.get();
  }
  private void restoreIconInitializer() {
    restoreIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "restoreIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      165, 0, 15, 15, false, false
    );
  }
  private static class restoreIconInitializer {
    static {
      _instance0.restoreIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return restoreIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource restoreIcon() {
    return restoreIconInitializer.get();
  }
  private void restoreOverIconInitializer() {
    restoreOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "restoreOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      150, 0, 15, 15, false, false
    );
  }
  private static class restoreOverIconInitializer {
    static {
      _instance0.restoreOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return restoreOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource restoreOverIcon() {
    return restoreOverIconInitializer.get();
  }
  private void rightIconInitializer() {
    rightIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      135, 0, 15, 15, false, false
    );
  }
  private static class rightIconInitializer {
    static {
      _instance0.rightIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightIcon() {
    return rightIconInitializer.get();
  }
  private void rightOverIconInitializer() {
    rightOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      120, 0, 15, 15, false, false
    );
  }
  private static class rightOverIconInitializer {
    static {
      _instance0.rightOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightOverIcon() {
    return rightOverIconInitializer.get();
  }
  private void saveIconInitializer() {
    saveIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "saveIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      105, 0, 15, 15, false, false
    );
  }
  private static class saveIconInitializer {
    static {
      _instance0.saveIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return saveIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource saveIcon() {
    return saveIconInitializer.get();
  }
  private void saveOverIconInitializer() {
    saveOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "saveOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      90, 0, 15, 15, false, false
    );
  }
  private static class saveOverIconInitializer {
    static {
      _instance0.saveOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return saveOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource saveOverIcon() {
    return saveOverIconInitializer.get();
  }
  private void searchIconInitializer() {
    searchIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "searchIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      75, 0, 15, 15, false, false
    );
  }
  private static class searchIconInitializer {
    static {
      _instance0.searchIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return searchIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource searchIcon() {
    return searchIconInitializer.get();
  }
  private void searchOverIconInitializer() {
    searchOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "searchOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      60, 0, 15, 15, false, false
    );
  }
  private static class searchOverIconInitializer {
    static {
      _instance0.searchOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return searchOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource searchOverIcon() {
    return searchOverIconInitializer.get();
  }
  private void unpinIconInitializer() {
    unpinIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "unpinIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      45, 0, 15, 15, false, false
    );
  }
  private static class unpinIconInitializer {
    static {
      _instance0.unpinIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return unpinIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource unpinIcon() {
    return unpinIconInitializer.get();
  }
  private void unpinOverIconInitializer() {
    unpinOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "unpinOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      30, 0, 15, 15, false, false
    );
  }
  private static class unpinOverIconInitializer {
    static {
      _instance0.unpinOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return unpinOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource unpinOverIcon() {
    return unpinOverIconInitializer.get();
  }
  private void upIconInitializer() {
    upIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "upIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      15, 0, 15, 15, false, false
    );
  }
  private static class upIconInitializer {
    static {
      _instance0.upIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return upIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource upIcon() {
    return upIconInitializer.get();
  }
  private void upOverIconInitializer() {
    upOverIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "upOverIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 15, 15, false, false
    );
  }
  private static class upOverIconInitializer {
    static {
      _instance0.upOverIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return upOverIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource upOverIcon() {
    return upOverIconInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.widget.core.client.button.Tools.ToolStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GH5EYDXDFEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDGEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.closeOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDHEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseIcon()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.collapseOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDJEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDKEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownOverIcon()).getHeight() + "px") ) + (";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleDownOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDLEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDMEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleLeftOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDNEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDOEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleRightOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDPEC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpIcon()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDAFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.doubleUpOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDBFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCFC{height:") + (((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.downOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDDFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDEFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.expandOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDFFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDGFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.gearOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDHFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftIcon()).getHeight() + "px")  + ";width:") + (((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.leftOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDJFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeIcon()).getTop() + "px  no-repeat") ) + (";}.GH5EYDXDKFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.maximizeOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDLFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDMFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeOverIcon()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minimizeOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDNFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDOFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.minusOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDPFC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinIcon()).getHeight() + "px") ) + (";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDAGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.pinOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDBHC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCHC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.unpinOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDBGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusOverIcon()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.plusOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDDGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDEGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.printOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDFGC{height:") + (((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDGGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.questionOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDHGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshIcon()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.refreshOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDJGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDKGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreOverIcon()).getHeight() + "px")  + ";width:") + (((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.restoreOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDLGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDMGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.rightOverIcon()).getTop() + "px  no-repeat") ) + (";}.GH5EYDXDNGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDOGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.saveOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDPGC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchIcon()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDAHC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchOverIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.searchOverIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDDHC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upIcon()).getHeight() + "px")  + ";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upIcon()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDEHC{height:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upOverIcon()).getHeight() + "px") ) + (";width:" + ((BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upOverIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upOverIcon()).getSafeUri().asString() + "\") -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upOverIcon()).getLeft() + "px -" + (BlueTools_BlueToolResources_default_StaticClientBundleGenerator.this.upOverIcon()).getTop() + "px  no-repeat")  + ";}");
      }
      public java.lang.String close(){
        return "GH5EYDXDFEC";
      }
      public java.lang.String closeOver(){
        return "GH5EYDXDGEC";
      }
      public java.lang.String collapse(){
        return "GH5EYDXDHEC";
      }
      public java.lang.String collapseOver(){
        return "GH5EYDXDIEC";
      }
      public java.lang.String doubleDown(){
        return "GH5EYDXDJEC";
      }
      public java.lang.String doubleDownOver(){
        return "GH5EYDXDKEC";
      }
      public java.lang.String doubleLeft(){
        return "GH5EYDXDLEC";
      }
      public java.lang.String doubleLeftOver(){
        return "GH5EYDXDMEC";
      }
      public java.lang.String doubleRight(){
        return "GH5EYDXDNEC";
      }
      public java.lang.String doubleRightOver(){
        return "GH5EYDXDOEC";
      }
      public java.lang.String doubleUp(){
        return "GH5EYDXDPEC";
      }
      public java.lang.String doubleUpOver(){
        return "GH5EYDXDAFC";
      }
      public java.lang.String down(){
        return "GH5EYDXDBFC";
      }
      public java.lang.String downOver(){
        return "GH5EYDXDCFC";
      }
      public java.lang.String expand(){
        return "GH5EYDXDDFC";
      }
      public java.lang.String expandOver(){
        return "GH5EYDXDEFC";
      }
      public java.lang.String gear(){
        return "GH5EYDXDFFC";
      }
      public java.lang.String gearOver(){
        return "GH5EYDXDGFC";
      }
      public java.lang.String left(){
        return "GH5EYDXDHFC";
      }
      public java.lang.String leftOver(){
        return "GH5EYDXDIFC";
      }
      public java.lang.String maximize(){
        return "GH5EYDXDJFC";
      }
      public java.lang.String maximizeOver(){
        return "GH5EYDXDKFC";
      }
      public java.lang.String minimize(){
        return "GH5EYDXDLFC";
      }
      public java.lang.String minimizeOver(){
        return "GH5EYDXDMFC";
      }
      public java.lang.String minus(){
        return "GH5EYDXDNFC";
      }
      public java.lang.String minusOver(){
        return "GH5EYDXDOFC";
      }
      public java.lang.String pin(){
        return "GH5EYDXDPFC";
      }
      public java.lang.String pinOver(){
        return "GH5EYDXDAGC";
      }
      public java.lang.String plus(){
        return "GH5EYDXDBGC";
      }
      public java.lang.String plusOver(){
        return "GH5EYDXDCGC";
      }
      public java.lang.String print(){
        return "GH5EYDXDDGC";
      }
      public java.lang.String printOver(){
        return "GH5EYDXDEGC";
      }
      public java.lang.String question(){
        return "GH5EYDXDFGC";
      }
      public java.lang.String questionOver(){
        return "GH5EYDXDGGC";
      }
      public java.lang.String refresh(){
        return "GH5EYDXDHGC";
      }
      public java.lang.String refreshOver(){
        return "GH5EYDXDIGC";
      }
      public java.lang.String restore(){
        return "GH5EYDXDJGC";
      }
      public java.lang.String restoreOver(){
        return "GH5EYDXDKGC";
      }
      public java.lang.String right(){
        return "GH5EYDXDLGC";
      }
      public java.lang.String rightOver(){
        return "GH5EYDXDMGC";
      }
      public java.lang.String save(){
        return "GH5EYDXDNGC";
      }
      public java.lang.String saveOver(){
        return "GH5EYDXDOGC";
      }
      public java.lang.String search(){
        return "GH5EYDXDPGC";
      }
      public java.lang.String searchOver(){
        return "GH5EYDXDAHC";
      }
      public java.lang.String unpin(){
        return "GH5EYDXDBHC";
      }
      public java.lang.String unpinOver(){
        return "GH5EYDXDCHC";
      }
      public java.lang.String up(){
        return "GH5EYDXDDHC";
      }
      public java.lang.String upOver(){
        return "GH5EYDXDEHC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.widget.core.client.button.Tools.ToolStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.widget.core.client.button.Tools.ToolStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "BC917274FAFEF3CFA9905F4D5C28E1C8.cache.png";
  private static com.google.gwt.resources.client.ImageResource closeIcon;
  private static com.google.gwt.resources.client.ImageResource closeOverIcon;
  private static com.google.gwt.resources.client.ImageResource collapseIcon;
  private static com.google.gwt.resources.client.ImageResource collapseOverIcon;
  private static com.google.gwt.resources.client.ImageResource doubleDownIcon;
  private static com.google.gwt.resources.client.ImageResource doubleDownOverIcon;
  private static com.google.gwt.resources.client.ImageResource doubleLeftIcon;
  private static com.google.gwt.resources.client.ImageResource doubleLeftOverIcon;
  private static com.google.gwt.resources.client.ImageResource doubleRightIcon;
  private static com.google.gwt.resources.client.ImageResource doubleRightOverIcon;
  private static com.google.gwt.resources.client.ImageResource doubleUpIcon;
  private static com.google.gwt.resources.client.ImageResource doubleUpOverIcon;
  private static com.google.gwt.resources.client.ImageResource downIcon;
  private static com.google.gwt.resources.client.ImageResource downOverIcon;
  private static com.google.gwt.resources.client.ImageResource expandIcon;
  private static com.google.gwt.resources.client.ImageResource expandOverIcon;
  private static com.google.gwt.resources.client.ImageResource gearIcon;
  private static com.google.gwt.resources.client.ImageResource gearOverIcon;
  private static com.google.gwt.resources.client.ImageResource leftIcon;
  private static com.google.gwt.resources.client.ImageResource leftOverIcon;
  private static com.google.gwt.resources.client.ImageResource maximizeIcon;
  private static com.google.gwt.resources.client.ImageResource maximizeOverIcon;
  private static com.google.gwt.resources.client.ImageResource minimizeIcon;
  private static com.google.gwt.resources.client.ImageResource minimizeOverIcon;
  private static com.google.gwt.resources.client.ImageResource minusIcon;
  private static com.google.gwt.resources.client.ImageResource minusOverIcon;
  private static com.google.gwt.resources.client.ImageResource pinIcon;
  private static com.google.gwt.resources.client.ImageResource pinOverIcon;
  private static com.google.gwt.resources.client.ImageResource plusIcon;
  private static com.google.gwt.resources.client.ImageResource plusOverIcon;
  private static com.google.gwt.resources.client.ImageResource printIcon;
  private static com.google.gwt.resources.client.ImageResource printOverIcon;
  private static com.google.gwt.resources.client.ImageResource questionIcon;
  private static com.google.gwt.resources.client.ImageResource questionOverIcon;
  private static com.google.gwt.resources.client.ImageResource refreshIcon;
  private static com.google.gwt.resources.client.ImageResource refreshOverIcon;
  private static com.google.gwt.resources.client.ImageResource restoreIcon;
  private static com.google.gwt.resources.client.ImageResource restoreOverIcon;
  private static com.google.gwt.resources.client.ImageResource rightIcon;
  private static com.google.gwt.resources.client.ImageResource rightOverIcon;
  private static com.google.gwt.resources.client.ImageResource saveIcon;
  private static com.google.gwt.resources.client.ImageResource saveOverIcon;
  private static com.google.gwt.resources.client.ImageResource searchIcon;
  private static com.google.gwt.resources.client.ImageResource searchOverIcon;
  private static com.google.gwt.resources.client.ImageResource unpinIcon;
  private static com.google.gwt.resources.client.ImageResource unpinOverIcon;
  private static com.google.gwt.resources.client.ImageResource upIcon;
  private static com.google.gwt.resources.client.ImageResource upOverIcon;
  private static com.sencha.gxt.widget.core.client.button.Tools.ToolStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      closeIcon(), 
      closeOverIcon(), 
      collapseIcon(), 
      collapseOverIcon(), 
      doubleDownIcon(), 
      doubleDownOverIcon(), 
      doubleLeftIcon(), 
      doubleLeftOverIcon(), 
      doubleRightIcon(), 
      doubleRightOverIcon(), 
      doubleUpIcon(), 
      doubleUpOverIcon(), 
      downIcon(), 
      downOverIcon(), 
      expandIcon(), 
      expandOverIcon(), 
      gearIcon(), 
      gearOverIcon(), 
      leftIcon(), 
      leftOverIcon(), 
      maximizeIcon(), 
      maximizeOverIcon(), 
      minimizeIcon(), 
      minimizeOverIcon(), 
      minusIcon(), 
      minusOverIcon(), 
      pinIcon(), 
      pinOverIcon(), 
      plusIcon(), 
      plusOverIcon(), 
      printIcon(), 
      printOverIcon(), 
      questionIcon(), 
      questionOverIcon(), 
      refreshIcon(), 
      refreshOverIcon(), 
      restoreIcon(), 
      restoreOverIcon(), 
      rightIcon(), 
      rightOverIcon(), 
      saveIcon(), 
      saveOverIcon(), 
      searchIcon(), 
      searchOverIcon(), 
      unpinIcon(), 
      unpinOverIcon(), 
      upIcon(), 
      upOverIcon(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("closeIcon", closeIcon());
        resourceMap.put("closeOverIcon", closeOverIcon());
        resourceMap.put("collapseIcon", collapseIcon());
        resourceMap.put("collapseOverIcon", collapseOverIcon());
        resourceMap.put("doubleDownIcon", doubleDownIcon());
        resourceMap.put("doubleDownOverIcon", doubleDownOverIcon());
        resourceMap.put("doubleLeftIcon", doubleLeftIcon());
        resourceMap.put("doubleLeftOverIcon", doubleLeftOverIcon());
        resourceMap.put("doubleRightIcon", doubleRightIcon());
        resourceMap.put("doubleRightOverIcon", doubleRightOverIcon());
        resourceMap.put("doubleUpIcon", doubleUpIcon());
        resourceMap.put("doubleUpOverIcon", doubleUpOverIcon());
        resourceMap.put("downIcon", downIcon());
        resourceMap.put("downOverIcon", downOverIcon());
        resourceMap.put("expandIcon", expandIcon());
        resourceMap.put("expandOverIcon", expandOverIcon());
        resourceMap.put("gearIcon", gearIcon());
        resourceMap.put("gearOverIcon", gearOverIcon());
        resourceMap.put("leftIcon", leftIcon());
        resourceMap.put("leftOverIcon", leftOverIcon());
        resourceMap.put("maximizeIcon", maximizeIcon());
        resourceMap.put("maximizeOverIcon", maximizeOverIcon());
        resourceMap.put("minimizeIcon", minimizeIcon());
        resourceMap.put("minimizeOverIcon", minimizeOverIcon());
        resourceMap.put("minusIcon", minusIcon());
        resourceMap.put("minusOverIcon", minusOverIcon());
        resourceMap.put("pinIcon", pinIcon());
        resourceMap.put("pinOverIcon", pinOverIcon());
        resourceMap.put("plusIcon", plusIcon());
        resourceMap.put("plusOverIcon", plusOverIcon());
        resourceMap.put("printIcon", printIcon());
        resourceMap.put("printOverIcon", printOverIcon());
        resourceMap.put("questionIcon", questionIcon());
        resourceMap.put("questionOverIcon", questionOverIcon());
        resourceMap.put("refreshIcon", refreshIcon());
        resourceMap.put("refreshOverIcon", refreshOverIcon());
        resourceMap.put("restoreIcon", restoreIcon());
        resourceMap.put("restoreOverIcon", restoreOverIcon());
        resourceMap.put("rightIcon", rightIcon());
        resourceMap.put("rightOverIcon", rightOverIcon());
        resourceMap.put("saveIcon", saveIcon());
        resourceMap.put("saveOverIcon", saveOverIcon());
        resourceMap.put("searchIcon", searchIcon());
        resourceMap.put("searchOverIcon", searchOverIcon());
        resourceMap.put("unpinIcon", unpinIcon());
        resourceMap.put("unpinOverIcon", unpinOverIcon());
        resourceMap.put("upIcon", upIcon());
        resourceMap.put("upOverIcon", upOverIcon());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'closeIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::closeIcon()();
      case 'closeOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::closeOverIcon()();
      case 'collapseIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::collapseIcon()();
      case 'collapseOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::collapseOverIcon()();
      case 'doubleDownIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleDownIcon()();
      case 'doubleDownOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleDownOverIcon()();
      case 'doubleLeftIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleLeftIcon()();
      case 'doubleLeftOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleLeftOverIcon()();
      case 'doubleRightIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleRightIcon()();
      case 'doubleRightOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleRightOverIcon()();
      case 'doubleUpIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleUpIcon()();
      case 'doubleUpOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::doubleUpOverIcon()();
      case 'downIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::downIcon()();
      case 'downOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::downOverIcon()();
      case 'expandIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::expandIcon()();
      case 'expandOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::expandOverIcon()();
      case 'gearIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::gearIcon()();
      case 'gearOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::gearOverIcon()();
      case 'leftIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::leftIcon()();
      case 'leftOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::leftOverIcon()();
      case 'maximizeIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::maximizeIcon()();
      case 'maximizeOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::maximizeOverIcon()();
      case 'minimizeIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::minimizeIcon()();
      case 'minimizeOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::minimizeOverIcon()();
      case 'minusIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::minusIcon()();
      case 'minusOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::minusOverIcon()();
      case 'pinIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::pinIcon()();
      case 'pinOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::pinOverIcon()();
      case 'plusIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::plusIcon()();
      case 'plusOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::plusOverIcon()();
      case 'printIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::printIcon()();
      case 'printOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::printOverIcon()();
      case 'questionIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::questionIcon()();
      case 'questionOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::questionOverIcon()();
      case 'refreshIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::refreshIcon()();
      case 'refreshOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::refreshOverIcon()();
      case 'restoreIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::restoreIcon()();
      case 'restoreOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::restoreOverIcon()();
      case 'rightIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::rightIcon()();
      case 'rightOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::rightOverIcon()();
      case 'saveIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::saveIcon()();
      case 'saveOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::saveOverIcon()();
      case 'searchIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::searchIcon()();
      case 'searchOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::searchOverIcon()();
      case 'unpinIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::unpinIcon()();
      case 'unpinOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::unpinOverIcon()();
      case 'upIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::upIcon()();
      case 'upOverIcon': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::upOverIcon()();
      case 'style': return this.@com.sencha.gxt.theme.blue.client.tools.BlueTools.BlueToolResources::style()();
    }
    return null;
  }-*/;
}
