package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources {
  private static ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator _instance0 = new ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator();
  private void anchorBottomInitializer() {
    anchorBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      9, 0, 9, 10, false, false
    );
  }
  private static class anchorBottomInitializer {
    static {
      _instance0.anchorBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorBottom() {
    return anchorBottomInitializer.get();
  }
  private void anchorLeftInitializer() {
    anchorLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      28, 0, 10, 9, false, false
    );
  }
  private static class anchorLeftInitializer {
    static {
      _instance0.anchorLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorLeft() {
    return anchorLeftInitializer.get();
  }
  private void anchorRightInitializer() {
    anchorRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      18, 0, 10, 9, false, false
    );
  }
  private static class anchorRightInitializer {
    static {
      _instance0.anchorRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorRight() {
    return anchorRightInitializer.get();
  }
  private void anchorTopInitializer() {
    anchorTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 9, 10, false, false
    );
  }
  private static class anchorTopInitializer {
    static {
      _instance0.anchorTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorTop() {
    return anchorTopInitializer.get();
  }
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 3, 3, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both0),
      0, 0, 1, 6, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both1),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both2),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      0, 0, 6, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both3),
      0, 0, 6, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 1, 6, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      38, 0, 6, 6, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both4),
      0, 0, 6, 6, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipNestedDivFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDES{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDDS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDKS{height:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-right:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDJS{height:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDLS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat") ) + (";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-left:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDBS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-right:") + ((bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDAS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px") ) + (";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDCS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"bottom")  + ";padding-left:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";line-height:") + (("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDFS{width:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDIS{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("left"+ " " +"0")  + ";padding-left:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDDS{background-color:" + ("white")  + ";}.GH5EYDXDBS{background-repeat:" + ("no-repeat")  + ";}")) : ((".GH5EYDXDES{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDDS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDKS{height:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-left:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDJS{height:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDLS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat") ) + (";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-right:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDBS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-left:") + ((bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDAS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px") ) + (";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDCS{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"bottom")  + ";padding-right:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";line-height:") + (("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDFS{width:" + ((ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDIS{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("right"+ " " +"0")  + ";padding-right:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDDS{background-color:" + ("white")  + ";}.GH5EYDXDBS{background-repeat:" + ("no-repeat")  + ";}"));
      }
      public java.lang.String bodyWrap(){
        return "GH5EYDXDPR";
      }
      public java.lang.String bottom(){
        return "GH5EYDXDAS";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDBS";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDCS";
      }
      public java.lang.String content(){
        return "GH5EYDXDDS";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDES";
      }
      public java.lang.String left(){
        return "GH5EYDXDFS";
      }
      public java.lang.String over(){
        return "GH5EYDXDGS";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDHS";
      }
      public java.lang.String right(){
        return "GH5EYDXDIS";
      }
      public java.lang.String top(){
        return "GH5EYDXDJS";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDKS";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDLS";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipNestedDivFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipNestedDivFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "8B01419F25408CE2011419BFC8D36C79.cache.png";
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "26370D87296E533C9E4B2750E15B154E.cache.png";
  private static final java.lang.String bundledImage_Both0 = GWT.getModuleBaseURL() + "398DA90014493B647E365575B763CC65.cache.png";
  private static final java.lang.String bundledImage_Both1 = GWT.getModuleBaseURL() + "5C3287D45AE9AC357973442BD1C79B50.cache.png";
  private static final java.lang.String bundledImage_Both2 = GWT.getModuleBaseURL() + "E056386B243EFB2074B795AA328DF3D9.cache.png";
  private static final java.lang.String bundledImage_Vertical = GWT.getModuleBaseURL() + "EEF9A65C13BD88F5AF90919184F09B4C.cache.png";
  private static final java.lang.String bundledImage_Both3 = GWT.getModuleBaseURL() + "9A138DD7570675FA0846C6CE6DD6EC9B.cache.png";
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "AEDBD12CE46FFD669E49E3B1C5830415.cache.png";
  private static final java.lang.String bundledImage_Both4 = GWT.getModuleBaseURL() + "B4E92D91F74ED97D536870F8D8B3B461.cache.png";
  private static com.google.gwt.resources.client.ImageResource anchorBottom;
  private static com.google.gwt.resources.client.ImageResource anchorLeft;
  private static com.google.gwt.resources.client.ImageResource anchorRight;
  private static com.google.gwt.resources.client.ImageResource anchorTop;
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipNestedDivFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      anchorBottom(), 
      anchorLeft(), 
      anchorRight(), 
      anchorTop(), 
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("anchorBottom", anchorBottom());
        resourceMap.put("anchorLeft", anchorLeft());
        resourceMap.put("anchorRight", anchorRight());
        resourceMap.put("anchorTop", anchorTop());
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'anchorBottom': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorBottom()();
      case 'anchorLeft': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorLeft()();
      case 'anchorRight': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorRight()();
      case 'anchorTop': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorTop()();
      case 'background': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipFrameResources::style()();
    }
    return null;
  }-*/;
}
