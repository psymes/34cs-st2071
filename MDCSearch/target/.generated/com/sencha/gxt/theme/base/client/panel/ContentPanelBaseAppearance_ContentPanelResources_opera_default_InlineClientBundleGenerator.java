package com.sencha.gxt.theme.base.client.panel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ContentPanelBaseAppearance_ContentPanelResources_opera_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelResources {
  private static ContentPanelBaseAppearance_ContentPanelResources_opera_default_InlineClientBundleGenerator _instance0 = new ContentPanelBaseAppearance_ContentPanelResources_opera_default_InlineClientBundleGenerator();
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDGGB{border-style:" + ("solid")  + ";border-width:" + ("0")  + ";outline:" + ("0"+ " " +"none")  + ";}.GH5EYDXDFGB{border-top-width:" + ("1px")  + ";}.GH5EYDXDEGB{position:" + ("relative")  + ";}.GH5EYDXDBGB{border-bottom:" + ("1px"+ " " +"solid")  + ";border-right:" + ("1px"+ " " +"solid")  + ";border-left:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";position:") + (("relative")  + ";}.GH5EYDXDDGB{position:" + ("relative")  + ";}")) : ((".GH5EYDXDGGB{border-style:" + ("solid")  + ";border-width:" + ("0")  + ";outline:" + ("0"+ " " +"none")  + ";}.GH5EYDXDFGB{border-top-width:" + ("1px")  + ";}.GH5EYDXDEGB{position:" + ("relative")  + ";}.GH5EYDXDBGB{border-bottom:" + ("1px"+ " " +"solid")  + ";border-left:" + ("1px"+ " " +"solid")  + ";border-right:" + ("1px"+ " " +"solid")  + ";border-top:" + ("0"+ " " +"none")  + ";overflow:" + ("hidden")  + ";position:") + (("relative")  + ";}.GH5EYDXDDGB{position:" + ("relative")  + ";}"));
      }
      public java.lang.String body(){
        return "GH5EYDXDBGB";
      }
      public java.lang.String bodyWrap(){
        return "GH5EYDXDCGB";
      }
      public java.lang.String footer(){
        return "GH5EYDXDDGB";
      }
      public java.lang.String header(){
        return "GH5EYDXDEGB";
      }
      public java.lang.String noHeader(){
        return "GH5EYDXDFGB";
      }
      public java.lang.String panel(){
        return "GH5EYDXDGGB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@com.sencha.gxt.theme.base.client.panel.ContentPanelBaseAppearance.ContentPanelResources::style()();
    }
    return null;
  }-*/;
}
