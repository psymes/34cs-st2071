package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources {
  private static TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator _instance0 = new TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (("input.GH5EYDXDFHC,textarea.GH5EYDXDFHC{border:" + ("1px"+ " " +"solid"+ " " +"#7eadd9")  + ";}input.GH5EYDXDGHC,textarea.GH5EYDXDGHC{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getTop() + "px  repeat-x")  + ";background-color:" + ("#fff")  + ";background-position:" + ("bottom")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#c30")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";}.GH5EYDXDHY{position:" + ("relative")  + ";right:") + (("0")  + ";top:" + ("0")  + ";zoom:" + ("1")  + ";white-space:" + ("nowrap")  + ";text-align:" + ("right")  + ";position:" + ("static")  + ";}.GH5EYDXDOX{font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDNX{color:" + ("gray")  + ";}.GH5EYDXDFY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getTop() + "px  repeat-x") ) + (";height:" + ("auto")  + ";background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDLX{background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:") + (("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";overflow:" + ("visible")  + ";}.GH5EYDXDPX{height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getTop() + "px  no-repeat")  + ";height:" + ("21px") ) + (";border-bottom:" + ("1px"+ " " +"solid")  + ";border-bottom-color:" + ("#b5b8c8")  + ";}.GH5EYDXDFHC .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDDY .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDMX .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDCY{cursor:" + ("pointer") ) + (";}")) : (("input.GH5EYDXDFHC,textarea.GH5EYDXDFHC{border:" + ("1px"+ " " +"solid"+ " " +"#7eadd9")  + ";}input.GH5EYDXDGHC,textarea.GH5EYDXDGHC{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.invalidLine()).getTop() + "px  repeat-x")  + ";background-color:" + ("#fff")  + ";background-position:" + ("bottom")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#c30")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";}.GH5EYDXDHY{position:" + ("relative")  + ";left:") + (("0")  + ";top:" + ("0")  + ";zoom:" + ("1")  + ";white-space:" + ("nowrap")  + ";text-align:" + ("left")  + ";position:" + ("static")  + ";}.GH5EYDXDOX{font:" + ("12px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDNX{color:" + ("gray")  + ";}.GH5EYDXDFY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.textBackground()).getTop() + "px  repeat-x") ) + (";height:" + ("auto")  + ";background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:" + ("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDLX{background-color:" + ("#fff")  + ";border:" + ("1px"+ " " +"#b5b8c8"+ " " +"solid")  + ";padding:") + (("1px"+ " " +"3px")  + ";resize:" + ("none")  + ";overflow:" + ("visible")  + ";}.GH5EYDXDPX{height:" + ("18px")  + ";line-height:" + ("18px")  + ";vertical-align:" + ("top")  + ";}.GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrow()).getTop() + "px  no-repeat")  + ";height:" + ("21px") ) + (";border-bottom:" + ("1px"+ " " +"solid")  + ";border-bottom-color:" + ("#b5b8c8")  + ";}.GH5EYDXDFHC .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowFocus()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDDY .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowOver()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDMX .GH5EYDXDGY{height:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getHeight() + "px")  + ";width:" + ((TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getSafeUri().asString() + "\") -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getLeft() + "px -" + (TriggerFieldDefaultAppearance_TriggerFieldResources_ie6_default_StaticClientBundleGenerator.this.triggerArrowClick()).getTop() + "px  no-repeat")  + ";height:" + ("21px")  + ";border-bottom-color:" + ("#7eadd9")  + ";}.GH5EYDXDCY{cursor:" + ("pointer") ) + (";}"));
      }
      public java.lang.String area(){
        return "GH5EYDXDLX";
      }
      public java.lang.String click(){
        return "GH5EYDXDMX";
      }
      public java.lang.String empty(){
        return "GH5EYDXDNX";
      }
      public java.lang.String field(){
        return "GH5EYDXDOX";
      }
      public java.lang.String file(){
        return "GH5EYDXDPX";
      }
      public java.lang.String focus(){
        return "GH5EYDXDFHC";
      }
      public java.lang.String invalid(){
        return "GH5EYDXDGHC";
      }
      public java.lang.String noedit(){
        return "GH5EYDXDCY";
      }
      public java.lang.String over(){
        return "GH5EYDXDDY";
      }
      public java.lang.String readonly(){
        return "GH5EYDXDEY";
      }
      public java.lang.String text(){
        return "GH5EYDXDFY";
      }
      public java.lang.String trigger(){
        return "GH5EYDXDGY";
      }
      public java.lang.String wrap(){
        return "GH5EYDXDHY";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldStyle css() {
    return cssInitializer.get();
  }
  private void invalidLineInitializer() {
    invalidLine = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "invalidLine",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 4, 3, false, false
    );
  }
  private static class invalidLineInitializer {
    static {
      _instance0.invalidLineInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return invalidLine;
    }
  }
  public com.google.gwt.resources.client.ImageResource invalidLine() {
    return invalidLineInitializer.get();
  }
  private void textBackgroundInitializer() {
    textBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textBackground",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal0),
      0, 0, 1, 18, false, false
    );
  }
  private static class textBackgroundInitializer {
    static {
      _instance0.textBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource textBackground() {
    return textBackgroundInitializer.get();
  }
  private void triggerArrowInitializer() {
    triggerArrow = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrow",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      85, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowInitializer {
    static {
      _instance0.triggerArrowInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrow;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrow() {
    return triggerArrowInitializer.get();
  }
  private void triggerArrowClickInitializer() {
    triggerArrowClick = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowClick",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      68, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowClickInitializer {
    static {
      _instance0.triggerArrowClickInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowClick;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowClick() {
    return triggerArrowClickInitializer.get();
  }
  private void triggerArrowFocusInitializer() {
    triggerArrowFocus = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowFocus",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      51, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowFocusInitializer {
    static {
      _instance0.triggerArrowFocusInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowFocus;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowFocus() {
    return triggerArrowFocusInitializer.get();
  }
  private void triggerArrowFocusClickInitializer() {
    triggerArrowFocusClick = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowFocusClick",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      34, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowFocusClickInitializer {
    static {
      _instance0.triggerArrowFocusClickInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowFocusClick;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowFocusClick() {
    return triggerArrowFocusClickInitializer.get();
  }
  private void triggerArrowFocusOverInitializer() {
    triggerArrowFocusOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowFocusOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      17, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowFocusOverInitializer {
    static {
      _instance0.triggerArrowFocusOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowFocusOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowFocusOver() {
    return triggerArrowFocusOverInitializer.get();
  }
  private void triggerArrowOverInitializer() {
    triggerArrowOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "triggerArrowOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 17, 24, false, false
    );
  }
  private static class triggerArrowOverInitializer {
    static {
      _instance0.triggerArrowOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return triggerArrowOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource triggerArrowOver() {
    return triggerArrowOverInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldStyle css;
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "2659A66C9CEC1586DA091ACEC4A3AE6B.cache.png";
  private static final java.lang.String bundledImage_Horizontal0 = GWT.getModuleBaseURL() + "785915E9E951485A0F06B31766E15B9A.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "3319E3F12F51D0EF071F7287660F0D54.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "2659A66C9CEC1586DA091ACEC4A3AE6B.cache.png";
  private static com.google.gwt.resources.client.ImageResource invalidLine;
  private static com.google.gwt.resources.client.ImageResource textBackground;
  private static com.google.gwt.resources.client.ImageResource triggerArrow;
  private static com.google.gwt.resources.client.ImageResource triggerArrowClick;
  private static com.google.gwt.resources.client.ImageResource triggerArrowFocus;
  private static com.google.gwt.resources.client.ImageResource triggerArrowFocusClick;
  private static com.google.gwt.resources.client.ImageResource triggerArrowFocusOver;
  private static com.google.gwt.resources.client.ImageResource triggerArrowOver;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      invalidLine(), 
      textBackground(), 
      triggerArrow(), 
      triggerArrowClick(), 
      triggerArrowFocus(), 
      triggerArrowFocusClick(), 
      triggerArrowFocusOver(), 
      triggerArrowOver(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("invalidLine", invalidLine());
        resourceMap.put("textBackground", textBackground());
        resourceMap.put("triggerArrow", triggerArrow());
        resourceMap.put("triggerArrowClick", triggerArrowClick());
        resourceMap.put("triggerArrowFocus", triggerArrowFocus());
        resourceMap.put("triggerArrowFocusClick", triggerArrowFocusClick());
        resourceMap.put("triggerArrowFocusOver", triggerArrowFocusOver());
        resourceMap.put("triggerArrowOver", triggerArrowOver());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::css()();
      case 'invalidLine': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::invalidLine()();
      case 'textBackground': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::textBackground()();
      case 'triggerArrow': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrow()();
      case 'triggerArrowClick': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrowClick()();
      case 'triggerArrowFocus': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrowFocus()();
      case 'triggerArrowFocusClick': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrowFocusClick()();
      case 'triggerArrowFocusOver': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrowFocusOver()();
      case 'triggerArrowOver': return this.@com.sencha.gxt.theme.base.client.field.TriggerFieldDefaultAppearance.TriggerFieldResources::triggerArrowOver()();
    }
    return null;
  }-*/;
}
