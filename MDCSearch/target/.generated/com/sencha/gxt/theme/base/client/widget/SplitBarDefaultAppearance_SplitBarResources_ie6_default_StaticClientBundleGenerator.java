package com.sencha.gxt.theme.base.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources {
  private static SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator _instance0 = new SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator();
  private void cssInitializer() {
    css = new com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDFRB{position:" + ("absolute")  + ";z-index:" + ("3")  + ";}.GH5EYDXDGRB{cursor:" + ("s-resize")  + ";cursor:" + ("row-resize")  + ";font-size:" + ("1px")  + ";line-height:" + ("1px")  + ";}.GH5EYDXDORB{cursor:" + ("w-resize")  + ";cursor:" + ("col-resize")  + ";}.GH5EYDXDNRB{position:" + ("absolute")  + ";background-color:" + ("#929090")  + ";font-size:") + (("1px")  + ";line-height:" + ("1px")  + ";z-index:" + ("200")  + ";}.GH5EYDXDHRB{position:" + ("absolute")  + ";top:" + ("0")  + ";right:" + ("0")  + ";display:" + ("block")  + ";width:" + ("5px")  + ";height:" + ("35px")  + ";cursor:" + ("pointer")  + ";filter:" + ("alpha(opacity=50)") ) + (";}.GH5EYDXDKRB{opacity:" + ("1")  + ";filter:" + ("none")  + ";filter:" + ("none")  + ";}.GH5EYDXDJRB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";}.GH5EYDXDLRB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getHeight() + "px")  + ";width:") + (((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GH5EYDXDJRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GH5EYDXDMRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDMRB,.GH5EYDXDIRB{height:" + ("5px")  + ";right:" + ("50%")  + ";margin-right:" + ("-17px")  + ";width:" + ("35px")  + ";}")) : ((".GH5EYDXDFRB{position:" + ("absolute")  + ";z-index:" + ("3")  + ";}.GH5EYDXDGRB{cursor:" + ("s-resize")  + ";cursor:" + ("row-resize")  + ";font-size:" + ("1px")  + ";line-height:" + ("1px")  + ";}.GH5EYDXDORB{cursor:" + ("e-resize")  + ";cursor:" + ("col-resize")  + ";}.GH5EYDXDNRB{position:" + ("absolute")  + ";background-color:" + ("#929090")  + ";font-size:") + (("1px")  + ";line-height:" + ("1px")  + ";z-index:" + ("200")  + ";}.GH5EYDXDHRB{position:" + ("absolute")  + ";top:" + ("0")  + ";left:" + ("0")  + ";display:" + ("block")  + ";width:" + ("5px")  + ";height:" + ("35px")  + ";cursor:" + ("pointer")  + ";filter:" + ("alpha(opacity=50)") ) + (";}.GH5EYDXDKRB{opacity:" + ("1")  + ";filter:" + ("none")  + ";filter:" + ("none")  + ";}.GH5EYDXDJRB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";}.GH5EYDXDLRB{border:" + ("0"+ " " +"none")  + ";width:" + ("5px")  + " !important;padding:" + ("0")  + ";height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getHeight() + "px")  + ";width:") + (((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniRight()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GH5EYDXDJRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniLeft()).getTop() + "px  no-repeat")  + ";top:" + ("48%")  + ";}.GH5EYDXDMRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniTop()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIRB{height:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getHeight() + "px")  + ";width:" + ((SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getSafeUri().asString() + "\") -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getLeft() + "px -" + (SplitBarDefaultAppearance_SplitBarResources_ie6_default_StaticClientBundleGenerator.this.miniBottom()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDMRB,.GH5EYDXDIRB{height:" + ("5px")  + ";left:" + ("50%")  + ";margin-left:" + ("-17px")  + ";width:" + ("35px")  + ";}"));
      }
      public java.lang.String bar(){
        return "GH5EYDXDFRB";
      }
      public java.lang.String horizontalBar(){
        return "GH5EYDXDGRB";
      }
      public java.lang.String mini(){
        return "GH5EYDXDHRB";
      }
      public java.lang.String miniBottom(){
        return "GH5EYDXDIRB";
      }
      public java.lang.String miniLeft(){
        return "GH5EYDXDJRB";
      }
      public java.lang.String miniOver(){
        return "GH5EYDXDKRB";
      }
      public java.lang.String miniRight(){
        return "GH5EYDXDLRB";
      }
      public java.lang.String miniTop(){
        return "GH5EYDXDMRB";
      }
      public java.lang.String proxy(){
        return "GH5EYDXDNRB";
      }
      public java.lang.String verticalBar(){
        return "GH5EYDXDORB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle get() {
      return css;
    }
  }
  public com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle css() {
    return cssInitializer.get();
  }
  private void miniBottomInitializer() {
    miniBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      10, 5, 35, 5, false, false
    );
  }
  private static class miniBottomInitializer {
    static {
      _instance0.miniBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniBottom() {
    return miniBottomInitializer.get();
  }
  private void miniLeftInitializer() {
    miniLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      5, 0, 5, 35, false, false
    );
  }
  private static class miniLeftInitializer {
    static {
      _instance0.miniLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniLeft() {
    return miniLeftInitializer.get();
  }
  private void miniRightInitializer() {
    miniRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 5, 35, false, false
    );
  }
  private static class miniRightInitializer {
    static {
      _instance0.miniRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniRight() {
    return miniRightInitializer.get();
  }
  private void miniTopInitializer() {
    miniTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "miniTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      10, 0, 35, 5, false, false
    );
  }
  private static class miniTopInitializer {
    static {
      _instance0.miniTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return miniTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource miniTop() {
    return miniTopInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarStyle css;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "E33C3AD6EC23F777175D92AD43594B97.cache.png";
  private static com.google.gwt.resources.client.ImageResource miniBottom;
  private static com.google.gwt.resources.client.ImageResource miniLeft;
  private static com.google.gwt.resources.client.ImageResource miniRight;
  private static com.google.gwt.resources.client.ImageResource miniTop;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      miniBottom(), 
      miniLeft(), 
      miniRight(), 
      miniTop(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("miniBottom", miniBottom());
        resourceMap.put("miniLeft", miniLeft());
        resourceMap.put("miniRight", miniRight());
        resourceMap.put("miniTop", miniTop());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::css()();
      case 'miniBottom': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniBottom()();
      case 'miniLeft': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniLeft()();
      case 'miniRight': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniRight()();
      case 'miniTop': return this.@com.sencha.gxt.theme.base.client.widget.SplitBarDefaultAppearance.SplitBarResources::miniTop()();
    }
    return null;
  }-*/;
}
