package com.sencha.gxt.theme.base.client.info;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources {
  private static InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator _instance0 = new InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 1, 1, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both0),
      0, 0, 1, 9, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both1),
      0, 0, 9, 9, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both2),
      0, 0, 9, 9, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both3),
      0, 0, 9, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both4),
      0, 0, 9, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both5),
      0, 0, 1, 9, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both6),
      0, 0, 9, 9, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both7),
      0, 0, 9, 9, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".x-has-width .GH5EYDXDBEB{width:" + ("100%")  + ";}.GH5EYDXDODB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDHEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDGEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIEB{overflow:" + ("hidden")  + ";background:") + (("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDMDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDLDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDNDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDCEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDFEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_ie6_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat") ) + (";}.GH5EYDXDODB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +background().getURL() + ""+ " " +"\\'"+ ","+ " " +"sizingMethod"+ " " +"\\="+ " " +"scale\\)")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDHEB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +topLeftBorder().getURL() + ""+ " " +"\\'\\)")  + ";width:" + (topLeftBorder().getWidth() + "px")  + ";height:" + (topLeftBorder().getHeight() + "px")  + ";}.GH5EYDXDGEB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +topBorder().getURL() + ""+ " " +"\\'"+ ","+ " " +"sizingMethod"+ " " +"\\="+ " " +"scale\\)")  + ";height:") + ((topBorder().getHeight() + "px")  + ";}.GH5EYDXDIEB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +topRightBorder().getURL() + ""+ " " +"\\'\\)")  + ";width:" + (topRightBorder().getWidth() + "px")  + ";height:" + (topRightBorder().getHeight() + "px")  + ";}.GH5EYDXDMDB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +bottomLeftBorder().getURL() + ""+ " " +"\\'\\)")  + ";width:" + (bottomLeftBorder().getWidth() + "px")  + ";height:" + (bottomLeftBorder().getHeight() + "px")  + ";}.GH5EYDXDLDB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +bottomBorder().getURL() + ""+ " " +"\\'"+ ","+ " " +"sizingMethod"+ " " +"\\="+ " " +"scale\\)") ) + (";height:" + (bottomBorder().getHeight() + "px")  + ";}.GH5EYDXDNDB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +bottomRightBorder().getURL() + ""+ " " +"\\'\\)")  + ";width:" + (bottomRightBorder().getWidth() + "px")  + ";height:" + (bottomRightBorder().getHeight() + "px")  + ";}.GH5EYDXDCEB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +leftBorder().getURL() + ""+ " " +"\\'"+ ","+ " " +"sizingMethod"+ " " +"\\="+ " " +"scale\\)")  + ";width:" + (leftBorder().getWidth() + "px")  + ";}.GH5EYDXDFEB{background-image:" + ("none")  + ";filter:" + ("progid\\:DXImageTransform\\.Microsoft\\.AlphaImageLoader\\(src"+ " " +"\\=\\'"+ " " +rightBorder().getURL() + ""+ " " +"\\'"+ ","+ " " +"sizingMethod"+ " " +"\\="+ " " +"scale\\)")  + ";width:") + ((rightBorder().getWidth() + "px")  + ";}");
      }
      public java.lang.String bottom(){
        return "GH5EYDXDLDB";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDMDB";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDNDB";
      }
      public java.lang.String content(){
        return "GH5EYDXDODB";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDPDB";
      }
      public java.lang.String focus(){
        return "GH5EYDXDAEB";
      }
      public java.lang.String frame(){
        return "GH5EYDXDBEB";
      }
      public java.lang.String left(){
        return "GH5EYDXDCEB";
      }
      public java.lang.String over(){
        return "GH5EYDXDDEB";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDEEB";
      }
      public java.lang.String right(){
        return "GH5EYDXDFEB";
      }
      public java.lang.String top(){
        return "GH5EYDXDGEB";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDHEB";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDIEB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "55CB90490B2A794AAD790A2B2070CE21.cache.png";
  private static final java.lang.String bundledImage_Both0 = GWT.getModuleBaseURL() + "3608CB618366C3D1EB679C845A642B7F.cache.png";
  private static final java.lang.String bundledImage_Both1 = GWT.getModuleBaseURL() + "2FC0A752E35879F7CCFB778AE7E39E7E.cache.png";
  private static final java.lang.String bundledImage_Both2 = GWT.getModuleBaseURL() + "B4417DF7EB257EAF5D1AB0A8D45C14BC.cache.png";
  private static final java.lang.String bundledImage_Both3 = GWT.getModuleBaseURL() + "BA806D0453BC94DFE78A220195300826.cache.png";
  private static final java.lang.String bundledImage_Both4 = GWT.getModuleBaseURL() + "BA806D0453BC94DFE78A220195300826.cache.png";
  private static final java.lang.String bundledImage_Both5 = GWT.getModuleBaseURL() + "3608CB618366C3D1EB679C845A642B7F.cache.png";
  private static final java.lang.String bundledImage_Both6 = GWT.getModuleBaseURL() + "2FB56AE2D875EEF4903D8A108BE3957F.cache.png";
  private static final java.lang.String bundledImage_Both7 = GWT.getModuleBaseURL() + "6CF988D38E9FBE24ABEA3E9D6711D31C.cache.png";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::style()();
    }
    return null;
  }-*/;
}
