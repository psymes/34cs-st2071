package com.sencha.gxt.theme.base.client.field;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipResources {
  private static ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator _instance0 = new ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator();
  private void anchorBottomInitializer() {
    anchorBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      9, 0, 9, 10, false, false
    );
  }
  private static class anchorBottomInitializer {
    static {
      _instance0.anchorBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorBottom() {
    return anchorBottomInitializer.get();
  }
  private void anchorLeftInitializer() {
    anchorLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      28, 0, 10, 9, false, false
    );
  }
  private static class anchorLeftInitializer {
    static {
      _instance0.anchorLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorLeft() {
    return anchorLeftInitializer.get();
  }
  private void anchorRightInitializer() {
    anchorRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      18, 0, 10, 9, false, false
    );
  }
  private static class anchorRightInitializer {
    static {
      _instance0.anchorRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorRight() {
    return anchorRightInitializer.get();
  }
  private void anchorTopInitializer() {
    anchorTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 9, 10, false, false
    );
  }
  private static class anchorTopInitializer {
    static {
      _instance0.anchorTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorTop() {
    return anchorTopInitializer.get();
  }
  private void errorIconInitializer() {
    errorIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "errorIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 16, 16, false, false
    );
  }
  private static class errorIconInitializer {
    static {
      _instance0.errorIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return errorIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource errorIcon() {
    return errorIconInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDDT{padding:" + ("2px")  + ";font-size:" + ("12px")  + ";}.GH5EYDXDET{float:" + ("left")  + ";}.GH5EYDXDAT{color:" + ("#444")  + ";font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDBT{color:" + ("#444")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDMS{zoom:" + ("1")  + ";}.GH5EYDXDNS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDPS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDOS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDAT{font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#444")  + ";}.GH5EYDXDBT{font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#444")  + ";padding-right:" + ("0")  + ";}.GH5EYDXDCT{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("-1"+ " " +"4px")  + ";padding-right:" + ("20px")  + ";padding-bottom:" + ("5px")  + ";}")) : ((".GH5EYDXDDT{padding:" + ("2px")  + ";font-size:" + ("12px")  + ";}.GH5EYDXDET{float:" + ("right")  + ";}.GH5EYDXDAT{color:" + ("#444")  + ";font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDBT{color:" + ("#444")  + ";font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";}.GH5EYDXDMS{zoom:" + ("1")  + ";}.GH5EYDXDNS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorBottom()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDPS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorRight()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDOS{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.anchorLeft()).getTop() + "px  no-repeat")  + ";position:" + ("absolute")  + ";}.GH5EYDXDAT{font:" + ("bold"+ " " +"11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#444")  + ";}.GH5EYDXDBT{font:" + ("11px"+ " " +"tahoma"+ ","+ " " +"arial"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";color:" + ("#444")  + ";padding-left:" + ("0")  + ";}.GH5EYDXDCT{height:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getHeight() + "px")  + ";width:" + ((ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getSafeUri().asString() + "\") -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getLeft() + "px -" + (ErrorTipDefaultAppearance_ErrorTipResources_ie6_default_StaticClientBundleGenerator.this.errorIcon()).getTop() + "px  no-repeat")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";background-position:" + ("-1"+ " " +"4px")  + ";padding-left:" + ("20px")  + ";padding-bottom:" + ("5px")  + ";}"));
      }
      public java.lang.String anchor(){
        return "GH5EYDXDMS";
      }
      public java.lang.String anchorBottom(){
        return "GH5EYDXDNS";
      }
      public java.lang.String anchorLeft(){
        return "GH5EYDXDOS";
      }
      public java.lang.String anchorRight(){
        return "GH5EYDXDPS";
      }
      public java.lang.String heading(){
        return "GH5EYDXDAT";
      }
      public java.lang.String text(){
        return "GH5EYDXDBT";
      }
      public java.lang.String textWrap(){
        return "GH5EYDXDCT";
      }
      public java.lang.String tip(){
        return "GH5EYDXDDT";
      }
      public java.lang.String tools(){
        return "GH5EYDXDET";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "7E4B69A275C9269640593E8B7F57AD68.cache.png";
  private static final java.lang.String bundledImage_None0 = GWT.getModuleBaseURL() + "D4A6540E96842E03327A041C241D2B63.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "D4A6540E96842E03327A041C241D2B63.cache.png";
  private static com.google.gwt.resources.client.ImageResource anchorBottom;
  private static com.google.gwt.resources.client.ImageResource anchorLeft;
  private static com.google.gwt.resources.client.ImageResource anchorRight;
  private static com.google.gwt.resources.client.ImageResource anchorTop;
  private static com.google.gwt.resources.client.ImageResource errorIcon;
  private static com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      anchorBottom(), 
      anchorLeft(), 
      anchorRight(), 
      anchorTop(), 
      errorIcon(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("anchorBottom", anchorBottom());
        resourceMap.put("anchorLeft", anchorLeft());
        resourceMap.put("anchorRight", anchorRight());
        resourceMap.put("anchorTop", anchorTop());
        resourceMap.put("errorIcon", errorIcon());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'anchorBottom': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipResources::anchorBottom()();
      case 'anchorLeft': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipResources::anchorLeft()();
      case 'anchorRight': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipResources::anchorRight()();
      case 'anchorTop': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipResources::anchorTop()();
      case 'errorIcon': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipResources::errorIcon()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.field.ErrorTipDefaultAppearance.ErrorTipResources::style()();
    }
    return null;
  }-*/;
}
