package com.sencha.gxt.theme.base.client.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ButtonCellDefaultAppearance_ButtonCellResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources {
  private static ButtonCellDefaultAppearance_ButtonCellResources_ie6_default_StaticClientBundleGenerator _instance0 = new ButtonCellDefaultAppearance_ButtonCellResources_ie6_default_StaticClientBundleGenerator();
  private void arrowInitializer() {
    arrow = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "arrow",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      14, 28, 10, 10, false, false
    );
  }
  private static class arrowInitializer {
    static {
      _instance0.arrowInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return arrow;
    }
  }
  public com.google.gwt.resources.client.ImageResource arrow() {
    return arrowInitializer.get();
  }
  private void arrowBottomInitializer() {
    arrowBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "arrowBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      14, 14, 200, 14, false, false
    );
  }
  private static class arrowBottomInitializer {
    static {
      _instance0.arrowBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return arrowBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource arrowBottom() {
    return arrowBottomInitializer.get();
  }
  private void splitInitializer() {
    split = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "split",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 14, 72, false, false
    );
  }
  private static class splitInitializer {
    static {
      _instance0.splitInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return split;
    }
  }
  public com.google.gwt.resources.client.ImageResource split() {
    return splitInitializer.get();
  }
  private void splitBottomInitializer() {
    splitBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "splitBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      14, 0, 200, 14, false, false
    );
  }
  private static class splitBottomInitializer {
    static {
      _instance0.splitBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return splitBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource splitBottom() {
    return splitBottomInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDNM{cursor:" + ("pointer")  + ";white-space:" + ("nowrap")  + ";}.GH5EYDXDOM .GH5EYDXDFN{width:" + ("100%")  + ";}.GH5EYDXDNM td{text-align:" + ("center")  + ";}.GH5EYDXDDN{padding:" + ("0"+ " " +"2px")  + ";}.GH5EYDXDMN{padding:" + ("0"+ " " +"2px")  + ";font-family:" + ("Tahoma"+ ","+ " " +"Arial"+ ","+ " " +"Verdana"+ ","+ " " +"sans-serif")  + ";font-size:" + ("11px")  + ";font-weight:" + ("normal")  + ";text-align:" + ("center")  + ";cursor:") + (("pointer")  + ";white-space:" + ("nowrap")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDN div{font-size:" + ("1px")  + ";}.GH5EYDXDJN .GH5EYDXDAN td,.GH5EYDXDJN .GH5EYDXDBN td,.GH5EYDXDJN .GH5EYDXDHN td{line-height:" + ("18px")  + ";}.GH5EYDXDGN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDGN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("20px")  + ";}.GH5EYDXDGN .GH5EYDXDAN td,.GH5EYDXDGN .GH5EYDXDBN td,.GH5EYDXDGN .GH5EYDXDHN td{line-height:" + ("24px")  + ";}.GH5EYDXDGN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDGN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("26px")  + ";}.GH5EYDXDEN .GH5EYDXDAN td,.GH5EYDXDEN .GH5EYDXDBN td,.GH5EYDXDEN .GH5EYDXDHN td,.GH5EYDXDEN .GH5EYDXDDN div{line-height:" + ("32px")  + ";}.GH5EYDXDEN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDEN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("34px")  + ";}.GH5EYDXDKN{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/split.gif")) ) + (";background-position:" + ("left"+ " " +"center")  + ";padding-left:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDLN{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/splitBottom.gif"))  + ";gwt-image:" + ("\"splitBottom\"")  + ";background-position:" + ("center"+ " " +"bottom")  + ";padding-bottom:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDLM{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/arrow.gif"))  + ";padding-left:" + ("10px")  + ";background-position:") + (("left"+ " " +"center")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDMM{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/arrowBottom.gif"))  + ";background-position:" + ("center"+ " " +"bottom")  + ";padding-bottom:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}")) : ((".GH5EYDXDNM{cursor:" + ("pointer")  + ";white-space:" + ("nowrap")  + ";}.GH5EYDXDOM .GH5EYDXDFN{width:" + ("100%")  + ";}.GH5EYDXDNM td{text-align:" + ("center")  + ";}.GH5EYDXDDN{padding:" + ("0"+ " " +"2px")  + ";}.GH5EYDXDMN{padding:" + ("0"+ " " +"2px")  + ";font-family:" + ("Tahoma"+ ","+ " " +"Arial"+ ","+ " " +"Verdana"+ ","+ " " +"sans-serif")  + ";font-size:" + ("11px")  + ";font-weight:" + ("normal")  + ";text-align:" + ("center")  + ";cursor:") + (("pointer")  + ";white-space:" + ("nowrap")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDDN div{font-size:" + ("1px")  + ";}.GH5EYDXDJN .GH5EYDXDAN td,.GH5EYDXDJN .GH5EYDXDBN td,.GH5EYDXDJN .GH5EYDXDHN td{line-height:" + ("18px")  + ";}.GH5EYDXDGN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDGN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("20px")  + ";}.GH5EYDXDGN .GH5EYDXDAN td,.GH5EYDXDGN .GH5EYDXDBN td,.GH5EYDXDGN .GH5EYDXDHN td{line-height:" + ("24px")  + ";}.GH5EYDXDGN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDGN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("26px")  + ";}.GH5EYDXDEN .GH5EYDXDAN td,.GH5EYDXDEN .GH5EYDXDBN td,.GH5EYDXDEN .GH5EYDXDHN td,.GH5EYDXDEN .GH5EYDXDDN div{line-height:" + ("32px")  + ";}.GH5EYDXDEN .GH5EYDXDCN .GH5EYDXDDN div,.GH5EYDXDEN .GH5EYDXDPM .GH5EYDXDDN div{height:" + ("34px")  + ";}.GH5EYDXDKN{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/split.gif")) ) + (";background-position:" + ("right"+ " " +"center")  + ";padding-right:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDLN{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/splitBottom.gif"))  + ";gwt-image:" + ("\"splitBottom\"")  + ";background-position:" + ("center"+ " " +"bottom")  + ";padding-bottom:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDLM{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/arrow.gif"))  + ";padding-right:" + ("10px")  + ";background-position:") + (("right"+ " " +"center")  + ";background-repeat:" + ("no-repeat")  + ";}.GH5EYDXDMM{background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/button/arrowBottom.gif"))  + ";background-position:" + ("center"+ " " +"bottom")  + ";padding-bottom:" + ("14px")  + ";background-repeat:" + ("no-repeat")  + ";}"));
      }
      public java.lang.String arrow(){
        return "GH5EYDXDLM";
      }
      public java.lang.String arrowBottom(){
        return "GH5EYDXDMM";
      }
      public java.lang.String button(){
        return "GH5EYDXDNM";
      }
      public java.lang.String hasWidth(){
        return "GH5EYDXDOM";
      }
      public java.lang.String iconBottom(){
        return "GH5EYDXDPM";
      }
      public java.lang.String iconLeft(){
        return "GH5EYDXDAN";
      }
      public java.lang.String iconRight(){
        return "GH5EYDXDBN";
      }
      public java.lang.String iconTop(){
        return "GH5EYDXDCN";
      }
      public java.lang.String iconWrap(){
        return "GH5EYDXDDN";
      }
      public java.lang.String large(){
        return "GH5EYDXDEN";
      }
      public java.lang.String mainTable(){
        return "GH5EYDXDFN";
      }
      public java.lang.String medium(){
        return "GH5EYDXDGN";
      }
      public java.lang.String noIcon(){
        return "GH5EYDXDHN";
      }
      public java.lang.String over(){
        return "GH5EYDXDIN";
      }
      public java.lang.String small(){
        return "GH5EYDXDJN";
      }
      public java.lang.String split(){
        return "GH5EYDXDKN";
      }
      public java.lang.String splitBottom(){
        return "GH5EYDXDLN";
      }
      public java.lang.String text(){
        return "GH5EYDXDMN";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "70C4E6A13682E0E7F0B3BE3AA2CB766F.cache.png";
  private static com.google.gwt.resources.client.ImageResource arrow;
  private static com.google.gwt.resources.client.ImageResource arrowBottom;
  private static com.google.gwt.resources.client.ImageResource split;
  private static com.google.gwt.resources.client.ImageResource splitBottom;
  private static com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      arrow(), 
      arrowBottom(), 
      split(), 
      splitBottom(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("arrow", arrow());
        resourceMap.put("arrowBottom", arrowBottom());
        resourceMap.put("split", split());
        resourceMap.put("splitBottom", splitBottom());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'arrow': return this.@com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources::arrow()();
      case 'arrowBottom': return this.@com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources::arrowBottom()();
      case 'split': return this.@com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources::split()();
      case 'splitBottom': return this.@com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources::splitBottom()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance.ButtonCellResources::style()();
    }
    return null;
  }-*/;
}
