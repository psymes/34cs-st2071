package com.sencha.gxt.theme.base.client.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ButtonTableFrameResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources {
  private static ButtonTableFrameResources_default_StaticClientBundleGenerator _instance0 = new ButtonTableFrameResources_default_StaticClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void backgroundOverBorderInitializer() {
    backgroundOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundOverBorderInitializer {
    static {
      _instance0.backgroundOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundOverBorder() {
    return backgroundOverBorderInitializer.get();
  }
  private void backgroundPressedBorderInitializer() {
    backgroundPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "backgroundPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 1, 1100, false, false
    );
  }
  private static class backgroundPressedBorderInitializer {
    static {
      _instance0.backgroundPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return backgroundPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource backgroundPressedBorder() {
    return backgroundPressedBorderInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 9, 1, 3, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      33, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomLeftOverBorderInitializer() {
    bottomLeftOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      30, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftOverBorderInitializer {
    static {
      _instance0.bottomLeftOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftOverBorder() {
    return bottomLeftOverBorderInitializer.get();
  }
  private void bottomLeftPressedBorderInitializer() {
    bottomLeftPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      27, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftPressedBorderInitializer {
    static {
      _instance0.bottomLeftPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftPressedBorder() {
    return bottomLeftPressedBorderInitializer.get();
  }
  private void bottomOverBorderInitializer() {
    bottomOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 6, 62, 3, false, false
    );
  }
  private static class bottomOverBorderInitializer {
    static {
      _instance0.bottomOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomOverBorder() {
    return bottomOverBorderInitializer.get();
  }
  private void bottomPressedBorderInitializer() {
    bottomPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 3, 52, 3, false, false
    );
  }
  private static class bottomPressedBorderInitializer {
    static {
      _instance0.bottomPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomPressedBorder() {
    return bottomPressedBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      24, 0, 3, 3, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void bottomRightOverBorderInitializer() {
    bottomRightOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      21, 0, 3, 3, false, false
    );
  }
  private static class bottomRightOverBorderInitializer {
    static {
      _instance0.bottomRightOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightOverBorder() {
    return bottomRightOverBorderInitializer.get();
  }
  private void bottomRightPressedBorderInitializer() {
    bottomRightPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      18, 0, 3, 3, false, false
    );
  }
  private static class bottomRightPressedBorderInitializer {
    static {
      _instance0.bottomRightPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightPressedBorder() {
    return bottomRightPressedBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void leftOverBorderInitializer() {
    leftOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftOverBorderInitializer {
    static {
      _instance0.leftOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftOverBorder() {
    return leftOverBorderInitializer.get();
  }
  private void leftPressedBorderInitializer() {
    leftPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 3, 1000, false, false
    );
  }
  private static class leftPressedBorderInitializer {
    static {
      _instance0.leftPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftPressedBorder() {
    return leftPressedBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 3, 1000, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void rightOverBorderInitializer() {
    rightOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 3, 1000, false, false
    );
  }
  private static class rightOverBorderInitializer {
    static {
      _instance0.rightOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightOverBorder() {
    return rightOverBorderInitializer.get();
  }
  private void rightPressedBorderInitializer() {
    rightPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 3, 1000, false, false
    );
  }
  private static class rightPressedBorderInitializer {
    static {
      _instance0.rightPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightPressedBorder() {
    return rightPressedBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 15, 1, 3, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      15, 0, 3, 3, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topLeftOverBorderInitializer() {
    topLeftOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      12, 0, 3, 3, false, false
    );
  }
  private static class topLeftOverBorderInitializer {
    static {
      _instance0.topLeftOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftOverBorder() {
    return topLeftOverBorderInitializer.get();
  }
  private void topLeftPressedBorderInitializer() {
    topLeftPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      9, 0, 3, 3, false, false
    );
  }
  private static class topLeftPressedBorderInitializer {
    static {
      _instance0.topLeftPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftPressedBorder() {
    return topLeftPressedBorderInitializer.get();
  }
  private void topOverBorderInitializer() {
    topOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 12, 62, 3, false, false
    );
  }
  private static class topOverBorderInitializer {
    static {
      _instance0.topOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topOverBorder() {
    return topOverBorderInitializer.get();
  }
  private void topPressedBorderInitializer() {
    topPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 52, 3, false, false
    );
  }
  private static class topPressedBorderInitializer {
    static {
      _instance0.topPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topPressedBorder() {
    return topPressedBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      6, 0, 3, 3, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void topRightOverBorderInitializer() {
    topRightOverBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightOverBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      3, 0, 3, 3, false, false
    );
  }
  private static class topRightOverBorderInitializer {
    static {
      _instance0.topRightOverBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightOverBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightOverBorder() {
    return topRightOverBorderInitializer.get();
  }
  private void topRightPressedBorderInitializer() {
    topRightPressedBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightPressedBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 3, 3, false, false
    );
  }
  private static class topRightPressedBorderInitializer {
    static {
      _instance0.topRightPressedBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightPressedBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightPressedBorder() {
    return topRightPressedBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.frame.TableFrame.TableFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".x-has-width .GH5EYDXDP1{width:" + ("100%")  + ";}.GH5EYDXDM1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.background()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat-x")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDF2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDE2{height:") + (((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDG2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDK1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  no-repeat") ) + (";}.GH5EYDXDJ1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDL1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDA2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDD2{width:") + (((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDA2 div,.GH5EYDXDD2 div{width:" + ("3px")  + ";}.x-toolbar-mark .GH5EYDXDA2,.x-toolbar-mark .GH5EYDXDM1,.x-toolbar-mark .GH5EYDXDD2{background:" + ("none")  + ";}.x-toolbar-mark .GH5EYDXDF2,.x-toolbar-mark .GH5EYDXDE2,.x-toolbar-mark .GH5EYDXDG2,.x-toolbar-mark .GH5EYDXDK1,.x-toolbar-mark .GH5EYDXDJ1,.x-toolbar-mark .GH5EYDXDL1{background-image:" + ("none")  + ";}.GH5EYDXDB2 .GH5EYDXDM1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getTop() + "px  repeat-x")  + ";height:" + ("100%")  + ";width:" + ("100%") ) + (";}.GH5EYDXDB2 .GH5EYDXDF2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDB2 .GH5EYDXDE2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDB2 .GH5EYDXDG2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDB2 .GH5EYDXDK1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDB2 .GH5EYDXDJ1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDB2 .GH5EYDXDD2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getTop() + "px  repeat-y") ) + (";}.GH5EYDXDB2 .GH5EYDXDL1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDB2 .GH5EYDXDA2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDO1 .GH5EYDXDM1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundOverBorder()).getTop() + "px  repeat-x")  + ";height:") + (("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDO1 .GH5EYDXDF2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDO1 .GH5EYDXDE2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topOverBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDO1 .GH5EYDXDG2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDO1 .GH5EYDXDK1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDO1 .GH5EYDXDJ1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomOverBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDO1 .GH5EYDXDD2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightOverBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDO1 .GH5EYDXDL1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightOverBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDO1 .GH5EYDXDA2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftOverBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDC2 .GH5EYDXDM1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundPressedBorder()).getHeight() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.backgroundPressedBorder()).getTop() + "px  repeat-x")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDC2 .GH5EYDXDF2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftPressedBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topLeftPressedBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDC2 .GH5EYDXDE2{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topPressedBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topPressedBorder()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDC2 .GH5EYDXDG2{height:") + (((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightPressedBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.topRightPressedBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDC2 .GH5EYDXDK1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftPressedBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomLeftPressedBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDC2 .GH5EYDXDJ1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomPressedBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomPressedBorder()).getTop() + "px  repeat-x") ) + (";}.GH5EYDXDC2 .GH5EYDXDD2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.rightPressedBorder()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDC2 .GH5EYDXDL1{height:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightPressedBorder()).getHeight() + "px")  + ";width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.bottomRightPressedBorder()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDC2 .GH5EYDXDA2{width:" + ((ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftPressedBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftPressedBorder()).getSafeUri().asString() + "\") -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftPressedBorder()).getLeft() + "px -" + (ButtonTableFrameResources_default_StaticClientBundleGenerator.this.leftPressedBorder()).getTop() + "px  repeat-y")  + ";}");
      }
      public java.lang.String bottom(){
        return "GH5EYDXDJ1";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDK1";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDL1";
      }
      public java.lang.String content(){
        return "GH5EYDXDM1";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDN1";
      }
      public java.lang.String focus(){
        return "GH5EYDXDO1";
      }
      public java.lang.String frame(){
        return "GH5EYDXDP1";
      }
      public java.lang.String left(){
        return "GH5EYDXDA2";
      }
      public java.lang.String over(){
        return "GH5EYDXDB2";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDC2";
      }
      public java.lang.String right(){
        return "GH5EYDXDD2";
      }
      public java.lang.String top(){
        return "GH5EYDXDE2";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDF2";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDG2";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.frame.TableFrame.TableFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.frame.TableFrame.TableFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "C173D97B135B42E4D4AE9869537DA55F.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "2E84D4E828B9DF7B6BA8A99EE77AFC13.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "163A42415CE141B089A1DD2F9A78FDE0.cache.png";
  private static final java.lang.String externalImage0 = GWT.getModuleBaseURL() + "ADB30B874E19BEF6B1FF3C04CF59A40A.cache.png";
  private static final java.lang.String externalImage1 = GWT.getModuleBaseURL() + "DC424DD572C0437842A2FC17955825A9.cache.png";
  private static final java.lang.String externalImage2 = GWT.getModuleBaseURL() + "5B242499B48B3DABD1140521ADE29B87.cache.png";
  private static final java.lang.String externalImage3 = GWT.getModuleBaseURL() + "18E4405964EE105D17E3818DB59279FE.cache.png";
  private static final java.lang.String externalImage4 = GWT.getModuleBaseURL() + "A4CB34068AD9928C9FA711FEA4248339.cache.png";
  private static final java.lang.String externalImage5 = GWT.getModuleBaseURL() + "CAE214EA005FA92C16E8C1AB3EE3EBD3.cache.png";
  private static final java.lang.String externalImage6 = GWT.getModuleBaseURL() + "B1E75465B29CBF9B11A7D624B8B0F972.cache.png";
  private static final java.lang.String externalImage7 = GWT.getModuleBaseURL() + "E25E09AB1BF1EABD36F825D8B5A3B1EA.cache.png";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource backgroundOverBorder;
  private static com.google.gwt.resources.client.ImageResource backgroundPressedBorder;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftOverBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftPressedBorder;
  private static com.google.gwt.resources.client.ImageResource bottomOverBorder;
  private static com.google.gwt.resources.client.ImageResource bottomPressedBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightOverBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightPressedBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource leftOverBorder;
  private static com.google.gwt.resources.client.ImageResource leftPressedBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource rightOverBorder;
  private static com.google.gwt.resources.client.ImageResource rightPressedBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftOverBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftPressedBorder;
  private static com.google.gwt.resources.client.ImageResource topOverBorder;
  private static com.google.gwt.resources.client.ImageResource topPressedBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.google.gwt.resources.client.ImageResource topRightOverBorder;
  private static com.google.gwt.resources.client.ImageResource topRightPressedBorder;
  private static com.sencha.gxt.theme.base.client.frame.TableFrame.TableFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      backgroundOverBorder(), 
      backgroundPressedBorder(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomLeftOverBorder(), 
      bottomLeftPressedBorder(), 
      bottomOverBorder(), 
      bottomPressedBorder(), 
      bottomRightBorder(), 
      bottomRightOverBorder(), 
      bottomRightPressedBorder(), 
      leftBorder(), 
      leftOverBorder(), 
      leftPressedBorder(), 
      rightBorder(), 
      rightOverBorder(), 
      rightPressedBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topLeftOverBorder(), 
      topLeftPressedBorder(), 
      topOverBorder(), 
      topPressedBorder(), 
      topRightBorder(), 
      topRightOverBorder(), 
      topRightPressedBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("backgroundOverBorder", backgroundOverBorder());
        resourceMap.put("backgroundPressedBorder", backgroundPressedBorder());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomLeftOverBorder", bottomLeftOverBorder());
        resourceMap.put("bottomLeftPressedBorder", bottomLeftPressedBorder());
        resourceMap.put("bottomOverBorder", bottomOverBorder());
        resourceMap.put("bottomPressedBorder", bottomPressedBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("bottomRightOverBorder", bottomRightOverBorder());
        resourceMap.put("bottomRightPressedBorder", bottomRightPressedBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("leftOverBorder", leftOverBorder());
        resourceMap.put("leftPressedBorder", leftPressedBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("rightOverBorder", rightOverBorder());
        resourceMap.put("rightPressedBorder", rightPressedBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topLeftOverBorder", topLeftOverBorder());
        resourceMap.put("topLeftPressedBorder", topLeftPressedBorder());
        resourceMap.put("topOverBorder", topOverBorder());
        resourceMap.put("topPressedBorder", topPressedBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("topRightOverBorder", topRightOverBorder());
        resourceMap.put("topRightPressedBorder", topRightPressedBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::background()();
      case 'backgroundOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::backgroundOverBorder()();
      case 'backgroundPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::backgroundPressedBorder()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomLeftBorder()();
      case 'bottomLeftOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomLeftOverBorder()();
      case 'bottomLeftPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomLeftPressedBorder()();
      case 'bottomOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomOverBorder()();
      case 'bottomPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomPressedBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomRightBorder()();
      case 'bottomRightOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomRightOverBorder()();
      case 'bottomRightPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::bottomRightPressedBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::leftBorder()();
      case 'leftOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::leftOverBorder()();
      case 'leftPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::leftPressedBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::rightBorder()();
      case 'rightOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::rightOverBorder()();
      case 'rightPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::rightPressedBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topLeftBorder()();
      case 'topLeftOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topLeftOverBorder()();
      case 'topLeftPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topLeftPressedBorder()();
      case 'topOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topOverBorder()();
      case 'topPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topPressedBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topRightBorder()();
      case 'topRightOverBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topRightOverBorder()();
      case 'topRightPressedBorder': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::topRightPressedBorder()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.button.ButtonTableFrameResources::style()();
    }
    return null;
  }-*/;
}
