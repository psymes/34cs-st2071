package com.sencha.gxt.theme.base.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class HeaderDefaultAppearance_HeaderResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderResources {
  private static HeaderDefaultAppearance_HeaderResources_default_StaticClientBundleGenerator _instance0 = new HeaderDefaultAppearance_HeaderResources_default_StaticClientBundleGenerator();
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDARB{padding:" + ("4px"+ " " +"5px"+ " " +"2px"+ " " +"3px")  + ";position:" + ("relative")  + ";}.GH5EYDXDDRB{float:" + ("right")  + ";}.GH5EYDXDCRB .GH5EYDXDDRB{width:" + ("18px")  + ";}.GH5EYDXDCRB .GH5EYDXDERB{right:" + ("20px")  + ";}.GH5EYDXDERB{font-family:" + ("tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";line-height:" + ("15px")  + ";}.GH5EYDXDBRB{float:" + ("left")  + ";}")) : ((".GH5EYDXDARB{padding:" + ("4px"+ " " +"3px"+ " " +"2px"+ " " +"5px")  + ";position:" + ("relative")  + ";}.GH5EYDXDDRB{float:" + ("left")  + ";}.GH5EYDXDCRB .GH5EYDXDDRB{width:" + ("18px")  + ";}.GH5EYDXDCRB .GH5EYDXDERB{left:" + ("20px")  + ";}.GH5EYDXDERB{font-family:" + ("tahoma"+ ","+ " " +"arial"+ ","+ " " +"verdana"+ ","+ " " +"sans-serif")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";line-height:" + ("15px")  + ";}.GH5EYDXDBRB{float:" + ("right")  + ";}"));
      }
      public java.lang.String header(){
        return "GH5EYDXDARB";
      }
      public java.lang.String headerBar(){
        return "GH5EYDXDBRB";
      }
      public java.lang.String headerHasIcon(){
        return "GH5EYDXDCRB";
      }
      public java.lang.String headerIcon(){
        return "GH5EYDXDDRB";
      }
      public java.lang.String headerText(){
        return "GH5EYDXDERB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'style': return this.@com.sencha.gxt.theme.base.client.widget.HeaderDefaultAppearance.HeaderResources::style()();
    }
    return null;
  }-*/;
}
