package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderResources {
  private static RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator _instance0 = new RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator();
  private void specialColumnInitializer() {
    specialColumn = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumn",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnInitializer {
    static {
      _instance0.specialColumnInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumn;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumn() {
    return specialColumnInitializer.get();
  }
  private void specialColumnSelectedInitializer() {
    specialColumnSelected = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "specialColumnSelected",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 24, 2, false, false
    );
  }
  private static class specialColumnSelectedInitializer {
    static {
      _instance0.specialColumnSelectedInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return specialColumnSelected;
    }
  }
  public com.google.gwt.resources.client.ImageResource specialColumnSelected() {
    return specialColumnSelectedInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".GH5EYDXDEJC .GH5EYDXDEIC,.GH5EYDXDEJC .GH5EYDXDGIC{background:" + ("none"+ " " +"repeat"+ " " +"scroll"+ " " +"0"+ " " +"0"+ " " +"transparent")  + ";border-width:" + ("0")  + ";}.GH5EYDXDEJC .x-grid-td-expander{width:" + ((RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumn()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumn()).getSafeUri().asString() + "\") -" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumn()).getLeft() + "px -" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumn()).getTop() + "px  repeat-y")  + ";vertical-align:" + ("top")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDDJC .x-grid-td-expander{width:" + ((RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumnSelected()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumnSelected()).getSafeUri().asString() + "\") -" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumnSelected()).getLeft() + "px -" + (RowExpanderDefaultAppearance_RowExpanderResources_ie8_default_InlineClientBundleGenerator.this.specialColumnSelected()).getTop() + "px  repeat-y")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.x-grid-td-expander .GH5EYDXDGIC{padding:" + ("0")  + " !important;height:" + ("100%")  + ";}.GH5EYDXDECB{width:" + ("20px")  + ";height:" + ("18px")  + ";background-position:" + ("4px"+ " " +"2px")  + ";background-repeat:" + ("no-repeat")  + ";background-color:" + ("transparent")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/row-expand-sprite.gif")) ) + (";}.GH5EYDXDBCB .GH5EYDXDECB{background-position:" + ("4px"+ " " +"2px")  + ";}.GH5EYDXDDCB .GH5EYDXDECB{background-position:" + ("-21px"+ " " +"2px")  + ";}.GH5EYDXDBCB .GH5EYDXDPIC{display:" + ("none")  + ";}.GH5EYDXDDCB .GH5EYDXDPIC{display:" + ("table-row")  + ";}.x-grid-td-expander .GH5EYDXDGIC{padding:" + ("0")  + " !important;height:" + ("100%")  + ";}");
      }
      public java.lang.String cell(){
        return "GH5EYDXDEIC";
      }
      public java.lang.String cellDirty(){
        return "GH5EYDXDFIC";
      }
      public java.lang.String cellInner(){
        return "GH5EYDXDGIC";
      }
      public java.lang.String cellInvalid(){
        return "GH5EYDXDHIC";
      }
      public java.lang.String cellSelected(){
        return "GH5EYDXDIIC";
      }
      public java.lang.String columnLines(){
        return "GH5EYDXDJIC";
      }
      public java.lang.String dataTable(){
        return "GH5EYDXDKIC";
      }
      public java.lang.String headerRow(){
        return "GH5EYDXDLIC";
      }
      public java.lang.String row(){
        return "GH5EYDXDMIC";
      }
      public java.lang.String rowAlt(){
        return "GH5EYDXDNIC";
      }
      public java.lang.String rowBody(){
        return "GH5EYDXDOIC";
      }
      public java.lang.String rowBodyRow(){
        return "GH5EYDXDPIC";
      }
      public java.lang.String rowCollapsed(){
        return "GH5EYDXDBCB";
      }
      public java.lang.String rowDirty(){
        return "GH5EYDXDAJC";
      }
      public java.lang.String rowExpanded(){
        return "GH5EYDXDDCB";
      }
      public java.lang.String rowExpander(){
        return "GH5EYDXDECB";
      }
      public java.lang.String rowHighlight(){
        return "GH5EYDXDBJC";
      }
      public java.lang.String rowOver(){
        return "GH5EYDXDCJC";
      }
      public java.lang.String rowSelected(){
        return "GH5EYDXDDJC";
      }
      public java.lang.String rowWrap(){
        return "GH5EYDXDEJC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAANklEQVR42rXNqREAIAwEQPqvLn2QH0xUmDmBxiBW76iqfhG5wWNd5glq0aIOLNaTFYiox+/gACe+uBEe1wRCAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAACCAYAAABCOhwFAAAAXUlEQVR42rXNvQ6CMBSA0b7/o/AsDgSiRmIwKNTW8iOaMPTe5qOzu8OZjwnfxC//Sbg1Yd/KsCh9dp+VblJuo9IG5foSGi9cnHB+CqfsaIV6iFR9pHxEisOG+XewA9F9tJvfRlttAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource specialColumn;
  private static com.google.gwt.resources.client.ImageResource specialColumnSelected;
  private static com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      specialColumn(), 
      specialColumnSelected(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("specialColumn", specialColumn());
        resourceMap.put("specialColumnSelected", specialColumnSelected());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'specialColumn': return this.@com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderResources::specialColumn()();
      case 'specialColumnSelected': return this.@com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderResources::specialColumnSelected()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.grid.RowExpanderDefaultAppearance.RowExpanderResources::style()();
    }
    return null;
  }-*/;
}
