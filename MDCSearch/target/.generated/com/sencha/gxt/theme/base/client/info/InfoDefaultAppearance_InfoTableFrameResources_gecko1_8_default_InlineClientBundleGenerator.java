package com.sencha.gxt.theme.base.client.info;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator implements com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources {
  private static InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator _instance0 = new InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator();
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 1, 1, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage0),
      0, 0, 1, 9, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage1),
      0, 0, 9, 9, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage2),
      0, 0, 9, 9, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage3),
      0, 0, 9, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage4),
      0, 0, 9, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage5),
      0, 0, 1, 9, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage6),
      0, 0, 9, 9, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage7),
      0, 0, 9, 9, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return (".x-has-width .GH5EYDXDBEB{width:" + ("100%")  + ";}.GH5EYDXDODB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDHEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDGEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIEB{overflow:" + ("hidden")  + ";background:") + (("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDMDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDLDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDNDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDCEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDFEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat") ) + (";}.GH5EYDXDODB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";height:" + ("100%")  + ";width:" + ("100%")  + ";}.GH5EYDXDHEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDGEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDIEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDMDB{overflow:") + (("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDLDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDNDB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDCEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat")  + ";}.GH5EYDXDFEB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (InfoDefaultAppearance_InfoTableFrameResources_gecko1_8_default_InlineClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";}");
      }
      public java.lang.String bottom(){
        return "GH5EYDXDLDB";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDMDB";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDNDB";
      }
      public java.lang.String content(){
        return "GH5EYDXDODB";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDPDB";
      }
      public java.lang.String focus(){
        return "GH5EYDXDAEB";
      }
      public java.lang.String frame(){
        return "GH5EYDXDBEB";
      }
      public java.lang.String left(){
        return "GH5EYDXDCEB";
      }
      public java.lang.String over(){
        return "GH5EYDXDDEB";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDEEB";
      }
      public java.lang.String right(){
        return "GH5EYDXDFEB";
      }
      public java.lang.String top(){
        return "GH5EYDXDGEB";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDHEB";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDIEB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQIHWNgYGDYDwAAxADAeqKnCAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAJCAYAAADzRkbkAAAAEElEQVR42mNgYGDYz0AkAQCNMAa43a/9mgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAN0lEQVR42mNgYGDYA8T7CWCGZcQoqiJGkRUxikBgAjGKVAl4AA6s8ChEAao4rMYKrKC+BgXPHgC+/S+TU5dnPgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAOUlEQVR42mNgYGDYTwDvYSBC0TJiFFURo8iKkKIJDFCAz8Gq+BTtgVmDS9EEZBNgYA/Um1XoumEAALvML5O9Y9QFAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAABCAYAAAAMwoR9AAAAD0lEQVR42mNgYGDYTwgDAHJMBrhaYxqVAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAABCAYAAAAMwoR9AAAAD0lEQVR42mNgYGDYTwgDAHJMBrhaYxqVAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAJCAYAAADzRkbkAAAAEElEQVR42mNgYGDYz0AkAQCNMAa43a/9mgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAP0lEQVR42mNgwA6sgLgKiJcB8R50SVUgngDE+9Ewiu49WBTsRzYBlwK4ogl4FOyHWbOfkKIqYhQtI0bRHkKKAJyGL5OgbXm4AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAPUlEQVR42mNgYGDYA8TLgLgKiK0YcID9aHgCEKsSUrQfaroVIUUwhaqEFMGsJqhoP8xaQoqqiFG0jBhFewCZVS+TqMU7fwAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'background': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.info.InfoDefaultAppearance.InfoTableFrameResources::style()();
    }
    return null;
  }-*/;
}
