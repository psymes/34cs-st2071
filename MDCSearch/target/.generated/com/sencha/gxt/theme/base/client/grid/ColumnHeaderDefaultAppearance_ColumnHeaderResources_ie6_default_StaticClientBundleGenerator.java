package com.sencha.gxt.theme.base.client.grid;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources {
  private static ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator _instance0 = new ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator();
  private void columnHeaderInitializer() {
    columnHeader = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "columnHeader",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(externalImage),
      0, 0, 2, 24, false, false
    );
  }
  private static class columnHeaderInitializer {
    static {
      _instance0.columnHeaderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return columnHeader;
    }
  }
  public com.google.gwt.resources.client.ImageResource columnHeader() {
    return columnHeaderInitializer.get();
  }
  private void columnHeaderOverInitializer() {
    columnHeaderOver = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "columnHeaderOver",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal0),
      0, 0, 2, 22, false, false
    );
  }
  private static class columnHeaderOverInitializer {
    static {
      _instance0.columnHeaderOverInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return columnHeaderOver;
    }
  }
  public com.google.gwt.resources.client.ImageResource columnHeaderOver() {
    return columnHeaderOverInitializer.get();
  }
  private void columnMoveBottomInitializer() {
    columnMoveBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "columnMoveBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      61, 5, 9, 9, false, false
    );
  }
  private static class columnMoveBottomInitializer {
    static {
      _instance0.columnMoveBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return columnMoveBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource columnMoveBottom() {
    return columnMoveBottomInitializer.get();
  }
  private void columnMoveTopInitializer() {
    columnMoveTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "columnMoveTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      48, 5, 9, 9, false, false
    );
  }
  private static class columnMoveTopInitializer {
    static {
      _instance0.columnMoveTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return columnMoveTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource columnMoveTop() {
    return columnMoveTopInitializer.get();
  }
  private void columnsIconInitializer() {
    columnsIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "columnsIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      32, 0, 16, 16, false, false
    );
  }
  private static class columnsIconInitializer {
    static {
      _instance0.columnsIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return columnsIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource columnsIcon() {
    return columnsIconInitializer.get();
  }
  private void sortAscInitializer() {
    sortAsc = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "sortAsc",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      61, 0, 13, 5, false, false
    );
  }
  private static class sortAscInitializer {
    static {
      _instance0.sortAscInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return sortAsc;
    }
  }
  public com.google.gwt.resources.client.ImageResource sortAsc() {
    return sortAscInitializer.get();
  }
  private void sortAscendingIconInitializer() {
    sortAscendingIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "sortAscendingIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      16, 0, 16, 16, false, false
    );
  }
  private static class sortAscendingIconInitializer {
    static {
      _instance0.sortAscendingIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return sortAscendingIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource sortAscendingIcon() {
    return sortAscendingIconInitializer.get();
  }
  private void sortDescInitializer() {
    sortDesc = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "sortDesc",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      48, 0, 13, 5, false, false
    );
  }
  private static class sortDescInitializer {
    static {
      _instance0.sortDescInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return sortDesc;
    }
  }
  public com.google.gwt.resources.client.ImageResource sortDesc() {
    return sortDescInitializer.get();
  }
  private void sortDescendingIconInitializer() {
    sortDescendingIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "sortDescendingIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 16, 16, false, false
    );
  }
  private static class sortDescendingIconInitializer {
    static {
      _instance0.sortDescendingIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return sortDescendingIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource sortDescendingIcon() {
    return sortDescendingIconInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDPHC{background:" + ("repeat-x"+ " " +"0"+ " " +"bottom")  + ";cursor:" + ("default")  + ";zoom:" + ("1")  + ";padding:" + ("0"+ " " +"0"+ " " +"0"+ " " +"0")  + ";background-color:" + ("#f9f9f9")  + ";height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getTop() + "px  repeat-x")  + ";background-color:" + ("#f9f9f9")  + ";height:" + ("auto")  + ";background-position:") + (("bottom")  + ";position:" + ("relative")  + ";}.GH5EYDXDOHC .GH5EYDXDJHC{line-height:" + ("15px")  + ";vertical-align:" + ("middle")  + ";border-right:" + ("1px"+ " " +"solid")  + ";border-left:" + ("1px"+ " " +"solid")  + ";-moz-outline:" + ("none")  + ";-moz-user-focus:" + ("normal")  + ";outline:" + ("0"+ " " +"none")  + ";font:" + ("11px"+ " " +"arial"+ ","+ " " +"tahoma"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";border-right-color:" + ("#eee") ) + (";border-left-color:" + ("#d0d0d0")  + ";}.GH5EYDXDLHC{overflow:" + ("hidden")  + ";zoom:" + ("1")  + ";float:" + ("right")  + ";cursor:" + ("inherit")  + ";position:" + ("relative")  + ";vertical-align:" + ("middle")  + ";overflow:" + ("hidden")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";padding:") + (("4px"+ " " +"5px"+ " " +"4px"+ " " +"3px")  + ";white-space:" + ("nowrap")  + ";}.GH5EYDXDJHC{-moz-user-select:" + ("none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.GH5EYDXDKHC{display:" + ("none")  + ";position:" + ("absolute")  + ";width:" + ("14px")  + ";background:" + ("no-repeat"+ " " +"right"+ " " +"center")  + ";left:" + ("0")  + ";top:" + ("0") ) + (";z-index:" + ("2")  + ";cursor:" + ("pointer")  + ";outline:" + ("none")  + ";background-color:" + ("#c3daf9")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/grid3-hd-btn.gif"))  + ";background-color:" + ("#c3daf9")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/grid3-hd-btn.gif"))  + ";}.GH5EYDXDNHC .GH5EYDXDKHC,.GH5EYDXDMHC .GH5EYDXDKHC{display:" + ("block")  + ";}a.GH5EYDXDKHC:hover{background-position:" + ("-14px"+ " " +"center")  + ";}td.GH5EYDXDNHC,td.GH5EYDXDCIC,td.GH5EYDXDBIC,td.GH5EYDXDMHC{border-right:" + ("1px"+ " " +"solid")  + ";border-left:") + (("1px"+ " " +"solid")  + ";}td.GH5EYDXDNHC .GH5EYDXDLHC,td.GH5EYDXDCIC .GH5EYDXDLHC,td.GH5EYDXDBIC .GH5EYDXDLHC,td.GH5EYDXDMHC .GH5EYDXDLHC{background:" + ("repeat-x"+ " " +"right"+ " " +"bottom")  + ";}td.GH5EYDXDNHC,td.GH5EYDXDCIC,td.GH5EYDXDBIC,td.GH5EYDXDMHC{border-right-color:" + ("#aaccf6")  + ";border-left-color:" + ("#aaccf6")  + ";}td.GH5EYDXDNHC .GH5EYDXDLHC,td.GH5EYDXDCIC .GH5EYDXDLHC,td.GH5EYDXDBIC .GH5EYDXDLHC,td.GH5EYDXDMHC .GH5EYDXDLHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getTop() + "px  repeat-x")  + ";background-color:" + ("#ebf3fd")  + ";background-position:" + ("right"+ " " +"bottom")  + ";}.GH5EYDXDDIC{background-repeat:" + ("no-repeat")  + ";display:" + ("none") ) + (";height:" + ("4px")  + ";width:" + ("13px")  + ";margin-right:" + ("3px")  + ";vertical-align:" + ("middle")  + ";}.GH5EYDXDBIC .GH5EYDXDDIC{display:" + ("inline")  + ";height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCIC .GH5EYDXDDIC{display:" + ("inline")  + ";height:") + (((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDHHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getTop() + "px  no-repeat")  + ";}")) : ((".GH5EYDXDPHC{background:" + ("repeat-x"+ " " +"0"+ " " +"bottom")  + ";cursor:" + ("default")  + ";zoom:" + ("1")  + ";padding:" + ("0"+ " " +"0"+ " " +"0"+ " " +"0")  + ";background-color:" + ("#f9f9f9")  + ";height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeader()).getTop() + "px  repeat-x")  + ";background-color:" + ("#f9f9f9")  + ";height:" + ("auto")  + ";background-position:") + (("bottom")  + ";position:" + ("relative")  + ";}.GH5EYDXDOHC .GH5EYDXDJHC{line-height:" + ("15px")  + ";vertical-align:" + ("middle")  + ";border-left:" + ("1px"+ " " +"solid")  + ";border-right:" + ("1px"+ " " +"solid")  + ";-moz-outline:" + ("none")  + ";-moz-user-focus:" + ("normal")  + ";outline:" + ("0"+ " " +"none")  + ";font:" + ("11px"+ " " +"arial"+ ","+ " " +"tahoma"+ ","+ " " +"helvetica"+ ","+ " " +"sans-serif")  + ";border-left-color:" + ("#eee") ) + (";border-right-color:" + ("#d0d0d0")  + ";}.GH5EYDXDLHC{overflow:" + ("hidden")  + ";zoom:" + ("1")  + ";float:" + ("left")  + ";cursor:" + ("inherit")  + ";position:" + ("relative")  + ";vertical-align:" + ("middle")  + ";overflow:" + ("hidden")  + ";-o-text-overflow:" + ("ellipsis")  + ";text-overflow:" + ("ellipsis")  + ";padding:") + (("4px"+ " " +"3px"+ " " +"4px"+ " " +"5px")  + ";white-space:" + ("nowrap")  + ";}.GH5EYDXDJHC{-moz-user-select:" + ("none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.GH5EYDXDKHC{display:" + ("none")  + ";position:" + ("absolute")  + ";width:" + ("14px")  + ";background:" + ("no-repeat"+ " " +"left"+ " " +"center")  + ";right:" + ("0")  + ";top:" + ("0") ) + (";z-index:" + ("2")  + ";cursor:" + ("pointer")  + ";outline:" + ("none")  + ";background-color:" + ("#c3daf9")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/grid3-hd-btn.gif"))  + ";background-color:" + ("#c3daf9")  + ";background-image:" + (com.sencha.gxt.core.client.util.ImageHelper.createModuleBasedUrl("base/images/grid/grid3-hd-btn.gif"))  + ";}.GH5EYDXDNHC .GH5EYDXDKHC,.GH5EYDXDMHC .GH5EYDXDKHC{display:" + ("block")  + ";}a.GH5EYDXDKHC:hover{background-position:" + ("-14px"+ " " +"center")  + ";}td.GH5EYDXDNHC,td.GH5EYDXDCIC,td.GH5EYDXDBIC,td.GH5EYDXDMHC{border-left:" + ("1px"+ " " +"solid")  + ";border-right:") + (("1px"+ " " +"solid")  + ";}td.GH5EYDXDNHC .GH5EYDXDLHC,td.GH5EYDXDCIC .GH5EYDXDLHC,td.GH5EYDXDBIC .GH5EYDXDLHC,td.GH5EYDXDMHC .GH5EYDXDLHC{background:" + ("repeat-x"+ " " +"left"+ " " +"bottom")  + ";}td.GH5EYDXDNHC,td.GH5EYDXDCIC,td.GH5EYDXDBIC,td.GH5EYDXDMHC{border-left-color:" + ("#aaccf6")  + ";border-right-color:" + ("#aaccf6")  + ";}td.GH5EYDXDNHC .GH5EYDXDLHC,td.GH5EYDXDCIC .GH5EYDXDLHC,td.GH5EYDXDBIC .GH5EYDXDLHC,td.GH5EYDXDMHC .GH5EYDXDLHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnHeaderOver()).getTop() + "px  repeat-x")  + ";background-color:" + ("#ebf3fd")  + ";background-position:" + ("left"+ " " +"bottom")  + ";}.GH5EYDXDDIC{background-repeat:" + ("no-repeat")  + ";display:" + ("none") ) + (";height:" + ("4px")  + ";width:" + ("13px")  + ";margin-left:" + ("3px")  + ";vertical-align:" + ("middle")  + ";}.GH5EYDXDBIC .GH5EYDXDDIC{display:" + ("inline")  + ";height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortAsc()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDCIC .GH5EYDXDDIC{display:" + ("inline")  + ";height:") + (((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.sortDesc()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDIHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveTop()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDHHC{height:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getHeight() + "px")  + ";width:" + ((ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getSafeUri().asString() + "\") -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getLeft() + "px -" + (ColumnHeaderDefaultAppearance_ColumnHeaderResources_ie6_default_StaticClientBundleGenerator.this.columnMoveBottom()).getTop() + "px  no-repeat")  + ";}"));
      }
      public java.lang.String columnMoveBottom(){
        return "GH5EYDXDHHC";
      }
      public java.lang.String columnMoveTop(){
        return "GH5EYDXDIHC";
      }
      public java.lang.String head(){
        return "GH5EYDXDJHC";
      }
      public java.lang.String headButton(){
        return "GH5EYDXDKHC";
      }
      public java.lang.String headInner(){
        return "GH5EYDXDLHC";
      }
      public java.lang.String headMenuOpen(){
        return "GH5EYDXDMHC";
      }
      public java.lang.String headOver(){
        return "GH5EYDXDNHC";
      }
      public java.lang.String headRow(){
        return "GH5EYDXDOHC";
      }
      public java.lang.String header(){
        return "GH5EYDXDPHC";
      }
      public java.lang.String headerInner(){
        return "GH5EYDXDAIC";
      }
      public java.lang.String sortAsc(){
        return "GH5EYDXDBIC";
      }
      public java.lang.String sortDesc(){
        return "GH5EYDXDCIC";
      }
      public java.lang.String sortIcon(){
        return "GH5EYDXDDIC";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "60053A3C374CD33DFA7FC43DED4B5AED.cache.png";
  private static final java.lang.String bundledImage_Horizontal0 = GWT.getModuleBaseURL() + "5C414D8A5FC47CF4B736ADE16C57060A.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "CB3615E3031D2AEB8584EAD4D77A1C7D.cache.png";
  private static final java.lang.String externalImage = GWT.getModuleBaseURL() + "60053A3C374CD33DFA7FC43DED4B5AED.cache.png";
  private static com.google.gwt.resources.client.ImageResource columnHeader;
  private static com.google.gwt.resources.client.ImageResource columnHeaderOver;
  private static com.google.gwt.resources.client.ImageResource columnMoveBottom;
  private static com.google.gwt.resources.client.ImageResource columnMoveTop;
  private static com.google.gwt.resources.client.ImageResource columnsIcon;
  private static com.google.gwt.resources.client.ImageResource sortAsc;
  private static com.google.gwt.resources.client.ImageResource sortAscendingIcon;
  private static com.google.gwt.resources.client.ImageResource sortDesc;
  private static com.google.gwt.resources.client.ImageResource sortDescendingIcon;
  private static com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      columnHeader(), 
      columnHeaderOver(), 
      columnMoveBottom(), 
      columnMoveTop(), 
      columnsIcon(), 
      sortAsc(), 
      sortAscendingIcon(), 
      sortDesc(), 
      sortDescendingIcon(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("columnHeader", columnHeader());
        resourceMap.put("columnHeaderOver", columnHeaderOver());
        resourceMap.put("columnMoveBottom", columnMoveBottom());
        resourceMap.put("columnMoveTop", columnMoveTop());
        resourceMap.put("columnsIcon", columnsIcon());
        resourceMap.put("sortAsc", sortAsc());
        resourceMap.put("sortAscendingIcon", sortAscendingIcon());
        resourceMap.put("sortDesc", sortDesc());
        resourceMap.put("sortDescendingIcon", sortDescendingIcon());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'columnHeader': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::columnHeader()();
      case 'columnHeaderOver': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::columnHeaderOver()();
      case 'columnMoveBottom': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::columnMoveBottom()();
      case 'columnMoveTop': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::columnMoveTop()();
      case 'columnsIcon': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::columnsIcon()();
      case 'sortAsc': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::sortAsc()();
      case 'sortAscendingIcon': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::sortAscendingIcon()();
      case 'sortDesc': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::sortDesc()();
      case 'sortDescendingIcon': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::sortDescendingIcon()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.grid.ColumnHeaderDefaultAppearance.ColumnHeaderResources::style()();
    }
    return null;
  }-*/;
}
