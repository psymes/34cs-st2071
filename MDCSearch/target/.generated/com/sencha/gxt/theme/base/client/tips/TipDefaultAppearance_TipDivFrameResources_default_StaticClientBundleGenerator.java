package com.sencha.gxt.theme.base.client.tips;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator implements com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources {
  private static TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator _instance0 = new TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator();
  private void anchorBottomInitializer() {
    anchorBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorBottom",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      9, 0, 9, 10, false, false
    );
  }
  private static class anchorBottomInitializer {
    static {
      _instance0.anchorBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorBottom() {
    return anchorBottomInitializer.get();
  }
  private void anchorLeftInitializer() {
    anchorLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      28, 0, 10, 9, false, false
    );
  }
  private static class anchorLeftInitializer {
    static {
      _instance0.anchorLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorLeft() {
    return anchorLeftInitializer.get();
  }
  private void anchorRightInitializer() {
    anchorRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      18, 0, 10, 9, false, false
    );
  }
  private static class anchorRightInitializer {
    static {
      _instance0.anchorRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorRight() {
    return anchorRightInitializer.get();
  }
  private void anchorTopInitializer() {
    anchorTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "anchorTop",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 9, 10, false, false
    );
  }
  private static class anchorTopInitializer {
    static {
      _instance0.anchorTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return anchorTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource anchorTop() {
    return anchorTopInitializer.get();
  }
  private void backgroundInitializer() {
    background = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "background",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 1, 1, false, false
    );
  }
  private static class backgroundInitializer {
    static {
      _instance0.backgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return background;
    }
  }
  public com.google.gwt.resources.client.ImageResource background() {
    return backgroundInitializer.get();
  }
  private void bottomBorderInitializer() {
    bottomBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both0),
      0, 0, 1, 3, false, false
    );
  }
  private static class bottomBorderInitializer {
    static {
      _instance0.bottomBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBorder() {
    return bottomBorderInitializer.get();
  }
  private void bottomLeftBorderInitializer() {
    bottomLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both1),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomLeftBorderInitializer {
    static {
      _instance0.bottomLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeftBorder() {
    return bottomLeftBorderInitializer.get();
  }
  private void bottomRightBorderInitializer() {
    bottomRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both2),
      0, 0, 3, 3, false, false
    );
  }
  private static class bottomRightBorderInitializer {
    static {
      _instance0.bottomRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRightBorder() {
    return bottomRightBorderInitializer.get();
  }
  private void leftBorderInitializer() {
    leftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "leftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      0, 0, 3, 1, false, false
    );
  }
  private static class leftBorderInitializer {
    static {
      _instance0.leftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return leftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource leftBorder() {
    return leftBorderInitializer.get();
  }
  private void rightBorderInitializer() {
    rightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "rightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both3),
      0, 0, 3, 1, false, false
    );
  }
  private static class rightBorderInitializer {
    static {
      _instance0.rightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return rightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource rightBorder() {
    return rightBorderInitializer.get();
  }
  private void topBorderInitializer() {
    topBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 1, 3, false, false
    );
  }
  private static class topBorderInitializer {
    static {
      _instance0.topBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topBorder() {
    return topBorderInitializer.get();
  }
  private void topLeftBorderInitializer() {
    topLeftBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeftBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      38, 0, 3, 3, false, false
    );
  }
  private static class topLeftBorderInitializer {
    static {
      _instance0.topLeftBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeftBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeftBorder() {
    return topLeftBorderInitializer.get();
  }
  private void topRightBorderInitializer() {
    topRightBorder = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRightBorder",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both4),
      0, 0, 3, 3, false, false
    );
  }
  private static class topRightBorderInitializer {
    static {
      _instance0.topRightBorderInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRightBorder;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRightBorder() {
    return topRightBorderInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipNestedDivFrameStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDLMB{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDKMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDBNB{height:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-right:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDANB{height:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDCNB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat") ) + (";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-left:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDIMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-right:") + ((bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDHMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px") ) + (";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDJMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("left"+ " " +"bottom")  + ";padding-left:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";line-height:") + (("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDMMB{width:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-right:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDPMB{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("left"+ " " +"0")  + ";padding-left:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDKMB{height:" + ("100%")  + ";}")) : ((".GH5EYDXDLMB{position:" + ("relative")  + ";outline:" + ("none")  + ";}.GH5EYDXDKMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.background()).getTop() + "px  repeat")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDBNB{height:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getHeight() + "px")  + ";width:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topLeftBorder()).getTop() + "px  no-repeat")  + ";padding-left:" + (topLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDANB{height:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topBorder()).getTop() + "px  repeat-x")  + ";width:" + ("auto")  + ";}.GH5EYDXDCNB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.topRightBorder()).getTop() + "px  repeat") ) + (";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"0")  + ";zoom:" + ("1")  + ";padding-right:" + (topRightBorder().getWidth() + "px")  + ";width:" + ("auto")  + ";}.GH5EYDXDIMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomLeftBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";width:" + ("auto")  + ";background-position:" + ("0"+ " " +"bottom")  + ";padding-left:") + ((bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDHMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomBorder()).getTop() + "px  repeat")  + ";background-position:" + ("0"+ " " +"bottom")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";overflow:" + ("visible")  + ";height:" + (bottomBorder().getHeight() + "px") ) + (";line-height:" + ("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDJMB{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.bottomRightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("no-repeat")  + ";background-position:" + ("right"+ " " +"bottom")  + ";padding-right:" + (bottomRightBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";line-height:") + (("1px")  + ";font-size:" + ("1px")  + ";}.GH5EYDXDMMB{width:" + ((TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.leftBorder()).getTop() + "px  repeat-y")  + ";padding-left:" + (bottomLeftBorder().getWidth() + "px")  + ";zoom:" + ("1")  + ";overflow:" + ("visible")  + ";height:" + ("auto")  + ";width:" + ("auto")  + ";}.GH5EYDXDPMB{overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getSafeUri().asString() + "\") -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getLeft() + "px -" + (TipDefaultAppearance_TipDivFrameResources_default_StaticClientBundleGenerator.this.rightBorder()).getTop() + "px  repeat")  + ";background-repeat:" + ("repeat-y")  + ";background-position:" + ("right"+ " " +"0")  + ";padding-right:" + (rightBorder().getWidth() + "px")  + ";overflow:" + ("visible")  + ";width:" + ("auto")  + ";height:" + ("auto")  + ";}.GH5EYDXDKMB{height:" + ("100%")  + ";}"));
      }
      public java.lang.String bodyWrap(){
        return "GH5EYDXDGMB";
      }
      public java.lang.String bottom(){
        return "GH5EYDXDHMB";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDIMB";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDJMB";
      }
      public java.lang.String content(){
        return "GH5EYDXDKMB";
      }
      public java.lang.String contentArea(){
        return "GH5EYDXDLMB";
      }
      public java.lang.String left(){
        return "GH5EYDXDMMB";
      }
      public java.lang.String over(){
        return "GH5EYDXDNMB";
      }
      public java.lang.String pressed(){
        return "GH5EYDXDOMB";
      }
      public java.lang.String right(){
        return "GH5EYDXDPMB";
      }
      public java.lang.String top(){
        return "GH5EYDXDANB";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDBNB";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDCNB";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipNestedDivFrameStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipNestedDivFrameStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "67B35088EAC0D7E6378AA8291B1B0A00.cache.png";
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "EDB226FA2FE845246F4FFBEE2748A8C4.cache.png";
  private static final java.lang.String bundledImage_Both0 = GWT.getModuleBaseURL() + "6E0BA51DEB3EF5201D2CAA2BFFC2BC7A.cache.png";
  private static final java.lang.String bundledImage_Both1 = GWT.getModuleBaseURL() + "BED4672804A050812679A843E933036A.cache.png";
  private static final java.lang.String bundledImage_Both2 = GWT.getModuleBaseURL() + "516D8569A08260A0ECD80210793632F3.cache.png";
  private static final java.lang.String bundledImage_Vertical = GWT.getModuleBaseURL() + "2A5A01D533C5126C392292BD0321557B.cache.png";
  private static final java.lang.String bundledImage_Both3 = GWT.getModuleBaseURL() + "C53515FA40A9E4B49B7C7CC7BAC5B5DB.cache.png";
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "EFBFFBDECF97DA4C46F2B447678733CE.cache.png";
  private static final java.lang.String bundledImage_Both4 = GWT.getModuleBaseURL() + "D68B2C21D59433D01001851DBC985568.cache.png";
  private static com.google.gwt.resources.client.ImageResource anchorBottom;
  private static com.google.gwt.resources.client.ImageResource anchorLeft;
  private static com.google.gwt.resources.client.ImageResource anchorRight;
  private static com.google.gwt.resources.client.ImageResource anchorTop;
  private static com.google.gwt.resources.client.ImageResource background;
  private static com.google.gwt.resources.client.ImageResource bottomBorder;
  private static com.google.gwt.resources.client.ImageResource bottomLeftBorder;
  private static com.google.gwt.resources.client.ImageResource bottomRightBorder;
  private static com.google.gwt.resources.client.ImageResource leftBorder;
  private static com.google.gwt.resources.client.ImageResource rightBorder;
  private static com.google.gwt.resources.client.ImageResource topBorder;
  private static com.google.gwt.resources.client.ImageResource topLeftBorder;
  private static com.google.gwt.resources.client.ImageResource topRightBorder;
  private static com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipNestedDivFrameStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      anchorBottom(), 
      anchorLeft(), 
      anchorRight(), 
      anchorTop(), 
      background(), 
      bottomBorder(), 
      bottomLeftBorder(), 
      bottomRightBorder(), 
      leftBorder(), 
      rightBorder(), 
      topBorder(), 
      topLeftBorder(), 
      topRightBorder(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("anchorBottom", anchorBottom());
        resourceMap.put("anchorLeft", anchorLeft());
        resourceMap.put("anchorRight", anchorRight());
        resourceMap.put("anchorTop", anchorTop());
        resourceMap.put("background", background());
        resourceMap.put("bottomBorder", bottomBorder());
        resourceMap.put("bottomLeftBorder", bottomLeftBorder());
        resourceMap.put("bottomRightBorder", bottomRightBorder());
        resourceMap.put("leftBorder", leftBorder());
        resourceMap.put("rightBorder", rightBorder());
        resourceMap.put("topBorder", topBorder());
        resourceMap.put("topLeftBorder", topLeftBorder());
        resourceMap.put("topRightBorder", topRightBorder());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'anchorBottom': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorBottom()();
      case 'anchorLeft': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorLeft()();
      case 'anchorRight': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorRight()();
      case 'anchorTop': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::anchorTop()();
      case 'background': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::background()();
      case 'bottomBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::bottomBorder()();
      case 'bottomLeftBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::bottomLeftBorder()();
      case 'bottomRightBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::bottomRightBorder()();
      case 'leftBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::leftBorder()();
      case 'rightBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::rightBorder()();
      case 'topBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::topBorder()();
      case 'topLeftBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::topLeftBorder()();
      case 'topRightBorder': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::topRightBorder()();
      case 'style': return this.@com.sencha.gxt.theme.base.client.tips.TipDefaultAppearance.TipDivFrameResources::style()();
    }
    return null;
  }-*/;
}
