package com.sencha.gxt.widget.core.client.form.error;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class SideErrorHandler_SideErrorResources_default_StaticClientBundleGenerator implements com.sencha.gxt.widget.core.client.form.error.SideErrorHandler.SideErrorResources {
  private static SideErrorHandler_SideErrorResources_default_StaticClientBundleGenerator _instance0 = new SideErrorHandler_SideErrorResources_default_StaticClientBundleGenerator();
  private void errorIconInitializer() {
    errorIcon = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "errorIcon",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 16, 16, false, false
    );
  }
  private static class errorIconInitializer {
    static {
      _instance0.errorIconInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return errorIcon;
    }
  }
  public com.google.gwt.resources.client.ImageResource errorIcon() {
    return errorIconInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "D4A6540E96842E03327A041C241D2B63.cache.png";
  private static com.google.gwt.resources.client.ImageResource errorIcon;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      errorIcon(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("errorIcon", errorIcon());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'errorIcon': return this.@com.sencha.gxt.widget.core.client.form.error.SideErrorHandler.SideErrorResources::errorIcon()();
    }
    return null;
  }-*/;
}
