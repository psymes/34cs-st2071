package com.sencha.gxt.data.shared.loader;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class PagingLoadResultBean_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getOffset(com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.PagingLoadResultBean::offset;
  }-*/;
  
  private static native void setOffset(com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance, int value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.PagingLoadResultBean::offset = value;
  }-*/;
  
  private static native int getTotalLength(com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance) /*-{
    return instance.@com.sencha.gxt.data.shared.loader.PagingLoadResultBean::totalLength;
  }-*/;
  
  private static native void setTotalLength(com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance, int value) 
  /*-{
    instance.@com.sencha.gxt.data.shared.loader.PagingLoadResultBean::totalLength = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance) throws SerializationException {
    setOffset(instance, streamReader.readInt());
    setTotalLength(instance, streamReader.readInt());
    
    com.sencha.gxt.data.shared.loader.ListLoadResultBean_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.sencha.gxt.data.shared.loader.PagingLoadResultBean instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.sencha.gxt.data.shared.loader.PagingLoadResultBean();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.sencha.gxt.data.shared.loader.PagingLoadResultBean instance) throws SerializationException {
    streamWriter.writeInt(getOffset(instance));
    streamWriter.writeInt(getTotalLength(instance));
    
    com.sencha.gxt.data.shared.loader.ListLoadResultBean_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.sencha.gxt.data.shared.loader.PagingLoadResultBean_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.PagingLoadResultBean_FieldSerializer.deserialize(reader, (com.sencha.gxt.data.shared.loader.PagingLoadResultBean)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.sencha.gxt.data.shared.loader.PagingLoadResultBean_FieldSerializer.serialize(writer, (com.sencha.gxt.data.shared.loader.PagingLoadResultBean)object);
  }
  
}
