package com.sencha.gxt.core.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator implements com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultResources {
  private static CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator _instance0 = new CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator();
  private void shimInitializer() {
    shim = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "shim",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 1, 1, false, false
    );
  }
  private static class shimInitializer {
    static {
      _instance0.shimInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return shim;
    }
  }
  public com.google.gwt.resources.client.ImageResource shim() {
    return shimInitializer.get();
  }
  private void stylesInitializer() {
    styles = new com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultStyles() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "styles";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDFK,.GH5EYDXDFK:focus{outline:" + ("none")  + ";}.GH5EYDXDIK{position:" + ("absolute")  + " !important;right:" + ("0")  + ";top:" + ("0")  + ";}.GH5EYDXDAK{display:" + ("none")  + " !important;}.GH5EYDXDBK{position:" + ("absolute")  + " !important;right:" + ("-10000px")  + " !important;top:" + ("-10000px")  + " !important;visibility:" + ("hidden")  + " !important;}.GH5EYDXDCK{visibility:" + ("hidden")  + " !important;}.GH5EYDXDLK,.GH5EYDXDLK *{-moz-user-select:") + (("none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.GH5EYDXDMK{-moz-user-select:" + ("-moz-none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.x-clear{clear:" + ("both")  + ";overflow:" + ("hidden")  + ";line-height:" + ("0")  + ";font-size:" + ("0")  + ";}.GH5EYDXDPJ{float:" + ("left") ) + (";}.GH5EYDXDMJ{cursor:" + ("w-resize")  + ";cursor:" + ("col-resize")  + ";}.GH5EYDXDNJ{cursor:" + ("n-resize")  + ";cursor:" + ("row-resize")  + ";}.GH5EYDXDEK{position:" + ("relative")  + ";display:" + ("inline-block")  + ";display:" + ("inline")  + ";}.GH5EYDXDHK{white-space:" + ("nowrap")  + ";}.GH5EYDXDKK{height:" + ((CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getHeight() + "px")  + ";width:" + ((CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getSafeUri().asString() + "\") -" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getLeft() + "px -" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getTop() + "px  no-repeat")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";display:" + ("none")  + ";position:" + ("absolute")  + ";right:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("20000")  + ";}.GH5EYDXDOJ *{color:" + ("gray")  + " !important;cursor:" + ("default") ) + (" !important;}.GH5EYDXDOJ{cursor:" + ("default")  + " !important;filter:" + ("alpha(opacity=60)")  + ";}")) : ((".GH5EYDXDFK,.GH5EYDXDFK:focus{outline:" + ("none")  + ";}.GH5EYDXDIK{position:" + ("absolute")  + " !important;left:" + ("0")  + ";top:" + ("0")  + ";}.GH5EYDXDAK{display:" + ("none")  + " !important;}.GH5EYDXDBK{position:" + ("absolute")  + " !important;left:" + ("-10000px")  + " !important;top:" + ("-10000px")  + " !important;visibility:" + ("hidden")  + " !important;}.GH5EYDXDCK{visibility:" + ("hidden")  + " !important;}.GH5EYDXDLK,.GH5EYDXDLK *{-moz-user-select:") + (("none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.GH5EYDXDMK{-moz-user-select:" + ("-moz-none")  + ";-khtml-user-select:" + ("none")  + ";-webkit-user-select:" + ("ignore")  + ";}.x-clear{clear:" + ("both")  + ";overflow:" + ("hidden")  + ";line-height:" + ("0")  + ";font-size:" + ("0")  + ";}.GH5EYDXDPJ{float:" + ("right") ) + (";}.GH5EYDXDMJ{cursor:" + ("e-resize")  + ";cursor:" + ("col-resize")  + ";}.GH5EYDXDNJ{cursor:" + ("n-resize")  + ";cursor:" + ("row-resize")  + ";}.GH5EYDXDEK{position:" + ("relative")  + ";display:" + ("inline-block")  + ";display:" + ("inline")  + ";}.GH5EYDXDHK{white-space:" + ("nowrap")  + ";}.GH5EYDXDKK{height:" + ((CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getHeight() + "px")  + ";width:" + ((CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getSafeUri().asString() + "\") -" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getLeft() + "px -" + (CommonStyles_CommonStylesDefaultAppearance_CommonDefaultResources_ie6_default_StaticClientBundleGenerator.this.shim()).getTop() + "px  no-repeat")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";display:" + ("none")  + ";position:" + ("absolute")  + ";left:" + ("0")  + ";top:" + ("0")  + ";z-index:" + ("20000")  + ";}.GH5EYDXDOJ *{color:" + ("gray")  + " !important;cursor:" + ("default") ) + (" !important;}.GH5EYDXDOJ{cursor:" + ("default")  + " !important;filter:" + ("alpha(opacity=60)")  + ";}"));
      }
      public java.lang.String clear(){
        return "x-clear";
      }
      public java.lang.String columnResize(){
        return "GH5EYDXDMJ";
      }
      public java.lang.String columnRowResize(){
        return "GH5EYDXDNJ";
      }
      public java.lang.String disabled(){
        return "GH5EYDXDOJ";
      }
      public java.lang.String floatRight(){
        return "GH5EYDXDPJ";
      }
      public java.lang.String hideDisplay(){
        return "GH5EYDXDAK";
      }
      public java.lang.String hideOffsets(){
        return "GH5EYDXDBK";
      }
      public java.lang.String hideVisibility(){
        return "GH5EYDXDCK";
      }
      public java.lang.String ignore(){
        return "GH5EYDXDDK";
      }
      public java.lang.String inlineBlock(){
        return "GH5EYDXDEK";
      }
      public java.lang.String noFocusOutline(){
        return "GH5EYDXDFK";
      }
      public java.lang.String nodrag(){
        return "GH5EYDXDGK";
      }
      public java.lang.String nowrap(){
        return "GH5EYDXDHK";
      }
      public java.lang.String positionable(){
        return "GH5EYDXDIK";
      }
      public java.lang.String repaint(){
        return "GH5EYDXDJK";
      }
      public java.lang.String shim(){
        return "GH5EYDXDKK";
      }
      public java.lang.String unselectable(){
        return "GH5EYDXDLK";
      }
      public java.lang.String unselectableSingle(){
        return "GH5EYDXDMK";
      }
    }
    ;
  }
  private static class stylesInitializer {
    static {
      _instance0.stylesInitializer();
    }
    static com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultStyles get() {
      return styles;
    }
  }
  public com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultStyles styles() {
    return stylesInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "8CB9E57BEDDE62E4F67DEB6E19F5308C.cache.png";
  private static com.google.gwt.resources.client.ImageResource shim;
  private static com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultStyles styles;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      shim(), 
      styles(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("shim", shim());
        resourceMap.put("styles", styles());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'shim': return this.@com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultResources::shim()();
      case 'styles': return this.@com.sencha.gxt.core.client.resources.CommonStyles.CommonStylesDefaultAppearance.CommonDefaultResources::styles()();
    }
    return null;
  }-*/;
}
