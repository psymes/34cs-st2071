package com.sencha.gxt.core.client.dom;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class Layer_LayerResources_default_StaticClientBundleGenerator implements com.sencha.gxt.core.client.dom.Layer.LayerResources {
  private static Layer_LayerResources_default_StaticClientBundleGenerator _instance0 = new Layer_LayerResources_default_StaticClientBundleGenerator();
  private void bottomCenterInitializer() {
    bottomCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomCenter",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 12, 1, 6, false, false
    );
  }
  private static class bottomCenterInitializer {
    static {
      _instance0.bottomCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomCenter() {
    return bottomCenterInitializer.get();
  }
  private void bottomLeftInitializer() {
    bottomLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      12, 0, 6, 6, false, false
    );
  }
  private static class bottomLeftInitializer {
    static {
      _instance0.bottomLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomLeft() {
    return bottomLeftInitializer.get();
  }
  private void bottomRightInitializer() {
    bottomRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 0, 6, 6, false, false
    );
  }
  private static class bottomRightInitializer {
    static {
      _instance0.bottomRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomRight() {
    return bottomRightInitializer.get();
  }
  private void middleCenterInitializer() {
    middleCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middleCenter",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Both),
      0, 0, 1, 1, false, false
    );
  }
  private static class middleCenterInitializer {
    static {
      _instance0.middleCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middleCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource middleCenter() {
    return middleCenterInitializer.get();
  }
  private void middleLeftInitializer() {
    middleLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middleLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      6, 0, 6, 1, false, false
    );
  }
  private static class middleLeftInitializer {
    static {
      _instance0.middleLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middleLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource middleLeft() {
    return middleLeftInitializer.get();
  }
  private void middleRightInitializer() {
    middleRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middleRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Vertical),
      0, 0, 6, 1, false, false
    );
  }
  private static class middleRightInitializer {
    static {
      _instance0.middleRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middleRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource middleRight() {
    return middleRightInitializer.get();
  }
  private void topCenterInitializer() {
    topCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topCenter",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_Horizontal),
      0, 6, 1, 6, false, false
    );
  }
  private static class topCenterInitializer {
    static {
      _instance0.topCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource topCenter() {
    return topCenterInitializer.get();
  }
  private void topLeftInitializer() {
    topLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topLeft",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      6, 0, 6, 6, false, false
    );
  }
  private static class topLeftInitializer {
    static {
      _instance0.topLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource topLeft() {
    return topLeftInitializer.get();
  }
  private void topRightInitializer() {
    topRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topRight",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 6, 6, false, false
    );
  }
  private static class topRightInitializer {
    static {
      _instance0.topRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource topRight() {
    return topRightInitializer.get();
  }
  private void styleInitializer() {
    style = new com.sencha.gxt.core.client.dom.Layer.LayerStyle() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "style";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GH5EYDXDOI{position:" + ("absolute")  + ";visibility:" + ("hidden")  + ";right:" + ("0")  + ";top:" + ("0")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDMI{position:" + ("absolute")  + ";}.GH5EYDXDMI *{overflow:" + ("hidden")  + ";padding:" + ("0")  + ";border:" + ("0")  + ";margin:" + ("0")  + ";clear:") + (("none")  + ";}.GH5EYDXDAJ,.GH5EYDXDFI{height:" + ("6px")  + ";float:" + ("right")  + ";}.GH5EYDXDBJ,.GH5EYDXDCJ,.GH5EYDXDGI,.GH5EYDXDHI{width:" + ("6px")  + ";height:" + ("6px")  + ";float:" + ("right")  + ";}.GH5EYDXDII{width:" + ("100%")  + ";}.GH5EYDXDKI,.GH5EYDXDLI{width:" + ("6px")  + ";float:" + ("right")  + ";height:" + ("100%")  + ";}.GH5EYDXDJI{float:" + ("right") ) + (";height:" + ("100%")  + ";background:" + ("transparent")  + ";}.GH5EYDXDPI,.GH5EYDXDEI{height:" + ("6px")  + ";overflow:" + ("hidden")  + ";width:" + ("100%")  + ";}.GH5EYDXDBJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDAJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDCJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDKI{width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDJI{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getTop() + "px  repeat") ) + (";}.GH5EYDXDLI{width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDGI{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDFI{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDHI{height:") + (((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getTop() + "px  repeat-x")  + ";}")) : ((".GH5EYDXDOI{position:" + ("absolute")  + ";visibility:" + ("hidden")  + ";left:" + ("0")  + ";top:" + ("0")  + ";overflow:" + ("hidden")  + ";}.GH5EYDXDMI{position:" + ("absolute")  + ";}.GH5EYDXDMI *{overflow:" + ("hidden")  + ";padding:" + ("0")  + ";border:" + ("0")  + ";margin:" + ("0")  + ";clear:") + (("none")  + ";}.GH5EYDXDAJ,.GH5EYDXDFI{height:" + ("6px")  + ";float:" + ("left")  + ";}.GH5EYDXDBJ,.GH5EYDXDCJ,.GH5EYDXDGI,.GH5EYDXDHI{width:" + ("6px")  + ";height:" + ("6px")  + ";float:" + ("left")  + ";}.GH5EYDXDII{width:" + ("100%")  + ";}.GH5EYDXDKI,.GH5EYDXDLI{width:" + ("6px")  + ";float:" + ("left")  + ";height:" + ("100%")  + ";}.GH5EYDXDJI{float:" + ("left") ) + (";height:" + ("100%")  + ";background:" + ("transparent")  + ";}.GH5EYDXDPI,.GH5EYDXDEI{height:" + ("6px")  + ";overflow:" + ("hidden")  + ";width:" + ("100%")  + ";}.GH5EYDXDBJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topLeft()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDAJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topCenter()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDCJ{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.topRight()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDKI{width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleLeft()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDJI{overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleCenter()).getTop() + "px  repeat") ) + (";}.GH5EYDXDLI{width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.middleRight()).getTop() + "px  repeat-y")  + ";}.GH5EYDXDGI{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getHeight() + "px")  + ";width:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomLeft()).getTop() + "px  no-repeat")  + ";}.GH5EYDXDFI{height:" + ((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomCenter()).getTop() + "px  repeat-x")  + ";}.GH5EYDXDHI{height:") + (((Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getSafeUri().asString() + "\") -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getLeft() + "px -" + (Layer_LayerResources_default_StaticClientBundleGenerator.this.bottomRight()).getTop() + "px  repeat-x")  + ";}"));
      }
      public java.lang.String bottom(){
        return "GH5EYDXDEI";
      }
      public java.lang.String bottomCenter(){
        return "GH5EYDXDFI";
      }
      public java.lang.String bottomLeft(){
        return "GH5EYDXDGI";
      }
      public java.lang.String bottomRight(){
        return "GH5EYDXDHI";
      }
      public java.lang.String middle(){
        return "GH5EYDXDII";
      }
      public java.lang.String middleCenter(){
        return "GH5EYDXDJI";
      }
      public java.lang.String middleLeft(){
        return "GH5EYDXDKI";
      }
      public java.lang.String middleRight(){
        return "GH5EYDXDLI";
      }
      public java.lang.String shadow(){
        return "GH5EYDXDMI";
      }
      public int shadowOffset() {
        return 4;
      }
      public java.lang.String shim(){
        return "GH5EYDXDOI";
      }
      public java.lang.String top(){
        return "GH5EYDXDPI";
      }
      public java.lang.String topCenter(){
        return "GH5EYDXDAJ";
      }
      public java.lang.String topLeft(){
        return "GH5EYDXDBJ";
      }
      public java.lang.String topRight(){
        return "GH5EYDXDCJ";
      }
    }
    ;
  }
  private static class styleInitializer {
    static {
      _instance0.styleInitializer();
    }
    static com.sencha.gxt.core.client.dom.Layer.LayerStyle get() {
      return style;
    }
  }
  public com.sencha.gxt.core.client.dom.Layer.LayerStyle style() {
    return styleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_Horizontal = GWT.getModuleBaseURL() + "0EEE3FCA7616D97D664A8CA5CA7F9629.cache.png";
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "4592D5774C42CA75558E1C18963FA962.cache.png";
  private static final java.lang.String bundledImage_Both = GWT.getModuleBaseURL() + "70BD28AE3E3135CCEE677C95BFC3F58D.cache.png";
  private static final java.lang.String bundledImage_Vertical = GWT.getModuleBaseURL() + "51D92CD3C430A3C26AD4FB3AB937D2E2.cache.png";
  private static com.google.gwt.resources.client.ImageResource bottomCenter;
  private static com.google.gwt.resources.client.ImageResource bottomLeft;
  private static com.google.gwt.resources.client.ImageResource bottomRight;
  private static com.google.gwt.resources.client.ImageResource middleCenter;
  private static com.google.gwt.resources.client.ImageResource middleLeft;
  private static com.google.gwt.resources.client.ImageResource middleRight;
  private static com.google.gwt.resources.client.ImageResource topCenter;
  private static com.google.gwt.resources.client.ImageResource topLeft;
  private static com.google.gwt.resources.client.ImageResource topRight;
  private static com.sencha.gxt.core.client.dom.Layer.LayerStyle style;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      bottomCenter(), 
      bottomLeft(), 
      bottomRight(), 
      middleCenter(), 
      middleLeft(), 
      middleRight(), 
      topCenter(), 
      topLeft(), 
      topRight(), 
      style(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("bottomCenter", bottomCenter());
        resourceMap.put("bottomLeft", bottomLeft());
        resourceMap.put("bottomRight", bottomRight());
        resourceMap.put("middleCenter", middleCenter());
        resourceMap.put("middleLeft", middleLeft());
        resourceMap.put("middleRight", middleRight());
        resourceMap.put("topCenter", topCenter());
        resourceMap.put("topLeft", topLeft());
        resourceMap.put("topRight", topRight());
        resourceMap.put("style", style());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'bottomCenter': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::bottomCenter()();
      case 'bottomLeft': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::bottomLeft()();
      case 'bottomRight': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::bottomRight()();
      case 'middleCenter': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::middleCenter()();
      case 'middleLeft': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::middleLeft()();
      case 'middleRight': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::middleRight()();
      case 'topCenter': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::topCenter()();
      case 'topLeft': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::topLeft()();
      case 'topRight': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::topRight()();
      case 'style': return this.@com.sencha.gxt.core.client.dom.Layer.LayerResources::style()();
    }
    return null;
  }-*/;
}
