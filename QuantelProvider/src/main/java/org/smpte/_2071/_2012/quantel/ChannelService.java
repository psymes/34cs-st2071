package org.smpte._2071._2012.quantel;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.Acquirable;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.Lockable;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ModeSupport;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.EventBroadcaster;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.StatusSupport;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.Eventer;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public interface ChannelService extends ZoneAware, EventBroadcaster, Eventer, StatusSupport, Acquirable, Lockable, ModeSupport, Device
{
    public void setServerService(org.smpte._2071._2012.quantel.ServerService serverService);
    
    
    public org.smpte._2071._2012.quantel.ServerService getServerService();
    
    
    public boolean isUp();
    
    
    public float getFrameRate();


    public FramedTime getReferenceTime();
    
    
    public void setName(String name);
    
    
    public String getName();
    
    
    public void setChannelID(int channelID);

    
    public int getChannelID();
}
