package org.smpte._2071._2012.quantel.impl.v4;

import java.util.logging.Level;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.smpte._2071._2012.quantel.impl.CorbaConnectionService;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.Acquire;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.ApproveResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.BooleanResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.DeviceLockedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.DeviceNotAcquiredFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.DeviceNotLockedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.LockRequestParameters;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.NameInUseFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.RequestDeniedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.RequestExpiredFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.RequestNotFoundFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.Session;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.SessionNotFoundFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.SessionRequestParameters;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.Sessions;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.TooManySessionsFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.control.URIResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.GetStatus;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.GetStatusResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.PollResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.RegisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.URIParameter;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.UnregisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.InvalidModeFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.Mode;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ModeExceptionFault;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.Modes;
import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityExceptionFault;

import Quentin.v4.Port;
import Quentin.v4.Server;

public class ChannelService extends org.smpte._2071._2012.quantel.impl.ChannelService
{
    public void start()
    throws Exception
    {
        super.start();
        refresh();
    }
    
    
    public void connected()
    {
    }
    
    
    public void disconnected()
    {
    }


    public void refresh()
    {
        if (name != null && name.trim().length() > 0)
        {
            Server server = null;
            Port port = null;
            try
            {
                server = serverService.getConnectionService().getConnectionObject(Server.class);
                port = server.getPort(name, Math.abs(name.hashCode()));
                
                if (port != null)
                {
                    String portIOR = serverService.getConnectionService().getORB().object_to_string(port);
                    if (connectionService == null)
                    {
                        connectionService = new CorbaConnectionService();
                    }
                    
                    if (!portIOR.equals(connectionService.getIORs()))
                    {
                        connectionService.disconnect();
                        connectionService.setIORs(portIOR);
                        connectionService.setProperties(serverService.getConnectionService().getProperties());
                        connectionService.addConnectionListener(this);
                    
                        try
                        {
                            connectionService.start();
                            connectionService.connect();
                        } catch (Exception e)
                        {
                            if (log.isLoggable(Level.INFO))
                            {
                                log.log(Level.WARNING, "Error connection to port " + serverService.getServerID() + "-" + channelID + " \"" + name + "\" - " + e.getMessage(), e);
                            } else
                            {
                                log.log(Level.WARNING, "Error connection to port " + serverService.getServerID() + "-" + channelID + " \"" + name + "\" - " + e.getMessage());
                            }
                        }
                    }
                }
            } finally
            {
                if (port != null)
                {
                    port._release();
                }
            }
        } else
        {
            if (connectionService != null)
            {
                connectionService.disconnect();
            }
            
            connectionService = null;
        }
    }


    @WebMethod(action = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/EventBroadcaster/registerCallback")
    @WebResult(name = "registerCallbackResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "registerCallbackResponse")
    public RegisterCallbackResponse registerCallback(@WebParam(name = "registerCallback", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "registerCallback")
    URIParameter registerCallback)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/EventBroadcaster/unregisterCallback")
    @WebResult(name = "unregisterCallbackResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "unregisterCallbackResponse")
    public UnregisterCallbackResponse unregisterCallback(@WebParam(name = "unregisterCallback", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "unregisterCallback")
    URIParameter unregisterCallback)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/Eventer/poll")
    @WebResult(name = "pollResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "pollResponse")
    public PollResponse poll(@WebParam(name = "poll", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "poll")
    URIParameter poll)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/StatusSupport/Status")
    @WebResult(name = "getStatusResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "getStatusResponse")
    public GetStatusResponse getStatus(@WebParam(name = "getStatus", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "getStatus")
    GetStatus getStatus)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/Acquired")
    @WebResult(name = "Boolean", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")
    @RequestWrapper(localName = "getAcquired", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetAcquired")
    @ResponseWrapper(localName = "getAcquiredResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.BooleanResponse")
    public boolean getAcquired()
    {
        // TODO Auto-generated method stub
        return false;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/AcquiredBy")
    @WebResult(name = "Sessions", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    @RequestWrapper(localName = "getAcquiredBy", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetAcquiredBy")
    @ResponseWrapper(localName = "getAcquiredByResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetAcquiredByResponse")
    public Sessions getAcquiredBy()
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/acquire")
    @WebResult(name = "acquireResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "acquireResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public URIResponse acquire(@WebParam(name = "acquire", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "acquire")
    Acquire acquire)
    throws DeviceLockedFault, DeviceNotAcquiredFault, NameInUseFault,
    SecurityExceptionFault, TooManySessionsFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/approve")
    @WebResult(name = "approveResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "approveResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public ApproveResponse approve(@WebParam(name = "approve", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "approve")
    URIResponse approve)
    throws RequestExpiredFault, RequestNotFoundFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/deny")
    @RequestWrapper(localName = "deny", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.Deny")
    @ResponseWrapper(localName = "denyResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.DenyResponse")
    public void deny(@WebParam(name = "ID", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    String id, @WebParam(name = "Message", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    String message)
    throws RequestExpiredFault, RequestNotFoundFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/request")
    @WebResult(name = "requestResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "requestResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public URIResponse request(@WebParam(name = "request", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "request")
    SessionRequestParameters request)
    throws DeviceNotAcquiredFault, RequestDeniedFault, SecurityExceptionFault,
    SessionNotFoundFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/force")
    @WebResult(name = "forceResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "forceResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public URIResponse force(@WebParam(name = "force", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "force")
    SessionRequestParameters force)
    throws DeviceNotAcquiredFault, SecurityExceptionFault, SessionNotFoundFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/release")
    @WebResult(name = "releaseResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "releaseResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public BooleanResponse release(@WebParam(name = "release", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "release")
    URIResponse release)
    throws SecurityExceptionFault, SessionNotFoundFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/Locked")
    @WebResult(name = "Boolean", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")
    @RequestWrapper(localName = "getLocked", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetLocked")
    @ResponseWrapper(localName = "getLockedResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetLockedResponse")
    public boolean getLocked()
    {
        // TODO Auto-generated method stub
        return false;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/LockedBy")
    @WebResult(name = "Session", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    @RequestWrapper(localName = "getLockedBy", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetLockedBy")
    @ResponseWrapper(localName = "getLockedByResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.GetLockedByResponse")
    public Session getLockedBy()
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/lock")
    @WebResult(name = "Boolean", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")
    @RequestWrapper(localName = "lock", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.Lock")
    @ResponseWrapper(localName = "lockResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.control.BooleanResponse")
    public boolean lock(@WebParam(name = "ID", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    String id, @WebParam(name = "Name", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")
    String name)
    throws DeviceNotAcquiredFault, DeviceNotLockedFault,
    SecurityExceptionFault, SessionNotFoundFault
    {
        // TODO Auto-generated method stub
        return false;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/requestLock")
    @WebResult(name = "requestLockResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "requestLockResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public URIResponse requestLock(@WebParam(name = "requestLock", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "requestLock")
    LockRequestParameters requestLock)
    throws DeviceNotLockedFault, RequestDeniedFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/forceLock")
    @WebResult(name = "forceLockResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "forceLockResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public URIResponse forceLock(@WebParam(name = "forceLock", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "forceLock")
    LockRequestParameters forceLock)
    throws DeviceNotLockedFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/unlock")
    @WebResult(name = "unlockResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "unlockResponse")
    @SOAPBinding(parameterStyle = ParameterStyle.BARE)
    public BooleanResponse unlock(@WebParam(name = "unlock", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", partName = "unlock")
    URIResponse unlock)
    throws SecurityExceptionFault, SessionNotFoundFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/mode/ModeSupport/Modes")
    @WebResult(name = "Modes", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode")
    @RequestWrapper(localName = "getModes", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.GetModes")
    @ResponseWrapper(localName = "getModesResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.GetModesResponse")
    public Modes getModes()
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/mode/ModeSupport/ActiveMode")
    @WebResult(name = "Mode", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode")
    @RequestWrapper(localName = "getActiveMode", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.GetActiveMode")
    @ResponseWrapper(localName = "getActiveModeResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.GetActiveModeResponse")
    public Mode getActiveMode()
    {
        // TODO Auto-generated method stub
        return null;
    }


    @WebMethod(action = "http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/mode/ModeSupport/changeMode")
    @RequestWrapper(localName = "changeMode", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ChangeMode")
    @ResponseWrapper(localName = "changeModeResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode", className = "org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ChangeModeResponse")
    public void changeMode(@WebParam(name = "Mode", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/mode")
    Mode mode)
    throws InvalidModeFault, ModeExceptionFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        
    }
}
