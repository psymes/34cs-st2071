package org.smpte._2071._2012.quantel.impl;

import org.smpte._2071._2012.quantel.ServerService;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.URLs;

public abstract class PoolService extends ZoneAware implements org.smpte._2071._2012.quantel.PoolService
{
    protected ServerService serverService;
    
    protected int poolID;
    
    protected String udn;
    
    protected org.smpte_ra.schemas._2071._2012.mdcf.types.Map attributes;

    protected URLs urls;


    public void setServerService(ServerService serverService)
    {
        this.serverService = serverService;
    }
    
    
    public ServerService getServerService()
    {
        return serverService;
    }
    
    
    public boolean isUp()
    {
        return serverService.isUp();
    }
    
    
    public void setPoolID(int poolID)
    {
        this.poolID = poolID;
    }
    
    
    public int getPoolID()
    {
        return poolID;
    }
    
    
    public String getName()
    {
        return serverService.getServerName() + "-" + getPoolID();
    }
    
    
    public String getUDN()
    {
        return udn;
    }


    public URLs getURLs()
    {
        return new URLs();
    }


    public boolean getOnline()
    {
        return isUp();
    }


    public org.smpte_ra.schemas._2071._2012.mdcf.types.Map getAttributes()
    {
        return attributes;
    }


    public Capabilities getCapabilities()
    {
        return capabilities;
    }


    @Override
    public void start()
    throws Exception
    {
        super.start();
        this.udn = "urn:smpte:udn:Quantel." + getZoneService().getZoneID() + ":type=PoolService,zone=" + getZoneService().getZoneID() + ",server=" + getServerService().getServerID() + ",pool=" + getPoolID();
    }
}
