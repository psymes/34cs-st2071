package org.smpte._2071._2012.quantel.impl;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.osgi.framework.BundleContext;
import org.smpte._2071._2012.quantel.CapabilityProvider;
import org.smpte._2071._2012.quantel.CorbaConnectionService;
import org.smpte._2071._2012.quantel.ZoneService;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capability;

public class ZoneAware extends Connectable implements org.smpte._2071._2012.quantel.ZoneAware, CapabilityProvider
{
    protected Logger log = Logger.getLogger(this.getClass().getName());
    
    protected Capabilities capabilities;

    protected ZoneService zoneService;

    protected BundleContext bundleContext;
    
    
    public void setBundleContext(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }


    public BundleContext getBundleContext()
    {
        return bundleContext;
    }


    public CorbaConnectionService getConnectionService()
    {
        if (connectionService != null)
        {
            return connectionService;
        } else if (zoneService != null)
        {
            return zoneService.getConnectionService();
        } else
        {
            return null;
        }
    }
    
    
    public boolean isConnected()
    {
        if (connectionService != null)
        {
            return connectionService.isConnected();
        } else if (zoneService != null)
        {
            return zoneService.getConnectionService().isConnected();
        } else
        {
            return false;
        }
    }
    
    
    public void setZoneService(org.smpte._2071._2012.quantel.ZoneService zoneService)
    {
        this.zoneService = zoneService;
    }
    
    
    public ZoneService getZoneService()
    {
        return zoneService;
    }


    public void start()
    throws Exception
    {
    }


    public void stop()
    {
    }
    
    
    public void refresh()
    {
        // TODO Auto-generated method stub
        
    }
    
    
    public void publishedEndpoint(Class<?> implementor, String uri)
    {
        ArrayList<String> list = new ArrayList<String>();
        
        if (this.capabilities == null)
        {
            this.capabilities =  new Capabilities();
        }
        
        list.add(uri);
        Capability capability = CapabilityFactory.newCapability(implementor, list);
    
        if (capability != null)
        {
            this.capabilities.getCapability().add(capability);
        }
    }
}
