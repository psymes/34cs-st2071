package org.smpte._2071._2012.quantel;

import org.osgi.framework.BundleContext;
import org.springframework.osgi.context.BundleContextAware;

public interface OSGiService extends Startable, Stoppable, BundleContextAware
{
    public void setBundleContext(BundleContext bundleContext);
    
    
    public BundleContext getBundleContext();
}
