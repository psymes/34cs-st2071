package org.smpte._2071._2012.quantel;


public interface ZoneAware extends OSGiService, Connectable, Refreshable
{
    public void setZoneService(org.smpte._2071._2012.quantel.ZoneService zoneService);
    
    
    public org.smpte._2071._2012.quantel.ZoneService getZoneService();
}
