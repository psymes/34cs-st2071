package org.smpte._2071._2012.quantel.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.smpte._2071._2012.quantel.CapabilityProvider;
import org.smpte._2071._2012.quantel.ClipResults;
import org.smpte._2071._2012.quantel.CorbaConnectionService.ConnectionListener;
import org.smpte._2071._2012.quantel.MDCHelper;
import org.smpte._2071._2012.quantel.NotConnectedException;
import org.smpte._2071._2012.quantel.Pool;
import org.smpte._2071._2012.quantel.Pool.Factory;
import org.smpte._2071._2012.quantel.RN;
import org.smpte._2071._2012.quantel.ServerService;
import org.smpte._2071._2012.quantel.Stoppable;
import org.smpte._2071._2012.quantel.Timecode;
import org.smpte._2071._2012.quantel.query.SQLConverterFactory;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capability;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformations;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformationsResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceNotFound;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceNotFoundFault_Exception;
import org.smpte_ra.schemas._2071._2012.mdcf.device.UDNFilter;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.PollResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.RegisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.URIParameter;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.UnregisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.UDNs;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.URLs;
import org.smpte_ra.schemas._2071._2012.mdcf.media.Create;
import org.smpte_ra.schemas._2071._2012.mdcf.media.Media;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaAsset;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaContainer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaCreationFailedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaDeletionFailedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaInstance;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaList;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaListResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaNotFoundFault;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaUpdateFailedFault;
import org.smpte_ra.schemas._2071._2012.mdcf.media.UMNFilter;
import org.smpte_ra.schemas._2071._2012.mdcf.media.UMNParameter;
import org.smpte_ra.schemas._2071._2012.mdcf.media.Update;
import org.smpte_ra.schemas._2071._2012.mdcf.query.InvalidQuery;
import org.smpte_ra.schemas._2071._2012.mdcf.query.InvalidQueryFault;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.security.EXCEPTIONSTATUS;
import org.smpte_ra.schemas._2071._2012.mdcf.security.EXCEPTIONTYPE;
import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityException;
import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityExceptionFault;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DATATYPE;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;
import org.smpte_ra.schemas._2071._2012.mdcf.types.Key;
import org.smpte_ra.schemas._2071._2012.mdcf.types.Keys;

public abstract class ZoneService extends Connectable implements org.smpte._2071._2012.quantel.ZoneService, CapabilityProvider, ConnectionListener
{
    public final static String DATA_FILE = "quantel.properties";
    
    public final static String PROP_LAST_CHANGE_NUMBER = "last_change_number";
    
    protected final static ThreadGroup threadGroup = new ThreadGroup("MDC (Quantel) Async Task Threads");
    
    protected final ExecutorService executor = Executors.newCachedThreadPool(new ThreadFactory()
    {
        private int threadNumber = 1;
        
        public Thread newThread(Runnable runnable)
        {
            Thread thread =  new Thread(threadGroup, runnable, "MDC (Quantel) Zone \"" + ZoneService.this.getName() + "\" (" + ZoneService.this.getZoneID() + ") Async Task Thread " + threadNumber++);
            thread.setDaemon(true);
            return thread;
        }
    });
    
    protected final Timer timer = new Timer("MDC (Quantel) Timer", true);

    protected final HashMap<Integer, ServerService> servers = new HashMap<Integer, ServerService>();
    
    protected final HashMap<String, ServiceRegistration> services = new HashMap<String, ServiceRegistration>();

    protected final ConcurrentLinkedQueue<ClipStateChangeListener> clipListeners = new ConcurrentLinkedQueue<ClipStateChangeListener>();
    
    protected final ConcurrentLinkedQueue<ServerStateChangeListener> serverListeners = new ConcurrentLinkedQueue<ServerStateChangeListener>(); 
    
    protected final ConcurrentLinkedQueue<PoolStateChangeListener> poolListeners = new ConcurrentLinkedQueue<PoolStateChangeListener>(); 
    
    protected final ConcurrentLinkedQueue<CopyStateChangeListener> copyListeners = new ConcurrentLinkedQueue<CopyStateChangeListener>(); 
    
    protected final ConcurrentLinkedQueue<ZoneStateChangeListener> zoneListeners = new ConcurrentLinkedQueue<ZoneStateChangeListener>();
    
    protected final Properties dataProps = new Properties();
    
    protected RN udn;
    
    protected String name;

    protected URLs urls;
    
    protected org.smpte_ra.schemas._2071._2012.mdcf.types.Map attributes;
    
    protected Capabilities capabilities = new Capabilities();
    
    protected BundleContext bundleContext;

    protected boolean loadDeviceDirectory;

    protected boolean loadMediaDirectory;
    
    protected boolean loadMediaAccess;
    
    protected int zoneID;

    protected UDNs namespaces;

    protected DeviceInformation parentDeviceDirectory;

    protected MediaContainer mediaContainer;

    protected SQLConverterFactory sqlConverterFactory = new SQLConverterFactory();
    
    protected String[] searchColumns;
    
    protected String selectStatement;
    
    
    public boolean isLoadDeviceDirectory()
    {
        return loadDeviceDirectory;
    }


    public void setLoadDeviceDirectory(boolean loadDeviceDirectory)
    {
        this.loadDeviceDirectory = loadDeviceDirectory;
    }


    public boolean isLoadMediaDirectory()
    {
        return loadMediaDirectory;
    }


    public void setLoadMediaDirectory(boolean loadMediaDirectory)
    {
        this.loadMediaDirectory = loadMediaDirectory;
    }


    public boolean isLoadMediaAccess()
    {
        return loadMediaAccess;
    }


    public void setLoadMediaAccess(boolean loadMediaAccess)
    {
        this.loadMediaAccess = loadMediaAccess;
    }
    
    
    public void setBundleContext(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }
    
    
    public BundleContext getBundleContext()
    {
        return bundleContext;
    }
    
    
    public void addListener(ClipStateChangeListener listener)
    {
        clipListeners.add(listener);
    }
    
    
    public void removeListener(ClipStateChangeListener listener)
    {
        clipListeners.remove(listener);
    }
    
    
    public void addListener(ServerStateChangeListener listener)
    {
        serverListeners.add(listener);
    }
    
    
    public void removeListener(ServerStateChangeListener listener)
    {
        serverListeners.remove(listener);
    }
    
    
    public void addListener(PoolStateChangeListener listener)
    {
        poolListeners.add(listener);
    }
    
    
    public void removeListener(PoolStateChangeListener listener)
    {
        poolListeners.remove(listener);
    }
    
    
    public void addListener(CopyStateChangeListener listener)
    {
        copyListeners.add(listener);
    }
    
    
    public void removeListener(CopyStateChangeListener listener)
    {
        copyListeners.remove(listener);
    }
    
    
    public void addListener(ZoneStateChangeListener listener)
    {
        zoneListeners.add(listener);
    }
    
    
    public void removeListener(ZoneStateChangeListener listener)
    {
        zoneListeners.remove(listener);
    }
    
    
    protected void registerServerService(ServerService serverService)
    {
        serverService.setBundleContext(bundleContext);
        
        Properties props = new Properties();
        props.put("UDN", serverService.getUDN());
        props.put("ZoneID", getZoneID());
        props.put("ZoneName", getName());
        props.put("ServerID", serverService.getServerID());
        props.put("ServerName", serverService.getServerName());
        props.put("FrameRate", serverService.getFrameRate());
        ServiceRegistration serviceRegistration = bundleContext.registerService("org.smpte._2071._2012.quantel.ServerService", serverService, props);
        servers.put(serverService.getServerID(), serverService);
        services.put(Integer.toString(serverService.getServerID()), serviceRegistration);
    }
    

    public ServerService getServerService(int serverID)
    {
        return servers.get(serverID);
    }
    
    
    public void refresh()
    {
        if (state == STATE_STARTING || state == STATE_STARTED)
        {
            try
            {
                ArrayList<Integer> tempServers = new ArrayList<Integer>(servers.keySet());
                ServiceRegistration registration = null;
                
                int[] serverIDs = getServerIDs();
                System.out.println("Servers for Zone " + Arrays.toString(serverIDs));
                for (int serverID : serverIDs)
                {
                    tempServers.remove(Integer.valueOf(serverID));
    
                    if ((registration = services.get(serverID)) == null)
                    {
                        try
                        {
                            System.out.println("Creating Service for Server " + serverID);
                            ServerService serverService = createServerService(serverID);
                            System.out.println("Registering Service for Server " + serverID);
                            registerServerService(serverService);
                            serverService.start();
                        } catch (NotConnectedException e)
                        {
                            throw e;
                        } catch (Exception e)
                        {
                            if (log.isLoggable(Level.INFO))
                            {
                                log.log(Level.SEVERE, "Error starting Server Service (" + serverID + ") - " + e.getMessage(), e);
                            } else
                            {
                                log.log(Level.SEVERE, "Error starting Server Service (" + serverID + ") - " + e.getMessage());
                            }
                        }
                    } else
                    {
                        ServerService serverService = (ServerService) bundleContext.getService(registration.getReference());
                        serverService.refresh();
                        bundleContext.ungetService(registration.getReference());
                    }
                }
                
                // Remove servers that no longer exist
                for (Integer serverID : tempServers)
                {
                    ServiceRegistration serviceRegistration = services.get(serverID);
                    try
                    {
                        Stoppable stoppable = (Stoppable) bundleContext.getService(serviceRegistration.getReference());
                        serviceRegistration.unregister();
                        try
                        {
                            stoppable.stop();
                        } catch (Exception e)
                        {
                            if (log.isLoggable(Level.INFO))
                            {
                                log.log(Level.WARNING, "Error stopping Service \"" + serverID + ") - " + e.getMessage(), e);
                            } else
                            {
                                log.log(Level.WARNING, "Error stopping Service \"" + serverID + ") - " + e.getMessage());
                            }
                        }
                    } catch(Exception e)
                    {
                        if (log.isLoggable(Level.INFO))
                        {
                            log.log(Level.WARNING, "Error unregistering Service (" + serverID + ") - " + e.getMessage(), e);
                        } else
                        {
                            log.log(Level.WARNING, "Error unregistering Service (" + serverID + ") - " + e.getMessage());
                        }
                    } finally
                    {
                        services.remove(serverID);
                    }
                }
            } catch (NotConnectedException e)
            {
                System.out.println("Exception: " + e.getClass().getSimpleName() + " - " + e.getMessage());
                log.warning(e.getMessage());
            }
        } else
        {
            
        }
    }
    
    
    public void connected()
    {
        try
        {
            udn = new RN("urn:smpte:udn:Quantel." + getZoneID() + ":type=ZoneService");
        } catch (Exception e)
        {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        
        searchColumns = buildSearchColumns();
        sqlConverterFactory.clearMappings();
        sqlConverterFactory.addFieldColumnMapping("Name", "Title");
        sqlConverterFactory.addFieldColumnMapping("UMN", "concat('urn:smpte:umn:" + udn.getNamespace() + ":type=media_instance;mid=',ClipGUID,';clipid=',ClipID,';clonezone=',CloneZone,';cloneid=',CloneID)");
        sqlConverterFactory.addFieldColumnMapping("Location", "concat('urn:smpte:umn:" + udn.getNamespace() + ":type=media_container;poolid=',PoolID)");
        
        StringBuilder builder = new StringBuilder("select ");
        for (int index = 0; index < searchColumns.length; index++)
        {
            builder.append(searchColumns[index]);
            if (index + 1 < searchColumns.length)
            {
                builder.append(",");
            }
        }
        builder.append(" from clips");
        selectStatement = builder.toString();
        
        refresh();
    }
    
    
    public void disconnected()
    {
        refresh();
    }
    
    
    protected void flushProps()
    {
        File file = bundleContext.getDataFile(DATA_FILE);
        if (file != null)
        {
            FileOutputStream out = null;
            try
            {
                out = new FileOutputStream(file);
                dataProps.store(out, "Quantel " + getZoneID() + "\"" + getName() + "\" Bundle Data Properties");
                out.flush();
            } catch (IOException e)
            {
                log.log(Level.WARNING, "Error writing dta file \"" + file + "\" - " + e.getMessage(), e);
            } finally
            {
                if (out != null)
                {
                    try
                    {
                        out.close();
                    } catch (IOException e)
                    {
                        // Ignore
                    }
                }
            }
        }
    }
    
    
    public void start()
    throws Exception
    {
        super.start();
        
        try
        {
            this.connectionService.connect();
        } catch (Exception e)
        {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        
        File dataFile = bundleContext.getDataFile(DATA_FILE);
        if (dataFile != null)
        {
            try
            {
                FileInputStream in = new FileInputStream(dataFile);
                dataProps.load(in);
            } catch (IOException e)
            {
                log.log(Level.WARNING, "Error reading from \"" + dataFile.getAbsolutePath() + "\" - " + e.getMessage(), e);
            }
        }
        
        String umn = "urn:smpte:umn:Quantel." + getZoneID() + ":type=media_container";
        String parentUMN = "urn:smpte:umn:Quantel:type=media_container";
        
        DateTime date = new DateTime();
        try
        {
            GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
            calendar.add(Calendar.SECOND, 30);
            date.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
            date.setHour(calendar.get(Calendar.HOUR));
            date.setMinute(calendar.get(Calendar.MINUTE));
            date.setSecond(calendar.get(Calendar.SECOND));
            date.setMicrosecond(calendar.get(Calendar.MILLISECOND) * 1000);
            BigInteger fromEpoch = BigInteger.valueOf(calendar.getTimeInMillis()).multiply(BigInteger.valueOf(1000));
            date.setMicrosFromEpoch(fromEpoch);
        } catch (Exception e)
        {
            log.log(Level.WARNING, "Could not create the MDCF DateTime for the MediaDirectory MediaContainer field - " + e.getMessage(), e);
        }
        
        mediaContainer = new MediaContainer();
        mediaContainer.setUMN(umn);
        mediaContainer.setUDN(udn.toString());
        mediaContainer.setName(getName());
        mediaContainer.setMetadata(new org.smpte_ra.schemas._2071._2012.mdcf.types.Map());
        mediaContainer.setLocation(parentUMN);
        mediaContainer.setCreated(date);
        mediaContainer.setModified(date);
    }
    
    
    @SuppressWarnings("unchecked")
    public void stop()
    {
        super.stop();
        
        executor.shutdown();
        timer.cancel();
        
        for (Map.Entry<Integer, ServiceRegistration> entry : services.entrySet().toArray(new Map.Entry[0]))
        {
            ServiceRegistration serviceRegistration = entry.getValue();
            try
            {
                Stoppable stoppable = (Stoppable) bundleContext.getService(serviceRegistration.getReference());
                serviceRegistration.unregister();
                try
                {
                    stoppable.stop();
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + entry.getKey() + ") - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + entry.getKey() + ") - " + e.getMessage());
                    }
                }
            } catch(Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + entry.getKey() + ") - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + entry.getKey() + ") - " + e.getMessage());
                }
            } finally
            {
                services.remove(entry.getKey());
            }
        }
    }
    
    
    public void publishedEndpoint(Class<?> implementor, String uri)
    {
        ArrayList<String> list = new ArrayList<String>();
        
        if (this.capabilities == null)
        {
            this.capabilities =  new Capabilities();
        }
        
        list.add(uri);
        Capability capability = CapabilityFactory.newCapability(implementor, list);
        
        if (capability != null)
        {
            this.capabilities.getCapability().add(capability);
        }
    }
    
    
    protected List<DeviceInformation> queryDeviceInformationForServices(String query, Class<?> iface) 
    throws InvalidSyntaxException
    {
        ArrayList<DeviceInformation> devices = new ArrayList<DeviceInformation>();
        
        ServiceReference[] references = bundleContext.getAllServiceReferences(iface.getClass().getName(), query);
        for (ServiceReference ref : references)
        {
            try
            {
                Device device = (Device) bundleContext.getService(ref);
                devices.add(MDCHelper.newDeviceInformation(device));
            } finally
            {
                bundleContext.ungetService(ref);
            }
        }
        
        return devices;
    }
    

    protected List<? extends JAXBElement<? extends Media>> convertClipResultsToMedia(ClipResults clipResults)
    {
        long start = System.nanoTime();
        ArrayList<JAXBElement<? extends Media>> results = new ArrayList<JAXBElement<? extends Media>>();
        
        org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory factory = new org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory();
        
        for (int row = 0; row < clipResults.size(); row++)
        {
            results.add(factory.createMediaInstance(convertClipResultsToMediaInstance(clipResults, row)));
        }
        
        System.out.println(getClass().getSimpleName() + ".convertClipResultsToMedia() - took " + ((double) (System.nanoTime() - start) / (double) 1000000) + " millis.");
        return results;
    }

    
    protected MediaInstance convertClipResultsToMediaInstance(final ClipResults clipResults, final int row)
    {
        long start = System.nanoTime();
        
        final int zoneID = getZoneID();
        final MediaInstance mi = new MediaInstance();;
        String clipID = null;
        String cloneID = null;
        String cloneZone = null;
        String poolID = null;
        String frames = null;
        String clipGUID = null;
        String created = null;
        String modified = null;

        mi.setMetadata(new org.smpte_ra.schemas._2071._2012.mdcf.types.Map());
        mi.getMetadata().setKeys(new Keys());
        
        final HashMap<String, Integer> columnIndex = new HashMap<String, Integer>();
        String[] columns = clipResults.columns();
        for (int column = 0; column < columns.length; column++)
        {
            columnIndex.put(columns[column].toLowerCase(), column);
        }
        
        Integer column = null;
        if ((column = columnIndex.remove("title")) != null) //"title".equalsIgnoreCase(columnName))
        {
            mi.setName(clipResults.getValue(row, column));
        }

        if ((column = columnIndex.remove("clipid")) != null)
        {
            clipID = clipResults.getValue(row, column);
        }

        if ((column = columnIndex.remove("cloneid")) != null)
        {
            cloneID = clipResults.getValue(row, column);
        }

        if ((column = columnIndex.remove("clonezone")) != null)
        {
            cloneZone = clipResults.getValue(row, column);
        }

        if ((column = columnIndex.remove("poolid")) != null)
        {
            poolID = clipResults.getValue(row, column);
        }
        
        if ((column = columnIndex.remove("frames")) != null)
        {
            frames = clipResults.getValue(row, column);
        }
        
        if ((column = columnIndex.remove("clipguid")) != null)
        {
            clipGUID = clipResults.getValue(row, column);
        }

        if ((column = columnIndex.remove("created")) != null)
        {
            created = clipResults.getValue(row, column);
        }
        
        if ((column = columnIndex.remove("modified")) != null)
        {
            modified = clipResults.getValue(row, column);
        }
        
        mi.setMIMEType("quantel/clip");
        mi.setUMN("urn:smpte:umn:Quantel." + zoneID + ":type=media_instance" + (clipGUID != null ? ";mid=" + clipGUID : "") + ";clipid=" + clipID + ";clonezone=" + cloneZone + ";cloneid=" + cloneID);
        mi.setLocation("urn:smpte:umn:Quantel." + zoneID + ":type=media_container;poolid=" + poolID);
        
        // Dividing work across threads for speed on multi-core systems.
        ConcurrentLinkedQueue<Future<?>> futures = new ConcurrentLinkedQueue<Future<?>>();
        futures.add(executor.submit(new Runnable()
        {
            public void run()
            {
                for (Map.Entry<String, Integer> entry : columnIndex.entrySet())
                {
                    Key key = new Key();
                    key.setContent(clipResults.getValue(row, entry.getValue()));
                    key.setId(entry.getKey());
                    key.setType(DATATYPE.STRING);
                    mi.getMetadata().getKeys().getKey().add(key);
                }
            }
        }));
        
        
        if (frames != null && frames.trim().length() > 0)
        {
            final String frameCount = frames;
            futures.add(executor.submit(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        mi.setDuration(timecodeToFramedTime(new Timecode(30, true, Long.parseLong(frameCount))));
                    } catch (Exception e)
                    {
                        log.log(Level.WARNING, "Frames \"" + frameCount + "\" is not a valid integer number!", e);
                    }
                }
            }));
        }
        
        futures.add(executor.submit(new Runnable()
        {
            public void run()
            {
                // TODO: Determine clip size in bytes.
                mi.setSize(BigInteger.valueOf(-1));
            }
        }));
        
        final String finalCreated = created;
        futures.add(executor.submit(new Runnable()
        {
            public void run()
            {
                try
                {
                    mi.setCreated(parseDateTime(finalCreated));
                } catch (ParseException e)
                {
                    log.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }));
        
        final String finalModified = modified;
        futures.add(executor.submit(new Runnable()
        {
            public void run()
            {
                try
                {
                    mi.setModified(parseDateTime(finalModified));
                } catch (ParseException e)
                {
                    log.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }));
        
        try
        {
            Future<?> future;
            while ((future = futures.peek()) != null)
            {
                if (future.isDone() || future.isCancelled())
                {
                    futures.remove();
                } else
                {
                    try
                    {
                        Thread.sleep(1);
                    } catch (InterruptedException e)
                    {
                        // ignore
                    }
                }
            }
        } catch (NoSuchElementException e)
        {
            log.warning("NoSuchElementException processing futures in convertClipResultsToMediaInstance().  Should not have happened, but OK.");
        }
        
        System.out.println(getClass().getSimpleName() + ".convertClipResultsToMediaItem() - took " + ((double) (System.nanoTime() - start) / (double) 1000000) + " millis.");
        
        return mi;
    }
    
    private final Pool<GregorianCalendar> calendarPool = new Pool<GregorianCalendar>(10, new Factory<GregorianCalendar>()
    {
        public GregorianCalendar newInstance()
        {
            return (GregorianCalendar) GregorianCalendar.getInstance();
        }
    }); 
    
    
    protected DateTime parseDateTime(String dateStr)
    throws ParseException
    {
        if (dateStr != null && dateStr.trim().length() > 0)
        {
            org.smpte_ra.schemas._2071._2012.mdcf.types.ObjectFactory factory = new org.smpte_ra.schemas._2071._2012.mdcf.types.ObjectFactory(); 
            DateTime dateTime = factory.createDateTime();
            
            GregorianCalendar calendar = null;
            try
            {
                calendar = calendarPool.borrow();
                calendar.setTime(new Date(Long.parseLong(dateStr)));
                dateTime.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
                dateTime.setHour(calendar.get(Calendar.HOUR));
                dateTime.setMinute(calendar.get(Calendar.MINUTE));
                dateTime.setSecond(calendar.get(Calendar.SECOND));
                dateTime.setMicrosecond(calendar.get(Calendar.MILLISECOND) * 1000);
                BigInteger fromEpoch = BigInteger.valueOf(calendar.getTimeInMillis()).multiply(BigInteger.valueOf(1000));
                dateTime.setMicrosFromEpoch(fromEpoch);
            } catch (Exception e)
            {
                log.log(Level.WARNING, "Could not create the MDCF DateTime for the MediaDirectory MediaContainer field - " + e.getMessage(), e);
            } finally
            {
                if (calendar != null)
                {
                    calendarPool.giveBack(calendar);
                }
            }
            
            return dateTime;
        }
        
        return null;
    }
    
    
    protected FramedTime timecodeToFramedTime(Timecode timecode)
    throws ParseException
    {
        FramedTime framedTime = new FramedTime();
        try
        {
            framedTime.setDate(null);
            framedTime.setHour(timecode.getHours());
            framedTime.setMinute(timecode.getMinutes());
            framedTime.setSecond(timecode.getSeconds());
            framedTime.setMicrosecond(timecode.getMilliseconds() * 1000);
            framedTime.setFrameRate(timecode.getFramesPerSecond() * 1000);
            framedTime.setScale(timecode.hasDropFrames() ? 1001 : 1000);
            framedTime.setFrame(timecode.getFrames());
            framedTime.setTotalFrames(BigInteger.valueOf(timecode.getFrame()));
            BigInteger fromEpoch = BigInteger.valueOf(timecode.getMillisecond()).multiply(BigInteger.valueOf(1000));
            framedTime.setMicrosFromEpoch(fromEpoch);
        } catch (Exception e)
        {
            log.log(Level.WARNING, "Could not create the MDCF DateTime for the MediaDirectory MediaContainer field - " + e.getMessage(), e);
        }
        return framedTime;
    }


    protected abstract String[] buildSearchColumns();
    
    
    protected abstract org.smpte._2071._2012.quantel.ServerService createServerService(int serverID);
    
    
    protected abstract ClipResults queryClips(String umn, QueryExpression query)
    throws InvalidQueryFault;

    
    /**
     ***************************************************************
     *** Begin MDCF Capability Interface method implementations. *** 
     ***************************************************************
     */
    
    public String getUDN()
    {
        return udn.toString();
    }


    public URLs getURLs()
    {
        return new URLs();
    }


    public boolean getOnline()
    {
        return isUp();
    }
    
    public org.smpte_ra.schemas._2071._2012.mdcf.types.Map getAttributes()
    {
        return attributes;
    }


    public Capabilities getCapabilities()
    {
        return capabilities;
    }


    public DeviceInformation getParentDeviceDirectory()
    {
        return parentDeviceDirectory;
    }


    public UDNs getNamespaces()
    {
        return namespaces;
    }


    public DeviceInformations ancestors(String udn)
    throws DeviceNotFoundFault_Exception, SecurityExceptionFault
    {
        DeviceInformation parent = null;
        DeviceInformations ancestors = new DeviceInformations();
        
        while ((parent = parent(udn)) != null)
        {
            ancestors.getDeviceInformation().add(parent);
        }
        
        return ancestors;
    }


    public DeviceInformation lookup(String udn)
    throws DeviceNotFoundFault_Exception, SecurityExceptionFault
    {
        DeviceInformation deviceInfo = null;
        
        Bundle bundle = bundleContext.getBundle();
        ServiceReference reference = bundleContext.getServiceReference("(UDN=" + udn + ")");
        if (reference != null && reference.isAssignableTo(bundle, Device.class.getName()))
        {
            try
            {
                Device device = (Device) bundleContext.getService(reference);
                if (device != null)
                {
                    deviceInfo = MDCHelper.newDeviceInformation(device);
                }
            } finally
            {
                bundleContext.ungetService(reference);
            }
        }
        
        return deviceInfo;
    }


    public DeviceInformation parent(String udn)
    throws DeviceNotFoundFault_Exception, SecurityExceptionFault
    {
        ServiceReference reference = null;
        List<DeviceInformation> list = null;
        
        try
        {
            reference = bundleContext.getServiceReference("(UDN=" + udn + ")");
            if (reference != null)
            {
                Bundle bundle = bundleContext.getBundle();
                
                if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ServerService.class.getName()))
                {
                    list = queryDeviceInformationForServices("(ZoneID=" + getZoneID() + ")", org.smpte._2071._2012.quantel.ZoneService.class);
                } else if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.PoolService.class.getName()) || 
                           reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ChannelService.class.getName()))
                {
                    Object serverID = reference.getProperty("ServerID");
                    list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(ServerID=" + serverID + "))", org.smpte._2071._2012.quantel.ServerService.class);
                }
            } else
            {
                DeviceNotFound nf = new DeviceNotFound();
                nf.setUDN(udn);
                nf.setMessage("No Device Found for UDN \"" + udn + "\"!");
                throw new DeviceNotFoundFault_Exception("", nf);
            }
        } catch (InvalidSyntaxException e)
        {
            log.log(Level.SEVERE, "Could not query services - " + e.getMessage(), e);
        }
        
        if (list != null)
        {
            if (list.size() == 1)
            {
                return list.get(0);
            } else
            {
                DeviceNotFound nf = new DeviceNotFound();
                nf.setUDN(udn);
                nf.setMessage("Too Many Devices Found for query!");
                throw new DeviceNotFoundFault_Exception("Too Many Devices Found for query!", nf);
            }
        } else
        {
            return null;
        }
    }


    public DeviceInformationsResponse children(UDNFilter udnFilter)
    throws DeviceNotFoundFault_Exception, InvalidQueryFault,
    SecurityExceptionFault
    {
        JAXBElement<? extends QueryExpression> queryElement = udnFilter.getQueryExpression();
        String udn = udnFilter.getUDN();
        
        QueryExpression query = queryElement != null ? queryElement.getValue() : null;
        
        Object object = null;
        ServiceReference reference = null;
        
        DeviceInformationsResponse response = new DeviceInformationsResponse();
        DeviceInformations devices = new DeviceInformations();
        response.setDeviceInformations(devices);
        
        try
        {
            reference = bundleContext.getServiceReference("(UDN=" + udn + ")");
            if (reference != null)
            {
                Bundle bundle = bundleContext.getBundle();
                
                if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ZoneService.class.getName()))
                {
                    // TODO: Filter and Sort the results before returning
                    List<DeviceInformation> list = queryDeviceInformationForServices("(ZoneID=" + getZoneID() + ")", org.smpte._2071._2012.quantel.ServerService.class);
                    devices.getDeviceInformation().addAll(list);
                } else if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ServerService.class.getName()))
                {
                    Object serverID = reference.getProperty("ServerID");
                    // TODO: Filter and Sort the results before returning
                    List<DeviceInformation> list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(ServerID=" + serverID + "))", org.smpte._2071._2012.quantel.PoolService.class);
                    list.addAll(queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(ServerID=" + serverID + "))", org.smpte._2071._2012.quantel.ChannelService.class));
                    devices.getDeviceInformation().addAll(list);
                }
            } else
            {
                DeviceNotFound nf = new DeviceNotFound();
                nf.setUDN(udn);
                nf.setMessage("No Device Found for UDN \"" + udn + "\"!");
                throw new DeviceNotFoundFault_Exception("", nf);
            }
        } catch (InvalidSyntaxException e)
        {
            InvalidQuery iq = new InvalidQuery();
            iq.setMessage("Invalid Query \"" + e.getMessage() + "\"");
            iq.setQueryExpression(queryElement);
            InvalidQueryFault iqf = new InvalidQueryFault("Invalid Query \"" + e.getMessage() + "\"", iq);
            log.log(Level.SEVERE, "Could not query services - " + e.getMessage(), e);
            throw iqf;
        } finally
        {
            if (object != null && reference != null)
            {
                bundleContext.ungetService(reference);
            }
        }
        
        return response;
    }


    public DeviceInformationsResponse siblings(UDNFilter udnFilter)
    throws DeviceNotFoundFault_Exception, InvalidQueryFault,
    SecurityExceptionFault
    {
        JAXBElement<? extends QueryExpression> queryElement = udnFilter.getQueryExpression();
        String udn = udnFilter.getUDN();
        
        QueryExpression query = queryElement != null ? queryElement.getValue() : null;
        
        DeviceInformationsResponse response = new DeviceInformationsResponse();
        DeviceInformations devices = new DeviceInformations();
        response.setDeviceInformations(devices);
        
        try
        {
            Bundle bundle = bundleContext.getBundle();
            ServiceReference reference = bundleContext.getServiceReference("(UDN=" + udn + ")");
            if (reference != null)
            {
                if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ServerService.class.getName()))
                {
                    Object serverID = reference.getProperty("ServerID");
                    List<DeviceInformation> list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(!ServerID=" + serverID + "))", org.smpte._2071._2012.quantel.ServerService.class);
                    devices.getDeviceInformation().addAll(list);
                } else if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.PoolService.class.getName()))
                {
                    Object serverID = reference.getProperty("ServerID");
                    Object poolID = reference.getProperty("PoolID");
                    List<DeviceInformation> list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(ServerID=" + serverID + ")(!PoolID=" + poolID + "))", org.smpte._2071._2012.quantel.PoolService.class);
                    devices.getDeviceInformation().addAll(list);
                } else if (reference.isAssignableTo(bundle, org.smpte._2071._2012.quantel.ChannelService.class.getName()))
                {
                    Object serverID = reference.getProperty("ServerID");
                    Object channelID = reference.getProperty("ChannelID");
                    List<DeviceInformation> list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(ServerID=" + serverID + ")(!ChannelID=" + channelID + "))", org.smpte._2071._2012.quantel.ChannelService.class);
                    devices.getDeviceInformation().addAll(list);
                }
            } else
            {
                DeviceNotFound nf = new DeviceNotFound();
                nf.setUDN(udn);
                nf.setMessage("No Device Found for UDN \"" + udn + "\"!");
                throw new DeviceNotFoundFault_Exception("", nf);
            }
        } catch (InvalidSyntaxException e)
        {
            InvalidQuery iq = new InvalidQuery();
            iq.setMessage("Invalid Query \"" + e.getMessage() + "\"");
            iq.setQueryExpression(queryElement);
            InvalidQueryFault iqf = new InvalidQueryFault("Invalid Query \"" + e.getMessage() + "\"", iq);
            log.log(Level.SEVERE, "Could not query services - " + e.getMessage(), e);
            throw iqf;
        }
        
        return response;
    }


    public DeviceInformationsResponse search(UDNFilter udnFilter)
    throws DeviceNotFoundFault_Exception, InvalidQueryFault,
    SecurityExceptionFault
    {
        JAXBElement<? extends QueryExpression> queryElement = udnFilter.getQueryExpression();
        String udn = udnFilter.getUDN();
        
        QueryExpression query = queryElement != null ? queryElement.getValue() : null;
        
        DeviceInformationsResponse response = new DeviceInformationsResponse();
        DeviceInformations devices = new DeviceInformations();
        response.setDeviceInformations(devices);
        
        try
        {
            List<DeviceInformation> list;
            if (udn != null)
            {
                list = queryDeviceInformationForServices("(&(ZoneID=" + getZoneID() + ")(UDN=" + udn + "))", null);
            } else
            {
                list = queryDeviceInformationForServices("(ZoneID=" + getZoneID() + ")", null);
            }
            // TODO: Filter and Sort
            
            devices.getDeviceInformation().addAll(list);
        } catch (InvalidSyntaxException e)
        {
            InvalidQuery iq = new InvalidQuery();
            iq.setMessage("Invalid Query \"" + e.getMessage() + "\"");
            iq.setQueryExpression(queryElement);
            InvalidQueryFault iqf = new InvalidQueryFault("Invalid Query \"" + e.getMessage() + "\"", iq);
            log.log(Level.SEVERE, "Could not query services - " + e.getMessage(), e);
            throw iqf;
        }
        
        return response;
    }


    public MediaContainer getMediaContainer()
    {
        return mediaContainer;
    }


    public MediaResponse create(Create create)
    throws MediaCreationFailedFault, MediaNotFoundFault, SecurityExceptionFault
    {
        SecurityException se = new SecurityException();
        se.setMessage("Unsupported Operation");
        se.setResource(getUDN());
        se.setStatus(EXCEPTIONSTATUS.ERROR);
        se.setType(EXCEPTIONTYPE.SECURITY_LAYER);
        SecurityExceptionFault sef = new SecurityExceptionFault("Unsupported Operation", se);
        throw sef;
    }


    public MediaResponse delete(UMNParameter umnParameter)
    throws MediaDeletionFailedFault, MediaNotFoundFault, SecurityExceptionFault
    {
        SecurityException se = new SecurityException();
        se.setMessage("Unsupported Operation");
        se.setResource(getUDN());
        se.setStatus(EXCEPTIONSTATUS.ERROR);
        se.setType(EXCEPTIONTYPE.SECURITY_LAYER);
        SecurityExceptionFault sef = new SecurityExceptionFault("Unsupported Operation", se);
        throw sef;
    }


    public MediaListResponse list(UMNFilter umnFilter)
    throws InvalidQueryFault, MediaNotFoundFault, SecurityExceptionFault
    {
        JAXBElement<? extends QueryExpression> queryElement = umnFilter.getQueryExpression();
        String umn = umnFilter.getUMN();
        
        QueryExpression query = queryElement != null ? queryElement.getValue() : null;
        
        MediaListResponse response = new MediaListResponse();
        MediaList mediaList = new MediaList();
        response.setMediaList(mediaList);
        
        org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory factory = new org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory();
        
        if (umn == null || getMediaContainer().getUMN().equals(umn))
        {
            // List Zone root.
            try
            {
                ServiceReference[] references = bundleContext.getAllServiceReferences(org.smpte._2071._2012.quantel.PoolService.class.getName(), "(ZoneID=" + getZoneID() + ")");
                for (ServiceReference reference : references)
                {
                    MediaContainer container = new MediaContainer();
                    container.setUMN("urn:smpte:umn:Quantel." + getZoneID() + ":type=media_container;poolid=" + reference.getProperty("PoolID"));
                    container.setLocation(getMediaContainer().getUMN());
                    container.setUDN((String) reference.getProperty("UDN"));
                    container.setName(getPoolName(Integer.parseInt((String) reference.getProperty("PoolID"))));
                    container.setCreated(null);
                    container.setModified(null);
                    mediaList.getMedia().add(factory.createMediaContainer(container));
                }
            } catch (Exception e)
            {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        } else
        {
            mediaList.getMedia().addAll(convertClipResultsToMedia(queryClips(umn, query)));
        }
        
        return response;
    }


    public MediaResponse lookup(UMNParameter umnParameter)
    throws MediaNotFoundFault, SecurityExceptionFault
    {
        String umn = umnParameter.getUMN();
        
        // TODO Auto-generated method stub
        return null;
    }


    public MediaAsset lookupAsset(String umn)
    throws MediaNotFoundFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    public MediaListResponse search(UMNFilter umnFilter)
    throws InvalidQueryFault, MediaNotFoundFault, SecurityExceptionFault
    {
        JAXBElement<? extends QueryExpression> queryElement = umnFilter.getQueryExpression();
        String umn = umnFilter.getUMN();
        
        QueryExpression query = queryElement != null ? queryElement.getValue() : null;
        
        MediaListResponse response = new MediaListResponse();
        MediaList mediaList = new MediaList();
        response.setMediaList(mediaList);
        
        org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory factory = new org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory();
        
        mediaList.getMedia().addAll(convertClipResultsToMedia(queryClips(umn, query)));
        
        return response;
    }


    public MediaResponse update(Update update)
    throws MediaNotFoundFault, MediaUpdateFailedFault, SecurityExceptionFault
    {
        SecurityException se = new SecurityException();
        se.setMessage("Unsupported Operation");
        se.setResource(getUDN());
        se.setStatus(EXCEPTIONSTATUS.ERROR);
        se.setType(EXCEPTIONTYPE.SECURITY_LAYER);
        SecurityExceptionFault sef = new SecurityExceptionFault("Unsupported Operation", se);
        throw sef;
    }


    public URLs lookupURLs(String umn)
    throws MediaNotFoundFault, SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    public PollResponse poll(URIParameter poll)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    public RegisterCallbackResponse registerCallback(URIParameter registerCallback)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }


    public UnregisterCallbackResponse unregisterCallback(URIParameter unregisterCallback)
    throws SecurityExceptionFault
    {
        // TODO Auto-generated method stub
        return null;
    }
}
