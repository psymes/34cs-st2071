package org.smpte._2071._2012.quantel.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

import org.omg.CORBA.Any;
import org.omg.CORBA.BAD_OPERATION;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.NO_RESPONSE;
import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Policy;
import org.omg.CORBA.Request;
import org.omg.CORBA.TCKind;
import org.omg.CORBA.TRANSIENT;
import org.omg.PortableServer.IdAssignmentPolicyValue;
import org.omg.PortableServer.ImplicitActivationPolicyValue;
import org.omg.PortableServer.LifespanPolicyValue;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAPackage.AdapterNonExistent;
import org.osgi.framework.BundleContext;
import org.smpte._2071._2012.quantel.NotConnectedException;
import org.smpte._2071._2012.quantel.Startable;

public class CorbaConnectionService implements org.smpte._2071._2012.quantel.CorbaConnectionService, Startable, Serializable
{
    public class Invoker implements InvocationHandler
    {
        private org.omg.CORBA.Object object;
        
        
        public Invoker(org.omg.CORBA.Object object)
        {
            this.object = object;
        }
        

        public Object invoke(Object proxy, Method method, Object[] args)
        throws Throwable
        {
            StringBuilder msg = new StringBuilder("[" + object.getClass().getSimpleName() + "]." + method.getName() + "(");
            if (args != null)
            {
                for (int index = 0; index < args.length; index++)
                {
                    msg.append(String.valueOf(args[index]));
                    if (index < args.length - 1)
                    {
                        msg.append(", ");
                    }
                }
            }
            msg.append(")");
            
            log.info(msg + ";");

            long start = System.nanoTime();
            
            Object result = null;
            Throwable throwable = null;
            boolean retry = false;
            
            for (int count = 0; count < EXECUTION_RETRY; count++)
            {
                if (!isConnected())
                {
                    connect();
                }
                
                if (object != null)
                {
                    try
                    {
                        result = method.invoke(object, args);
                        retry = false;
                    } catch (TRANSIENT e)
                    {
                        retry = true;
                    } catch (COMM_FAILURE e)
                    {
                        retry = true;
                    } catch (OBJECT_NOT_EXIST e)
                    {
                        retry = true;
                    } catch (NO_RESPONSE e)
                    {
                        retry = true;
                    } catch (InvocationTargetException e)
                    {
                        if (e.getCause() instanceof COMM_FAILURE)
                        {
                            retry = true;
                        } else if (e.getCause() instanceof TRANSIENT)
                        {
                            retry = true;
                        } else if (e.getCause() instanceof OBJECT_NOT_EXIST)
                        {
                            retry = true;
                        } else if (e.getCause() instanceof NO_RESPONSE)
                        {
                            retry = true;
                        } else
                        {
                            throwable = e.getCause();
                        }
                    } catch (Throwable e)
                    {
                        throwable = e;
                    }
                    
                    if (retry)
                    {
                        disconnect();
                    } else
                    {
                        break;
                    }
                } else
                {
                    throwable = new IOException("Could not execute operation \"" + method.getName() + "\" not connected.");
                    break;
                }
            }
            
            double millis = (double) (System.nanoTime() - start) / (double) 1000000;
            String tooLong = millis < 1000 ? "" : " !!!!! Long Execution !!!!!";
            
            if (throwable != null)
            {
                msg.append(" threw " + throwable.getClass().getName() + " message:\"" + throwable.getMessage() + "\" - took " + millis + " millis." + tooLong);
                System.out.println(msg);
                log.info(msg.toString());
                throw throwable;
            } else
            {
                msg.append(" returned " + (result == null ? "null" : result) + " - took " + millis + " millis." + tooLong);
                System.out.println(msg);
                log.info(msg.toString());
                return result;
            }
        }
    }
    
    private static transient final long serialVersionUID = 201206162116L;

    private static transient Logger log = Logger.getLogger(CorbaConnectionService.class.getName());
    
    private static final int EXECUTION_RETRY = 3;

    private BundleContext bundleContext;

    private String[] iors;
    
    private Map<String, String> orbProperties;
    
    private transient boolean systemORB = false;
    
    private transient ORB orb;
    
    private transient org.omg.CORBA.Object object;

    private transient org.omg.CORBA.Object proxyObject;

    private transient String version = "0.0.0";

    private transient POA poa;

    private transient ArrayList<ConnectionListener> listeners = new ArrayList<ConnectionListener>();
    
    private transient Map<Class<?>, org.omg.CORBA.Object> cache = new HashMap<Class<?>, org.omg.CORBA.Object>();

    private boolean wasConnected = false;


    public void setBundleContext(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }


    public BundleContext getBundleContext()
    {
        return bundleContext;
    }
    

    public void connect()
    throws Exception
    {
        for (String ior : iors)
        {
            log.info("***** Connecting to remote object \"" + ior + "\"");
            try
            {
                this.object = connectToIOR(ior);
                
                if (this.object != null)
                {
                    log.info("***** Connected to remote object \"" + ior + "\"");
                    break;
                }
            } catch (Exception e)
            {
                log.log(Level.WARNING, "Could not connect to IOR \"" + ior + "\" - " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        }
        
        notifyListeners(isConnected());
        
        if (object == null)
        {
            cache.clear();
            IOException ioe = new IOException("Could not connect to any of the specified IORs \"" + getIORs() + "\".");
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.SEVERE, "Could not connect to any of the specified IORs [" + getIORs() + "].", ioe);
            } else
            {
                log.log(Level.SEVERE, "Could not connect to any of the specified IORs [" + getIORs() + "].");
            }
            throw ioe;
        }
    }
    

    protected synchronized org.omg.CORBA.Object connectToIOR(String ior)
    throws Exception
    {
        org.omg.CORBA.Object object = null;
        
        if (!ior.startsWith("ior") && !ior.startsWith("IOR"))
        {
            URLConnection connection = new java.net.URL(ior).openConnection();
            connection.setConnectTimeout(1000);
            connection.setReadTimeout(1000);
            BufferedReader iorReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            ior = iorReader.readLine();
        }
        
        try
        {
            object = orb.string_to_object(ior);
            if (object == null)
            {
                cache.clear();
                IOException ioe = new IOException("Object not available for IOR at \"" + ior + "\".");
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.SEVERE, "Object not available for IOR at \"" + ior + "\".", ioe);
                } else
                {
                    log.log(Level.SEVERE, "Object not available for IOR at \"" + ior + "\".");
                }
                throw ioe;
            }
        } catch (Throwable e)
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.SEVERE, "Error connecting to remote object \"" + ior + "\" - " + e.getMessage(), e);
            } else
            {
                log.log(Level.SEVERE, "Error connecting to remote object \"" + ior + "\" - " + e.getMessage());
            }
            
            NotConnectedException nce = new NotConnectedException(e.getMessage());
            nce.setStackTrace(e.getStackTrace());
            throw nce;
        }
        
        Request request = object._request("getProperty");
        request.set_return_type(orb.get_primitive_tc(TCKind.tk_wstring));
        Any any = request.add_in_arg();
        any.insert_wstring("SoftwareVersion");
        request.invoke();
        
        Exception ex = request.env().exception();
        if (ex == null)
        {
            version = request.return_value().extract_wstring();
        } else if (ex instanceof BAD_OPERATION)
        {
            // Default to version 0.0.0
            version = "0.0.0";
        } else
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.SEVERE, ex.getClass().getSimpleName() + ": " + ex.getMessage(), ex);
            } else
            {
                log.log(Level.SEVERE, ex.getClass().getSimpleName() + ": " + ex.getMessage());
            }
            throw ex;
        }
        
        System.out.println("Object for IOR at \"" + ior + "\" is version " + version + ".");
        log.info("Object for IOR at \"" + ior + "\" is version " + version + ".");
        
        return object;
    }
    
    
    public void disconnect()
    {
        if (this.object != null)
        {
            try
            {
                this.object._release();
            } catch (Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, e.getClass().getSimpleName() + ": " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, e.getClass().getSimpleName() + ": " + e.getMessage());
                }
            }
            
            try
            {
                orb.disconnect(this.object);
            } catch (Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, e.getClass().getSimpleName() + ": " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, e.getClass().getSimpleName() + ": " + e.getMessage());
                }
            }
            
            this.object = null;
            this.proxyObject = null;
            cache.clear();
            notifyListeners(false);
        }
    }

    
    public String getVersion()
    {
        return version;
    }
    

    public boolean isConnected()
    {
        return this.object != null;
    }
    

    @SuppressWarnings("unchecked")
    public <T extends org.omg.CORBA.Object> T getConnectionObject(Class<T> type)
    throws NotConnectedException
    {
        if (this.proxyObject == null)
        {
            if (!isConnected())
            {
                try
                {
                    connect();
                } catch (Exception e)
                {
                    throw new NotConnectedException(e.getMessage());
                }
            }
            
            if (isConnected())
            {
                org.omg.CORBA.Object object = cache.get(type);
                if (object == null)
                {
                    try
                    {
                        object = narrow(type, this.object);
                        cache.put(type, object);
                    } catch (ClassNotFoundException e)
                    {
                        throw new NotConnectedException("Helper class for \"" + type.getSimpleName() + "\" not found.");
                    } catch (NoSuchMethodException e)
                    {
                        throw new NotConnectedException("Helper class \"" + type.getSimpleName() + "Helper\" does not have a narrow method.");
                    } catch (InvocationTargetException e)
                    {
                        throw new NotConnectedException("Helper class \"" + type.getSimpleName() + "Helper\" raise exception - " + e.getMessage());
                    }
                }
                
                Class<?> clazz = object.getClass();
                ArrayList<Class<?>> interfaceList = new ArrayList<Class<?>>();
                while (clazz != null)
                {
                    Class<?>[] tempInterfaces = clazz.getInterfaces();
                    for (int index = 0; index < tempInterfaces.length; index++)
                    {
                        interfaceList.add(tempInterfaces[index]);
                    }
                    clazz = clazz.getSuperclass();
                }
                
                Class<?>[] interfaces = (Class[]) interfaceList.toArray(new Class[interfaceList.size()]);
                
                log.info("***** Creating Connection Proxy for remote object \"" + getIORs() + "\"");
                this.proxyObject = (T) Proxy.newProxyInstance(getClass().getClassLoader(), interfaces, new Invoker(object));
            } else
            {
                throw new NotConnectedException("Not Connected to remote object \"" + getIORs() + "\".");
            }
        }
        
        log.info("***** Returning Connection Proxy for remote object \"" + getIORs() + "\"");
        return (T) this.proxyObject;
    }
    
    
    private org.omg.CORBA.Object narrow(Class<?> baseInterface, org.omg.CORBA.Object object) 
    throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException
    {
        try
        {
            Class<?> helperClass = Class.forName(baseInterface.getName() + "Helper");
            Method method = helperClass.getMethod("narrow", new Class[] {org.omg.CORBA.Object.class});
            return (org.omg.CORBA.Object) method.invoke(null, new Object[] {object});
        } catch (ClassNotFoundException e)
        {
            throw new ClassNotFoundException("The specified interface \"" + baseInterface + "\" does not have a helper class \"" + baseInterface.getName() + "Helper\".");
        } catch (SecurityException e)
        {
            throw new NoSuchMethodException("The specified interface \"" + baseInterface + "\" does not have a helper class \"" + baseInterface.getName() + "Helper\" that has an accessible \"narrow(org.omg.CORBA.Object)\" method.");
        } catch (NoSuchMethodException e)
        {
            throw new NoSuchMethodException("The specified interface \"" + baseInterface + "\" does not have a helper class \"" + baseInterface.getName() + "Helper\" that has an \"narrow(org.omg.CORBA.Object)\" method.");
        } catch (IllegalArgumentException e)
        {
            throw new IllegalArgumentException("Argument Error for the \"narrow(org.omg.CORBA.Object)\" method for the helper class helper class \"" + baseInterface.getName() + "Helper - " + e.getMessage());
        } catch (IllegalAccessException e)
        {
            throw new NoSuchMethodException("The specified interface \"" + baseInterface + "\" does not have a helper class \"" + baseInterface.getName() + "Helper\" that has an accessible \"narrow(org.omg.CORBA.Object)\" method.");
        }
    }
    
    
    public ORB getORB()
    {
        return orb;
    }
    

    public POA getPOA()
    {
        return poa;
    }


    public String getIORs()
    {
        if (iors == null)
        {
            return null;
        } else
        {
            StringBuilder builder = new StringBuilder();
            for (String ior : iors)
            {
                builder.append(ior).append(",");
            }
            
            if (builder.length() > 1)
            {
                builder.setLength(builder.length() - 1);
            }
            
            return builder.toString();
        }
    }
    
    
    public void setIORs(String iors)
    {
        ArrayList<String> list = new ArrayList<String>(); 
        String[] temp = iors.split(",");
        
        for (int index = 0; index < temp.length; index++)
        {
            if (temp[index] != null)
            {
                temp[index] = temp[index].trim();
                if (temp[index].length() > 0)
                {
                    list.add(temp[index]);
                }
            }
        }
        
        this.iors = list.toArray(new String[list.size()]);  
    }


    public void setProperties(Map<String, String> props)
    {
        this.orbProperties = props;
    }
    
    
    public Map<String, String> getProperties()
    {
        return new HashMap<String, String>(orbProperties);
    }
    
    
    public void addConnectionListener(ConnectionListener listener)
    {
        listeners.add(listener);
    }
    
    
    public void removeConnectionListener(ConnectionListener listener)
    {
        listeners.remove(listener);
    }
    
    
    protected void notifyListeners(boolean connected)
    {
        if (wasConnected != connected)
        {
            for (ConnectionListener listener : listeners)
            {
                try
                {
                    if (connected)
                    {
                        listener.connected();
                    } else
                    {
                        listener.disconnected();
                    }
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Exception thrown in ConnectionListener - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Exception thrown in ConnectionListener - " + e.getMessage());
                    }
                }
            }
            
            wasConnected = connected;
        }
    }
    
    
    public void start()
    throws Exception
    {
        System.out.println("***** Starting Service " + this.getClass().getSimpleName() + " *****");
        
        String[] orbNames = new String[] 
        {
            "java:comp/ORB", 
            "java:/comp/ORB", 
            "java:module/ORB", 
            "java:/module/ORB", 
            "java:/JBossCorbaORB", 
            "java:JBossCorbaORB"
        };
        
        Context context = new InitialContext();
        Object object = null;
        cache.clear();
        
        log.info("Searching Naming Services for system ORB.");
        for (String orbName : orbNames)
        {
            try
            {
                object = context.lookup(orbName);
            } catch (Exception e)
            {
                // Ignore
            }
            
            if (object != null)
            {
                orb = (ORB) PortableRemoteObject.narrow(object, ORB.class);
                System.out.println("***** Using system ORB \"" + orbName + "\" [" + orb.getClass().getName() + "] *****");
                log.info("Using system ORB \"" + orbName + "\" [" + orb.getClass().getName() + "]");
                systemORB = true;
                break;
            }
        }
        
        if (object == null)
        {
            log.info("Could not locate an ORB through Naming Services!  Creating a new ORB.");
            Properties props = new Properties();
            props.putAll(orbProperties);
            orb = org.omg.CORBA.ORB.init((String[]) null, props);
            System.out.println("***** Created instance ORB " + orb.getClass().getName() + " *****");
            log.info("Created instance ORB \"" + orb.getClass().getName() + "\"");
            systemORB = false;
        }
        
        try
        {
            POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            
            Policy[] policies = new Policy[3];
            policies[0] = rootPOA.create_lifespan_policy(LifespanPolicyValue.TRANSIENT);
            policies[1] = rootPOA.create_id_assignment_policy(IdAssignmentPolicyValue.SYSTEM_ID);
            policies[2] = rootPOA.create_implicit_activation_policy(ImplicitActivationPolicyValue.IMPLICIT_ACTIVATION);
            
            try
            {
                poa = rootPOA.find_POA("BiDirPOA", true);
            } catch (AdapterNonExistent e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.INFO, "Could not find reference to the BiDirPOA.  Creating a new BiDirPOA.");
                } else
                {
                    log.log(Level.INFO, "Could not find reference to the BiDirPOA.  Creating a new BiDirPOA.");
                }
            }
            
            if (poa == null)
            {
                poa = rootPOA.create_POA("BiDirPOA", rootPOA.the_POAManager(), policies);
                poa.the_POAManager().activate();
            }
        } catch (Exception e)
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.WARNING, "Could not get a reference to the POA! " + e.getClass().getSimpleName() + ": " + e.getMessage(), e);
            } else
            {
                log.log(Level.WARNING, "Could not get a reference to the POA! " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        }
    }


    public void stop()
    {
        System.out.println("***** Stopping Service " + this.getClass().getSimpleName() + " *****");
        
        disconnect();
        
        if (orb != null)
        {
            if (!systemORB)
            {
                System.out.println("***** Shutting down ORB " + orb.getClass().getName() + " *****");
                orb.shutdown(true);
            }
            orb = null;
        }
    }
}
