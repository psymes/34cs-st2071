package org.smpte._2071._2012.quantel;

import java.util.List;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public interface ServerService extends ZoneAware, Device
{
    public void setServerID(int serverID);
    
    
    public int getServerID();
    
    
    public void refresh();
    
    
    public String getServerName();
    
    
    public boolean isUp();
    
    
    public float getFrameRate();
    
    
    public String[] getChanPorts();
    
    
    public FramedTime getReferenceTime();


    public int getNumberOfChannels();
    
    
    public int[] getPoolIDs();
    
    
    public List<DeviceInformation> getChildren();
}
