package org.smpte._2071._2012.quantel;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;

public interface PoolService extends ZoneAware, Device
{
    public void setServerService(org.smpte._2071._2012.quantel.ServerService serverService);
    
    
    public org.smpte._2071._2012.quantel.ServerService getServerService();
    
    
    public boolean isUp();
    
    
    public void setPoolID(int poolID);
    
    
    public int getPoolID();
}
