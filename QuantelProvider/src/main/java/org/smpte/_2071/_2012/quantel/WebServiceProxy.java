package org.smpte._2071._2012.quantel;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.jws.WebService;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

@WebService(name="WebServiceProxy", targetNamespace="http://www.smpte-ra.org/schemas/2071/2012/mdc", serviceName="WebServiceProxy")
public class WebServiceProxy implements InvocationHandler
{
    private Logger log;
    
    private BundleContext bundleContext;
    
    private ServiceReference serviceReference;

    private String endpointName;
    
    
    protected WebServiceProxy(BundleContext bundleContext, ServiceReference serviceReference, String endpointName)
    {
        this.bundleContext = bundleContext;
        this.serviceReference = serviceReference;
        this.endpointName = endpointName;

        log = Logger.getLogger(endpointName);
    }
    
    
    public Object invoke(Object object, Method method, Object[] args)
    throws Throwable
    {
        if (method.getDeclaringClass() == Object.class)
        {
            Object delegate = bundleContext.getService(serviceReference);
            
            try
            {
                return method.invoke(delegate, args);
            } finally
            {
                bundleContext.ungetService(serviceReference);
            }
        } else
        {
            long start = System.nanoTime();
            StringBuilder msg = new StringBuilder("[" + endpointName + "]." + method.getName() + "(");
            if (args != null)
            {
                for (int index = 0; index < args.length; index++)
                {
                    msg.append(String.valueOf(args[index]));
                    if (index < args.length - 1)
                    {
                        msg.append(", ");
                    }
                }
            }
            msg.append(")");
            log.info(msg + ";");
            
            Object result = null;
            Throwable throwable = null;
            try
            {
                Object delegate = bundleContext.getService(serviceReference);
                
                if (delegate instanceof Connectable)
                {
                    if (!((Connectable) delegate).isConnected())
                    {
                        throw new NotConnectedException();
                    }
                }
                
                result = method.invoke(delegate, args);
            } catch (InvocationTargetException e)
            {
                throwable = e.getTargetException();
            } catch (Throwable e)
            {
                throwable = e;
            } finally
            {
                bundleContext.ungetService(serviceReference);
                double millis = (double) (System.nanoTime() - start) / (double) 1000000;
                String tooLong = millis < 1000 ? "" : " !!!!! Long Execution !!!!!";
                
                if (throwable != null)
                {
                    msg.append(" threw " + throwable.getClass().getName() + " message:\"" + throwable.getMessage() + "\" - took " + millis + " millis." + tooLong);
                    System.out.println(msg);
                    log.info(msg.toString());
                } else
                {
                    msg.append(" returned " + (result == null ? "null" : result) + " - took " + millis + " millis." + tooLong);
                    System.out.println(msg);
                    log.info(msg.toString());
                }
            }
            
            if (throwable != null)
            {
                throw throwable;
            } else
            {
                return result;
            }
        }
    }
}

