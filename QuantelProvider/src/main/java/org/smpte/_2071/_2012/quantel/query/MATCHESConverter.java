package org.smpte._2071._2012.quantel.query;

import org.smpte_ra.schemas._2071._2012.mdcf.query.MATCHES;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public class MATCHESConverter extends SQLConverter
{
    
    public MATCHESConverter(SQLConverterFactory factory, QueryExpression expression)
    {
        super(factory, expression);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        StringBuilder builder = new StringBuilder();
        
        if (expression != null)
        {
            final MATCHES matches = (MATCHES) expression;
            final String field = matches.getField();
            final String regexp = matches.getRegExp();
            
            builder.append(factory.getColumnName(field)).append(" REGEXP \"").append(regexp).append("\"");
        }
        
        return builder.toString();
    }
}
