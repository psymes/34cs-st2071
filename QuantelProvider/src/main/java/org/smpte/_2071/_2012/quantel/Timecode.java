package org.smpte._2071._2012.quantel;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * @author Steve Posick
 */
@XmlRootElement(name="Timecode")
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(name="Timecode")
public class Timecode
{
    /**
     * format YYYYMMDDHH24mm
     */
    private static final long serialVersionUID = 200901131700L;
    
    public static enum BCD_TYPE
    {
        BIG_ENDEAN, LITTLE_ENDEAN;
    }
    
    public static enum Type
    {
        OFFSET, SOURCE, TIME_OF_DAY
    }

    public static final int HOURS = 1;
    
    public static final int MINUTES = 2;
    
    public static final int SECONDS = 3;
    
    public static final int FRAMES = 4;
    
    public static final int MILLISECONDS = 5;
    
    public static final int TOTAL_FRAMES = 6;
    
    public static final int TOTAL_MILLISECONDS = 7;
    
    public static final int TIMECODE = 8;
    
    private static class CalculatedValues
    {
        private CalculatedValues(int fps, boolean dropframe, long frame)
        {
            if (frame < 0)
            {
                frame = 0;
            }
            
            framesToDrop = fps / 15; // 2 per 30 frames
            nondropFramesPerHour = 3600 * fps;
            nondropFramesPerTenMinutes = 600 * fps;
            nondropFramesPerMinute = 60 * fps;
            
            if (dropframe)
            {
                frame_nondrop = convertToNonDropFrames(fps, frame);
                framesPerHour = (int) convertToDropFrames(fps, nondropFramesPerHour);
                framesPerTenMinutes = nondropFramesPerTenMinutes - (framesToDrop * 9);
                framesPerMinute = nondropFramesPerMinute - framesToDrop;
            } else
            {
                frame_nondrop = frame;
                framesPerHour = nondropFramesPerHour;
                framesPerTenMinutes = nondropFramesPerTenMinutes;
                framesPerMinute = nondropFramesPerMinute;
            }
            
            nondropMillisPerFrame = (double) ((double) 1000 / (double) fps);
            if (dropframe)
            {
                millisPerFrame = (double) ((double) 1001 / (double) fps);
            } else
            {
                millisPerFrame = nondropMillisPerFrame;
            }
            
            int tensOfMinutes = (int) (frame / framesPerTenMinutes);
            int frameThisInterval = (int) frame - (framesPerTenMinutes * tensOfMinutes);
            
            minutes = (int) (frameThisInterval / nondropFramesPerMinute) % 60;
            seconds = (int) (frameThisInterval / fps) % 60;
            
            if (dropframe)
            {
                frameInSecond = (int) (frameThisInterval % fps) + (minutes * framesToDrop);
            } else
            {
                frameInSecond = (int) (frameThisInterval % fps);
            }
            
            hours = tensOfMinutes / 6;
            minutes += (tensOfMinutes * 10) % 60;
            
            int secondsOver = frameInSecond / fps;
            while (secondsOver > 0)
            {
                frameInSecond %= fps;
                seconds += secondsOver;
                
                int minutesOver = seconds / 60;
                if (minutesOver > 0)
                {
                    seconds %= 60;
                    minutes += minutesOver;
                }
                
                int hoursOver = minutes / 60;
                if (hoursOver > 0)
                {
                    minutes %= 60;
                    hours += hoursOver;
                }
                
                if (dropframe && seconds == 0 && minutes % 10 != 0)
                {
                    frameInSecond += framesToDrop;
                }
                secondsOver = frameInSecond / fps;
            }
            
            // Handle invalid timecode values (dropped frames are specified.  Ex. 00:01:00;00
            if (dropframe)
            {
                if (minutes % 10 != 0)
                {
                    if (seconds == 0)
                    {
                        if (frameInSecond == 0 || frameInSecond == 1)
                        {
                            frameInSecond = 2;
                        }
                    }
                }
            }
            
            millisecond = (long) Math.round((double) frame * millisPerFrame);
            milliseconds = (int) (millisecond % (long) 1000);
            
            Calendar calendar = Calendar.getInstance();
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            
            long baseMillis = calendar.getTimeInMillis();
            time = new Date(baseMillis + millisecond);
            
            int bcd;
            int droppedFrames = dropframe ? 1 : 0;
            
            bcd = (droppedFrames & 0x00000001) << 30;
            bcd += (((hours / 10) & 0x00000003) << 28);
            bcd += (((hours % 10) & 0x0000000F) << 24);
            bcd += (((minutes / 10) & 0x00000007) << 20);
            bcd += (((minutes % 10) & 0x0000000F) << 16);
            bcd += (((seconds / 10) & 0x00000007) << 12);
            bcd += (((seconds % 10) & 0x0000000F) << 8);
            bcd += (((frameInSecond / 10) & 0x00000007) << 4);
            bcd += ((frameInSecond % 10) & 0x0000000F);
            
            timecodeBCD = bcd;
        }
        
        private int framesPerHour = -1;

        private int framesPerTenMinutes = -1;

        private int framesPerMinute = -1;

        private int framesToDrop = -1;

        private double millisPerFrame = -1;

        private int hours = -1;

        private int minutes = -1;
        
        private int seconds = -1;
        
        private int frameInSecond = -1;

        private long frame_nondrop = -1;

        private int nondropFramesPerHour = -1;

        private int nondropFramesPerTenMinutes = -1;

        private int nondropFramesPerMinute = -1;

        private double nondropMillisPerFrame = -1;

        private long millisecond = -1;

        private int milliseconds = -1;
        
        private Date time = null;
        
        private int timecodeBCD = -1;
    }
    
    @XmlAttribute(name="frames")
    protected long frame = -1;
    
    @XmlAttribute(name="dropframe")
    protected boolean dropframe = false;
    
    @XmlAttribute(name="fps", required=true)
    protected int fps = -1;

    @XmlAttribute(name="timecode_type")
    private Type type = Type.TIME_OF_DAY;
    
    private String timecodeString = null;
    
    private transient CalculatedValues calculatedValues = null;
    
    
    /**
     * Constructs an uninitialized Timecode object.  Object must be initialized before using.
     */
    protected Timecode()
    {
    }
    
    
    /**
     * Creates a new Timecode object from the specified Timecode String.
     * 
     * @param text The Timecode String
     */
    public Timecode(int fps, String text)
    {
        setFramesPerSecond(fps);
        setXmlValue(text);
    }


    /**
     * Constructs a default Timecode object with the specified frames per second and drop frames.
     *  
     * @param framesPerSecond The frames per second
     * @param dropFrames True if drop frame
     */
    public Timecode(int framesPerSecond, boolean dropFrames)
    {
        setFramesPerSecond(framesPerSecond);
        setDropFrames(dropFrames);
    }
    
    
    /**
     * <p>
     * Timecode stored in a 32-bit integer. The timecode is stored as Binary
     * Code Decimal.
     * </p>
     * <pre>
     * Bit 3 2         1         0 
     *     10987654321098765432109876543210
     *     0dHHhhhh0MMMmmmm0SSSssss0FFFffff
     * </pre>
     *
     * <ul>
     * <li> 0 - A mandatory zero bit (though certain functions may pack in extra control bits)</li> 
     * <li> d - Drop frame flag. Valid only in NTSC</li>
     * <li> HH - Tens of hours</li>
     * <li> hhhh - Units of hours</li>
     * <li> MMM - Tens of minutes</li>
     * <li> mmmm - Units of minutes</li>
     * <li> SSS - Tens of seconds</li> 
     * <li> ssss - Units of seconds</li> 
     * <li> FFF - Tens of frames</li> 
     * <li> ffff - Units of frames</li>
     * </ul>
     * A negative timecode (anything with the MS bit set) is invalid.
     * 
     * @param timecode The timecode in Quantel Format.
     */
    public Timecode(int framesPerSecond, int timecode)
    {
        setFramesPerSecond(framesPerSecond);
        setTimecode(timecode);
    }
    
    
    /**
     * <p>
     * Timecode stored in a 32-bit integer. The timecode is stored as Binary
     * Code Decimal.
     * </p>
     * <pre>
     * Big Endean
     * Bit 3 2         1         0 
     *     10987654321098765432109876543210
     *     0dHHhhhh0MMMmmmm0SSSssss0FFFffff
     *     
     * Little Endean
     * Bit 3 2         1         0 
     *     10987654321098765432109876543210
     *     0dFFffff0SSSssss0MMMmmmm0HHHhhhh
     * </pre>
     *
     * <ul>
     * <li> 0 - A mandatory zero bit (though certain functions may pack in extra control bits)</li> 
     * <li> d - Drop frame flag. Valid only in NTSC</li>
     * <li> HH - Tens of hours</li>
     * <li> hhhh - Units of hours</li>
     * <li> MMM - Tens of minutes</li>
     * <li> mmmm - Units of minutes</li>
     * <li> SSS - Tens of seconds</li> 
     * <li> ssss - Units of seconds</li> 
     * <li> FFF - Tens of frames</li> 
     * <li> ffff - Units of frames</li>
     * </ul>
     * A negative timecode (anything with the MS bit set) is invalid.
     * 
     * @param timecode The timecode in Quantel Format.
     */
    public Timecode(int framesPerSecond, int timecode, BCD_TYPE bcdType)
    {
        setFramesPerSecond(framesPerSecond);
        setTimecode(timecode, bcdType);
    }
    

    public Timecode(int fps, boolean dropframe, int hours, int minutes, int seconds, int frames, int subframe)
    {
        setFramesPerSecond(fps);
        
        clearTimes();
        this.dropframe = dropframe;
        
        long nondrop_frame = (hours * (3600 * fps)) + (minutes * (60 * fps)) + (seconds * fps) + frames;
        
        if (dropframe)
        {
            frame = convertToDropFrames(fps, nondrop_frame);
        } else
        {
            frame = nondrop_frame;
        }
        
        /* TODO: Implement subframes
        if (subframe > 0)
        {
            double factor = (double) fps / (double) 30;
            frame += Math.round((factor * ((double) subframe / (double) 100)));
        }
        */
    }


    /**
     * Creates a new Timecode object for the specified frame.
     * 
     * @param framesPerSecond
     * @param dropFrame
     * @param frame
     */
    public Timecode(int framesPerSecond, boolean dropFrame, long frame)
    {
        setFramesPerSecond(framesPerSecond);
        setDropFrames(dropFrame);
        setFrame(frame);
    }


    public Type getType()
    {
        return type;
    }


    public void setType(Type type)
    {
        this.type = type;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#get(int)
     */
    public long get(int type)
    {
        switch (type)
        {
            case HOURS:
                return getHours();
            case MINUTES:
                return getMinutes();
            case SECONDS:
                return getSeconds();
            case FRAMES:
                return getFrames();
            case MILLISECONDS:
                return getMilliseconds();
            case TOTAL_FRAMES:
                return getFrame();
            case TOTAL_MILLISECONDS:
                return getMillisecond();
            case TIMECODE:
                return toBCD(this);
            default:
                throw new IllegalArgumentException("Type \"" + type + "\" is unknown.");
        }
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getFrame()
     */
    public long getFrame()
    {
        initTimes();
        
        return frame;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getFrames()
     */
    public int getFrames()
    {
        initTimes();

        return calculatedValues.frameInSecond;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getFramesPerSecond()
     */
    public int getFramesPerSecond()
    {
        return fps;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getHours()
     */
    public int getHours()
    {
        initTimes();
        
        return calculatedValues.hours;
    }
    
    /**
     * @see com.espn.marcus.media.Timecode#getTimecode()
     */
    public int getTimecode()
    {
        initTimes();
        
        return calculatedValues.timecodeBCD;
    }
    
    
    private synchronized void initTimes()
    {
        if (calculatedValues == null)
        {
            // Initialized the calculatedValues with the string representation if frame not set.
            if (frame < 0 && timecodeString != null)
            {
                try
                {
                    Timecode newTimecode = parse(fps, timecodeString);
                    clone(newTimecode);
                    calculatedValues = new CalculatedValues(fps, newTimecode.dropframe, newTimecode.frame);
                } catch (ParseException e)
                {
                    e.printStackTrace();
                }
            } else
            {
                calculatedValues = new CalculatedValues(fps, dropframe, frame);
            }
        }
    }
    
    
    private synchronized void clearTimes()
    {
        calculatedValues = null;
        timecodeString = null;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getMillisecond()
     */
    public long getMillisecond()
    {
        initTimes();
        
        return calculatedValues.millisecond;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getMilliseconds()
     */
    public int getMilliseconds()
    {
        initTimes();
        
        return calculatedValues.milliseconds;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getMinutes()
     */
    public int getMinutes()
    {
        initTimes();
        
        return calculatedValues.minutes;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getSeconds()
     */
    public int getSeconds()
    {
        initTimes();
        
        return calculatedValues.seconds;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#getTime()
     */
    public Date getTime()
    {
        initTimes();
        
        return calculatedValues.time;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#hasDropFrames()
     */
    public boolean hasDropFrames()
    {
        initTimes();
        
        return dropframe;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#setDropFrames(boolean)
     */
    public synchronized void setDropFrames(boolean dropFrames)
    {
        clearTimes();
        this.dropframe = dropFrames;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#setFrame(long)
     */
    public synchronized void setFrame(long frame)
    {
        clearTimes();
        this.frame = frame;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#setFramesPerSecond(int)
     */
    public synchronized void setFramesPerSecond(int fps)
    {
        clearTimes();
        this.fps = fps;
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#setMillisecond(long)
     */
    public synchronized void setMillisecond(long millisecond)
    {
        clearTimes();
        
        double millisPerFrame;
        if (dropframe)
        {
            millisPerFrame = (double) ((double) 1001 / (double) fps);
        } else
        {
            millisPerFrame = (double) ((double) 1000 / (double) fps);
        }

        frame = (long) Math.round((double) millisecond / millisPerFrame);
    }
    
    
    public synchronized void setTime(Calendar calendar)
    {
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int millis = calendar.get(Calendar.MILLISECOND);
        
        setMillisecond((hours * 3600000) + (minutes * 60000) + (seconds * 1000) + millis);
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#setTime(java.util.Date)
     */
    public synchronized void setTime(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        setTime(calendar);
    }
    
    
    public synchronized void setTimecode(int timecode)
    {
        setTimecode(timecode, BCD_TYPE.BIG_ENDEAN);
    }
    
    
    /**
     * @see com.espn.marcus.media.Timecode#setTimecode(int)
     */
    public synchronized void setTimecode(int timecode, BCD_TYPE bcdType)
    {
        clearTimes();
        this.dropframe = (timecode & 0x40000000) == 0x40000000;
        
        int hours;
        int minutes;
        int seconds;
        int frames;
        
        switch (bcdType)
        {
            case BIG_ENDEAN :
                hours = ((timecode & 0x30000000) >>> 28) * 10;
                hours += (timecode & 0x0F000000) >>> 24;
                minutes = ((timecode & 0x00700000) >>> 20) * 10;
                minutes += (timecode & 0x000F0000) >>> 16;
                seconds = ((timecode & 0x00007000) >>> 12) * 10;
                seconds += (timecode & 0x00000F00) >>> 8;
                frames = ((timecode & 0x00000070) >>> 4) * 10;
                frames += (timecode & 0x0000000F);
                break;
            case LITTLE_ENDEAN :
                frames = ((timecode & 0x30000000) >>> 28) * 10;
                frames += (timecode & 0x0F000000) >>> 24;
                seconds = ((timecode & 0x00700000) >>> 20) * 10;
                seconds += (timecode & 0x000F0000) >>> 16;
                minutes = ((timecode & 0x00007000) >>> 12) * 10;
                minutes += (timecode & 0x00000F00) >>> 8;
                hours = ((timecode & 0x00000070) >>> 4) * 10;
                hours += (timecode & 0x0000000F);
                break;
            default :
                throw new IllegalArgumentException("BCD type must be one of the supported types.");    
        }
        
        // Handle invalid timecode values (dropped frames are specified.  Ex. 00:01:00;00
        if (dropframe)
        {
            if (minutes % 10 != 0)
            {
                if (seconds == 0)
                {
                    if (frames == 0 || frames == 1)
                    {
                        frames = 2;
                    }
                }
            }
        }
        
        long nondrop_frame = (hours * (3600 * fps)) + (minutes * (60 * fps)) + (seconds * fps) + frames;
        
        if (dropframe)
        {
            frame = convertToDropFrames(fps, nondrop_frame);
        } else
        {
            frame = nondrop_frame;
        }
    }
    
    
    /**
     * @see com.espn.marcus.media.Timecode#add(int, long)
     */
    public synchronized void add(int type, long value)
    {
        switch (type)
        {
            case HOURS:
                frame += (value * getFramesPerHour());
                break;
            case MINUTES:
                frame += (value * getFramesPerMinute());
                break;
            case SECONDS:
                if (dropframe)
                {
                    int oldMinutes = getMinutes();
                    frame += (value * fps);
                    CalculatedValues myCalculatedValues = new CalculatedValues(fps, dropframe, frame);
                    int newMinutes = myCalculatedValues.minutes;
                    
                    // adjust for minute boundries
                    if (oldMinutes != newMinutes && newMinutes % 10 != 0)
                    {
                        frame -= _framesToDrop();
                    }
                } else
                {
                    frame += (value * fps);
                }
                break;
            case FRAMES:
            case TOTAL_FRAMES:
                frame += value;
                break;
            case MILLISECONDS:
            case TOTAL_MILLISECONDS:
                frame += Math.round(((double) value / (double) getMillisPerFrame()));
                break;
            case TIMECODE:
                Timecode tempTimecode = new Timecode(fps, (int) value);
                frame += tempTimecode.getFrame();
                break;
            default:
                throw new IllegalArgumentException("Type \"" + type + "\" is unknown.");
        }
        
        clearTimes();
    }
    

    /**
     * @see com.espn.marcus.media.Timecode#subtract(int, long)
     */
    public synchronized void subtract(int type, long value)
    {
        switch (type)
        {
            case HOURS:
                frame -= (value * getFramesPerHour());
                break;
            case MINUTES:
                frame -= (value * getFramesPerMinute());
                break;
            case SECONDS:
                if (dropframe)
                {
                    int oldMinutes = getMinutes();
                    frame -= (value * fps);
                    CalculatedValues myCalculatedValues = new CalculatedValues(fps, dropframe, frame);
                    int newMinutes = myCalculatedValues.minutes;
                    
                    // adjust for minute boundries
                    if (oldMinutes != newMinutes && oldMinutes % 10 != 0)
                    {
                        frame += _framesToDrop();
                    }
                } else
                {
                    frame -= (value * fps);
                }
                break;
            case FRAMES:
            case TOTAL_FRAMES:
                frame -= value;
                break;
            case MILLISECONDS:
            case TOTAL_MILLISECONDS:
                frame -= (value * getMillisPerFrame());
                break;
            case TIMECODE:
                Timecode tempTimecode = new Timecode(fps, (int) value);
                frame -= tempTimecode.getFrame();
                break;
            default:
                throw new IllegalArgumentException("Type \"" + type + "\" is unknown.");
        }
        
        clearTimes();
    }
    
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        initTimes();
        
        if (timecodeString == null)
        {
            int hours = calculatedValues.hours;
            int minutes = calculatedValues.minutes;
            int seconds = calculatedValues.seconds;
            int frames = calculatedValues.frameInSecond;
//            int subframe = -1;
            
            /* TODO implement subframes
            if (fps > 30)
            {
                double factor = (double) fps / (double) 30;
                int oldFrames = frames;
                frames = (int) (oldFrames / factor);
                subframe = (int) Math.round(oldFrames % factor);
            }
            */
            
            StringBuffer buffer = new StringBuffer(50);
            buffer.append((hours < 10 ? "0" : "") + hours);
            buffer.append(":");
            buffer.append((minutes < 10 ? "0" : "") + minutes);
            buffer.append(":");
            buffer.append((seconds < 10 ? "0" : "") + seconds);
            if (dropframe)
            {
                buffer.append(";");
            } else
            {
                buffer.append(":");
            }
            buffer.append((frames < 10 ? "0" : "") + frames);
            
            /* TODO implement subframes
            if (subframe > 0)
            {
                buffer.append("%").append(subframe < 10 ? "0" : "").append(subframe);
            }
            */
            
            timecodeString = buffer.toString();
        }
        
        return timecodeString;
    }
    
    
    /**
     * Takes the values from the specified timecode and copies them to this one.
     * 
     * @param timecode The timecode to clone
     */
    public void clone(Timecode timecode)
    {
        if (timecode != null)
        {
            setFramesPerSecond(timecode.getFramesPerSecond());
            setDropFrames(timecode.hasDropFrames());
            setFrame(timecode.getFrame());
        }
    }
    
    
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object object)
    {
        if (object instanceof Timecode)
        {
            Timecode testTimecode = (Timecode) object;
            if (getFrame() == testTimecode.getFrame() && getFramesPerSecond() == testTimecode.getFramesPerSecond() && hasDropFrames() == testTimecode.hasDropFrames())
            {
                return true;
            }
        }
        
        return false;
    }
    

    /**
     * Parses the specified timecode string into a valid timecode object.
     * 
     * @param fps Frames Per Second
     * @param timecode The timecode string in the format "00:00:00[:|.|;]00"
     * @return
     * @throws ParseException
     */
    public static Timecode parse(int fps, String timecode)
    throws ParseException
    {
        if (timecode == null)
        {
            throw new ParseException("Timecode cannot be null", 0);
        }
        
        timecode = timecode.trim();
        
        if (!timecode.matches("[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}[:\\.;,][0-9]{1,2}[(@[0-9]{1,2})]"))
        {
            throw new ParseException("Timecode in not in valid format of \"00:00:00[:|.|;|,]00[%00]\"", 0);
        }
        
        boolean droppedFrames = timecode.indexOf(';') > 0 || timecode.indexOf(',') > 0;
        String[] split = timecode.split("[:.;,%]");
        
        int hours = Integer.parseInt(split[0]);
        int minutes = Integer.parseInt(split[1]);
        int seconds = Integer.parseInt(split[2]);
        int frames = Integer.parseInt(split[3]);
        int subframe = 0;
        if (split.length > 4)
        {
            subframe = Integer.parseInt(split[4]);
        }
        
        // Handle invalid timecode values by rounding them up (dropped frames are specified.  Ex. 00:01:00;00).
        if (droppedFrames)
        {
            if (minutes % 10 != 0)
            {
                if (seconds == 0)
                {
                    if (frames == 0 || frames == 1)
                    {
                        frames = 2;
                    }
                }
            }
        }
        return new Timecode(fps, droppedFrames, hours, minutes, seconds, frames, subframe);
    }
    
    
    /**
     * Converts from non drop frame to drop frame duration
     * 
     * @param numframes
     * @return
     */
    public static long convertToDropFrames(int framesPerSecond, long numframes)
    {
        int framesToDrop = framesPerSecond / 15; // 2 per 30 frames
        int framesPerMinute = framesPerSecond * 60;
        int framesPerTenMinutes = framesPerMinute * 10;
        
        long totalFramesToDrop = ((numframes / framesPerMinute) * framesToDrop) - ((numframes / framesPerTenMinutes) * framesToDrop);
        return numframes - totalFramesToDrop;
    }
    

    /**
     * Converts from drop frame to non drop frame duration
     * 
     * @param numframes
     * @return
     */
    public static long convertToNonDropFrames(int framesPerSecond, long numframes)
    {
        int framesToDrop = framesPerSecond / 15; // 2 per 30 frames
        int framesPerMinute = (framesPerSecond * 60) - framesToDrop;
        int framesPerTenMinutes = (framesPerMinute * 10) + framesToDrop;
        
//        long retFrames = numframes + framesToDrop * (int) Math.floor(numframes / framesPerMinute) - framesToDrop * (int) Math.floor(numframes / framesPerTenMinutes);
        long retFrames = numframes + (framesToDrop * (numframes / framesPerMinute)) - (framesToDrop * (numframes / framesPerTenMinutes));
        
        return retFrames;
    }
    

    /**
     * Tests if the specified BCD value is valid.
     * 
     * @param timecode
     * @return
     */
    public static boolean isValidBCD(int timecode)
    {
        return (timecode & 0x80000000) == 0;
    }
    

    /**
     * Converts an Timecode into the BCD format.
     * 
     * @param timecode The timecode
     * @return The BCD
     */
    public static int toBCD(Timecode timecode)
    {
        return toBCD(timecode, BCD_TYPE.BIG_ENDEAN);
    }
    

    /**
     * Converts an ITimecode into the BCD format.
     * 
     * @param timecode The timecode
     * @return The BCD
     */
    public static int toBCD(Timecode timecode, BCD_TYPE bcdType)
    {
        int bcd;
        int droppedFrames = timecode.hasDropFrames() ? 1 : 0;
        int hours = timecode.getHours();
        int minutes = timecode.getMinutes();
        int seconds = timecode.getSeconds();
        int frames = timecode.getFrames();
        
        switch (bcdType)
        {
            case BIG_ENDEAN :
                bcd = (droppedFrames & 0x00000001) << 30;
                bcd += (((hours / 10) & 0x00000003) << 28);
                bcd += (((hours % 10) & 0x0000000F) << 24);
                bcd += (((minutes / 10) & 0x00000007) << 20);
                bcd += (((minutes % 10) & 0x0000000F) << 16);
                bcd += (((seconds / 10) & 0x00000007) << 12);
                bcd += (((seconds % 10) & 0x0000000F) << 8);
                bcd += (((frames / 10) & 0x00000007) << 4);
                bcd += ((frames % 10) & 0x0000000F);
                return bcd;
                
            case LITTLE_ENDEAN :
                bcd = (droppedFrames & 0x00000001) << 30;
                bcd += (((frames / 10) & 0x00000003) << 28);
                bcd += (((frames % 10) & 0x0000000F) << 24);
                bcd += (((seconds / 10) & 0x00000007) << 20);
                bcd += (((seconds % 10) & 0x0000000F) << 16);
                bcd += (((minutes / 10) & 0x00000007) << 12);
                bcd += (((minutes % 10) & 0x0000000F) << 8);
                bcd += (((hours / 10) & 0x00000007) << 4);
                bcd += ((hours % 10) & 0x0000000F);
                return bcd;
            default :
                throw new IllegalArgumentException("BCD type must be one of the supported types.");
        }
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    public int getFramesPerHour()
    {
        initTimes();
        
        return calculatedValues.framesPerHour;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    protected int _nondropFramesPerHour()
    {
        initTimes();
        
        return calculatedValues.nondropFramesPerHour;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    public int getFramesPerMinute()
    {
        initTimes();
        
        return calculatedValues.framesPerMinute;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    protected int _nondropFramesPerMinute()
    {
        initTimes();
        
        return calculatedValues.nondropFramesPerMinute;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    protected int _framesToDrop()
    {
        initTimes();
        
        return calculatedValues.framesToDrop;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    public double getMillisPerFrame()
    {
        initTimes();
        
        return calculatedValues.millisPerFrame;
    }
    
    
    /**
     * Done this way because of JAXB deserialization and I was too lazy to write a real deserializer
     * 
     * @return
     */
    protected double _nondropMillisPerFrame()
    {
        initTimes();
        
        return calculatedValues.nondropMillisPerFrame;
    }
    
    
    protected long _frame_nondrop()
    {
        initTimes();
        
        return calculatedValues.frame_nondrop;
    }
    
    
    protected String getXmlValue()
    {
        return toString();
    }
    
    
    @XmlValue()
    protected synchronized void setXmlValue(String timecodeString)
    {
        clearTimes();
        if (timecodeString != null)
        {
            this.timecodeString = timecodeString.trim();
        }
    }
}
