package org.smpte._2071._2012.quantel.query;

import javax.xml.bind.JAXBElement;

import org.smpte_ra.schemas._2071._2012.mdcf.query.NOT;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public class NOTConverter extends SQLConverter
{
    
    public NOTConverter(SQLConverterFactory factory, QueryExpression not)
    {
        super(factory, not);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        StringBuilder builder = new StringBuilder();
        
        if (expression != null)
        {
            JAXBElement<? extends QueryExpression> element = ((NOT) expression).getQueryExpression();
            
            if (element != null)
            {
                builder.append("!(");
                QueryExpression expr = element.getValue();
                SQLConverter coverter = factory.getConverter(expr);
                builder.append(coverter.toSQL());
                builder.append(")");
            }
        }
        
        return builder.toString();
    }
}
