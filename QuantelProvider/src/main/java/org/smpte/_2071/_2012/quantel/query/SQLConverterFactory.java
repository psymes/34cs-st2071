package org.smpte._2071._2012.quantel.query;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public class SQLConverterFactory
{
    private final Map<String, String> columnMappings;
    
    private final Map<String, String> fieldMappings;
    
    
    public SQLConverterFactory()
    {
        fieldMappings = new HashMap<String, String>();
        columnMappings = new HashMap<String, String>();
    }
    
    
    public void addFieldColumnMapping(String fieldName, String columnName)
    {
        fieldMappings.put(fieldName, columnName);
        columnMappings.put(columnName, fieldName);
    }
    
    
    public void removeFieldColumnMapping(String fieldName, String columnName)
    {
        fieldMappings.remove(fieldName);
        columnMappings.remove(columnName);
    }
    
    
    public Map<String, String> getFieldMapping()
    {
        return new HashMap<String, String>(fieldMappings);
    }
    
    
    public Map<String, String> getColumnMapping()
    {
        return new HashMap<String, String>(columnMappings);
    }
    
    
    public String getColumnName(String field)
    {
        if (fieldMappings.containsKey(field))
        {
            return fieldMappings.get(field);
        } else
        {
            return field;
        }
    }
    
    
    public String getFieldName(String column)
    {
        if (columnMappings.containsKey(column))
        {
            return columnMappings.get(column);
        } else
        {
            return column;
        }
    }
    
    
    @SuppressWarnings("unchecked")
    public SQLConverter getConverter(QueryExpression expression)
    throws SQLConverterException
    {
        String converterName = getClass().getPackage().getName() + "." + expression.getClass().getSimpleName() + "Converter";
        
        try
        {
            Class<SQLConverter> clazz;
            try
            {
                clazz = (Class<SQLConverter>) getClass().getClassLoader().loadClass(converterName);
            } catch (ClassNotFoundException e)
            {
                try
                {
                    clazz = (Class<SQLConverter>) Thread.currentThread().getContextClassLoader().loadClass(converterName);
                } catch (ClassNotFoundException e1)
                {
                    clazz = (Class<SQLConverter>) Class.forName(converterName);
                }
            }
            
            return clazz.getConstructor(SQLConverterFactory.class, QueryExpression.class).newInstance(this, expression);
        } catch (InvocationTargetException ite)
        {
            Throwable e = ite.getTargetException();
            SQLConverterException ie = new SQLConverterException(e.toString());
            ie.setStackTrace(e.getStackTrace());
            throw ie;
        } catch (Exception e)
        {
            SQLConverterException ie = new SQLConverterException(e.toString());
            ie.setStackTrace(e.getStackTrace());
            throw ie;
        }
    }


    public void clearMappings()
    {
        fieldMappings.clear();
        columnMappings.clear();
    }
}
