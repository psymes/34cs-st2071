package org.smpte._2071._2012.quantel;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceDirectory;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.EventBroadcaster;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.Eventer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaAccess;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaDirectory;

public interface ZoneService extends Connectable, OSGiService, Refreshable, Device, DeviceDirectory, MediaDirectory, MediaAccess, Eventer, EventBroadcaster
{
    public static interface ClipStateChangeListener
    {
        public void clipCreated(int clipID);
        
        
        public void clipModified(int clipID);
        
        
        public void clipHidden(int clipID);
        
        
        public void clipDeleted(int clipID);
    }
    
    
    public static interface ServerStateChangeListener
    {
        public void serverUp(int serverID);
        
        
        public void serverDown(int serverID);
        
        
        public void serverChanged(int serverID);
    }
    
    
    public static interface PoolStateChangeListener
    {
        public void poolUp(int poolID);
        
        
        public void poolDown(int poolID);
    }
    
    
    public static interface CopyStateChangeListener
    {
        public void copyComplete(int clipID);
        
        
        public void copyFailed(int clipID);
    }
    
    
    public static interface ZoneStateChangeListener
    {
        public void zoneUp(int zoneID);
        
        
        public void zoneDown(int zoneID);
        
        
        public void zoneFailover(int zoneID);
    }
    
    
    public String getName();
    
    
    public int getZoneID();
    
    
    public int[] getServerIDs();
    
    
    public String getPoolName(int poolID);
    
    
    public String getServerIOR(int serverID)
    throws InvalidIdentifierException, NotConnectedException;
    
    
    public ServerService getServerService(int serverID);
    
    
    public boolean isUp();
    
    
    public void addListener(ClipStateChangeListener listener);
    
    
    public void removeListener(ClipStateChangeListener listener);
    
    
    public void addListener(ServerStateChangeListener listener);
    
    
    public void removeListener(ServerStateChangeListener listener);
    
    
    public void addListener(PoolStateChangeListener listener);
    
    
    public void removeListener(PoolStateChangeListener listener);
    
    
    public void addListener(CopyStateChangeListener listener);
    
    
    public void removeListener(CopyStateChangeListener listener);
    
    
    public void addListener(ZoneStateChangeListener listener);
    
    
    public void removeListener(ZoneStateChangeListener listener);
}
