package org.smpte._2071._2012.quantel.impl;

import java.util.logging.Logger;

import org.smpte._2071._2012.quantel.CorbaConnectionService;
import org.smpte._2071._2012.quantel.CorbaConnectionService.ConnectionListener;
import org.smpte._2071._2012.quantel.Startable;
import org.smpte._2071._2012.quantel.Stoppable;

public abstract class Connectable implements Startable, Stoppable, org.smpte._2071._2012.quantel.Connectable, ConnectionListener
{
    protected Logger log = Logger.getLogger(this.getClass().getName());
    
    protected int state = -1;
    
    protected CorbaConnectionService connectionService;


    public void setConnectionService(CorbaConnectionService connectionService)
    {
        System.out.println("***** Setting Connection Service for " + this.getClass().getSimpleName() + " \"" + connectionService.getIORs() + "\" *****");
        this.connectionService = connectionService;
    }
    
    
    public CorbaConnectionService getConnectionService()
    {
        return this.connectionService;
    }
    
    
    public boolean isConnected()
    {
        return this.connectionService != null && this.connectionService.isConnected();
    }
    
    
    public void start()
    throws Exception
    {
        state = STATE_STARTING;
        System.out.println("***** Starting Service " + this.getClass().getSimpleName() + " *****");
        this.connectionService.addConnectionListener(this);
        state = STATE_STARTED;
    }
    
    
    public void stop()
    {
        state = STATE_STOPPING;
        System.out.println("***** Stopping Service " + this.getClass().getSimpleName() + " *****");
        connectionService.disconnect();
        state = STATE_STOPPED;
    }
    
    
    public void connected()
    {
        System.out.println("***** Service " + this.getClass().getSimpleName() + " Connected to \"" + connectionService.getIORs() + "\" *****");
    }
    
    
    public void disconnected()
    {
        System.out.println("***** Service " + this.getClass().getSimpleName() + " Disonnected from \"" + connectionService.getIORs() + "\" *****");
    }
}
