package org.smpte._2071._2012.quantel;

public class InvalidIdentifierException extends Exception
{
    private static final long serialVersionUID = 201206271143L;


    public InvalidIdentifierException()
    {
    }
    
    
    public InvalidIdentifierException(String message)
    {
        super(message);
    }
    
    
    public InvalidIdentifierException(Throwable cause)
    {
        super(cause);
    }
    
    
    public InvalidIdentifierException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
