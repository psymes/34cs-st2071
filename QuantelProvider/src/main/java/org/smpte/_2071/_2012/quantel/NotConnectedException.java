package org.smpte._2071._2012.quantel;

public class NotConnectedException extends RuntimeException
{
    private static final long serialVersionUID = 201206162216L;


    public NotConnectedException()
    {
    }
    
    
    public NotConnectedException(String m)
    {
        super(m);
    }
    
    
    public NotConnectedException(Throwable t)
    {
        super(t);
    }
    
    
    public NotConnectedException(String m, Throwable t)
    {
        super(m, t);
    }
}
