package org.smpte._2071._2012.quantel;

public interface Stoppable
{
    public static final int STATE_STOPPING = 3;
    
    public static final int STATE_STOPPED = 4;
    

    public void stop();
}
