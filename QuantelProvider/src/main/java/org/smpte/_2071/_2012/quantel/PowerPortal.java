package org.smpte._2071._2012.quantel;

import org.osgi.framework.BundleContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class PowerPortal implements ZoneAware, ApplicationEventPublisherAware
{
    public static class PowerPortalEvent extends ApplicationEvent
    {
        private static final long serialVersionUID = 7289980649532730897L;
        
        private boolean started;
        

        public PowerPortalEvent(PowerPortal source, boolean started)
        {
            super(source);
            this.started = started;
        }
        
        
        public PowerPortal getSource()
        {
            return (PowerPortal) super.getSource();
        }
        
        
        public boolean isStarted()
        {
            return started;
        }
        
        
        public boolean isStopped()
        {
            return !started;
        }
    }
    
    private String uri;
    
    private String username;
    
    private String password;
    
    private int maxSessions;
    
    private String type;
    
    private boolean mixed;

    private ApplicationEventPublisher publisher;

    private ZoneService zoneService;

    private BundleContext bundleContext;

    private CorbaConnectionService connectionService;


    public CorbaConnectionService getConnectionService()
    {
        return connectionService;
    }
    
    
    public boolean isConnected()
    {
        return connectionService.isConnected();
    }


    public BundleContext getBundleContext()
    {
        return this.bundleContext;
    }


    public void setConnectionService(CorbaConnectionService connectionService)
    {
        this.connectionService = connectionService;
    }
    

    public String getURI()
    {
        return uri;
    }
    

    public void setURI(String uri)
    {
        this.uri = uri;
    }
    

    public String getUsername()
    {
        return username;
    }
    

    public void setUsername(String username)
    {
        this.username = username;
    }
    

    public String getPassword()
    {
        return password;
    }
    

    public void setPassword(String password)
    {
        this.password = password;
    }
    

    public int getMaxSessions()
    {
        return maxSessions;
    }
    

    public void setMaxSessions(int maxSessions)
    {
        this.maxSessions = maxSessions;
    }
    

    public String getType()
    {
        return type;
    }
    

    public void setType(String type)
    {
        this.type = type;
    }
    

    public boolean isMixed()
    {
        return mixed;
    }
    

    public void setMixed(boolean mixed)
    {
        this.mixed = mixed;
    }


    public void setApplicationEventPublisher(ApplicationEventPublisher publisher)
    {
        this.publisher = publisher;
    }


    public void start()
    throws Exception
    {
        publisher.publishEvent(new PowerPortalEvent(this, true));
    }


    public void refresh()
    {
    }


    public void stop()
    {
        publisher.publishEvent(new PowerPortalEvent(this, false));
    }


    public void setBundleContext(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }


    public void setZoneService(ZoneService zoneService)
    {
        this.zoneService = zoneService;
    }


    public ZoneService getZoneService()
    {
        return zoneService;
    }
}
