package org.smpte._2071._2012.quantel;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import javax.xml.datatype.DatatypeFactory;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

import com.sun.istack.logging.Logger;

public class MDCHelper
{
    private static Logger log = Logger.getLogger(MDCHelper.class);
    

    public static final DeviceInformation newDeviceInformation(Device device)
    {
        DeviceInformation deviceInformation = new DeviceInformation();
        deviceInformation.setUDN(device.getUDN());
        deviceInformation.setCapabilities(device.getCapabilities());
        deviceInformation.setAttributes(device.getAttributes());
        deviceInformation.setName(device.getName());
        deviceInformation.setOnline(device.getOnline());
        deviceInformation.setURLs(device.getURLs());
        
        DateTime validTil = new DateTime();
        try
        {
            GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
            calendar.add(Calendar.SECOND, 30);
            validTil.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
            validTil.setHour(calendar.get(Calendar.HOUR));
            validTil.setMinute(calendar.get(Calendar.MINUTE));
            validTil.setSecond(calendar.get(Calendar.SECOND));
            validTil.setMicrosecond(calendar.get(Calendar.MILLISECOND) * 1000);
            BigInteger fromEpoch = BigInteger.valueOf(calendar.getTimeInMillis()).multiply(BigInteger.valueOf(1000));
            validTil.setMicrosFromEpoch(fromEpoch);
            deviceInformation.setValidTill(validTil);
        } catch (Exception e)
        {
            log.log(Level.WARNING, "Could not create the MDCF DateTime for the DeviceInformtaion ValidTill field - " + e.getMessage(), e);
        }
        
        return deviceInformation;
    }
}
