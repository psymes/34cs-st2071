package org.smpte._2071._2012.quantel;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public final class Pool<T>
{
    public static interface Factory<T>
    {
        public T newInstance();
    }
    
    
    private final BlockingQueue<T> objects;
    
    private final Factory<T> factory;
    
    
    public Pool(int size, Factory<T> factory)
    {
        this.objects = new ArrayBlockingQueue<T>(size, false);
        this.factory = factory;
    }
    
    
    public T borrow()
    throws InterruptedException
    {
        if (this.objects.peek() == null && this.objects.remainingCapacity() > 0)
        {
            this.objects.offer(factory.newInstance());
        }
        return this.objects.take();
    }
    
    
    public void giveBack(T object)
    {
        if (!this.objects.offer(object))
        {
            throw new RuntimeException("Pool Too Full");
        }
    }
}
