package org.smpte._2071._2012.quantel.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.smpte._2071._2012.quantel.ChannelService;
import org.smpte._2071._2012.quantel.CorbaConnectionService;
import org.smpte._2071._2012.quantel.MDCHelper;
import org.smpte._2071._2012.quantel.CorbaConnectionService.ConnectionListener;
import org.smpte._2071._2012.quantel.NotConnectedException;
import org.smpte._2071._2012.quantel.Stoppable;
import org.smpte._2071._2012.quantel.ZoneService.ServerStateChangeListener;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.URLs;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public abstract class ServerService extends ZoneAware implements org.smpte._2071._2012.quantel.ServerService, ConnectionListener, ServerStateChangeListener
{
    protected HashMap<String, ServiceRegistration> services = new HashMap<String, ServiceRegistration>();
    
    protected CorbaConnectionService connectionService;

    protected int serverID;
    
    protected String udn;
    
    protected org.smpte_ra.schemas._2071._2012.mdcf.types.Map attributes;

    protected URLs urls;


    public void setConnectionService(CorbaConnectionService connectionService)
    {
        this.connectionService = connectionService;
    }


    public CorbaConnectionService getConnectionService()
    {
        return connectionService;
    }


    public void setServerID(int serverID)
    {
        this.serverID = serverID;
    }


    public int getServerID()
    {
        return serverID;
    }
    
    
    protected FramedTime serverTimeToFramedTime(int refTime)
    {
        // TODO: Convert Server Reference Time to Framed Time
        return null;
    }
    
    
    public void connected()
    {
        refresh();
    }
    
    
    public void disconnected()
    {
    }


    public void serverUp(int serverID)
    {
        if (serverID == getServerID())
        {
            try
            {
                String ior = zoneService.getServerIOR(serverID);
                connectionService.setIORs(ior);
                connectionService.connect();
            } catch (Exception e)
            {
                log.log(Level.WARNING, e.getMessage());
            }
            
            refresh();
        }
    }


    public void serverDown(int serverID)
    {
        if (serverID == getServerID())
        {
            connectionService.disconnect();
            
            refresh();
        }
    }


    public void serverChanged(int serverID)
    {
        if (serverID == getServerID())
        {
            refresh();
        }
    }


    public void refresh()
    {
        ServiceRegistration registration = null;
        int numChannels = getNumberOfChannels();
        String[] chanPorts = getChanPorts();
        if (numChannels > 0)
        {
            for (int index = 0; index < numChannels; index++)
            {
                if ((registration = services.get("C" + serverID + "-" + index)) == null)
                {
                    try
                    {
                        System.out.println("Creating Service for Channel " + serverID + "-" + index);
                        ChannelService channelService = createChannelService(this, index, chanPorts[index]);
                        System.out.println("Registering Service for Channel " + serverID + "-" + index);
                        registerChannelService(channelService);
                        channelService.start();
                    } catch (NotConnectedException e)
                    {
                        throw e;
                    } catch (Exception e)
                    {
                        if (log.isLoggable(Level.INFO))
                        {
                            log.log(Level.SEVERE, "Error starting Channel Service (" + serverID + "-" + index + ") - " + e.getMessage(), e);
                        } else
                        {
                            log.log(Level.SEVERE, "Error starting Channel Service (" + serverID + "-" + index + ") - " + e.getMessage());
                        }
                    }
                } else
                {
                    ChannelService channelService = (ChannelService) bundleContext.getService(registration.getReference());
                    channelService.setName(chanPorts[index]);
                    channelService.refresh();
                    bundleContext.ungetService(registration.getReference());
                }
            }
        }
        
        int[] poolIDs = getPoolIDs();
        for (int poolID : poolIDs)
        {
            if ((registration = services.get("P" + serverID + "-" + poolID)) == null)
            {
                System.out.println("Creating Service for Pool " + serverID + "-" + poolID);
                org.smpte._2071._2012.quantel.PoolService poolService = createPoolService(this, poolID);
                System.out.println("Registering Service for Pool " + serverID + "-" + poolID);
                registerPoolService(poolService);
                try
                {
                    poolService.start();
                } catch (Exception e)
                {
                    log.log(Level.WARNING, e.getMessage(), e);
                }
            } else
            {
                PoolService poolService = (PoolService) bundleContext.getService(registration.getReference());
                poolService.refresh();
                bundleContext.ungetService(registration.getReference());
            }
        }
    }


    public void start()
    throws Exception
    {
        super.start();
        
        connectionService.addConnectionListener(this);
        
        zoneService.addListener(this);
        
        udn = "urn:smpte:udn:Quantel." + getZoneService().getZoneID() + ":type=ServerService,zone=" + getZoneService().getZoneID() + ",server=" + getServerID() + ",name=" + getName();
        try
        {
            String ior = zoneService.getServerIOR(serverID);
            connectionService.setIORs(ior);
            connectionService.connect();
        } catch (Exception e)
        {
            log.log(Level.WARNING, e.getMessage());
        }
    }


    @SuppressWarnings("unchecked")
    public void stop()
    {
        super.stop();
        
        for (Map.Entry<String, ServiceRegistration> entry : services.entrySet().toArray(new Map.Entry[0]))
        {
            ServiceRegistration serviceRegistration = entry.getValue();
            try
            {
                Stoppable stoppable = (Stoppable) bundleContext.getService(serviceRegistration.getReference());
                serviceRegistration.unregister();
                try
                {
                    stoppable.stop();
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + entry.getKey() + ") - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + entry.getKey() + ") - " + e.getMessage());
                    }
                }
            } catch(Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + entry.getKey() + ") - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + entry.getKey() + ") - " + e.getMessage());
                }
            } finally
            {
                services.remove(entry.getKey());
            }
        }
    }
    
    
    protected abstract org.smpte._2071._2012.quantel.ChannelService createChannelService(org.smpte._2071._2012.quantel.ServerService serverService, int channelID, String name);


    protected abstract org.smpte._2071._2012.quantel.PoolService createPoolService(org.smpte._2071._2012.quantel.ServerService serverService, int poolID);
    
    
    protected String generatePoolServiceName(org.smpte._2071._2012.quantel.PoolService poolService)
    {
        return "P" + poolService.getServerService().getServerID() + "-" + poolService.getPoolID();
    }

    
    protected void registerChannelService(org.smpte._2071._2012.quantel.ChannelService channelService)
    {
        channelService.setBundleContext(bundleContext);
        org.smpte._2071._2012.quantel.ServerService serverService = channelService.getServerService();
        
        Properties props = new Properties();
        props.put("UDN", channelService.getUDN());
        props.put("ZoneID", Integer.toString(serverService.getZoneService().getZoneID()));
        props.put("ZoneName", serverService.getZoneService().getName());
        props.put("ServerID", Integer.toString(serverService.getServerID()));
        props.put("ServerName", serverService.getServerName());
        props.put("FrameRate", Float.toString(serverService.getFrameRate()));
        props.put("ChannelID", Integer.toString(channelService.getChannelID()));
        ServiceRegistration serviceRegistration = bundleContext.registerService("org.smpte._2071._2012.quantel.ChannelService", channelService, props);
        services.put("C" + serverService.getServerID() + "-" + channelService.getChannelID(), serviceRegistration);
    }

    
    protected void registerPoolService(org.smpte._2071._2012.quantel.PoolService poolService)
    {
        poolService.setBundleContext(bundleContext);
        org.smpte._2071._2012.quantel.ServerService serverService = poolService.getServerService();
        
        Properties props = new Properties();
        props.put("UDN", poolService.getUDN());
        props.put("ZoneID", Integer.toString(serverService.getZoneService().getZoneID()));
        props.put("ZoneName", serverService.getZoneService().getName());
        props.put("ServerID", Integer.toString(serverService.getServerID()));
        props.put("ServerName", serverService.getServerName());
        props.put("FrameRate", Float.toString(serverService.getFrameRate()));
        props.put("PoolID", Integer.toString(poolService.getPoolID()));
        ServiceRegistration serviceRegistration = bundleContext.registerService("org.smpte._2071._2012.quantel.PoolService", poolService, props);
        services.put("P" + serverService.getServerID() + "-" + poolService.getPoolID(), serviceRegistration);
    }
    
    
    public List<DeviceInformation> getChildren()
    {
        ArrayList<DeviceInformation> children = new ArrayList<DeviceInformation>();
        Bundle bundle = bundleContext.getBundle();
        
        for (ServiceRegistration registration : services.values())
        {
            ServiceReference reference = registration.getReference();
            if (reference.isAssignableTo(bundle, Device.class.getName()))
            {
                try
                {
                    Device device = (Device) bundleContext.getService(reference);
                    children.add(MDCHelper.newDeviceInformation(device));
                    bundleContext.ungetService(reference);
                } catch (Exception e)
                {
                    log.log(Level.WARNING, "Error getting server child - " + e.getMessage(), e);
                }
            }
        }
        
        return children;
    }
    
    
    public boolean isUp()
    {
        return zoneService.getConnectionService().isConnected() && connectionService.isConnected() && !testDown();
    }
    
    
    protected abstract boolean testDown();


    public String getName()
    {
        return getServerName();
    }
    
    
    public String getUDN()
    {
        return udn;
    }


    public URLs getURLs()
    {
        return new URLs();
    }


    public boolean getOnline()
    {
        return isUp();
    }


    public org.smpte_ra.schemas._2071._2012.mdcf.types.Map getAttributes()
    {
        return attributes;
    }


    public Capabilities getCapabilities()
    {
        return capabilities;
    }
}
