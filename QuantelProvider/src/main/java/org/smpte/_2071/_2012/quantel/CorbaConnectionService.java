package org.smpte._2071._2012.quantel;

import java.util.Map;

import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;

public interface CorbaConnectionService extends OSGiService
{
    public static interface ConnectionListener
    {
        public void connected();
        
        
        public void disconnected();
    }
    
    
    public void connect()
    throws Exception;
    

    public void disconnect();


    public String getVersion();
    
    
    public boolean isConnected();
    
    
    public <T extends org.omg.CORBA.Object> T getConnectionObject(Class<T> type)
    throws NotConnectedException;
    
    
    public void setProperties(Map<String, String> props);
    
    
    public Map<String, String> getProperties();
    
    
    public void setIORs(String iors);
    
    
    public String getIORs();
    
    
    public ORB getORB();
    
    
    public POA getPOA();
    
    
    public void addConnectionListener(ConnectionListener listener);
    
    
    public void removeConnectionListener(ConnectionListener listener);
}
