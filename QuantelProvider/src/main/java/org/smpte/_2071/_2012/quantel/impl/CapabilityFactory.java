package org.smpte._2071._2012.quantel.impl;

import java.util.ArrayList;

import org.smpte_ra.schemas._2071._2012.mdcf.device.Capability;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.URLs;

public class CapabilityFactory
{
    public static final String UCN_PREFIX = "urn:smpte:ucn:";
    
    public static final String VERSION_1_0 = "_v1.0";
    
    
    public static Capability newCapability(Class<?> iface, ArrayList<String> urls)
    {
        StringBuilder temp = new StringBuilder();
        
        if (iface.isInterface())
        {
            String name = iface.getSimpleName();
            for (int index = 0; index < name.length(); index++)
            {
                char c = name.charAt(index);
                
                if (index > 0)
                {
                    temp.append(Character.toLowerCase(c));
                } else if (Character.isUpperCase(c))
                {
                    temp.append('_');
                    temp.append(Character.toLowerCase(c));
                } else
                {
                    temp.append(c);
                }
            }
            
            if (temp.length() > 0)
            {
                Capability capability = new Capability();
                capability.setUCN(UCN_PREFIX + temp.toString() + VERSION_1_0);
                
                if (urls != null)
                {
                    capability.setURLs(new URLs());
                    for (String url : urls)
                    {
                        capability.getURLs().getURL().add(url);
                    }
                }
                
                return capability;
            }
        }
        
        
        return null;
    }
}
