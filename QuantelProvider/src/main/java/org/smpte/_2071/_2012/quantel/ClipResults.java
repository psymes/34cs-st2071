package org.smpte._2071._2012.quantel;

import java.util.HashMap;
import java.util.Map;

public class ClipResults
{
    private final int rowCount;
    
    private final String[] data;
    
    private final String[] columns;
    
    private final Map<String, Integer> columnMap = new HashMap<String, Integer>();
    
    
    public ClipResults(String[] columns, String[] data)
    {
        this.columns = columns;
        this.data = data;
        this.rowCount = data.length / columns.length;
        
        for (int index = 0; index < columns.length; index++)
        {
            columnMap.put(columns[index], index);
        }
    }
    
    
    public String[] columns()
    {
        return columns;
    }
    
    
    public String getValue(int row, int column)
    {
        int index = (row * columns.length) + column;
        return data.length > index ? data[index] : null;
    }
    
    
    public String getValue(int row, String column)
    {
        if (column != null)
        {
            Integer index = columnMap.get(column);
            if (index != null)
            {
                return getValue(row, index);
            }
        }
        
        return null;
    }
    
    
    public int size()
    {
        return rowCount;
    }
}
