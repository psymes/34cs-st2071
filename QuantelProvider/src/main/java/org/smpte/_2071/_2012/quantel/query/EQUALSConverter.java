package org.smpte._2071._2012.quantel.query;

import java.math.BigDecimal;

import org.smpte_ra.schemas._2071._2012.mdcf.query.EQUALS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

public class EQUALSConverter extends SQLConverter
{
    
    public EQUALSConverter(SQLConverterFactory factory, QueryExpression expression)
    {
        super(factory, expression);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        StringBuilder builder = new StringBuilder();
        
        if (expression != null)
        {
            final EQUALS equals = (EQUALS) expression;
            final String field = equals.getField();
            final BigDecimal number = equals.getNumber();
            final Object object = equals.getObject();
            final DateTime time = equals.getTime();
            
            builder.append(factory.getColumnName(field)).append(" = ");
            
            if (number != null)
            {
                builder.append(number.toPlainString());
            } else if (time != null)
            {
                builder.append(dateTimeToString(time));
            } else if (object != null)
            {
                builder.append(objectToString(object));
            }
        }
        
        return builder.toString();
    }
}
