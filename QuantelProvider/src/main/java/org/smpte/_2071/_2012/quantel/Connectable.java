package org.smpte._2071._2012.quantel;


public interface Connectable
{
    public void setConnectionService(org.smpte._2071._2012.quantel.CorbaConnectionService connectionService);
    
    
    public org.smpte._2071._2012.quantel.CorbaConnectionService getConnectionService();
    
    
    public boolean isConnected();
}
