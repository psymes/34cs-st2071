package org.smpte._2071._2012.quantel.query;

import java.math.BigDecimal;

import org.smpte_ra.schemas._2071._2012.mdcf.query.LESSTHAN;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

public class LESSTHANConverter extends SQLConverter
{
    
    public LESSTHANConverter(SQLConverterFactory factory, QueryExpression expression)
    {
        super(factory, expression);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        StringBuilder builder = new StringBuilder();
        
        if (expression != null)
        {
            final LESSTHAN expr = (LESSTHAN) expression;
            final String field = expr.getField();
            final BigDecimal number = expr.getNumber();
            final Object object = expr.getObject();
            final DateTime time = expr.getTime();
            
            builder.append(factory.getColumnName(field)).append(" < ");
            
            if (number != null)
            {
                builder.append(number.toPlainString());
            } else if (time != null)
            {
                builder.append(dateTimeToString(time));
            } else if (object != null)
            {
                builder.append(objectToString(object));
            }
        }
        
        return builder.toString();
    }
    
}
