package org.smpte._2071._2012.quantel;

public interface Startable
{
    public static final int STATE_STARTING = 1;
    
    public static final int STATE_STARTED = 2;
    
    
    public void start()
    throws Exception;
}
