package org.smpte._2071._2012.quantel.query;

public class SQLConverterException extends RuntimeException
{
    private static final long serialVersionUID = -3169812487090847930L;


    public SQLConverterException()
    {
    }
    
    
    public SQLConverterException(String message)
    {
        super(message);
    }
    
    
    public SQLConverterException(Throwable cause)
    {
        super(cause);
    }
    
    
    public SQLConverterException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
