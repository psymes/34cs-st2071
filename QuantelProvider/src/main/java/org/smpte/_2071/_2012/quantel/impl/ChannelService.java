package org.smpte._2071._2012.quantel.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.osgi.framework.ServiceRegistration;
import org.smpte._2071._2012.quantel.CorbaConnectionService;
import org.smpte._2071._2012.quantel.CorbaConnectionService.ConnectionListener;
import org.smpte._2071._2012.quantel.ServerService;
import org.smpte._2071._2012.quantel.Startable;
import org.smpte._2071._2012.quantel.Stoppable;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Capabilities;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.URLs;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public abstract class ChannelService extends ZoneAware implements org.smpte._2071._2012.quantel.ChannelService, ConnectionListener
{
    protected String udn;
    
    protected org.smpte_ra.schemas._2071._2012.mdcf.types.Map attributes;

    protected URLs urls;
    
    protected ServerService serverService;
    
    protected String name;

    protected int channelID;
    
    protected CorbaConnectionService connectionService;

    private List<ServiceRegistration> services = new ArrayList<ServiceRegistration>();


    public void setConnectionService(CorbaConnectionService connectionService)
    {
        this.connectionService = connectionService;
    }


    public CorbaConnectionService getConnectionService()
    {
        return connectionService;
    }


    public void setServerService(ServerService serverService)
    {
        this.serverService = serverService;
    }
    
    
    public ServerService getServerService()
    {
        return serverService;
    }
    
    
    public boolean isUp()
    {
        return serverService.isUp();
    }
    
    
    public float getFrameRate()
    {
        return serverService.getFrameRate();
    }
    

    public FramedTime getReferenceTime()
    {
        return serverService.getReferenceTime();
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public String getName()
    {
        return name;
    }
    
    
    public void setChannelID(int channelID)
    {
        this.channelID = channelID;
    }
    
    
    public int getChannelID()
    {
        return channelID;
    }
    
    
    @Override
    public void start()
    throws Exception
    {
        for (ServiceRegistration serviceRegistration : services.toArray(new ServiceRegistration[services.size()]))
        {
            try
            {
                Startable startable = (Startable) bundleContext.getService(serviceRegistration.getReference());
                try
                {
                    startable.start();
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + serviceRegistration + ") - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + serviceRegistration + ") - " + e.getMessage());
                    }
                }
            } catch(Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + serviceRegistration + ") - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + serviceRegistration + ") - " + e.getMessage());
                }
            } finally
            {
                bundleContext.ungetService(serviceRegistration.getReference());
            }
        }
        
        udn = "urn:smpte:udn:Quantel." + getZoneService().getZoneID() + ":type=ChannelService,zone=" + getZoneService().getZoneID() + ",server=" + getServerService().getServerID() + ",channel=" + getChannelID();
    }
    
    
    @Override
    public void stop()
    {
        for (ServiceRegistration serviceRegistration : services.toArray(new ServiceRegistration[services.size()]))
        {
            try
            {
                Stoppable stoppable = (Stoppable) bundleContext.getService(serviceRegistration.getReference());
                serviceRegistration.unregister();
                try
                {
                    stoppable.stop();
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + serviceRegistration + ") - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Error stopping Service \"" + serviceRegistration + ") - " + e.getMessage());
                    }
                }
            } catch(Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + serviceRegistration + ") - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error unregistering Service (" + serviceRegistration + ") - " + e.getMessage());
                }
            } finally
            {
                services.remove(serviceRegistration);
            }
        }
    }


    public String getUDN()
    {
        return udn;
    }


    public URLs getURLs()
    {
        return new URLs();
    }


    public boolean getOnline()
    {
        return isUp();
    }

    
    public org.smpte_ra.schemas._2071._2012.mdcf.types.Map getAttributes()
    {
        return attributes;
    }


    public Capabilities getCapabilities()
    {
        return capabilities;
    }
}
