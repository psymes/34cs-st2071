package org.smpte._2071._2012.quantel;

public final class FastCharSequence implements CharSequence
{
    private final int end;
    
    private final int start;
    
    private final CharSequence source;

    private String string;
    
    
    public FastCharSequence(CharSequence source)
    {
        this(source, 0, source.length());
    }
    
    
    public FastCharSequence(CharSequence source, int start, int end)
    {
        this.end = end;
        this.start = start;
        this.source = source;
    }
    
    
    public final int length()
    {
        return end - start;
    }
    
    
    public final char charAt(int index)
    {
        return source.charAt(start + index);
    }
    
    
    public final FastCharSequence subSequence(int start, int end)
    {
        return new FastCharSequence(source, this.start + start, this.start + end);
    }
    
    
    public final int getStartPosInSource()
    {
        return this.start; 
    }
    
    
    public final int getEndPosInSource()
    {
        return this.end; 
    }
    
    
    public int indexOf(int ch, int fromIndex)
    {
        if (fromIndex < 0)
        {
            fromIndex = 0;
        } else if (fromIndex >= length())
        {
            return -1;
        }
        
        final int start = this.start;
        final int end = this.end;
        fromIndex = fromIndex + start;
        
        if (ch < Character.MIN_SUPPLEMENTARY_CODE_POINT) 
        {
            for (; fromIndex < end; fromIndex++)
            {
                if (source.charAt(fromIndex) == ch)
                {
                    return fromIndex - start;
                }
            }
        }
        
        if (ch <= Character.MAX_CODE_POINT)
        {
            // handle supplementary characters here
            char[] surrogates = Character.toChars(ch);
            for (; fromIndex < end; fromIndex++)
            {
                if (source.charAt(fromIndex) == surrogates[0])
                {
                    int index = fromIndex + 1;
                    if (index == end)
                    {
                        break;
                    }
                    if (source.charAt(index) == surrogates[1])
                    {
                        return fromIndex - start;
                    }
                }
            }
        }

        return -1;
    }
    
    
    public String toString()
    {
        if (string == null)
        {
            char[] chars = new char[length()];
            for (int index = 0; index < chars.length; index++)
            {
                chars[index] = charAt(index);
            }
            
            string = new String(chars);
        }
        
        return string;
    }
    
    
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        
        if (o instanceof CharSequence)
        {
            CharSequence that = (CharSequence) o;
            final int thisLength = length(); /* avoid getfield & invoke* opcodes */
            final int thatLength = that.length(); /* avoid getfield & invoke* opcodes */
            final CharSequence source = this.source; /* avoid getfield opcode */
            final int start = this.start; /* avoid getfield opcode */
            
            if (thisLength == thatLength)
            {
                for (int index = 0; index < thisLength; index++)
                {
                    if (source.charAt(start + index) != that.charAt(index))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
}
