package org.smpte._2071._2012.quantel;

public interface Refreshable
{
    public void refresh();
}
