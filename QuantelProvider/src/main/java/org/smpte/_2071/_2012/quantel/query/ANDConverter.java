package org.smpte._2071._2012.quantel.query;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.smpte_ra.schemas._2071._2012.mdcf.query.AND;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

public class ANDConverter extends SQLConverter
{
    public ANDConverter(SQLConverterFactory factory, QueryExpression and)
    {
        super(factory, and);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        StringBuilder builder = new StringBuilder();
        
        if (expression != null)
        {
            List<JAXBElement<? extends QueryExpression>> list = ((AND) expression).getQueryExpression();
            
            if (list != null)
            {
                int length = list.size();
                if (length > 0)
                {
                    builder.append("(");
                    for (int index = 0; index < length; index++)
                    {
                        JAXBElement<? extends QueryExpression> element = list.get(index);
                        QueryExpression expr = element.getValue();
                        SQLConverter coverter = factory.getConverter(expr);
                        builder.append(coverter.toSQL());
                        if (index + 1 < length)
                        {
                            builder.append(" AND ");
                        }
                    }
                    builder.append(")");
                }
            }
        }
        
        return builder.toString();
    }
}
