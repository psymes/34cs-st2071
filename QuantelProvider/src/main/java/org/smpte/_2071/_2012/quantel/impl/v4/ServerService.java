package org.smpte._2071._2012.quantel.impl.v4;

import java.util.logging.Level;

import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

import Quentin.v4.Server;
import Quentin.v4.ServerInfo;

public class ServerService extends org.smpte._2071._2012.quantel.impl.ServerService
{
    private String serverName;
    
    private int numChannels;

    private int[] pools;

    
    public String getServerName()
    {
        return serverName;
    }
    
    
    public float getFrameRate()
    {
        String framerate = connectionService.getConnectionObject(Server.class).getProperty(Server.frameRate);
        
        try
        {
            return Float.parseFloat(framerate);
        } catch (Exception e)
        {
            return -1f;
        }
    }


    public String[] getChanPorts()
    {
        return connectionService.getConnectionObject(Server.class).getChanPorts();
    }


    public FramedTime getReferenceTime()
    {
        return serverTimeToFramedTime(connectionService.getConnectionObject(Server.class).getRefTime());
    }


    protected boolean testDown()
    {
        boolean down;
        
        try
        {
            down = connectionService.getConnectionObject(Server.class).getServerInfo().down;
        } catch (Exception e)
        {
            down = true;
        }
        
        return down;
    }
    
    
    public int getNumberOfChannels()
    {
        return numChannels;
    }
    
    
    public int[] getPoolIDs()
    {
        return pools;
    }


    public void connected()
    {
        ServerInfo info = connectionService.getConnectionObject(Server.class).getServerInfo();
        serverName = info.name;
        numChannels = info.numChannels;
        pools = info.pools;
        
        super.connected();
    }


    public void disconnected()
    {
        super.disconnected();
    }
    
    
    public void refresh()
    {
        super.refresh();
    }
    
    
    @Override
    protected org.smpte._2071._2012.quantel.ChannelService createChannelService(org.smpte._2071._2012.quantel.ServerService serverService, int channelID, String name)
    {
        ChannelService channelService = new ChannelService();
        
        channelService.setZoneService(serverService.getZoneService());
        channelService.setServerService(serverService);
        channelService.setChannelID(channelID);
        channelService.setName(name);
        channelService.setBundleContext(bundleContext);
        
        try
        {
            channelService.start();
        } catch (Exception e)
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.WARNING, "Error starting ChannelService (" + serverService.getServerID() + "-" + channelID + ") - " + e.getMessage(), e);
            } else
            {
                log.log(Level.WARNING, "Error starting ChannelService (" + serverService.getServerID() + "-" + channelID + ") - " + e.getMessage());
            }
        }
        
        return channelService;
    }
    
    
    @Override
    protected org.smpte._2071._2012.quantel.PoolService createPoolService(org.smpte._2071._2012.quantel.ServerService serverService, int poolID)
    {
        PoolService poolService = new PoolService();
        
        poolService.setZoneService(serverService.getZoneService());
        poolService.setServerService(serverService);
        poolService.setPoolID(poolID);
        
        try
        {
            poolService.start();
        } catch (Exception e)
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.WARNING, "Error starting PoolService (" + serverService.getServerID() + "-" + poolID + ") - " + e.getMessage(), e);
            } else
            {
                log.log(Level.WARNING, "Error starting PoolService (" + serverService.getServerID() + "-" + poolID + ") - " + e.getMessage());
            }
        }
        
        return poolService;
    }
    
    
    @Override
    public void start()
    throws Exception
    {
        super.start();
    }
}
