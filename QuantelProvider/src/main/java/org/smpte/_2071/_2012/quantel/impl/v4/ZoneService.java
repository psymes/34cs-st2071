package org.smpte._2071._2012.quantel.impl.v4;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.ServantAlreadyActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;
import org.smpte._2071._2012.quantel.ClipResults;
import org.smpte._2071._2012.quantel.InvalidIdentifierException;
import org.smpte._2071._2012.quantel.NotConnectedException;
import org.smpte._2071._2012.quantel.RN;
import org.smpte._2071._2012.quantel.impl.CorbaConnectionService;
import org.smpte._2071._2012.quantel.query.SQLConverterFactory;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.PollResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.RegisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.URIParameter;
import org.smpte_ra.schemas._2071._2012.mdcf.device.event.UnregisterCallbackResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.identity.UDNs;
import org.smpte_ra.schemas._2071._2012.mdcf.query.InvalidQuery;
import org.smpte_ra.schemas._2071._2012.mdcf.query.InvalidQueryFault;
import org.smpte_ra.schemas._2071._2012.mdcf.query.PAGE;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.query.SORTBY;
import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityExceptionFault;

import Quentin.v4.BadColumnData;
import Quentin.v4.BadIdent;
import Quentin.v4.ClipProperty;
import Quentin.v4.ColumnDesc;
import Quentin.v4.DatabaseError;
import Quentin.v4.Server;
import Quentin.v4.StateChangeInfo;
import Quentin.v4.StateChangeListener;
import Quentin.v4.StateChangeListenerHelper;
import Quentin.v4.StateChangeListenerPOA;
import Quentin.v4.StateChangeType;
import Quentin.v4.ZonePortal;


public class ZoneService extends org.smpte._2071._2012.quantel.impl.ZoneService
{
    protected class StateChangeListenerPOAImpl extends StateChangeListenerPOA
    {
        public void newChanges(StateChangeInfo[] changes)
        {
            lastChange = System.currentTimeMillis();
            
            if (changes != null && changes.length > 0)
            {
                if (log.isLoggable(Level.FINE))
                {
                    log.info("newChanges - " + changes.length + " changes.");
                }
                
                for (int index = 0; index < changes.length; index++)
                {
                    final int type = changes[index].type.value();
                    final int ident = changes[index].ident;
                    
                    System.out.println("State Change received for Zone \"" + ZoneService.this.name + "\" (" + ZoneService.this.zoneID + "): changeNum=" + changes[index].changeNum + ", type=" + typeToString(changes[index].type.value()) + ", ident=" + ident + " ");
                    
                    executor.execute(new Runnable()
                    {
                        public void run()
                        {
                            notifyListeners(type, ident);
                        }
                    });
                }
                
                dataProps.put(PROP_LAST_CHANGE_NUMBER, Integer.toString(changes[changes.length - 1].changeNum));;
                flushProps();
            }
        }
        
        
        protected String typeToString(int type)
        {
            switch (type)
            {
                case StateChangeType._clipCreated :
                    return "ClipCreated";
                case StateChangeType._clipDeleted :
                    return "ClipDeleted";
                case StateChangeType._clipHidden :
                    return "ClipHidden";
                case StateChangeType._clipModified :
                    return "ClipModified";
                case StateChangeType._copyComplete :
                    return "CopyComplete";
                case StateChangeType._copyFailed :
                    return "CopyFailed";
                case StateChangeType._poolDown :
                    return "PoolDown";
                case StateChangeType._poolUp :
                    return "PoolUp";
                case StateChangeType._serverChanged :
                    return "ServerChanged";
                case StateChangeType._serverDown :
                    return "ServerDown";
                case StateChangeType._serverUp :
                    return "ServerUp";
                case StateChangeType._zoneDown :
                    return "ZoneDown";
                case StateChangeType._zoneUp :
                    return "ZoneUp";
                case StateChangeType._zoneFailover :
                    return "ZoneFailover";
            }
            
            return "Unknown";
        }
    }
    
    private byte[] stateChangeListenerIOD;
    
    private long lastChange;

    private StateChangeListener listener;


    public int getZoneID()
    {
        if (zoneID == 0)
        {
            ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
            zoneID = portal.getZoneNumber();
        }
        
        return zoneID;
    }


    public String getName()
    {
        if (name == null)
        {
            ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
            try
            {
                name = portal.getZoneName(getZoneID());
            } catch (BadIdent e)
            {
                log.log(Level.WARNING, "Bad ident on my zone.  How did this happen?!?!", e);
            }
        }
        
        return name;
    }
    
    
    public boolean isUp()
    {
        return this.connectionService.isConnected();
    }
    
    
    public int[] getServerIDs()
    {
        int[] results = connectionService.getConnectionObject(ZonePortal.class).getServers(true);
        return results;
    }
    
    
    public String getServerIOR(int serverID) 
    throws InvalidIdentifierException , NotConnectedException
    {
        Server server = null;
        try
        {
            server = connectionService.getConnectionObject(ZonePortal.class).getServer(serverID);
            return connectionService.getORB().object_to_string(server);
        } catch (BadIdent e)
        {
            throw new InvalidIdentifierException("Server \"" + serverID + "\" does not exist.");
        } catch (NotConnectedException e)
        {
            throw e;
        } finally
        {
            if (server != null)
            {
                try
                {
                    server._release();
                } catch (Exception e)
                {
                    // Ignore
                }
            }
        }
    }


    public ServerService createServerService(int serverID)
    {
        ServerService serverService = null;
        
        try
        {
            CorbaConnectionService connectionService = new CorbaConnectionService();
            connectionService.setProperties(this.connectionService.getProperties());
            connectionService.start();
            
            serverService = new ServerService();
            serverService.setConnectionService(connectionService);
            serverService.setServerID(serverID);
            serverService.setBundleContext(bundleContext);
            serverService.setZoneService(this);
            
            try
            {
                serverService.start();
            } catch (Exception e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error starting ServerService (" + serverID + ") - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error starting ServerService (" + serverID + ") - " + e.getMessage());
                }
            }
        } catch (Exception e)
        {
            if (log.isLoggable(Level.INFO))
            {
                log.log(Level.WARNING, "Error creating ServerService (" + serverID + ") - " + e.getMessage(), e);
            } else
            {
                log.log(Level.WARNING, "Error creating ServerService (" + serverID + ") - " + e.getMessage());
            }
        }
            
        return serverService;
    }
    
    
    public String getPoolName(int poolID)
    {
        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        try
        {
            return portal.getPoolInfo(poolID).name;
        } catch (Exception e)
        {
            log.log(Level.WARNING, e.getMessage(), e);
            return null;
        }
    }
    
    
    public void connected()
    {
        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        
        zoneID = portal.getZoneNumber();
        
        try
        {
            name = portal.getZoneName(zoneID);
        } catch (BadIdent e)
        {
            log.warning(e.getMessage());
        }
        
        System.out.println("***** Connected to Zone \"" + name + "\" (" + zoneID + ")");
        activateStateChangeListener();
        
        super.connected();
    }


    public void disconnected()
    {
        System.out.println("***** Disconnected from Zone \"" + name + "\" (" + zoneID + ")");
        
        notifyListeners(StateChangeType._zoneDown, zoneID);
        deactivateStateChangeListener();
        
        super.disconnected();
    }


    public void start()
    throws Exception
    {
        super.start();
        
        try
        {
            String tempUDN = getUDN();
            int pos = tempUDN.lastIndexOf(':');
            if (pos >= 0)
            {
                String namespace = tempUDN.substring(0, pos).trim();
                if (namespace.length() > 0)
                {
                    this.namespaces = new UDNs();
                    this.namespaces.getUDN().add(namespace);
                }
            }
        } catch (Exception e)
        {
            log.log(Level.WARNING, "Error setting Namespaces - " + e.getMessage(), e);
        }
        
        
        timer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                long now = System.currentTimeMillis();
                if ((ZoneService.this.lastChange + 600000) < now && connectionService.isConnected())
                {
                    System.out.println("***** State Change Listener for Zone \"" + name + "\" (" + zoneID + ") is stale.  Reactivating. *****");
                    
                    deactivateStateChangeListener();
                    activateStateChangeListener();
                }
            }
        }, 1000, 1000);
    }
    
    
    protected void activateStateChangeListener()
    {
        if (this.stateChangeListenerIOD == null || this.listener == null)
        {
            ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
            
            try
            {
                POA poa = this.connectionService.getPOA();
                
                if (this.stateChangeListenerIOD == null)
                {
                    StateChangeListenerPOAImpl servant = new StateChangeListenerPOAImpl();
                    this.stateChangeListenerIOD = poa.activate_object(servant);
                }
                
                if (this.listener == null)
                {
                    org.omg.CORBA.Object object = poa.id_to_reference(this.stateChangeListenerIOD);
                    this.listener = StateChangeListenerHelper.narrow(object);
                }
                
                try
                {
                    System.out.println("***** State Change Listener Added to Zone \"" + name + "\" (" + zoneID + ")");
                    portal.addNamedStateChangeListener("MDC (Quantel) - " + Math.abs(this.hashCode()), this.listener, 0x7FFFFFFF, 30);
                    lastChange = System.currentTimeMillis();
                    String temp = dataProps.getProperty(PROP_LAST_CHANGE_NUMBER);
                    if (temp != null)
                    {
                        int lastChangeReceived = Integer.parseInt(temp);
                        StateChangeInfo[] changes = portal.getStateChanges(lastChangeReceived);
                        if (changes != null)
                        {
                            this.listener.newChanges(changes);
                        }
                    }
                } catch (Exception e)
                {
                    if (log.isLoggable(Level.INFO))
                    {
                        log.log(Level.WARNING, "Error Adding Named State Change Listener to Zone \"" + this.name + "\" (" + this.zoneID + ") - " + e.getMessage(), e);
                    } else
                    {
                        log.log(Level.WARNING, "Error Adding Named State Change Listener to Zone \"" + this.name + "\" (" + this.zoneID + ") - " + e.getMessage());
                    }
                }
            } catch (ObjectNotActive e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Object Not Active - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Object Not Active - " + e.getMessage());
                }
            } catch (WrongPolicy e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Wrong Policy - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Wrong Policy - " + e.getMessage());
                }
            } catch (ServantAlreadyActive e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Servant Already Active - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.WARNING, "Error Activating State Change Listener for Zone \"" + this.name + "\" (" + this.zoneID + ").  Servant Already Active - " + e.getMessage());
                }
            }
        }
    }
    
    
    protected void deactivateStateChangeListener()
    {
        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        POA poa = this.connectionService.getPOA();
        
        if (this.listener != null)
        {
            try
            {
                System.out.println("***** State Change Listener Removed from Zone \"" + name + "\" (" + zoneID + ")");
                portal.removeStateChangeListener(this.listener);
            } catch (Exception e)
            {
                // Ignore
            } finally
            {
                if (listener != null)
                {
                    try
                    {
                        this.listener._release();
                    } catch (Throwable e)
                    {
                        if (log.isLoggable(Level.INFO))
                        {
                            log.log(Level.INFO, "Error Releasing State Change Listener - " + e.getMessage(), e);
                        } else
                        {
                            log.log(Level.INFO, "Error Releasing State Change Listener - " + e.getMessage());
                        }
                    }
                }
                
                this.listener = null;
            }
        }
            
        if (this.stateChangeListenerIOD != null)
        {
            try
            {
                org.omg.CORBA.Object object = poa.id_to_reference(this.stateChangeListenerIOD);
                if (object != null)
                {
                    poa.deactivate_object(this.stateChangeListenerIOD);
                    object._release();
                }
            } catch (Throwable e)
            {
                if (log.isLoggable(Level.INFO))
                {
                    log.log(Level.INFO, "Error Deactivating State Change Listener - " + e.getMessage(), e);
                } else
                {
                    log.log(Level.INFO, "Error Deactivating State Change Listener - " + e.getMessage());
                }
            } finally
            {
                this.stateChangeListenerIOD = null;
            }
        }
    }


    protected void notifyListeners(int type, int ident)
    {
        switch (type)
        {
            case StateChangeType._clipCreated :
                for (ClipStateChangeListener listener : clipListeners)
                {
                    listener.clipCreated(ident);
                }
                break;
            case StateChangeType._clipDeleted :
                for (ClipStateChangeListener listener : clipListeners)
                {
                    listener.clipDeleted(ident);
                }
                break;
            case StateChangeType._clipHidden :
                for (ClipStateChangeListener listener : clipListeners)
                {
                    listener.clipHidden(ident);
                }
                break;
            case StateChangeType._clipModified :
                for (ClipStateChangeListener listener : clipListeners)
                {
                    listener.clipModified(ident);
                }
                break;
            case StateChangeType._copyComplete :
                for (CopyStateChangeListener listener : copyListeners)
                {
                    listener.copyComplete(ident);
                }
                break;
            case StateChangeType._copyFailed :
                for (CopyStateChangeListener listener : copyListeners)
                {
                    listener.copyFailed(ident);
                }
                break;
            case StateChangeType._poolDown :
                for (PoolStateChangeListener listener : poolListeners)
                {
                    listener.poolDown(ident);
                }
                break;
            case StateChangeType._poolUp :
                for (PoolStateChangeListener listener : poolListeners)
                {
                    listener.poolUp(ident);
                }
                break;
            case StateChangeType._serverChanged :
                for (ServerStateChangeListener listener : serverListeners)
                {
                    listener.serverChanged(ident);
                }
                break;
            case StateChangeType._serverDown :
                for (ServerStateChangeListener listener : serverListeners)
                {
                    listener.serverDown(ident);
                }
                break;
            case StateChangeType._serverUp :
                for (ServerStateChangeListener listener : serverListeners)
                {
                    listener.serverUp(ident);
                }
                break;
            case StateChangeType._zoneDown :
                for (ZoneStateChangeListener listener : zoneListeners)
                {
                    listener.zoneDown(ident);
                }
                break;
            case StateChangeType._zoneUp :
                for (ZoneStateChangeListener listener : zoneListeners)
                {
                    listener.zoneUp(ident);
                }
                break;
            case StateChangeType._zoneFailover :
                for (ZoneStateChangeListener listener : zoneListeners)
                {
                    listener.zoneFailover(ident);
                }
                break;
        }
    }


    public void refresh()
    {
        super.refresh();
    }
    

    protected ClipResults queryClips(String umn, QueryExpression query)
    throws InvalidQueryFault
    {
        StringBuilder where = new StringBuilder();
        if (umn != null)
        {
            try
            {
                RN udn = new RN(getUDN());
                RN rn = new RN(umn);
                String id = rn.getAttribute("poolid");
                if (udn.inNamespace(rn.getNamespace()) && id != null)
                {
                    try
                    {
                        int poolID = Integer.parseInt(id);
                        where.append(" where PoolID").append(" = ").append(Integer.toString(poolID));
                    } catch (Exception e)
                    {
                        log.warning(umn + " does not have a valid pool ID.");
                    }
                } else
                {
                    throw new ParseException("UMN specifies incorrect namespace or PoolID is not specified.", 0);
                }
            } catch (ParseException e)
            {
                log.log(Level.WARNING, "Error parsing UMN \"" + umn + "\" - " + e.getMessage(), e);
            }
        }
        
        if (where.length() == 0)
        {
            where.append(" where");
        } else
        {
            where.append(" and");
        }
        
        where.append(" ");
        where.append(sqlConverterFactory.getConverter(query).toSQL());

        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        
        ;
        try
        {
            return new ClipResults(searchColumns, portal.directQuery(selectStatement + where.toString()));
        } catch (DatabaseError e)
        {
            InvalidQuery invalidQuery = new InvalidQuery();
            invalidQuery.setMessage("Database Error - " + e.error);
            throw new InvalidQueryFault(invalidQuery.getMessage(), invalidQuery);
        }
        /*
        PAGE page = query != null ? query.getPAGE() : null;
        List<SORTBY> sortBy = query != null ? query.getSORTBY() : null;
        
        ArrayList<ClipProperty> clipProperties = new ArrayList<ClipProperty>();
        
        if (umn != null)
        {
            try
            {
                RN udn = new RN(getUDN());
                RN rn = new RN(umn);
                String id = rn.getAttribute("poolid");
                if (udn.inNamespace(rn.getNamespace()) && id != null)
                {
                    try
                    {
                        int poolID = Integer.parseInt(id);
                        clipProperties.add(new ClipProperty("PoolID", Integer.toString(poolID)));
                    } catch (Exception e)
                    {
                        log.warning(umn + " does not have a valid pool ID.");
                    }
                } else
                {
                    throw new ParseException("UMN specifies incorrect namespace or PoolID is not specified.", 0);
                }
            } catch (ParseException e)
            {
                log.log(Level.WARNING, "Error parsing UMN \"" + umn + "\" - " + e.getMessage(), e);
            }
        }
        
        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        
        try
        {
            return new ClipResults(searchColumns, portal.searchClipsWithOffset(clipProperties.toArray(new ClipProperty[clipProperties.size()]), searchColumns, page != null ? page.getOffset().intValue() : 0, page != null ? page.getPageSize().intValue() : 2000));
        } catch (BadColumnData e)
        {
            if (e.badData == null)
            {
                InvalidQuery invalidQuery = new InvalidQuery();
                invalidQuery.setMessage("Bad Column \"" + e.colName + "\" used for Query.");
                throw new InvalidQueryFault(invalidQuery.getMessage(), invalidQuery);
            } else
            {
                InvalidQuery invalidQuery = new InvalidQuery();
                invalidQuery.setMessage("Column \"" + e.colName + "\" Contains Bad Data [" + e.badData + "].");
                throw new InvalidQueryFault(invalidQuery.getMessage(), invalidQuery);
            }
        } catch (DatabaseError e)
        {
            InvalidQuery invalidQuery = new InvalidQuery();
            invalidQuery.setMessage("Database Error - " + e.error);
            throw new InvalidQueryFault(invalidQuery.getMessage(), invalidQuery);
        }
        */
    }


    @Override
    protected String[] buildSearchColumns()
    {
        ArrayList<String> columnNames = new ArrayList<String>();
        ZonePortal portal = connectionService.getConnectionObject(ZonePortal.class);
        
        ColumnDesc[] columns = portal.getColumnDescriptions();
        
        for (ColumnDesc column : columns)
        {
            if (column.searchable)
            {
                columnNames.add(column.columnName);
            }
        }

        return columnNames.toArray(new String[columnNames.size()]);
    }
}
