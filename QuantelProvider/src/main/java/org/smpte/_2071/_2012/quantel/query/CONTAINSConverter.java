package org.smpte._2071._2012.quantel.query;

import java.math.BigInteger;

import javax.xml.bind.JAXBElement;

import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatSegment;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaSegment;
import org.smpte_ra.schemas._2071._2012.mdcf.query.CONTAINS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

public class CONTAINSConverter extends SQLConverter
{
    private static final String sql = "'X' = (select 'X' " +
                                               "from essenceFragments e LEFT OUTER JOIN rushtimecodes t ON (clips.clipID = ClipID AND e.rushID = t.rushID) " +
                                              "where e.bad = 0 " +
                                                "and e.tracknum = 0 " +
                                                "and e.tracktype = 0 ";

    private static final String orderBy = " order by e.clipID, e.tracknum, e.start, e.finish)";
    
    
    public CONTAINSConverter(SQLConverterFactory factory, QueryExpression expression)
    {
        super(factory, expression);
    }
    
    
    @Override
    public String toSQL()
    throws SQLConverterException
    {
        CONTAINS contains = (CONTAINS) this.expression;
        
        JAXBElement<? extends MediaPointer> element = contains.getMediaPointer();
        MediaPointer pointer = element.getValue();
        MediaSegment segment = null;
        MediaFormatPointer formatPointer = null;
        MediaFormatSegment formatSegment = null;
        String source = pointer.getSource();
        BigInteger inpointOffset = pointer.getInpointOffset();
        BigInteger outpointOffset = pointer.getOutpointOffset();
        DateTime inpoint = null;
        DateTime outpoint = null;
        String format = null;
        
        if (pointer instanceof MediaSegment)
        {
            segment = (MediaSegment) pointer;
            inpoint = segment.getInpoint();
            outpoint = segment.getOutpoint();
        }
        
        if (pointer instanceof MediaFormatPointer)
        {
            formatPointer = (MediaFormatPointer) pointer;
            format = formatPointer.getFormat();
        } else if (pointer instanceof MediaFormatSegment)
        {
            formatSegment = (MediaFormatSegment) pointer;
            format = formatSegment.getFormat();
        }

        StringBuilder builder = new StringBuilder(sql.length() + orderBy.length() + 200);
        builder.append(sql);
        
        if (inpointOffset != null && outpointOffset != null)
        {
            builder.append("");
        }
        
        builder.append(orderBy);
        
        // TODO Auto-generated method stub
        return "1=1";
    }
}
