package org.smpte._2071._2012.quantel;

public interface CapabilityProvider
{
    public void publishedEndpoint(Class<?> implementor, String uri);
}
