package org.smpte._2071._2012.quantel.query;

import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;

public abstract class SQLConverter
{
    protected SQLConverterFactory factory;

    protected QueryExpression expression;
    
    
    protected SQLConverter(SQLConverterFactory factory, QueryExpression expression)
    {
        this.factory = factory;
        this.expression = expression;
    }
    
    
    public abstract String toSQL()
    throws SQLConverterException;


    protected String objectToString(Object object)
    {
        Class<?> clazz = object.getClass();
        
        if (object instanceof CharSequence)
        {
            return "\"" + object.toString() + "\"";
        } else if (object instanceof Number)
        {
            return object.toString();
        } else if (Boolean.TYPE == clazz || Boolean.class == clazz)
        {
            return object.toString().toLowerCase();
        } else if (Character.TYPE == clazz || Character.class == clazz)
        {
            return "\"" + object.toString() + "\"";
        } else if (object instanceof DateTime)
        {
            return dateTimeToString((DateTime) object);
        } else
        {
            return object.toString();
        }
    }


    protected String dateTimeToString(DateTime object)
    {
        StringBuilder builder = new StringBuilder();
        DateTime time = (DateTime) object;
        
        // TODO: Convert DateTime and FramedTime to String understood by MySQL.
        if (object instanceof FramedTime)
        {
            FramedTime ftime = (FramedTime) object;
        }
        return builder.toString();
    }
}
