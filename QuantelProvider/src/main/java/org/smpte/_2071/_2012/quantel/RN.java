package org.smpte._2071._2012.quantel;

import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class RN
{
    private final static String SCOPE = "urn:smpte:";
    
    public static enum Type
    {
        UDN, UMN;

        public static Type fromText(CharSequence sequence)
        {
            if (sequence.length() == 3)
            {
                switch (sequence.charAt(0))
                {
                    case 'U' :
                    case 'u' :
                        switch (sequence.charAt(1))
                        {
                            case 'D' :
                            case 'd' :
                                switch (sequence.charAt(1))
                                {
                                    case 'N' :
                                    case 'n' :
                                        return UDN;
                                }
                                break;
                            case 'M' :
                            case 'm' :
                                switch (sequence.charAt(2))
                                {
                                    case 'N' :
                                    case 'n' :
                                        return UMN;
                                }
                                break;
                        }
                        break;
                }
            }
            
            return null;
        }
    }
    
    
    private String raw;
    
    private Type rnType;

    private String namespace;

    private String attributeText;
    
    private Map<String, String> attributes = new TreeMap<String, String>();
    
    
    public RN(String rn)
    throws ParseException
    {
        this.raw = rn;
        parseRN(rn);
    }
    
    
    public Type getType()
    {
        return rnType;
    }
    
    
    public String getNamespace()
    {
        return namespace;
    }
    
    
    public String toString()
    {
        return raw;
    }


    public String getAttribute(String key)
    {
        return attributes.get(key);
    }


    protected void parseRN(String source) 
    throws ParseException
    {
        FastCharSequence rn = new FastCharSequence(source);
        int start = 0;
        
        //
        final int length = rn.length(); /* avoid getfield & invokevirtual opcodes */
        final int scopeLength = SCOPE.length(); /* avoid getfield & invokevirtual opcodes */
        FastCharSequence scope = rn.subSequence(0, scopeLength);
        if (scope.equals(SCOPE))
        {
            start = scopeLength;
            int pos = rn.indexOf(':', start);
            if (pos >= 0 && pos + 1 < length)
            {
                FastCharSequence temp = rn.subSequence(start, pos);
                rnType = Type.fromText(temp);
                start = pos + 1;
                if (start < length)
                {
                    pos = rn.indexOf(':', start);
                    if (pos >= 0)
                    {
                        this.namespace = source.substring(start, pos);
                        start = pos + 1;
                        this.attributeText = source.substring(start).trim();
                        
                        if (attributeText.length() > 0)
                        {
                            boolean onKey = true;
                            String key = null;
                            String value = null;
                            StringTokenizer tokenizer = new StringTokenizer(attributeText, "=;,", true);
                            String token = null;
                            
                            try
                            {
                                while ((token = tokenizer.nextToken()) != null)
                                {
                                    if (";".equals(token) || ",".equals(token))
                                    {
                                        onKey = true;
                                        
                                        if (key != null)
                                        {
                                            attributes.put(key, value);
                                        }
                                        key = null;
                                        value = null;
                                    } else if ("=".equals(token))
                                    {
                                        onKey = false;
                                    } else
                                    {
                                        if (onKey)
                                        {
                                            key = token;
                                        } else
                                        {
                                            value = token;
                                        }
                                    }
                                }
                            } catch (NoSuchElementException e)
                            {
                                // ignore
                            }
                            
                            if (key != null)
                            {
                                attributes.put(key, value);
                            }
                        }
                    } else
                    {
                        throw new ParseException("\"" + rn + "\" is not a valid RN, must start with \"" + SCOPE + "[udn|umn]:[namespace[.sub]]:\"!", start);
                    }
                } else
                {
                    throw new ParseException("\"" + rn + "\" is not a valid RN, must start with \"" + SCOPE + "[udn|umn]:[namespace[.sub]]:\"!", start);
                }
            } else
            {
                throw new ParseException("\"" + rn + "\" is not a valid RN, must start with \"" + SCOPE + "[udn|umn]:[namespace[.sub]]:\"!", start);
            }
        } else
        {
            throw new ParseException("\"" + rn + "\" is not a valid RN, must start with \"" + SCOPE + "\"!", start);
        }
    }


    public boolean inNamespace(String thatNamespace)
    {
        if (this.namespace == null || this.namespace.length() == 0)
        {
            return true;
        } else if (thatNamespace.startsWith(this.namespace + "."))
        {
            return true;
        } else
        {
            return false;
        }
    }
}
