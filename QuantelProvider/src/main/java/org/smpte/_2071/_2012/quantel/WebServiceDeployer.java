package org.smpte._2071._2012.quantel;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.MTOMFeature;

import org.apache.cxf.feature.AbstractFeature;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;
import org.apache.cxf.ws.addressing.WSAddressingFeature;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.springframework.osgi.context.BundleContextAware;

public class WebServiceDeployer implements Startable, Stoppable, ServiceListener, BundleContextAware
{
    private static final String DEFAULT_MDCF_SERVICE_NAME = "DeviceService";

    private static final Object DEFAULT_MDCF_PORT_SUFFIX = "Port";

    protected class WebServiceInfo
    {
        private Class<?> implementor;
        
        private Class<?>[] interfaces;
        
        private WebService annotation;


        protected WebServiceInfo(Class<?> implementor, WebService annotation)
        {
            this.implementor = implementor;
            this.annotation = annotation;
            
            if (implementor.isInterface())
            {
                interfaces = new Class<?>[] {implementor};
            } else
            {
                Class<?>[] ifaces = implementor.getInterfaces();
                if (ifaces != null)
                {
                    ArrayList<Class<?>> list = new ArrayList<Class<?>>();
                    for (Class<?> iface : ifaces)
                    {
                        if (iface.getAnnotation(annotation.getClass()) != null)
                        {
                            list.add(iface);
                        }
                    }
                    
                    interfaces = list.toArray(new Class<?>[list.size()]);
                }
            }
        }
        

        public Class<?> getImplementorClass()
        {
            return implementor;
        }
        
        
        public Class<?>[] getInterfaces()
        {
            return interfaces;
        }


        public WebService getAnnotation()
        {
            return annotation;
        }
    }
    
    
    protected class WebServiceRegistration
    {
        private ServiceReference reference;
        
        private Endpoint endpoint;
        
        
        protected WebServiceRegistration(ServiceReference reference, Endpoint endpoint)
        {
            this.reference = reference;
            this.endpoint = endpoint;
        }
        
        
        public ServiceReference getServiceReference()
        {
            return reference;
        }
        
        
        public Endpoint getEndpoint()
        {
            return endpoint;
        }
    }


    private BundleContext bundleContext;
    
    private String addressPrefix;
    
    private String urlPrefix;
    
    private HashMap<Object, List<WebServiceRegistration>> endpoints = new HashMap<Object, List<WebServiceRegistration>>(); 
    
    private ArrayList<AbstractFeature> features = new ArrayList<AbstractFeature>();


    public String getAddressPrefix()
    {
        return addressPrefix;
    }


    public void setAddressPrefix(String addressPrefix)
    {
        if (!addressPrefix.endsWith("/"))
        {
            addressPrefix += "/";
        }
        
        this.addressPrefix = addressPrefix;
    }


    public String getURLPrefix()
    {
        return urlPrefix;
    }


    public void setURLPrefix(String urlPrefix)
    {
        if (!urlPrefix.endsWith("/"))
        {
            urlPrefix += "/";
        }
        
        this.urlPrefix = urlPrefix;
    }


    public void setBundleContext(BundleContext context)
    {
        this.bundleContext = context;
    }


    public void serviceChanged(ServiceEvent event)
    {
        switch (event.getType())
        {
            case ServiceEvent.REGISTERED :
            case ServiceEvent.MODIFIED :
                System.out.println("Service " + event.getServiceReference() + " was " + (event.getType() == ServiceEvent.MODIFIED ? "modified" : event.getType() == ServiceEvent.REGISTERED ? "registered" : "unknown") + ".");
                Object object = bundleContext.getService(event.getServiceReference());
                List<WebServiceInfo> webServices = findWebServicesNew(object.getClass());
                bundleContext.ungetService(event.getServiceReference());
                if (webServices != null && webServices.size() > 0)
                {
                    System.out.println("Service " + event.getServiceReference() + " is a WebService.");
                    endpoints.put(event.getServiceReference(), deployWebServices(webServices, event.getServiceReference()));
                }
                break;
            case ServiceEvent.UNREGISTERING :
                List<WebServiceRegistration> registrations = endpoints.get(event.getServiceReference());
                if (registrations != null)
                {
                    for (WebServiceRegistration registration : registrations)
                    {
                        System.out.println("Stopping WebService Endpoint for \"" + registration.getServiceReference() + "\" " + ((EndpointImpl) registration.getEndpoint()).getAddress());
                        registration.getEndpoint().stop();
                        bundleContext.ungetService(registration.getServiceReference());
                    }
                }
                endpoints.remove(event.getServiceReference());
                break;
        }
    }
    
    
    protected List<WebServiceRegistration> deployWebServices(List<WebServiceInfo> webServices, ServiceReference serviceReference)
    {
        List<WebServiceRegistration> registrations = null;
        if (webServices != null)
        {
            Object delegate = bundleContext.getService(serviceReference);
            registrations = new ArrayList<WebServiceRegistration>();
            
            EndpointImpl endpoint = null;
            
            for (WebServiceInfo ws : webServices)
            {
                try
                {
                    WebService webService = ws.getAnnotation();
                    String targetNamespace = webService.targetNamespace() == null ? "http://" + ws.getImplementorClass().getPackage().getName() : webService.targetNamespace();
                    String serviceNamespace = targetNamespace;
                    String temp = webService.name();
                    String serviceName = webService.serviceName();
                    String portName = !isEmpty(temp) ? temp : webService.portName();
                    String wsdlLocation = webService.wsdlLocation();
                    
                    Object propName = serviceReference.getProperty("Name");
                    Object propZoneID = serviceReference.getProperty("ZoneID");
                    Object propServerID = serviceReference.getProperty("ServerID");
                    Object propPoolID = serviceReference.getProperty("PoolID");
                    Object propChannelID = serviceReference.getProperty("ChannelID");
                    Object propServiceName = serviceReference.getProperty("ServiceName");
                    Object propEndpointName = serviceReference.getProperty("EndpointName");
                    
                    if (propZoneID == null)
                    {
                        ZoneService zoneService = null;
                        
                        if (delegate instanceof ZoneService)
                        {
                            zoneService = (ZoneService) delegate;
                        } else if (delegate instanceof ZoneAware)
                        {
                            zoneService = ((ZoneAware) delegate).getZoneService();
                        }
                        
                        if (zoneService != null)
                        {
                            propZoneID = zoneService.getZoneID();
                        }
                    }
                    
                    /*
                    if (isEmpty(serviceName))
                    {
                        serviceName = serviceReference.toString();
                        int pos = serviceName.lastIndexOf('.');
                        if (pos > 0 && pos < serviceName.length())
                        {
                            serviceName = serviceName.substring(pos + 1);
                            
                            pos = serviceName.indexOf(']');
                            if (pos > 0 && pos < serviceName.length())
                            {
                                serviceName = serviceName.substring(0, pos);
                            }
                        }
                    }
                    */
                    
                    String interfaceName = ws.getImplementorClass().getSimpleName();
                    
                    StringBuilder endpointName = new StringBuilder();
                    if (propEndpointName != null)
                    {
                        endpointName.append(propEndpointName);
                    } else
                    {
                        endpointName.append(!isEmpty(portName) ? portName : interfaceName).append(DEFAULT_MDCF_PORT_SUFFIX);
                        
                        /*
                        if (propName != null)
                        {
                            endpointName.append("-").append(propName);
                        } else
                        {
                            if (propServerID != null)
                            {
                                endpointName.append("-").append(propServerID);
                            }
                            if (propPoolID != null)
                            {
                                endpointName.append("-").append(propPoolID);
                            }
                            if (propChannelID != null)
                            {
                                endpointName.append("-").append(propChannelID);
                            }
                        }
                        */
                    }
                    
                    StringBuilder publishedPath = new StringBuilder();
                    
                    if (propZoneID != null)
                    {
                        publishedPath.append(propZoneID);
                    }
                    
                    if (propServerID != null)
                    {
                        if (publishedPath.length() > 0)
                        {
                            publishedPath.append("/");
                        }
                        publishedPath.append(propServerID);
                        
                        if (propPoolID != null)
                        {
                            publishedPath.append("-").append(propPoolID);
                        }
                        if (propChannelID != null)
                        {
                            publishedPath.append("-").append(propChannelID);
                        }
                    } else if (propPoolID != null)
                    {
                        if (publishedPath.length() > 0)
                        {
                            publishedPath.append("/");
                        }
                        publishedPath.append("P").append(propPoolID);
                    } else if (propChannelID != null)
                    {
                        if (publishedPath.length() > 0)
                        {
                            publishedPath.append("/");
                        }
                        publishedPath.append("C").append(propChannelID);
                    }
    
                    String publishedServiceName = !isEmpty(propServiceName) ? propServiceName.toString() : (!isEmpty(serviceName) ? serviceName : DEFAULT_MDCF_SERVICE_NAME);
                    String publishedPortName = endpointName.toString(); 
                    
                    Object object = Proxy.newProxyInstance(delegate.getClass().getClassLoader(), ws.getInterfaces(), new WebServiceProxy(bundleContext, serviceReference, publishedPortName));
                    endpoint = (EndpointImpl) Endpoint.create(object);
                    endpoint.getBus().getFeatures().add(new WSAddressingFeature());
                    endpoint.getBus().getFeatures().add(new LoggingFeature());
                    
                    endpoint.setAddress(addressPrefix + (!addressPrefix.endsWith("/") ? "/" : "") + publishedPath + "/" + publishedServiceName + "/" + publishedPortName);
                    endpoint.setServiceName(new QName(serviceNamespace, publishedServiceName));
                    endpoint.setEndpointName(new QName(targetNamespace, publishedPortName));
                    
                    List<AbstractFeature> endpointFeatures = endpoint.getFeatures();
                    if (endpointFeatures == null)
                    {
                        endpointFeatures = this.features;
                    } else
                    {
                        endpointFeatures.addAll(this.features);
                    }
                    endpoint.setFeatures(endpointFeatures);
                    
                    if (!isEmpty(wsdlLocation))
                    {
                        endpoint.setWsdlLocation(wsdlLocation);
                    }
                    
                    System.out.println("Deploying Web Service for " + endpoint.getBeanName() + " - " + endpoint.getEndpointName() + " @" + endpoint.getAddress());
                    endpoint.publish();
                    
                    if (delegate instanceof CapabilityProvider)
                    {
                        ((CapabilityProvider) delegate).publishedEndpoint(ws.getImplementorClass(), (urlPrefix != null ? (urlPrefix + (!urlPrefix.endsWith("/") ? "/" : "")) : "") + endpoint.getAddress());
                    }
                    
                    registrations.add(new WebServiceRegistration(serviceReference, endpoint));
                } catch (Exception e)
                {
                    if (endpoint != null)
                    {
                        System.err.println("Error deploying Web Service " + endpoint.getBeanName() + " - " + endpoint.getEndpointName() + " @" + endpoint.getAddress());
                    }
                    e.printStackTrace(System.err);
                }
            }
            
            bundleContext.ungetService(serviceReference);
        }
        
        return registrations;
    }
    
    
    private boolean isEmpty(Object object)
    {
        return object == null || object.toString().trim().length() == 0;
    }


    /**
     * Complete algorithm that checks full class hierarchy for WebService annotations.  Doesn't work with CXF though, because
     * CXF only tests the interfaces directly assigned to the class and not the interfaces of the interfaces.
     * 
     * @param cls
     * @return
     */
    protected List<WebServiceInfo> findWebServicesNew(Class<?> cls)
    {
        if (cls == null)
        {
            return null;
        }
        
        ArrayList<WebServiceInfo> webServices = new ArrayList<WebServiceInfo>();
        Stack<Class<?>> stack = new Stack<Class<?>>();
        
        stack.push(cls);
        Class<?> clazz = null;
        while (!stack.isEmpty())
        {
            clazz = stack.pop();
            WebService annotation = clazz.getAnnotation(WebService.class);
            if (annotation != null)
            {
                webServices.add(new WebServiceInfo(clazz, annotation));
            }
            
            Class<?>[] interfaces = clazz.getInterfaces();
            for (Class<?> iface : interfaces)
            {
                if (iface != null)
                {
                    stack.push(iface);
                }
            }
            
            if (clazz.getSuperclass() != null)
            {
                stack.push(clazz.getSuperclass());
            }
        }
        
        return webServices;
    }
    
    
    /**
     * Uses the same algorithm as CXF to find WebService interfaces
     * 
     * @param clazz
     * @return
     */
    protected List<WebServiceInfo> findWebServices(Class<?> cls)
    {
        ArrayList<WebServiceInfo> interfaces = new ArrayList<WebServiceInfo>();
        
        if (cls == null) 
        {
            return interfaces;
        }
        
        Stack<Class<?>> stack = new Stack<Class<?>>();
        stack.push(cls);
        
        Class<?> clazz = null;
        while (!stack.isEmpty())
        {
            clazz = stack.pop();
            if (clazz != null)
            {
                WebService annotation = clazz.getAnnotation(WebService.class);
                if (annotation != null)
                {
                    interfaces.add(new WebServiceInfo(clazz, annotation));
                }
                
                Class<?>[] ifaces = clazz.getInterfaces();
                if (ifaces != null)
                {
                    for (Class<?> iface : ifaces) 
                    {
                        annotation = iface.getAnnotation(WebService.class);
                        if (annotation != null) 
                        {
                            interfaces.add(new WebServiceInfo(clazz, annotation));
                        }
                    }
                }
                
                stack.push(clazz.getSuperclass());
            }
        }
        
        return interfaces;
    }


    public void start()
    throws Exception
    {
        if (bundleContext == null)
        {
            throw new IllegalArgumentException("Bundle Context was not set for BundleContextAware Bean \"" + getClass().getName() + "\"!");
        }
        
        bundleContext.addServiceListener(this);
        
        features.add(new WSAddressingFeature());
        features.add(new LoggingFeature());
    }
    
    
    public void stop()
    {
        if (bundleContext != null)
        {
            bundleContext.removeServiceListener(this);
        }
    }
}
