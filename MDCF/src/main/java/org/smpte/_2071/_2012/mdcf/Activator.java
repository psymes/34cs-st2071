package org.smpte._2071._2012.mdcf;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator
{
    
    public void start(BundleContext context)
    throws Exception
    {
        System.out.println("Starting MDCF");
    }
    
    
    public void stop(BundleContext context)
    throws Exception
    {
        System.out.println("Stopped MDCF");
    }
    
}
