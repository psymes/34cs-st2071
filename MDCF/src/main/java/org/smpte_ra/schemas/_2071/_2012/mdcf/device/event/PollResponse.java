
package org.smpte_ra.schemas._2071._2012.mdcf.device.event;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceEvent;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ModeEvent;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaEvent;


/**
 * <p>Java class for pollResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pollResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event}Event" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pollResponse", propOrder = {
    "event"
})
public class PollResponse {

    @XmlElementRef(name = "Event", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", type = JAXBElement.class)
    protected JAXBElement<? extends Event> event;

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link ModeEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Event }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     
     */
    public JAXBElement<? extends Event> getEvent() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link ModeEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Event }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     
     */
    public void setEvent(JAXBElement<? extends Event> value) {
        this.event = ((JAXBElement<? extends Event> ) value);
    }

}
