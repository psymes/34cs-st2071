
package org.smpte_ra.schemas._2071._2012.mdcf.device;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebFault(name = "DeviceNotFound", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device")
public class DeviceNotBoundFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private DeviceNotFound faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public DeviceNotBoundFault(String message, DeviceNotFound faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public DeviceNotBoundFault(String message, DeviceNotFound faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceNotFound
     */
    public DeviceNotFound getFaultInfo() {
        return faultInfo;
    }

}
