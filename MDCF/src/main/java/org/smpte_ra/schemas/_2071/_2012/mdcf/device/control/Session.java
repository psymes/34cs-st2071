
package org.smpte_ra.schemas._2071._2012.mdcf.device.control;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;


/**
 * <p>Java class for Session complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Session">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control}SessionID"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control}Who"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control}Name"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control}AcquiredAt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Session", propOrder = {
    "sessionID",
    "who",
    "name",
    "acquiredAt"
})
public class Session {

    @XmlElement(name = "SessionID", required = true)
    protected String sessionID;
    @XmlElement(name = "Who", required = true)
    protected String who;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "AcquiredAt", required = true)
    protected DateTime acquiredAt;

    /**
     * Gets the value of the sessionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionID() {
        return sessionID;
    }

    /**
     * Sets the value of the sessionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionID(String value) {
        this.sessionID = value;
    }

    /**
     * Gets the value of the who property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWho() {
        return who;
    }

    /**
     * Sets the value of the who property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWho(String value) {
        this.who = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the acquiredAt property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getAcquiredAt() {
        return acquiredAt;
    }

    /**
     * Sets the value of the acquiredAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setAcquiredAt(DateTime value) {
        this.acquiredAt = value;
    }

}
