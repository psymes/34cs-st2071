
package org.smpte_ra.schemas._2071._2012.mdcf.media;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;


/**
 * <p>Java class for MediaSegment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MediaSegment">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}MediaPointer">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Inpoint"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Outpoint"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaSegment", propOrder = {
    "inpoint",
    "outpoint"
})
@XmlSeeAlso({
    MediaFormatSegment.class
})
public class MediaSegment
    extends MediaPointer
{

    @XmlElement(name = "Inpoint", required = true)
    protected DateTime inpoint;
    @XmlElement(name = "Outpoint", required = true)
    protected DateTime outpoint;

    /**
     * Gets the value of the inpoint property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getInpoint() {
        return inpoint;
    }

    /**
     * Sets the value of the inpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setInpoint(DateTime value) {
        this.inpoint = value;
    }

    /**
     * Gets the value of the outpoint property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getOutpoint() {
        return outpoint;
    }

    /**
     * Sets the value of the outpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setOutpoint(DateTime value) {
        this.outpoint = value;
    }

}
