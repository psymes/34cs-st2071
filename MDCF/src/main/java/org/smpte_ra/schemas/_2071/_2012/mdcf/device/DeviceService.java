
package org.smpte_ra.schemas._2071._2012.mdcf.device;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "DeviceService", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", wsdlLocation = "file:/Users/Shared/DropBox/Dropbox/SMPTE/MDC/workspace/MDC/MDCF/src/main/xml/device_services.wsdl")
public class DeviceService
    extends Service
{

    private final static URL DEVICESERVICE_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceService.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceService.class.getResource(".");
            url = new URL(baseUrl, "file:/Users/Shared/DropBox/Dropbox/SMPTE/MDC/workspace/MDC/MDCF/src/main/xml/device_services.wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'file:/Users/Shared/DropBox/Dropbox/SMPTE/MDC/workspace/MDC/MDCF/src/main/xml/device_services.wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        DEVICESERVICE_WSDL_LOCATION = url;
    }

    public DeviceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public DeviceService() {
        super(DEVICESERVICE_WSDL_LOCATION, new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceService"));
    }

    /**
     * 
     * @return
     *     returns Device
     */
    @WebEndpoint(name = "DevicePort")
    public Device getDevicePort() {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DevicePort"), Device.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Device
     */
    @WebEndpoint(name = "DevicePort")
    public Device getDevicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DevicePort"), Device.class, features);
    }

    /**
     * 
     * @return
     *     returns DeviceDirectory
     */
    @WebEndpoint(name = "DeviceDirectoryPort")
    public DeviceDirectory getDeviceDirectoryPort() {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceDirectoryPort"), DeviceDirectory.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns DeviceDirectory
     */
    @WebEndpoint(name = "DeviceDirectoryPort")
    public DeviceDirectory getDeviceDirectoryPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceDirectoryPort"), DeviceDirectory.class, features);
    }

    /**
     * 
     * @return
     *     returns DeviceRegistry
     */
    @WebEndpoint(name = "DeviceRegistryPort")
    public DeviceRegistry getDeviceRegistryPort() {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceRegistryPort"), DeviceRegistry.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns DeviceRegistry
     */
    @WebEndpoint(name = "DeviceRegistryPort")
    public DeviceRegistry getDeviceRegistryPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceRegistryPort"), DeviceRegistry.class, features);
    }

}
