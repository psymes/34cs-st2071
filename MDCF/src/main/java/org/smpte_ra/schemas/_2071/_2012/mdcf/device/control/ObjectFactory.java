
package org.smpte_ra.schemas._2071._2012.mdcf.device.control;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.smpte_ra.schemas._2071._2012.mdcf.device.control package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Name_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "Name");
    private final static QName _Lock_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "lock");
    private final static QName _GetLockedByResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getLockedByResponse");
    private final static QName _ForceLock_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "forceLock");
    private final static QName _TooManySessions_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "TooManySessions");
    private final static QName _ApproveResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "approveResponse");
    private final static QName _Release_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "release");
    private final static QName _RequestNotFound_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "RequestNotFound");
    private final static QName _Approve_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "approve");
    private final static QName _ID_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "ID");
    private final static QName _Request_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "request");
    private final static QName _DenyResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "denyResponse");
    private final static QName _Force_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "force");
    private final static QName _ForceResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "forceResponse");
    private final static QName _Message_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "Message");
    private final static QName _Acquire_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "acquire");
    private final static QName _GetLockedResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getLockedResponse");
    private final static QName _RequestResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "requestResponse");
    private final static QName _Unlock_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "unlock");
    private final static QName _RequestID_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "RequestID");
    private final static QName _UnlockResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "unlockResponse");
    private final static QName _RequestLockResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "requestLockResponse");
    private final static QName _GetLockedBy_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getLockedBy");
    private final static QName _ForceLockResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "forceLockResponse");
    private final static QName _GetLocked_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getLocked");
    private final static QName _Deny_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "deny");
    private final static QName _AcquireResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "acquireResponse");
    private final static QName _DeviceLocked_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "DeviceLocked");
    private final static QName _SessionNotFound_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "SessionNotFound");
    private final static QName _DeviceNotAcquired_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "DeviceNotAcquired");
    private final static QName _ReleaseResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "releaseResponse");
    private final static QName _GetAcquiredBy_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getAcquiredBy");
    private final static QName _GetAcquired_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getAcquired");
    private final static QName _Session_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "Session");
    private final static QName _RequestLock_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "requestLock");
    private final static QName _GetAcquiredResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getAcquiredResponse");
    private final static QName _GetAcquiredByResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "getAcquiredByResponse");
    private final static QName _NameInUse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "NameInUse");
    private final static QName _Sessions_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "Sessions");
    private final static QName _SessionID_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "SessionID");
    private final static QName _DeviceNotLocked_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "DeviceNotLocked");
    private final static QName _AcquiredAt_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "AcquiredAt");
    private final static QName _RequestEvent_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "RequestEvent");
    private final static QName _RequestExpired_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "RequestExpired");
    private final static QName _RequestDenied_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "RequestDenied");
    private final static QName _Who_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "Who");
    private final static QName _LockResponse_QNAME = new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", "lockResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.smpte_ra.schemas._2071._2012.mdcf.device.control
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Deny }
     * 
     */
    public Deny createDeny() {
        return new Deny();
    }

    /**
     * Create an instance of {@link Acquire }
     * 
     */
    public Acquire createAcquire() {
        return new Acquire();
    }

    /**
     * Create an instance of {@link SessionRequestParameters }
     * 
     */
    public SessionRequestParameters createSessionRequestParameters() {
        return new SessionRequestParameters();
    }

    /**
     * Create an instance of {@link Sessions }
     * 
     */
    public Sessions createSessions() {
        return new Sessions();
    }

    /**
     * Create an instance of {@link TooManySessions }
     * 
     */
    public TooManySessions createTooManySessions() {
        return new TooManySessions();
    }

    /**
     * Create an instance of {@link URIResponse }
     * 
     */
    public URIResponse createURIResponse() {
        return new URIResponse();
    }

    /**
     * Create an instance of {@link GetLockedResponse }
     * 
     */
    public GetLockedResponse createGetLockedResponse() {
        return new GetLockedResponse();
    }

    /**
     * Create an instance of {@link DeviceNotAcquired }
     * 
     */
    public DeviceNotAcquired createDeviceNotAcquired() {
        return new DeviceNotAcquired();
    }

    /**
     * Create an instance of {@link GetAcquiredByResponse }
     * 
     */
    public GetAcquiredByResponse createGetAcquiredByResponse() {
        return new GetAcquiredByResponse();
    }

    /**
     * Create an instance of {@link ApproveResponse }
     * 
     */
    public ApproveResponse createApproveResponse() {
        return new ApproveResponse();
    }

    /**
     * Create an instance of {@link RequestDenied }
     * 
     */
    public RequestDenied createRequestDenied() {
        return new RequestDenied();
    }

    /**
     * Create an instance of {@link RequestExpired }
     * 
     */
    public RequestExpired createRequestExpired() {
        return new RequestExpired();
    }

    /**
     * Create an instance of {@link DeviceNotLocked }
     * 
     */
    public DeviceNotLocked createDeviceNotLocked() {
        return new DeviceNotLocked();
    }

    /**
     * Create an instance of {@link RequestNotFound }
     * 
     */
    public RequestNotFound createRequestNotFound() {
        return new RequestNotFound();
    }

    /**
     * Create an instance of {@link GetAcquiredBy }
     * 
     */
    public GetAcquiredBy createGetAcquiredBy() {
        return new GetAcquiredBy();
    }

    /**
     * Create an instance of {@link GetAcquired }
     * 
     */
    public GetAcquired createGetAcquired() {
        return new GetAcquired();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link BooleanResponse }
     * 
     */
    public BooleanResponse createBooleanResponse() {
        return new BooleanResponse();
    }

    /**
     * Create an instance of {@link GetLocked }
     * 
     */
    public GetLocked createGetLocked() {
        return new GetLocked();
    }

    /**
     * Create an instance of {@link DeviceLocked }
     * 
     */
    public DeviceLocked createDeviceLocked() {
        return new DeviceLocked();
    }

    /**
     * Create an instance of {@link SessionsNotFound }
     * 
     */
    public SessionsNotFound createSessionsNotFound() {
        return new SessionsNotFound();
    }

    /**
     * Create an instance of {@link Lock }
     * 
     */
    public Lock createLock() {
        return new Lock();
    }

    /**
     * Create an instance of {@link NameInUse }
     * 
     */
    public NameInUse createNameInUse() {
        return new NameInUse();
    }

    /**
     * Create an instance of {@link RequestEvent }
     * 
     */
    public RequestEvent createRequestEvent() {
        return new RequestEvent();
    }

    /**
     * Create an instance of {@link LockRequestParameters }
     * 
     */
    public LockRequestParameters createLockRequestParameters() {
        return new LockRequestParameters();
    }

    /**
     * Create an instance of {@link GetLockedBy }
     * 
     */
    public GetLockedBy createGetLockedBy() {
        return new GetLockedBy();
    }

    /**
     * Create an instance of {@link GetLockedByResponse }
     * 
     */
    public GetLockedByResponse createGetLockedByResponse() {
        return new GetLockedByResponse();
    }

    /**
     * Create an instance of {@link DenyResponse }
     * 
     */
    public DenyResponse createDenyResponse() {
        return new DenyResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "Name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Lock }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "lock")
    public JAXBElement<Lock> createLock(Lock value) {
        return new JAXBElement<Lock>(_Lock_QNAME, Lock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLockedByResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getLockedByResponse")
    public JAXBElement<GetLockedByResponse> createGetLockedByResponse(GetLockedByResponse value) {
        return new JAXBElement<GetLockedByResponse>(_GetLockedByResponse_QNAME, GetLockedByResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockRequestParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "forceLock")
    public JAXBElement<LockRequestParameters> createForceLock(LockRequestParameters value) {
        return new JAXBElement<LockRequestParameters>(_ForceLock_QNAME, LockRequestParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TooManySessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "TooManySessions")
    public JAXBElement<TooManySessions> createTooManySessions(TooManySessions value) {
        return new JAXBElement<TooManySessions>(_TooManySessions_QNAME, TooManySessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApproveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "approveResponse")
    public JAXBElement<ApproveResponse> createApproveResponse(ApproveResponse value) {
        return new JAXBElement<ApproveResponse>(_ApproveResponse_QNAME, ApproveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "release")
    public JAXBElement<URIResponse> createRelease(URIResponse value) {
        return new JAXBElement<URIResponse>(_Release_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestNotFound }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "RequestNotFound")
    public JAXBElement<RequestNotFound> createRequestNotFound(RequestNotFound value) {
        return new JAXBElement<RequestNotFound>(_RequestNotFound_QNAME, RequestNotFound.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "approve")
    public JAXBElement<URIResponse> createApprove(URIResponse value) {
        return new JAXBElement<URIResponse>(_Approve_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "ID")
    public JAXBElement<String> createID(String value) {
        return new JAXBElement<String>(_ID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SessionRequestParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "request")
    public JAXBElement<SessionRequestParameters> createRequest(SessionRequestParameters value) {
        return new JAXBElement<SessionRequestParameters>(_Request_QNAME, SessionRequestParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DenyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "denyResponse")
    public JAXBElement<DenyResponse> createDenyResponse(DenyResponse value) {
        return new JAXBElement<DenyResponse>(_DenyResponse_QNAME, DenyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SessionRequestParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "force")
    public JAXBElement<SessionRequestParameters> createForce(SessionRequestParameters value) {
        return new JAXBElement<SessionRequestParameters>(_Force_QNAME, SessionRequestParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "forceResponse")
    public JAXBElement<URIResponse> createForceResponse(URIResponse value) {
        return new JAXBElement<URIResponse>(_ForceResponse_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "Message")
    public JAXBElement<String> createMessage(String value) {
        return new JAXBElement<String>(_Message_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Acquire }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "acquire")
    public JAXBElement<Acquire> createAcquire(Acquire value) {
        return new JAXBElement<Acquire>(_Acquire_QNAME, Acquire.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLockedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getLockedResponse")
    public JAXBElement<GetLockedResponse> createGetLockedResponse(GetLockedResponse value) {
        return new JAXBElement<GetLockedResponse>(_GetLockedResponse_QNAME, GetLockedResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "requestResponse")
    public JAXBElement<URIResponse> createRequestResponse(URIResponse value) {
        return new JAXBElement<URIResponse>(_RequestResponse_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "unlock")
    public JAXBElement<URIResponse> createUnlock(URIResponse value) {
        return new JAXBElement<URIResponse>(_Unlock_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "RequestID")
    public JAXBElement<String> createRequestID(String value) {
        return new JAXBElement<String>(_RequestID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "unlockResponse")
    public JAXBElement<BooleanResponse> createUnlockResponse(BooleanResponse value) {
        return new JAXBElement<BooleanResponse>(_UnlockResponse_QNAME, BooleanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "requestLockResponse")
    public JAXBElement<URIResponse> createRequestLockResponse(URIResponse value) {
        return new JAXBElement<URIResponse>(_RequestLockResponse_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLockedBy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getLockedBy")
    public JAXBElement<GetLockedBy> createGetLockedBy(GetLockedBy value) {
        return new JAXBElement<GetLockedBy>(_GetLockedBy_QNAME, GetLockedBy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "forceLockResponse")
    public JAXBElement<URIResponse> createForceLockResponse(URIResponse value) {
        return new JAXBElement<URIResponse>(_ForceLockResponse_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLocked }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getLocked")
    public JAXBElement<GetLocked> createGetLocked(GetLocked value) {
        return new JAXBElement<GetLocked>(_GetLocked_QNAME, GetLocked.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deny }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "deny")
    public JAXBElement<Deny> createDeny(Deny value) {
        return new JAXBElement<Deny>(_Deny_QNAME, Deny.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link URIResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "acquireResponse")
    public JAXBElement<URIResponse> createAcquireResponse(URIResponse value) {
        return new JAXBElement<URIResponse>(_AcquireResponse_QNAME, URIResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeviceLocked }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "DeviceLocked")
    public JAXBElement<DeviceLocked> createDeviceLocked(DeviceLocked value) {
        return new JAXBElement<DeviceLocked>(_DeviceLocked_QNAME, DeviceLocked.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SessionsNotFound }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "SessionNotFound")
    public JAXBElement<SessionsNotFound> createSessionNotFound(SessionsNotFound value) {
        return new JAXBElement<SessionsNotFound>(_SessionNotFound_QNAME, SessionsNotFound.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeviceNotAcquired }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "DeviceNotAcquired")
    public JAXBElement<DeviceNotAcquired> createDeviceNotAcquired(DeviceNotAcquired value) {
        return new JAXBElement<DeviceNotAcquired>(_DeviceNotAcquired_QNAME, DeviceNotAcquired.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "releaseResponse")
    public JAXBElement<BooleanResponse> createReleaseResponse(BooleanResponse value) {
        return new JAXBElement<BooleanResponse>(_ReleaseResponse_QNAME, BooleanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAcquiredBy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getAcquiredBy")
    public JAXBElement<GetAcquiredBy> createGetAcquiredBy(GetAcquiredBy value) {
        return new JAXBElement<GetAcquiredBy>(_GetAcquiredBy_QNAME, GetAcquiredBy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAcquired }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getAcquired")
    public JAXBElement<GetAcquired> createGetAcquired(GetAcquired value) {
        return new JAXBElement<GetAcquired>(_GetAcquired_QNAME, GetAcquired.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Session }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "Session")
    public JAXBElement<Session> createSession(Session value) {
        return new JAXBElement<Session>(_Session_QNAME, Session.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockRequestParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "requestLock")
    public JAXBElement<LockRequestParameters> createRequestLock(LockRequestParameters value) {
        return new JAXBElement<LockRequestParameters>(_RequestLock_QNAME, LockRequestParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getAcquiredResponse")
    public JAXBElement<BooleanResponse> createGetAcquiredResponse(BooleanResponse value) {
        return new JAXBElement<BooleanResponse>(_GetAcquiredResponse_QNAME, BooleanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAcquiredByResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "getAcquiredByResponse")
    public JAXBElement<GetAcquiredByResponse> createGetAcquiredByResponse(GetAcquiredByResponse value) {
        return new JAXBElement<GetAcquiredByResponse>(_GetAcquiredByResponse_QNAME, GetAcquiredByResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NameInUse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "NameInUse")
    public JAXBElement<NameInUse> createNameInUse(NameInUse value) {
        return new JAXBElement<NameInUse>(_NameInUse_QNAME, NameInUse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "Sessions")
    public JAXBElement<Sessions> createSessions(Sessions value) {
        return new JAXBElement<Sessions>(_Sessions_QNAME, Sessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "SessionID")
    public JAXBElement<String> createSessionID(String value) {
        return new JAXBElement<String>(_SessionID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeviceNotLocked }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "DeviceNotLocked")
    public JAXBElement<DeviceNotLocked> createDeviceNotLocked(DeviceNotLocked value) {
        return new JAXBElement<DeviceNotLocked>(_DeviceNotLocked_QNAME, DeviceNotLocked.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "AcquiredAt")
    public JAXBElement<DateTime> createAcquiredAt(DateTime value) {
        return new JAXBElement<DateTime>(_AcquiredAt_QNAME, DateTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "RequestEvent", substitutionHeadNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", substitutionHeadName = "DeviceEvent")
    public JAXBElement<RequestEvent> createRequestEvent(RequestEvent value) {
        return new JAXBElement<RequestEvent>(_RequestEvent_QNAME, RequestEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestExpired }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "RequestExpired")
    public JAXBElement<RequestExpired> createRequestExpired(RequestExpired value) {
        return new JAXBElement<RequestExpired>(_RequestExpired_QNAME, RequestExpired.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestDenied }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "RequestDenied")
    public JAXBElement<RequestDenied> createRequestDenied(RequestDenied value) {
        return new JAXBElement<RequestDenied>(_RequestDenied_QNAME, RequestDenied.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "Who")
    public JAXBElement<String> createWho(String value) {
        return new JAXBElement<String>(_Who_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BooleanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control", name = "lockResponse")
    public JAXBElement<BooleanResponse> createLockResponse(BooleanResponse value) {
        return new JAXBElement<BooleanResponse>(_LockResponse_QNAME, BooleanResponse.class, null, value);
    }

}
