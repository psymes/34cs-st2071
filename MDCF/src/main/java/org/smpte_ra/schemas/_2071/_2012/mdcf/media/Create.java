
package org.smpte_ra.schemas._2071._2012.mdcf.media;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Media"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}MediaFormatPointer" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create", propOrder = {
    "media",
    "mediaFormatPointer"
})
public class Create {

    @XmlElementRef(name = "Media", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/media", type = JAXBElement.class)
    protected JAXBElement<? extends Media> media;
    @XmlElement(name = "MediaFormatPointer", required = true)
    protected List<MediaFormatPointer> mediaFormatPointer;

    /**
     * Gets the value of the media property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MediaFile }{@code >}
     *     {@link JAXBElement }{@code <}{@link Media }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaContainer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaBundle }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaAsset }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaInstance }{@code >}
     *     
     */
    public JAXBElement<? extends Media> getMedia() {
        return media;
    }

    /**
     * Sets the value of the media property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MediaFile }{@code >}
     *     {@link JAXBElement }{@code <}{@link Media }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaContainer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaBundle }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaAsset }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaInstance }{@code >}
     *     
     */
    public void setMedia(JAXBElement<? extends Media> value) {
        this.media = ((JAXBElement<? extends Media> ) value);
    }

    /**
     * Gets the value of the mediaFormatPointer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mediaFormatPointer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMediaFormatPointer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MediaFormatPointer }
     * 
     * 
     */
    public List<MediaFormatPointer> getMediaFormatPointer() {
        if (mediaFormatPointer == null) {
            mediaFormatPointer = new ArrayList<MediaFormatPointer>();
        }
        return this.mediaFormatPointer;
    }

}
