
package org.smpte_ra.schemas._2071._2012.mdcf.query;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryExpression complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryExpression">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}PAGE" minOccurs="0"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}SORT_BY" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryExpression", propOrder = {
    "page",
    "sortby"
})
@XmlSeeAlso({
    GREATERTHAN.class,
    IMPLEMENTS.class,
    MATCHES.class,
    CONTAINS.class,
    OR.class,
    LESSTHAN.class,
    EQUALS.class,
    NOT.class,
    AND.class
})
public abstract class QueryExpression {

    @XmlElement(name = "PAGE")
    protected PAGE page;
    @XmlElement(name = "SORT_BY")
    protected List<SORTBY> sortby;

    /**
     * Gets the value of the page property.
     * 
     * @return
     *     possible object is
     *     {@link PAGE }
     *     
     */
    public PAGE getPAGE() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     * @param value
     *     allowed object is
     *     {@link PAGE }
     *     
     */
    public void setPAGE(PAGE value) {
        this.page = value;
    }

    /**
     * Gets the value of the sortby property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sortby property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSORTBY().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SORTBY }
     * 
     * 
     */
    public List<SORTBY> getSORTBY() {
        if (sortby == null) {
            sortby = new ArrayList<SORTBY>();
        }
        return this.sortby;
    }

}
