
package org.smpte_ra.schemas._2071._2012.mdcf.device.event;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.smpte_ra.schemas._2071._2012.mdcf.security.SecurityExceptionFault;


/**
 * 
 *             UCN = urn:smpte:ucn:eventer_v1.0
 *         
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebService(name = "Eventer", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    org.smpte_ra.schemas._2071._2012.mdcf.security.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.query.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.identity.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.device.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.media.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.types.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.device.event.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.device.control.ObjectFactory.class,
    org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ObjectFactory.class
})
public interface Eventer {


    /**
     * 
     * @param poll
     * @return
     *     returns org.smpte_ra.schemas._2071._2012.mdcf.device.event.PollResponse
     * @throws SecurityExceptionFault
     */
    @WebMethod(action = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/Eventer/poll")
    @WebResult(name = "pollResponse", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "pollResponse")
    public PollResponse poll(
        @WebParam(name = "poll", targetNamespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", partName = "poll")
        URIParameter poll)
        throws SecurityExceptionFault
    ;

}
