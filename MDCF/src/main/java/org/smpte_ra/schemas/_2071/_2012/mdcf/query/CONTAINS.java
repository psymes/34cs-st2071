
package org.smpte_ra.schemas._2071._2012.mdcf.query;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaFormatSegment;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaPointer;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaSegment;


/**
 * <p>Java class for CONTAINS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CONTAINS">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}QueryExpression">
 *       &lt;choice>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}MediaPointer"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CONTAINS", propOrder = {
    "mediaPointer"
})
public class CONTAINS
    extends QueryExpression
{

    @XmlElementRef(name = "MediaPointer", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/media", type = JAXBElement.class)
    protected JAXBElement<? extends MediaPointer> mediaPointer;

    /**
     * Gets the value of the mediaPointer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MediaPointer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaFormatSegment }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaFormatPointer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaSegment }{@code >}
     *     
     */
    public JAXBElement<? extends MediaPointer> getMediaPointer() {
        return mediaPointer;
    }

    /**
     * Sets the value of the mediaPointer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MediaPointer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaFormatSegment }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaFormatPointer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaSegment }{@code >}
     *     
     */
    public void setMediaPointer(JAXBElement<? extends MediaPointer> value) {
        this.mediaPointer = ((JAXBElement<? extends MediaPointer> ) value);
    }

}
