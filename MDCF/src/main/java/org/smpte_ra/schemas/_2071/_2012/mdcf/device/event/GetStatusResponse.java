
package org.smpte_ra.schemas._2071._2012.mdcf.device.event;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceEvent;
import org.smpte_ra.schemas._2071._2012.mdcf.device.mode.ModeEvent;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaEvent;


/**
 * <p>Java class for getStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStatusResponse", propOrder = {
    "status"
})
public class GetStatusResponse {

    @XmlElementRef(name = "Status", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/event", type = JAXBElement.class)
    protected JAXBElement<? extends Status> status;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link ModeEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     
     */
    public JAXBElement<? extends Status> getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link ModeEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaEvent }{@code >}
     *     {@link JAXBElement }{@code <}{@link Status }{@code >}
     *     
     */
    public void setStatus(JAXBElement<? extends Status> value) {
        this.status = ((JAXBElement<? extends Status> ) value);
    }

}
