
package org.smpte_ra.schemas._2071._2012.mdcf.query;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NOT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NOT">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}QueryExpression">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}QueryExpression"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NOT", propOrder = {
    "queryExpression"
})
public class NOT
    extends QueryExpression
{

    @XmlElementRef(name = "QueryExpression", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/query", type = JAXBElement.class)
    protected JAXBElement<? extends QueryExpression> queryExpression;

    /**
     * Gets the value of the queryExpression property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GREATERTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link LESSTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link CONTAINS }{@code >}
     *     {@link JAXBElement }{@code <}{@link IMPLEMENTS }{@code >}
     *     {@link JAXBElement }{@code <}{@link EQUALS }{@code >}
     *     {@link JAXBElement }{@code <}{@link AND }{@code >}
     *     {@link JAXBElement }{@code <}{@link OR }{@code >}
     *     {@link JAXBElement }{@code <}{@link MATCHES }{@code >}
     *     {@link JAXBElement }{@code <}{@link NOT }{@code >}
     *     {@link JAXBElement }{@code <}{@link QueryExpression }{@code >}
     *     
     */
    public JAXBElement<? extends QueryExpression> getQueryExpression() {
        return queryExpression;
    }

    /**
     * Sets the value of the queryExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GREATERTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link LESSTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link CONTAINS }{@code >}
     *     {@link JAXBElement }{@code <}{@link IMPLEMENTS }{@code >}
     *     {@link JAXBElement }{@code <}{@link EQUALS }{@code >}
     *     {@link JAXBElement }{@code <}{@link AND }{@code >}
     *     {@link JAXBElement }{@code <}{@link OR }{@code >}
     *     {@link JAXBElement }{@code <}{@link MATCHES }{@code >}
     *     {@link JAXBElement }{@code <}{@link NOT }{@code >}
     *     {@link JAXBElement }{@code <}{@link QueryExpression }{@code >}
     *     
     */
    public void setQueryExpression(JAXBElement<? extends QueryExpression> value) {
        this.queryExpression = ((JAXBElement<? extends QueryExpression> ) value);
    }

}
