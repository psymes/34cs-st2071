
package org.smpte_ra.schemas._2071._2012.mdcf.media;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.types.Exception;


/**
 * <p>Java class for MediaCreationFailed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MediaCreationFailed">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/types}Exception">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Media"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaCreationFailed", propOrder = {
    "media"
})
public class MediaCreationFailed
    extends Exception
{

    @XmlElementRef(name = "Media", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/media", type = JAXBElement.class)
    protected JAXBElement<? extends Media> media;

    /**
     * Gets the value of the media property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MediaFile }{@code >}
     *     {@link JAXBElement }{@code <}{@link Media }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaContainer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaBundle }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaAsset }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaInstance }{@code >}
     *     
     */
    public JAXBElement<? extends Media> getMedia() {
        return media;
    }

    /**
     * Sets the value of the media property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MediaFile }{@code >}
     *     {@link JAXBElement }{@code <}{@link Media }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaContainer }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaBundle }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaAsset }{@code >}
     *     {@link JAXBElement }{@code <}{@link MediaInstance }{@code >}
     *     
     */
    public void setMedia(JAXBElement<? extends Media> value) {
        this.media = ((JAXBElement<? extends Media> ) value);
    }

}
