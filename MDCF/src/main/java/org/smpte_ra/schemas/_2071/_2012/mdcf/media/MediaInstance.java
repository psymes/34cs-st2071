
package org.smpte_ra.schemas._2071._2012.mdcf.media;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.types.FramedTime;


/**
 * <p>Java class for MediaInstance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MediaInstance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}MediaFile">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Duration"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/media}Composition"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaInstance", propOrder = {
    "duration",
    "composition"
})
@XmlSeeAlso({
    MediaBundle.class
})
public class MediaInstance
    extends MediaFile
{

    @XmlElement(name = "Duration", required = true)
    protected FramedTime duration;
    @XmlElement(name = "Composition", required = true)
    protected MediaFormatSegments composition;

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link FramedTime }
     *     
     */
    public FramedTime getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link FramedTime }
     *     
     */
    public void setDuration(FramedTime value) {
        this.duration = value;
    }

    /**
     * Gets the value of the composition property.
     * 
     * @return
     *     possible object is
     *     {@link MediaFormatSegments }
     *     
     */
    public MediaFormatSegments getComposition() {
        return composition;
    }

    /**
     * Sets the value of the composition property.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaFormatSegments }
     *     
     */
    public void setComposition(MediaFormatSegments value) {
        this.composition = value;
    }

}
