
package org.smpte_ra.schemas._2071._2012.mdcf.media;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.smpte_ra.schemas._2071._2012.mdcf.query.AND;
import org.smpte_ra.schemas._2071._2012.mdcf.query.CONTAINS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.EQUALS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.GREATERTHAN;
import org.smpte_ra.schemas._2071._2012.mdcf.query.IMPLEMENTS;
import org.smpte_ra.schemas._2071._2012.mdcf.query.LESSTHAN;
import org.smpte_ra.schemas._2071._2012.mdcf.query.MATCHES;
import org.smpte_ra.schemas._2071._2012.mdcf.query.NOT;
import org.smpte_ra.schemas._2071._2012.mdcf.query.OR;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;


/**
 * <p>Java class for UMNFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UMNFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/identity}UMN"/>
 *         &lt;element ref="{http://www.smpte-ra.org/schemas/2071/2012/mdcf/query}QueryExpression" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UMNFilter", propOrder = {
    "umn",
    "queryExpression"
})
public class UMNFilter {

    @XmlElement(name = "UMN", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/identity", required = true)
    protected String umn;
    @XmlElementRef(name = "QueryExpression", namespace = "http://www.smpte-ra.org/schemas/2071/2012/mdcf/query", type = JAXBElement.class)
    protected JAXBElement<? extends QueryExpression> queryExpression;

    /**
     * Gets the value of the umn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUMN() {
        return umn;
    }

    /**
     * Sets the value of the umn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUMN(String value) {
        this.umn = value;
    }

    /**
     * Gets the value of the queryExpression property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GREATERTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link LESSTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link CONTAINS }{@code >}
     *     {@link JAXBElement }{@code <}{@link IMPLEMENTS }{@code >}
     *     {@link JAXBElement }{@code <}{@link EQUALS }{@code >}
     *     {@link JAXBElement }{@code <}{@link AND }{@code >}
     *     {@link JAXBElement }{@code <}{@link OR }{@code >}
     *     {@link JAXBElement }{@code <}{@link MATCHES }{@code >}
     *     {@link JAXBElement }{@code <}{@link NOT }{@code >}
     *     {@link JAXBElement }{@code <}{@link QueryExpression }{@code >}
     *     
     */
    public JAXBElement<? extends QueryExpression> getQueryExpression() {
        return queryExpression;
    }

    /**
     * Sets the value of the queryExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GREATERTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link LESSTHAN }{@code >}
     *     {@link JAXBElement }{@code <}{@link CONTAINS }{@code >}
     *     {@link JAXBElement }{@code <}{@link IMPLEMENTS }{@code >}
     *     {@link JAXBElement }{@code <}{@link EQUALS }{@code >}
     *     {@link JAXBElement }{@code <}{@link AND }{@code >}
     *     {@link JAXBElement }{@code <}{@link OR }{@code >}
     *     {@link JAXBElement }{@code <}{@link MATCHES }{@code >}
     *     {@link JAXBElement }{@code <}{@link NOT }{@code >}
     *     {@link JAXBElement }{@code <}{@link QueryExpression }{@code >}
     *     
     */
    public void setQueryExpression(JAXBElement<? extends QueryExpression> value) {
        this.queryExpression = ((JAXBElement<? extends QueryExpression> ) value);
    }

}
