// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 1.1.4322.2032
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

// 
// This source code was auto-generated by Mono Web Services Description Language Utility
//


/// <remarks/>
[System.Web.Services.WebServiceBinding(Name="AcquirablePort", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public class AcquirableHTTPBinding : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    public AcquirableHTTPBinding() {
        this.Url = "http://localhost:8088/Acquirable";
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/Acquired", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("getAcquiredResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public BooleanResponse getAcquired([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] getAcquired getAcquired) {
        object[] results = this.Invoke("getAcquired", new object[] {
                    getAcquired});
        return ((BooleanResponse)(results[0]));
    }
    
    public System.IAsyncResult BegingetAcquired(getAcquired getAcquired, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("getAcquired", new object[] {
                    getAcquired}, callback, asyncState);
    }
    
    public BooleanResponse EndgetAcquired(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((BooleanResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/AcquiredBy", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("getAcquiredByResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public getAcquiredByResponse getAcquiredBy([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] getAcquiredBy getAcquiredBy) {
        object[] results = this.Invoke("getAcquiredBy", new object[] {
                    getAcquiredBy});
        return ((getAcquiredByResponse)(results[0]));
    }
    
    public System.IAsyncResult BegingetAcquiredBy(getAcquiredBy getAcquiredBy, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("getAcquiredBy", new object[] {
                    getAcquiredBy}, callback, asyncState);
    }
    
    public getAcquiredByResponse EndgetAcquiredBy(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((getAcquiredByResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/acquire", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("acquireResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public URIResponse acquire([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] acquire acquire) {
        object[] results = this.Invoke("acquire", new object[] {
                    acquire});
        return ((URIResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginacquire(acquire acquire, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("acquire", new object[] {
                    acquire}, callback, asyncState);
    }
    
    public URIResponse Endacquire(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((URIResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/approve", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("approveResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public approveResponse approve([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] URIResponse approve) {
        object[] results = this.Invoke("approve", new object[] {
                    approve});
        return ((approveResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginapprove(URIResponse approve, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("approve", new object[] {
                    approve}, callback, asyncState);
    }
    
    public approveResponse Endapprove(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((approveResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/deny", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("denyResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public denyResponse deny([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] deny deny) {
        object[] results = this.Invoke("deny", new object[] {
                    deny});
        return ((denyResponse)(results[0]));
    }
    
    public System.IAsyncResult Begindeny(deny deny, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("deny", new object[] {
                    deny}, callback, asyncState);
    }
    
    public denyResponse Enddeny(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((denyResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/request", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("requestResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public URIResponse request([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] SessionRequestParameters request) {
        object[] results = this.Invoke("request", new object[] {
                    request});
        return ((URIResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginrequest(SessionRequestParameters request, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("request", new object[] {
                    request}, callback, asyncState);
    }
    
    public URIResponse Endrequest(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((URIResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/force", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("forceResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public URIResponse force([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] SessionRequestParameters force) {
        object[] results = this.Invoke("force", new object[] {
                    force});
        return ((URIResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginforce(SessionRequestParameters force, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("force", new object[] {
                    force}, callback, asyncState);
    }
    
    public URIResponse Endforce(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((URIResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Acquirable/release", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("releaseResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public BooleanResponse release([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] URIResponse release) {
        object[] results = this.Invoke("release", new object[] {
                    release});
        return ((BooleanResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginrelease(URIResponse release, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("release", new object[] {
                    release}, callback, asyncState);
    }
    
    public BooleanResponse Endrelease(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((BooleanResponse)(results[0]));
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getAcquired {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute("unlockResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class BooleanResponse {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")]
    public bool Boolean;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getAcquiredBy {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getAcquiredByResponse {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItem(IsNullable=false)]
    public Session[] Sessions;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class Session {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
    public string SessionID;
    
    /// <remarks/>
    public string Who;
    
    /// <remarks/>
    public string Name;
    
    /// <remarks/>
    public DateTime AcquiredAt;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")]
[System.Xml.Serialization.XmlRootAttribute("AcquiredAt", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FramedTime))]
public class DateTime {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
    public System.DateTime Date;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string Hour;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string Minute;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string Second;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
    public string Microsecond;
    
    /// <remarks/>
    public System.UInt64 MicrosFromEpoch;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")]
public class FramedTime : DateTime {
    
    /// <remarks/>
    public System.UInt64 Frame;
    
    /// <remarks/>
    public System.UInt64 FrameRate;
    
    /// <remarks/>
    public System.UInt64 Scale;
    
    /// <remarks/>
    public System.UInt64 TotalFrames;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class acquire {
    
    /// <remarks/>
    public string Name;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute("unlock", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class URIResponse {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/identity", DataType="anyURI")]
    public string URI;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class approveResponse {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class deny {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
    public string ID;
    
    /// <remarks/>
    public string Message;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class denyResponse {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute("force", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class SessionRequestParameters {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
    public string ID;
    
    /// <remarks/>
    public string Name;
    
    /// <remarks/>
    public string Message;
}

/// <remarks/>
[System.Web.Services.WebServiceBinding(Name="LockablePort", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
public class LockableHTTPBinding : System.Web.Services.Protocols.SoapHttpClientProtocol {
    
    public LockableHTTPBinding() {
        this.Url = "http://localhost:8088/Lockable";
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/Locked", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("getLockedResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public getLockedResponse getLocked([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] getLocked getLocked) {
        object[] results = this.Invoke("getLocked", new object[] {
                    getLocked});
        return ((getLockedResponse)(results[0]));
    }
    
    public System.IAsyncResult BegingetLocked(getLocked getLocked, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("getLocked", new object[] {
                    getLocked}, callback, asyncState);
    }
    
    public getLockedResponse EndgetLocked(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((getLockedResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/LockedBy", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("getLockedByResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public getLockedByResponse getLockedBy([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] getLockedBy getLockedBy) {
        object[] results = this.Invoke("getLockedBy", new object[] {
                    getLockedBy});
        return ((getLockedByResponse)(results[0]));
    }
    
    public System.IAsyncResult BegingetLockedBy(getLockedBy getLockedBy, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("getLockedBy", new object[] {
                    getLockedBy}, callback, asyncState);
    }
    
    public getLockedByResponse EndgetLockedBy(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((getLockedByResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/approve", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("approveResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public approveResponse approve([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] URIResponse approve) {
        object[] results = this.Invoke("approve", new object[] {
                    approve});
        return ((approveResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginapprove(URIResponse approve, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("approve", new object[] {
                    approve}, callback, asyncState);
    }
    
    public approveResponse Endapprove(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((approveResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/deny", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("denyResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public denyResponse deny([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] deny deny) {
        object[] results = this.Invoke("deny", new object[] {
                    deny});
        return ((denyResponse)(results[0]));
    }
    
    public System.IAsyncResult Begindeny(deny deny, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("deny", new object[] {
                    deny}, callback, asyncState);
    }
    
    public denyResponse Enddeny(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((denyResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/lock", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("lockResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public BooleanResponse @lock([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] @lock @lock) {
        object[] results = this.Invoke("lock", new object[] {
                    @lock});
        return ((BooleanResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginlock(@lock @lock, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("lock", new object[] {
                    @lock}, callback, asyncState);
    }
    
    public BooleanResponse Endlock(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((BooleanResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/requestLock", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("requestLockResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public URIResponse requestLock([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] LockRequestParameters requestLock) {
        object[] results = this.Invoke("requestLock", new object[] {
                    requestLock});
        return ((URIResponse)(results[0]));
    }
    
    public System.IAsyncResult BeginrequestLock(LockRequestParameters requestLock, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("requestLock", new object[] {
                    requestLock}, callback, asyncState);
    }
    
    public URIResponse EndrequestLock(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((URIResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/forceLock", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("forceLockResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public URIResponse forceLock([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] LockRequestParameters forceLock) {
        object[] results = this.Invoke("forceLock", new object[] {
                    forceLock});
        return ((URIResponse)(results[0]));
    }
    
    public System.IAsyncResult BeginforceLock(LockRequestParameters forceLock, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("forceLock", new object[] {
                    forceLock}, callback, asyncState);
    }
    
    public URIResponse EndforceLock(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((URIResponse)(results[0]));
    }
    
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.smpte-ra.org/wsdl/2071/2012/mdcf/device/control/Lockable/unlock", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
    [return: System.Xml.Serialization.XmlElementAttribute("unlockResponse", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
    public BooleanResponse unlock([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")] URIResponse unlock) {
        object[] results = this.Invoke("unlock", new object[] {
                    unlock});
        return ((BooleanResponse)(results[0]));
    }
    
    public System.IAsyncResult Beginunlock(URIResponse unlock, System.AsyncCallback callback, object asyncState) {
        return this.BeginInvoke("unlock", new object[] {
                    unlock}, callback, asyncState);
    }
    
    public BooleanResponse Endunlock(System.IAsyncResult asyncResult) {
        object[] results = this.EndInvoke(asyncResult);
        return ((BooleanResponse)(results[0]));
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getLocked {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getLockedResponse {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/types")]
    public bool Boolean;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getLockedBy {
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class getLockedByResponse {
    
    /// <remarks/>
    public Session Session;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class @lock {
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(DataType="anyURI")]
    public string ID;
    
    /// <remarks/>
    public string Name;
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
[System.Xml.Serialization.XmlRootAttribute("forceLock", Namespace="http://www.smpte-ra.org/schemas/2071/2012/mdcf/device/control")]
public class LockRequestParameters {
    
    /// <remarks/>
    public string Name;
    
    /// <remarks/>
    public string Message;
}
