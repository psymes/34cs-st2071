#!/bin/sh

CONFIG=$1

if [ "$CONFIG" == "" ]; then
    echo Command Line\: analyze.sh \<config file\>
else
    /Java/wsi-test-tools/java/bin/Analyzer.sh -config $1 -assertionDescription true -replace true -messageEntry no
fi
