package org.smpte._2071._2012.mdcd.impl;

import java.math.BigInteger;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.smpte._2071._2012.mdcd.DiscoveryService;
import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.ServiceName;
import org.smpte._2071._2012.mdcd.naming.NameRecord;
import org.smpte._2071._2012.mdcd.naming.NamingService;
import org.smpte._2071._2012.mdcd.naming.NamingServices;
import org.smpte._2071._2012.mdcd.net.DHCPClient;
import org.smpte._2071._2012.mdcd.net.InetAddressUtils;
import org.smpte._2071._2012.mdcd.net.NetworkTopologyDiscoveryService;
import org.smpte._2071._2012.mdcd.net.NetworkTopologyDiscoveryServiceImpl;
import org.smpte._2071._2012.mdcd.net.NetworkTopologyListener;
import org.smpte_ra.schemas._2071._2012.mdcf.device.Device;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceDirectory;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformations;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformationsResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.device.UDNFilter;
import org.smpte_ra.schemas._2071._2012.mdcf.media.Media;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaDirectory;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaList;
import org.smpte_ra.schemas._2071._2012.mdcf.media.MediaListResponse;
import org.smpte_ra.schemas._2071._2012.mdcf.media.UMNFilter;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;
import org.smpte_ra.schemas._2071._2012.mdcf.types.DateTime;

public class DiscoveryServiceImpl implements DiscoveryService
{
    private static final Logger log = Logger.getLogger(DiscoveryServiceImpl.class.getName());
    
    private static final int DEFAULT_DNS_NAMING_PORT = 53;

    private NetworkTopologyDiscoveryService topology;
    
    private NamingServices namingServices = new NamingServices();
    
    private Set<DiscoveryListener> listeners = new ConcurrentHashSet<DiscoveryListener>();

    private Set<String> configuredBrowseDomains = new LinkedHashSet<String>();
    
    private ExecutorService cachedExecutor = Executors.newCachedThreadPool(new ThreadFactory()
    {
        @Override
        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(r, "Discover Service Cached Execution Thread");
            t.setDaemon(false);
            return t;
        }
    });
    
    private ExecutorService topologyChangeExecutor = Executors.newSingleThreadExecutor(new ThreadFactory()
    {
        @Override
        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(r, "Network Topology Change Thread");
            t.setDaemon(true);
            return t;
        }
    });
    
    private ExecutorService dispatchExecutor = Executors.newCachedThreadPool(new ThreadFactory()
    {
        @Override
        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(r, "Discovery Listener Dispatch Thread");
            t.setDaemon(true);
            return t;
        }
    });
    
    private DiscoveryListener discoveryListenerDispatcher = new DiscoveryListener()
    {
        @Override
        public void namingServiceAdded(final InetAddress address, final String domain, final NamingService namingService)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.namingServiceAdded(address, domain, namingService);
                    }
                });
            }
        }
        
        
        @Override
        public void addressAdded(final NetworkInterface networkInterface, final InetAddress address)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.addressAdded(networkInterface, address);
                    }
                });
            }
        }


        @Override
        public void addressRemoved(final NetworkInterface networkInterface, final InetAddress address)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.addressRemoved(networkInterface, address);
                    }
                });
            }
        }


        @Override
        public void browseDomainAdded(final InetAddress address, final String domain)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.browseDomainAdded(address, domain);
                    }
                });
            }
        }


        @Override
        public void browseDomainRemoved(final InetAddress address, final String domain)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.browseDomainRemoved(address, domain);
                    }
                });
            }
        }


        @Override
        public void namingServiceRemoved(final InetAddress address, final String domain, final NamingService namingService)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.namingServiceRemoved(address, domain, namingService);
                    }
                });
            }
        }


        @Override
        public void deviceFound(final InetAddress address, final String domain, final DeviceInformation device)
        {
            for (final DiscoveryListener listener : listeners)
            {
                dispatchExecutor.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.deviceFound(address, domain, device);
                    }
                });
            }
        }
    };
    
    private NetworkTopologyListener topologyListener = new NetworkTopologyListener()
    {
        @Override
        public void interfaceAdded(final NetworkInterface networkInterface, final List<InetAddress> addresses)
        {
            try
            {
                if (networkInterface!= null && networkInterface.isUp() && addresses != null && !addresses.isEmpty())
                {
                    for (final InetAddress address : addresses)
                    {
                        dispatchExecutor.submit(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                defaultBrowseDomains(address);
                                findAdditionalBrowseDomains(address);
                                discoveryListenerDispatcher.addressAdded(networkInterface, address);
                            }
                        });
                    }
                }
            } catch (Exception e)
            {
                Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
            }
        }


        @Override
        public void addressAdded(final NetworkInterface networkInterface, final InetAddress address)
        {
            dispatchExecutor.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        if (networkInterface != null && networkInterface.isUp() && address != null)
                        {
                            defaultBrowseDomains(address);
                            findAdditionalBrowseDomains(address);
                            discoveryListenerDispatcher.addressAdded(networkInterface, address);
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                    }
                }
            });
        }


        @Override
        public void interfaceRemoved(final NetworkInterface networkInterface, final List<InetAddress> addresses)
        {
            dispatchExecutor.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        if (networkInterface!= null && networkInterface.isUp() && addresses != null && !addresses.isEmpty())
                        {
                            for (InetAddress address : addresses)
                            {
                                removeBrowseDomains(address);
                                discoveryListenerDispatcher.addressRemoved(networkInterface, address);
                            }
                            
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                    }
                }
            });
        }


        @Override
        public void addressRemoved(final NetworkInterface networkInterface, final InetAddress address)
        {
            dispatchExecutor.submit(new Runnable()
            {
                
                @Override
                public void run()
                {
                    try
                    {
                        if (networkInterface!= null && networkInterface.isUp() && address != null)
                        {
                            removeBrowseDomains(address);
                            discoveryListenerDispatcher.addressRemoved(networkInterface, address);
                        }
                        
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                    }
                }
            });
        }
    };
    
    
    public DiscoveryServiceImpl(NetworkTopologyDiscoveryService topology)
    {
        this.topology = topology;
    }
    
    
    /**
     * In order for a DNS based service discovery protocol to work properly its clients must be able 
     * to determine which domains must use to find services.  The following steps shall be used to 
     * create a list of the "base" / default list of browse domains. The steps below are based on 
     * the process defined in Section 11 of the DNS-SD specification [RFC 6763].  However, additional 
     * steps have been added to include the use of local configuration, such as the host name configured 
     * for the local system, manually configured domain names, and the hostnames determined by reverse 
     * DNS lookup of each local Internet interface address.
     * 
     * 1.  The Reverse Subnet Mask domain for IPv4 and IPv6 addresses, as defined by Section 11 
     *     of the DNS-SD specification [RFC 6763].  i.e. 192.168.1.0/24 shall be 0.1.168.192.in-addr.arpa,
     *     10.75.0.0/16 shall be 0.0.75.10.in-addr.arpa.
     * 2.  Manually configured browse domains.
     * 3.  The domain name portion of the hostname configured for the client.
     * 4.  For Internet Protocol v4 networks with DHCP services available.
     *     a.  Add the domain name as provided by the networks DHCP services, as defined RFC 2132.
     *     b.  Add domains found using the DHCP Domain Search Option as defined by RFC 3397.
     * 5.  For Internet Protocol v6 networks.
     *     a.  Add domains using the IPv6 Router Advertisement Options, as defined in RFC 6106. 
     * 6.  The domain name portion of each hostname(s) associated to the Reverse Domain Name lookup 
     *     of the Internet Address, refer to RFC 2317, RFC 3596 Section 2.5, RFC 1033, & RFC 1912.
     * 7.   "local", if Multicast DNS services are used.
     * 
     * This method determines the "base" / default browse domains for the provided local Internet address. 
     * 
     * @param address
     */
    protected void defaultBrowseDomains(InetAddress localAddress)
    {
        /* fine */ log.info("Generating Default Browse Domains for address \"" + localAddress + "\".");
        
        NamingService namingService = namingServices.getDefaultNamingService(localAddress);
        
        try
        {
            
            // Step 1.
            // Add Subnet Mask Domain
            int networkPrefixLength = topology.getNetworkPrefixLength(localAddress);
            if (networkPrefixLength > 0)
            {
                InetAddress subnetAddress = InetAddressUtils.subnetAddress(localAddress, networkPrefixLength);
                String reverseSubnetDomain = InetAddressUtils.reverseSubnetMaskDomain(subnetAddress, networkPrefixLength);
                discoveryListenerDispatcher.namingServiceAdded(localAddress, reverseSubnetDomain, namingService);
                namingServices.register(localAddress, reverseSubnetDomain, namingService);
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error searching subnet address for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }

        try
        {
            // Step 2.
            // Add Manually configured Domains
            for (String browseDomain : configuredBrowseDomains)
            {
                discoveryListenerDispatcher.namingServiceAdded(localAddress, browseDomain, namingService);
                namingServices.register(localAddress, browseDomain, namingService);
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error configuring domains for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
            
        // Step 3.
        // Add Domain Name from configured hostname and default Java Reverse Address Lookup
        try
        {
            HashSet<String> names = new HashSet<String>();
            
            /*
             * Java is too stupid to recognize that an InetAddress equal to its localhost address is localhost
             * and therefore will not return the proper hostname from the local system cache.  
             * InetAddress.getLocalHost().getHostName() is used to fix this issue if 
             * InetAddress.equals(InetAddress.getLocalHost()).
             */
            // Step 3.
            InetAddress localhost = InetAddress.getLocalHost();
            if (localAddress.equals(localhost))
            {
                String hostname = localhost.getHostName();
                if (!hostname.contains(localAddress.getHostAddress()))
                {
                    names.add(hostname);
                }
            }
            
            namesLoop:
            for (String name : names)
            {
                try
                {
                    String domain = namingServices.determineParentDomain(name);
                    
                    for (String multicastDomain : NamingServices.LOCALLINK_DNS_DOMAINS)
                    {
                        if (multicastDomain.equals(domain))
                        {
                            // Skip this name, it is a multicast domain
                            continue namesLoop;
                        }
                    }
                    
                    discoveryListenerDispatcher.namingServiceAdded(localAddress, domain, namingService);
                    namingServices.register(localAddress, domain, namingService);
                } catch (Exception e)
                {
                    Utils.log(log, Level.WARNING, "Error attempting to determine domain name for \"" + name + "\" on interface \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                }
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error resolving localhost name for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
            
        try
        {
            // If IPv4
            if (localAddress.getAddress().length == 4)
            {
                // Step 4.
                // Gets domain names using RFCs 2132 & 3397.
                try
                {
                    Set<String> names = DHCPClient.resolveDHCPDomainNames(localAddress);
                    if (names != null && names.size() > 0)
                    {
                        for (String name : names)
                        {
                            discoveryListenerDispatcher.namingServiceAdded(localAddress, name, namingService);
                            namingServices.register(localAddress, name, namingService);
                        }
                    }
                } catch (BindException e)
                {
                    Utils.log(log, Level.WARNING, "DHCP could not be used for interface \"" + localAddress + "\", software does not have the privileges required to issue DHCP queries.", Level.FINE, e);
                } catch (Exception e)
                {
                    Utils.log(log, Level.WARNING, "DHCP could not be used for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                }
            // If IPv6
            } else
            {
                // Step 5.
                // TODO: IPv6 Router Advertisement Options, as defined in RFC 6106
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error resolving domains through DHCP for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
            
        try
        {
            // Step 6.
            // Reverse DNS Lookups!
            List<NameRecord> records = namingService.reverseQuery(localAddress);
            if (records != null && records.size() > 0)
            {
                for (NameRecord record : records)
                {
                    String parentDomain = namingServices.determineParentDomain(record.getTarget());
                    discoveryListenerDispatcher.namingServiceAdded(localAddress, parentDomain, namingService);
                    namingServices.register(localAddress, parentDomain, namingService);
                }
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error issuing reverse lookup for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
         
        try
        {
            // Step 7.
            NamingService mDNSNamingService = namingServices.newNamingService(MULTICAST_DNS_DOMAIN, localAddress, null);
            if (mDNSNamingService != null)
            {
                namingServices.register(localAddress, MULTICAST_DNS_DOMAIN, mDNSNamingService);
                discoveryListenerDispatcher.namingServiceAdded(localAddress, MULTICAST_DNS_DOMAIN, mDNSNamingService);
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error creating mDNS services for \"" + localAddress + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        /* fine */ log.info("Default Browse Domains for address \"" + localAddress + "\" are " + namingServices.getDomains(localAddress) + ".");
        
        findAdditionalNamingServices(localAddress);
    }
    
    
    protected Set<NamingService> findAdditionalNamingServices(InetAddress localAddress)
    {
        /* fine */ log.info("Finding Additional Naming Services for address \"" + localAddress + "\".");
        Set<NamingService> namingServiceInstances = new LinkedHashSet<NamingService>();
        ConcurrentLinkedQueue<String> domains = new ConcurrentLinkedQueue<String>(namingServices.getDomains(localAddress));
        
        for (String domain : domains)
        {
            namingServiceInstances.addAll(findAdditionalNamingServices(localAddress, domain));
        }
        
        /* fine */ log.info("Finished Finding Additional Naming Services for address \"" + localAddress + "\".");
        return namingServiceInstances;
    }
    
    
    protected Set<NamingService> findAdditionalNamingServices(InetAddress localAddress, String domain)
    {
        Set<NamingService> namingServiceInstances = new LinkedHashSet<NamingService>();
        
        try
        {
            NamingService namingService = namingServices.getNamingService(localAddress, domain);
            if (namingService != null)
            {
                try
                {
                    List<ServiceInstance> services = namingService.serviceDiscovery(new ServiceName(_DOMAIN_UDP, domain));
                    if (services != null && services.size() > 0)
                    {
                        for (ServiceInstance service : services)
                        {
                            try
                            {
                                Set<InetSocketAddress> serverAddresses = new LinkedHashSet<InetSocketAddress>();
                                int port = service.getPort();
                                port = (port > 0 ? port : DEFAULT_DNS_NAMING_PORT);
                                for (InetAddress inetAddress : service.getAddresses())
                                {
                                    serverAddresses.add(new InetSocketAddress(inetAddress, port));
                                }
                                
                                if (serverAddresses.size() > 0 && !namingServices.contains(localAddress, domain, serverAddresses))
                                {
                                    NamingService newNamingService = namingServices.newNamingService(domain, localAddress, serverAddresses);
                                    namingServices.register(localAddress, domain, newNamingService);
                                    namingServiceInstances.add(newNamingService);
                                    discoveryListenerDispatcher.namingServiceAdded(localAddress, domain, newNamingService);
                                    /* fine */ log.info("Adding Naming Service " + newNamingService + " for address \"" + localAddress + "\".");
                                }
                            } catch (Exception e)
                            {
                                Utils.log(log, Level.WARNING, "Error adding Naming Service \"" + service.getName() + "\" for domain \"" + domain + "\" - " + e.getMessage(), Level.FINE, e);
                            }
                        }
                    } else
                    {
                        /* fine */ log.info("No additional DNS Servers discovered!");
                    }
                } catch (Exception e)
                {
                    Utils.log(log, Level.WARNING, "Could not lookup \"" + _DOMAIN_UDP + "." + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                }
            } else
            {
                log.warning("Could not interrogate domain \"" + domain + "\" Naming Service could not be found!");
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error attempting to discover additional Name Service on interface \"" + localAddress + " for domain \"" + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        return namingServiceInstances;
    }


    protected void findAdditionalBrowseDomains(InetAddress localAddress)
    {
        /* fine */ log.info("Finding Additional Browse Domains for address \"" + localAddress + "\".");
        
        ConcurrentLinkedQueue<String> domains = new ConcurrentLinkedQueue<String>(namingServices.getDomains(localAddress));
        for (String domain : domains)
        {
            domains.addAll(findAdditionalBrowseDomains(localAddress, domain));
        }
        /* fine */ log.info("Finished Finding Additional Browse Domains for address \"" + localAddress + "\".");
        log.info("Browse Domains for address \"" + localAddress + "\" are " + domains);
    }
    
    
    protected Set<String> findAdditionalBrowseDomains(InetAddress localAddress, String domain)
    {
        Set<String> browseDomains = new LinkedHashSet<String>();
        
        try
        {
            NamingService namingService = namingServices.getNamingService(localAddress, domain);
            if (namingService != null)
            {
                for (String queryName : BROWSE_DOMAIN_QUERY_NAMES)
                {
                    try
                    {
                        List<NameRecord> records = namingService.query(new ServiceName(queryName, domain));
                        if (records != null && records.size() > 0)
                        {
                            for (NameRecord record : records)
                            {
                                String browseDomain = record.getTarget();
                                if (!browseDomain.equals(domain))
                                {
                                    browseDomains.add(browseDomain);
                                    /* fine */ log.info("Additional Browse Domain \"" + browseDomain + "\" found for address \"" + localAddress + "\".");
                                    discoveryListenerDispatcher.namingServiceAdded(localAddress, browseDomain, namingService);
                                    namingServices.register(localAddress, browseDomain, namingService);
                                    
                                    findAdditionalNamingServices(localAddress, domain);
                                }
                            }
                        } else
                        {
                            /* fine */ log.info("No additional browse domains discovered for \"" + queryName + "." + domain + "\".");
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Could not lookup \"" + _DOMAIN_UDP + "." + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                }
            } else
            {
                log.warning("Could not interrogate domain \"" + domain + "\" Naming Service could not be found!");
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error attempting to discover additional Name Service on interface \"" + localAddress + " for domain \"" + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        return browseDomains;
    }
    
    
    protected Map<String, NamingService> findRegistrationDomains(InetAddress address, String domain)
    {
        Map<String, NamingService> namingServicesByDomain = new LinkedHashMap<String, NamingService>();
        Set<String> registrationDomains = new LinkedHashSet<String>();
        
        try
        {
            NamingService namingService = namingServices.getNamingService(address, domain);
            if (namingService != null)
            {
                for (String queryName : REGISTRATION_DOMAIN_QUERY_NAMES)
                {
                    try
                    {
                        List<NameRecord> records = namingService.query(new ServiceName(queryName, domain));
                        if (records != null && records.size() > 0)
                        {
                            for (NameRecord record : records)
                            {
                                String browseDomain = record.getTarget();
                                registrationDomains.add(browseDomain);
                                namingServicesByDomain.put(browseDomain, namingService);
                            }
                        } else
                        {
                            /* fine */ log.info("No additional browse domains discovered for \"" + queryName + "." + domain + "\" !");
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Could not lookup \"" + queryName + "." + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                    
                    if (registrationDomains.size() > 0)
                    {
                        for (String registrationDomain : registrationDomains)
                        {
                            findRegistrationDomains(address, registrationDomain);
                        }
                        
                        // Exit as soon as registration domains are found.
                        break;
                    }
                }
            } else
            {
                log.warning("Could not interrogate domain \"" + domain + "\" Naming Service could not be found!");
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error attempting to discover registration domains on interface \"" + address + " for domain \"" + domain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        return namingServicesByDomain;
    }
    
    
    protected List<ServiceInstance> findServices(InetAddress address, ServiceName serviceName)
    throws NamingException
    {
        String domain = serviceName.getDomain();
        NamingService namingService = namingServices.getNamingService(address, domain);
        return namingService.serviceDiscovery(serviceName);
    }
    
    
    protected List<DeviceInformation> findDevicesWithDD(InetAddress address, String browseDomain, QueryExpression query, String udn)
    {
        List<DeviceInformation> devices = new ArrayList<DeviceInformation>();
        
        try
        {
            List<ServiceInstance> services = findServices(address, new ServiceName(SERVICE_TYPE_DEVICE_DIRECTORY, browseDomain));
            if (services != null && services.size() > 0)
            {
                for (ServiceInstance instance : services)
                {
                    Map<String, String> textRecord = instance.getTextRecord();
                    if (textRecord.size() > 0)
                    {
                        try
                        {
                            URL endpointURL = generateEndpointURL(instance, textRecord);
                            
                            Service service = Service.create(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DeviceDirectoryPort"));
                            DeviceDirectory dd = service.getPort(DeviceDirectory.class);
                            ((BindingProvider) dd).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL.toString());
                            
                            // TODO: Implement QueryExpression.apply(Device) && QueryExpression.apply(Media)
                            // if (query.apply(dd))
                            // {
                            UDNFilter filter = new UDNFilter();
                            filter.setUDN(udn);
                            filter.setQueryExpression(new JAXBElement<QueryExpression>(new QName("QueryExpression"), QueryExpression.class, query));
                            DeviceInformationsResponse response = dd.search(filter);
                            if (response != null)
                            {
                                DeviceInformations deviceInformations = response.getDeviceInformations();
                                if (deviceInformations != null)
                                {
                                    List<DeviceInformation> tempDevices = deviceInformations.getDeviceInformation();
                                    if (tempDevices != null && tempDevices.size() > 0)
                                    {
                                        devices.addAll(tempDevices);
                                    }
                                }
                            }
                            // }
                        } catch (MalformedURLException e)
                        {
                            Utils.log(log, Level.SEVERE, "Could not create URL to Device Directory endpoint " + instance, Level.FINE, e);
                        }
                    } else
                    {
                        log.severe("Configuration Error: Service Instance does not contain a Device Directory TXT Record - " + instance);
                    }
                }
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error attempting to discover Device Directories on interface \"" + address + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        return devices;
    }
    
    
    protected List<DeviceInformation> findDevicesWithoutDD(InetAddress address, String browseDomain, QueryExpression query, String udn)
    {
        List<DeviceInformation> devices = new ArrayList<DeviceInformation>();
        
        try
        {
            List<ServiceInstance> services = findServices(address, new ServiceName(SERVICE_TYPE_DEVICE, browseDomain));
            if (services != null && services.size() > 0)
            {
                for (ServiceInstance instance : services)
                {
                    Map<String, String> textRecord = instance.getTextRecord();
                    if (textRecord.size() > 0)
                    {
                        try
                        {
                            URL endpointURL = generateEndpointURL(instance, textRecord);
                            
                            Service service = Service.create(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "DevicePort"));
                            Device device = service.getPort(Device.class);
                            ((BindingProvider) device).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL.toString());
                            
                            // TODO : Implement QueryExpression.apply(Device) && QueryExpression.apply(Media)
                            // if (query.apply(device))
                            // {
                                devices.add(deviceToDeviceInformation(device));
                            // }
                        } catch (MalformedURLException e)
                        {
                            Utils.log(log, Level.SEVERE, "Could not create URL to Device Directory endpoint " + instance, Level.FINE, e);
                        }
                    } else
                    {
                        log.severe("Configuration Error: Service Instance does not contain a Device Directory TXT Record - " + instance);
                    }
                }
            }
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error attempting to discover Device Directories on interface \"" + address + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        return devices;
    }
    
    
    protected URL generateEndpointURL(ServiceInstance instance, Map<String, String> textRecord)
    throws MalformedURLException
    {
        String protocol = textRecord.get(TXT_PROP_PROTOCOL);
        String host = textRecord.get(TXT_PROP_HOST);
        String port = textRecord.get(TXT_PROP_PORT);
        String path = textRecord.get(TXT_PROP_PATH);
        
        if (protocol == null || protocol.length() == 0)
        {
            protocol = "mdcp";
        }
        
        if (host == null || host.length() == 0)
        {
            host = instance.getHost();
        }
        
        if (port == null || port.length() == 0)
        {
            if (instance.getPort() > 0)
            {
                port = Integer.toString(instance.getPort());
            }
        }
        
        StringBuilder builder = new StringBuilder();
        builder.append(protocol).append("://").append(host);
        if (port != null && port.length() > 0)
        {
            builder.append(":").append(port);
        }
        if (!path.startsWith("/"))
        {
            builder.append("/");
        }
        builder.append(path);
        
        return new URL(builder.toString());
    }
    
    
    protected DeviceInformation deviceToDeviceInformation(Device device)
    {
        DeviceInformation information = new DeviceInformation();
        
        information.setName(device.getName());
        information.setUDN(device.getUDN());
        information.setAttributes(device.getAttributes());
        information.setCapabilities(device.getCapabilities());
        information.setOnline(device.getOnline());
        information.setURLs(device.getURLs());
        
        try
        {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(System.currentTimeMillis() + 30000);
            XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            DateTime dateTime = new DateTime();
            dateTime.setDate(xmlCalendar);
            dateTime.setHour(calendar.get(Calendar.HOUR));
            dateTime.setMinute(calendar.get(Calendar.MINUTE));
            dateTime.setSecond(calendar.get(Calendar.SECOND));
            dateTime.setMicrosecond(calendar.get(Calendar.MILLISECOND) * 1000);
            dateTime.setMicrosFromEpoch(BigInteger.valueOf(calendar.getTimeInMillis()).multiply(BigInteger.valueOf(1000)));
            information.setValidTill(dateTime);
        } catch (Exception e)
        {
            Utils.log(log, Level.WARNING, "Error creating ValidTill date for DeviceInformation \"" + information.getUDN() + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
        }
        
        
        return information;
    }
    

    @Override
    public void addDiscoveryListener(DiscoveryListener listener)
    {
        this.listeners.add(listener);
    }
    
    
    @Override
    public Set<InetAddress> getInternetAddresses()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
    @Override
    public Set<String> getBrowseDomains()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
    @Override
    public Set<String> getRegistrationDomains()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
    @Override
    public Map<String, List<DeviceInformation>> listDevices(final InetAddress localAddress, final String udn, final QueryExpression query)
    throws NamingException
    {
        final Map<String, List<DeviceInformation>> results = new LinkedHashMap<String, List<DeviceInformation>>();
        
        final Map<String, DeviceInformation> devices = new LinkedHashMap<String, DeviceInformation>();
        
        final Set<String> browseDomains = namingServices.getDomains(localAddress);
        
        List<Future<?>> futures = new ArrayList<Future<?>>();
        for (final String browseDomain : browseDomains)
        {
            futures.add(cachedExecutor.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        List<DeviceInformation> deviceList = findDevicesWithDD(localAddress, browseDomain, query, udn);
                        for (DeviceInformation info : deviceList)
                        {
                            try
                            {
                                String infoUDN = info.getUDN();
                                synchronized (devices)
                                {
                                    if (!devices.containsKey(infoUDN))
                                    {
                                        devices.put(infoUDN, info);
                                    }
                                }
                            } catch (Exception e)
                            {
                                Utils.log(log, Level.WARNING, "Error attempting to discover Devices with Device Directory on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                            }
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Error attempting to discover Devices on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                }
            }));
        }
        
        // Wait for Device Directory Search to complete
        boolean done = false;
        loop:
        while (!done)
        {
            for (Future<?> future : futures)
            {
                if (!future.isDone())
                {
                    try
                    {
                        future.get();
                    } catch (Exception e)
                    {
                        // ignore
                    }
                    continue loop;
                }
                done = true;
            }
        }
            
        for (final String browseDomain : browseDomains)
        {
            cachedExecutor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        List<DeviceInformation> deviceList = findDevicesWithoutDD(localAddress, browseDomain, query, udn);
                        for (DeviceInformation info : deviceList)
                        {
                            try
                            {
                                String infoUDN = info.getUDN();
                                synchronized (devices)
                                {
                                    if (!devices.containsKey(infoUDN))
                                    {
                                        devices.put(infoUDN, info);
                                    }
                                }
                            } catch (Exception e)
                            {
                                Utils.log(log, Level.WARNING, "Error attempting to discover Devices without Device Directory on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                            }
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Error attempting to discover Devices on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                }
            });
        }
        
        // Wait for DNS-SD Search to complete
        done = false;
        loop:
        while (!done)
        {
            for (Future<?> future : futures)
            {
                if (!future.isDone())
                {
                    try
                    {
                        future.get();
                    } catch (Exception e)
                    {
                        // ignore
                    }
                    continue loop;
                }
                done = true;
            }
        }
            
        for (final String browseDomain : browseDomains)
        {
            Collection<DeviceInformation> resultingDevices = devices.values();
            // TODO: Implements QueryExpression.sort(Collection<Device>) && QueryExpression.sort(Collection<Media>) 
            // results.put(browseDomain, query.sort(resultingDevices));
            results.put(browseDomain, new ArrayList<DeviceInformation>(resultingDevices));
        }
        
        return results;
    }
    
    
    @Override
    public void browseDevices(final InetAddress localAddress, final String udn, final QueryExpression query, final DiscoveryListener listener)
    throws NamingException
    {
        final HashSet<String> devices = new HashSet<String>();
        
        Set<String> browseDomains = namingServices.getDomains(localAddress);
        for (final String browseDomain : browseDomains)
        {
            cachedExecutor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        List<DeviceInformation> deviceList = findDevicesWithDD(localAddress, browseDomain, query, udn);
                        for (DeviceInformation info : deviceList)
                        {
                            try
                            {
                                String infoUDN = info.getUDN();
                                boolean added = false;
                                synchronized (devices)
                                {
                                    added = devices.add(infoUDN);
                                }
                                
                                if (added)
                                {
                                    listener.deviceFound(localAddress, browseDomain, info);
                                }
                            } catch (Exception e)
                            {
                                Utils.log(log, Level.WARNING, "Error attempting to discover Devices with Device Directory on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                            }
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Error attempting to discover Devices on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                }
            });
            
            cachedExecutor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        List<DeviceInformation> deviceList = findDevicesWithoutDD(localAddress, browseDomain, query, udn);
                        for (DeviceInformation info : deviceList)
                        {
                            try
                            {
                                String infoUDN = info.getUDN();
                                boolean added = false;
                                synchronized (devices)
                                {
                                    added = devices.add(infoUDN);
                                }
                                
                                if (added)
                                {
                                    listener.deviceFound(localAddress, browseDomain, info);
                                }
                            } catch (Exception e)
                            {
                                Utils.log(log, Level.WARNING, "Error attempting to discover Devices without Device Directory on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                            }
                        }
                    } catch (Exception e)
                    {
                        Utils.log(log, Level.WARNING, "Error attempting to discover Devices on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
                    }
                }
            });
        }
    }
    
    
    @Override
    public List<Media> findMedia(InetAddress localAddress, String umn, QueryExpression query)
    {
        List<Media> media = new ArrayList<Media>();
        
        Set<String> browseDomains = namingServices.getDomains(localAddress);
        for (String browseDomain : browseDomains)
        {
            try
            {
                List<ServiceInstance> services = findServices(localAddress, new ServiceName(SERVICE_TYPE_MEDIA_DIRECTORY, browseDomain));
                if (services != null && services.size() > 0)
                {
                    for (ServiceInstance instance : services)
                    {
                        Map<String, String> textRecord = instance.getTextRecord();
                        if (textRecord.size() > 0)
                        {
                            try
                            {
                                URL endpointURL = generateEndpointURL(instance, textRecord);
                                
                                Service service = Service.create(new QName("http://www.smpte-ra.org/schemas/2071/2012/mdcf/device", "MediaDirectoryPort"));
                                MediaDirectory md = service.getPort(MediaDirectory.class);
                                ((BindingProvider) md).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL.toString());
                                
                                // TODO: Implement QueryExpression.apply(Device) && QueryExpression.apply(Media)
                                // if (query.apply(md))
                                // {
                                UMNFilter filter = new UMNFilter();
                                filter.setUMN(umn);
                                filter.setQueryExpression(new JAXBElement<QueryExpression>(new QName("QueryExpression"), QueryExpression.class, query));
                                MediaListResponse response = md.search(filter);
                                if (response != null)
                                {
                                    MediaList mediaList = response.getMediaList();
                                    if (mediaList != null)
                                    {
                                        List<JAXBElement<? extends Media>> mediaElements = mediaList.getMedia();
                                        if (mediaElements != null && mediaElements.size() > 0)
                                        {
                                            for (JAXBElement<? extends Media> mediaElement : mediaElements)
                                            {
                                                media.add(mediaElement.getValue());
                                            }
                                        }
                                    }
                                }
                                // }
                            } catch (MalformedURLException e)
                            {
                                Utils.log(log, Level.SEVERE, "Could not create URL to Media Directory endpoint " + instance, Level.FINE, e);
                            }
                        } else
                        {
                            log.severe("Configuration Error: Service Instance does not contain a Media Directory TXT Record - " + instance);
                        }
                    }
                }
            } catch (Exception e)
            {
                Utils.log(log, Level.WARNING, "Error attempting to discover Media Directories on interface \"" + localAddress + " for domain \"" + browseDomain + "\"" + (e.getMessage() != null ? " - " + e.getMessage() : "."), Level.FINE, e);
            }
        }
        
        return media;
    }
    
    
    protected void removeBrowseDomains(InetAddress address)
    {
        namingServices.unregisterAll(address);
    }
    
    
    @Override
    public void registerDevice(InetAddress localAddress, DeviceInformation device)
    throws NamingException
    {
        // TODO Auto-generated method stub
        
    }
    
    
    @Override
    public void unregisterDevice(DeviceInformation device)
    throws NamingException
    {
        // TODO Auto-generated method stub
        
    }
    
    
    @Override
    public List<ServiceInstance> browseServices(ServiceName serviceName)
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
    @Override
    public void start()
    throws Exception
    {
        /* fine */ log.info("Discovery Service Starting");
        
        if (topology == null)
        {
            topology = new NetworkTopologyDiscoveryServiceImpl(true, true, true, true); 
        }
        
        topology.addNetworkTopologyListener(this.topologyListener);
        
        topology.start();
        log.info("Discovery Service Started");
    }
    
    
    @Override
    public void stop()
    {
        /* fine */ log.info("Discovery Service Stopping");
        topology.stop();
        topologyChangeExecutor.shutdownNow();
        dispatchExecutor.shutdownNow();
        cachedExecutor.shutdownNow();
        log.info("Discovery Service Stopped");
    }
    
    
    public static void main(String[] args)
    throws Exception
    {
        NetworkTopologyDiscoveryServiceImpl topology = new NetworkTopologyDiscoveryServiceImpl(false, true, true, false);
        DiscoveryServiceImpl discoveryService = new DiscoveryServiceImpl(topology);
        discoveryService.start();
        discoveryService.stop();
    }
}
