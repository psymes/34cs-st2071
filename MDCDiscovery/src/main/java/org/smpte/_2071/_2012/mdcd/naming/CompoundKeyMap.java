package org.smpte._2071._2012.mdcd.naming;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class CompoundKeyMap<K1, K2, V extends Object>
{
    private HashMap<K1, HashMap<K2, V>> data = new HashMap<K1, HashMap<K2, V>>();
    
    
    public synchronized V put(K1 key1, K2 key2, V value)
    {
        HashMap<K2, V> subdata = data.get(key1);
        if (subdata == null)
        {
            subdata = new HashMap<K2, V>();
            data.put(key1, subdata);
        }
        
        return subdata.put(key2, value);
    }
    
    
    public synchronized V get(K1 key1, K2 key2)
    {
        HashMap<K2, V> subdata = data.get(key1);
        if (subdata != null)
        {
            return subdata.get(key2);
        }
        
        return null;
    }
    
    
    public synchronized boolean containsKey(K1 key1, K2 key2)
    {
        HashMap<K2, V> subdata = data.get(key1);
        if (subdata != null)
        {
            return subdata.containsKey(key2);
        }
        
        return false;
    }
    
    
    public synchronized V remove(K1 key1, K2 key2)
    {
        V result = null;
        
        HashMap<K2, V> subdata = data.get(key1);
        if (subdata != null)
        {
            result = subdata.remove(key2);
            if (subdata.isEmpty())
            {
                data.remove(key1);
            }
        }
        
        return result;
    }


    public synchronized int size()
    {
        int size = 0;
        for (K1 key : data.keySet())
        {
            HashMap<K2, V> subdata = data.get(key);
            if (subdata != null)
            {
                size += subdata.size();
            }
        }
        
        return size;
    }


    public String toString()
    {
        return data.toString();
    }


    public void clear()
    {
        data.clear();
    }


    public HashMap<K1, Set<K2>> keyMap()
    {
        HashMap<K1, Set<K2>> set = new HashMap<K1, Set<K2>>();
        
        for (K1 key : data.keySet())
        {
            HashMap<K2, V> subdata = data.get(key);
            if (subdata != null)
            {
                set.put(key, subdata.keySet());
            }
        }
        
        return set;
    }


    public Collection<V> values()
    {
        List<V> list = new ArrayList<V>();
        
        for (K1 key : data.keySet())
        {
            HashMap<K2, V> subdata = data.get(key);
            if (subdata != null)
            {
                list.addAll(subdata.values());
            }
        }
        
        return list;
    }


    public Set<Entry<K1, HashMap<K2, V>>> entrySet()
    {
        return data.entrySet();
    }
}
