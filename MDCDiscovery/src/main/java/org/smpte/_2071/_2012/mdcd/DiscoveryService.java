package org.smpte._2071._2012.mdcd;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.naming.NamingService;
import org.smpte_ra.schemas._2071._2012.mdcf.device.DeviceInformation;
import org.smpte_ra.schemas._2071._2012.mdcf.media.Media;
import org.smpte_ra.schemas._2071._2012.mdcf.query.QueryExpression;

/**
 * @author Steve Posick
 *
 */
public interface DiscoveryService
{
    public static final String MULTICAST_DNS_DOMAIN = "local.";
    
    public static final String SERVICE_TYPE_MDC = "_mdc._tcp";
    
    public static final String SERVICE_TYPE_DEVICE = "_d._sub." + SERVICE_TYPE_MDC;
    
    public static final String SERVICE_TYPE_DEVICE_DIRECTORY = "_dd._sub." + SERVICE_TYPE_MDC;
    
    public static final String SERVICE_TYPE_MEDIA_DIRECTORY = "_md._sub." + SERVICE_TYPE_MDC;
    
    public static final String _DOMAIN_UDP = "_domain._udp";

    public static final String[] BROWSE_DOMAIN_QUERY_NAMES = new String[] {"db._dns-sd._udp",
                                                                            "b._dns-sd._udp",
                                                                            "lb._dns-sd._udp"};
    
    public static final String[] REGISTRATION_DOMAIN_QUERY_NAMES = new String[] {"dr._dns-sd._udp",
                                                                                  "r._dns-sd._udp"};

    public static final String UCN_DEVICE_v1 = "urn:smpte:ucn:device_v1";

    public static final String UCN_DEVICE_DIRECTORY_v1 = "urn:smpte:ucn:device_directory_v1";

    public static final String UCN_MEDIA_DIRECTORY_v1 = "urn:smpte:ucn:media_directory_v1";

    public static final String TXT_PROP_UCN = "ucn";

    public static final String TXT_PROP_PROTOCOL = "proto";

    public static final String TXT_PROP_PORT = "port";

    public static final String TXT_PROP_HOST = "host";
    
    public static final String TXT_PROP_PATH = "path";
    
    /**
     * The Discovery Listener is used to notify Discovery clients of changes to the Network Topology 
     * and when new Browse Domains or Naming Services and Devices are found.
     * 
     * @author Steve Posick
     */
    public static interface DiscoveryListener
    {
        public void addressAdded(final NetworkInterface networkInterface, final InetAddress address);
        
        
        public void addressRemoved(final NetworkInterface networkInterface, final InetAddress address);
        
        
        public void browseDomainAdded(final InetAddress address, final String domain);
        
        
        public void browseDomainRemoved(final InetAddress address, final String domain);
        
        
        public void namingServiceAdded(final InetAddress address, final String domain, final NamingService namingService);
        
        
        public void namingServiceRemoved(final InetAddress address, final String domain, final NamingService namingService);
        
        
        public void deviceFound(final InetAddress address, final String domain, final DeviceInformation device);
    }
    
    
    /**
     * Adds a Discovery Listener
     * 
     * @param listener The Discovery Listener
     */
    public void addDiscoveryListener(DiscoveryListener listener);
    
    
    /**
     * Lists the Internet Addresses available to the discovery service.
     * The returned Set is concurrent, meaning that new Internet Addresses are added as they are discovered.
     *  
     * @return Returns the concurrent set of available local Internet Addressed.
     */
    public Set<InetAddress> getInternetAddresses();
    
    
    /**
     * Returns the set of browse domains available on the network, via Unicast DNS and Multicast DNS.
     * The returned Set is concurrent, meaning that new browse domains are added as they are discovered.
     * 
     * @return the concurrent set of browse domains available on the network
     */
    public Set<String> getBrowseDomains();
    
    
    /**
     * Returns the set of registration domains available on the network, via Unicast DNS and Multicast DNS.
     * The returned Set is concurrent, meaning that new registration domains are added as they are discovered.
     * 
     * @return the concurrent set of registration domains available on the network
     */
    public Set<String> getRegistrationDomains();
    
    
    /**
     * Browses the DNS infrastructure for the Device(s) that match the namespace UDN and
     * Query Expression.  This method blocks until the browse process is complete. 
     * 
     * @param localAddress The local interface to browse.  If null or 0.0.0.0, all local interfaces are browsed.
     * @param udn The namespace UDN narrowing the Devices to a specific namespace hierarchy.
     * @param query The Query Expression limiting the Devices
     * @return The Device(s) that match the namespace UDN and Query Expression by domain.
     */
    public Map<String, List<DeviceInformation>> listDevices(InetAddress localAddress, String udn, QueryExpression query)
    throws NamingException;
    
    
    /**
     * Uses the MDC Discovery to local all of the applicable Media Directories and queries them for
     * the Media indicated by the UMN and Query Expression. 
     * 
     * @param localAddress The local interface to browse.  If null or 0.0.0.0, all local interfaces are browsed.
     * @param umn The namespace UMN narrowing the Media to a specific namespace hierarchy.
     * @param query The Query Expression limiting the Media
     * @return
     */
    public List<Media> findMedia(InetAddress localAddress, String umn, QueryExpression query);
    
    
    /**
     * Asynchronously Browses the DNS infrastructure for the Device(s) that match the namespace UDN and
     * Query Expression.  Devices that are found are sent to the specified Discovery Listener, if the 
     * Discover Listener is not specified, events are sent to all Discovery Listeners registered to this 
     * service instance.
     *  
     * @param localAddress The local interface to browse.  If null or 0.0.0.0, all local interfaces are browsed.
     * @param udn The namespace UDN narrowing the Devices to a specific namespace hierarchy.
     * @param query The Query Expression limiting the Devices
     * @param listener The Listener to broadcast events to, if null, events will be broadcast to all Discovery
     *                 Listeners registered to this Discovery Service instance.
     */
    public void browseDevices(InetAddress localAddress, String udn, QueryExpression query, DiscoveryListener listener)
    throws NamingException;
    
    
    /**
     * Registers the specified device for Discovery.
     * 
     * @param device The device to register.
     */
    public void registerDevice(InetAddress localAddress, DeviceInformation device)
    throws NamingException;
    
    
    /**
     * Unregister a device from the Discovery system.
     * 
     * @param instance The Device to unregister. 
     */
    public void unregisterDevice(DeviceInformation device)
    throws NamingException;

    
    /**
     * Searches the Discovery Service for the Services matching the specified service instance name, subtype + type or type.
     * e.g. 
     *     MyService._d._sub._mdc._tcp.local,  _d._sub._mdc._tcp.local,   or _mdc._tcp.local
     *     MyService._d._sub._mdc._tcp,        _d._sub._mdc._tcp,         or _mdc._tcp
     * 
     * @param serviceName The Discovery Query.  Service instance name, subtype + type or type, with optional domain.
     * 
     * @return Returns a list of services of the specified service type.
     */
    public List<ServiceInstance> browseServices(ServiceName serviceName)
    throws NamingException;
    
    
    /**
     * Starts the Discovery Service and its time processes.
     * 
     * @throws Exception if the Discover Service cannot be started for any reason.
     */
    public void start()
    throws Exception;
    
    
    /**
     * Stops the Discovery Service and its time processes, freeing resources.
     */
    public void stop();
}
