package org.smpte._2071._2012.mdcd.naming;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.impl.ConcurrentHashSet;
import org.smpte._2071._2012.mdcd.impl.Utils;
import org.smpte._2071._2012.mdcd.naming.NamingService.NamingListener;

public abstract class AbstractNamingServiceImpl implements NamingService, NamingListener
{
    protected final Logger log = Logger.getLogger(getClass().getName());
    
    protected InetAddress localAddress;

    protected Set<String> domains = new LinkedHashSet<String>();

    protected Set<InetSocketAddress> serverAddresses = new LinkedHashSet<InetSocketAddress>();
    
    protected Set<NamingListener> listeners = new ConcurrentHashSet<NamingService.NamingListener>();


    public AbstractNamingServiceImpl()
    {
        super();
    }
    

    @Override
    public void setLocalAddress(InetAddress localAddress)
    throws NamingException
    {
        this.localAddress = localAddress;
    }


    @Override
    public InetAddress getLocalAddress()
    {
        return this.localAddress;
    }
    
    
    @Override
    public Set<String> getDomains()
    {
        synchronized (this.domains)
        {
            return Collections.unmodifiableSet(this.domains);
        }
    }
    
    
    @Override
    public boolean isAssociatedToDomain(String domain)
    {
        synchronized (this.domains)
        {
            return this.domains.contains(domain);
        }
    }
    

    @Override
    public boolean associateToDomain(String domain)
    throws NamingException
    {
        synchronized (this.domains)
        {
            if (this.domains.add(domain))
            {
                domainAssociatedToNamingService(this, domain);
                return true;
            } else
            {
                return false;
            }
        }
    }


    @Override
    public boolean unassociateToDomain(String domain)
    throws NamingException
    {
        synchronized (this.domains)
        {
            if (this.domains.remove(domain))
            {
                domainUnassociatedToNamingService(this, domain);
                return true;
            } else
            {
                return false;
            }
        }
    }
    
    
    @Override
    public void setServerAddresses(Set<InetSocketAddress> addresses)
    throws NamingException
    {
        this.serverAddresses = new LinkedHashSet<InetSocketAddress>(addresses);
    }
    
    
    @Override
    public boolean addServerAddresses(Set<InetSocketAddress> addresses)
    throws NamingException
    {
        boolean addressAdded = false;
        
        if (addresses != null)
        {
            synchronized (serverAddresses)
            {
                for (InetSocketAddress address : addresses)
                {
                    if (this.serverAddresses.add(address))
                    {
                        addressAdded = true;
                    }
                }
            }
        }
        
        return addressAdded;
    }
    
    
    @Override
    public Set<InetSocketAddress> getServerAddresses()
    {
        synchronized (this.domains)
        {
            return Collections.unmodifiableSet(this.serverAddresses);
        }
    }

    
    @Override
    public void close()
    throws IOException
    {
        synchronized (this.domains)
        {
            for (String domain : this.domains)
            {
                try
                {
                    unassociateToDomain(domain);
                } catch (NamingException e)
                {
                    Utils.log(log, Level.INFO, e.getMessage(), Level.FINE, e);
                }
            }
        }
    }
    
    
    public void addNamingListener(NamingListener listener)
    {
        listeners.add(listener);
    }
    
    
    public void removeNamingListener(NamingListener listener)
    {
        listeners.add(listener);
    }
    
    
    public void namingServiceAdded(ServiceInstance namingService, InetAddress localAddress)
    {
        for (NamingListener listener : listeners)
        {
            listener.namingServiceAdded(namingService, localAddress);
        }
    }
    
    
    public void namingServiceRemoved(ServiceInstance namingService, InetAddress localAddress)
    {
        for (NamingListener listener : listeners)
        {
            listener.namingServiceRemoved(namingService, localAddress);
        }
    }
    
    
    public void domainAssociatedToNamingService(NamingService namingService, String domain)
    {
        for (NamingListener listener : listeners)
        {
            listener.domainAssociatedToNamingService(namingService, domain);
        }
    }
    
    
    public void domainUnassociatedToNamingService(NamingService namingService, String domain)
    {
        for (NamingListener listener : listeners)
        {
            listener.domainUnassociatedToNamingService(namingService, domain);
        }
    }
    
    private Integer hashcode = null;
    
    @Override
    public int hashCode()
    {
        if (hashcode == null)
        {
            hashcode = toString().hashCode();
        }
        
        return hashcode;
    }
    
    
    @Override
    public String toString()
    {
        return "NamingService [Local Address: " + this.localAddress + ", domains: " + this.domains + "]"; 
    }
    
    
    @Override
    public NAMING_TYPE getNamingType()
    {
        return NAMING_TYPE.UNICAST_DNS;
    }
}
