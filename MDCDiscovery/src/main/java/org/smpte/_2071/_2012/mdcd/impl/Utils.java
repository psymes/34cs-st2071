package org.smpte._2071._2012.mdcd.impl;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils
{
    public static final <K, V>void put(Map<K, Set<V>> map, K key, V value)
    {
        Set<V> set = map.get(key);
        
        if (set == null)
        {
            set = new HashSet<V>();
            map.put(key, set);
        }
        
        if (!set.contains(value))
        {
            set.add(value);
        }
    }
    
    
    public static final <K, V>boolean remove(Map<K, Set<V>> map, K key, V value)
    {
        boolean result = false;
        Set<V> set = map.get(key);
        
        if (set != null)
        {
            result = set.remove(value);
        }
        
        if (set == null || set.isEmpty())
        {
            map.remove(key);
        }
        
        return result;
    }
    
    
    public static void log(Logger log, Level level, String message, Level threshold, Throwable e)
    {
        String[] caller = inferCaller();
        if (log.isLoggable(threshold == null ? Level.FINE : threshold))
        {
            log.logp(level, caller[0], caller[1], message, e);
        } else
        {
            log.logp(level, caller[0], caller[1], message);
        }
    }
    
    
    public static String[] inferCaller()
    {
        // Get the stack trace.
        String[] source = new String[] {"org.smpte._2071._2012.mdcd.impl.Utils", "log"};
        StackTraceElement stack[] = (new Throwable()).getStackTrace();
        // First, search back to a method in the Logger class.
        int ix = 0;
        while (ix < stack.length)
        {
            StackTraceElement frame = stack[ix];
            String cname = frame.getClassName();
            if (cname.equals("org.smpte._2071._2012.mdcd.impl.Utils"))
            {
                break;
            }
            ix++;
        }
        // Now search for the first frame before the "Logger" class.
        while (ix < stack.length)
        {
            StackTraceElement frame = stack[ix];
            String cname = frame.getClassName();
            if (!cname.equals("org.smpte._2071._2012.mdcd.impl.Utils"))
            {
                // We've found the relevant frame.
                source[0] = cname;
                source[1] = frame.getMethodName();
                return source;
            }
            ix++;
        }
        // We haven't found a suitable frame, so just punt. This is
        // OK as we are only committed to making a "best effort" here.
        return source;
    }
    
    
    public static <T extends Throwable>T convertThrowable(Class<T> clazz, String message, Throwable root)
    {
        T t = null;
        
        try
        {
            Constructor<T> constructor = clazz.getConstructor(String.class);
            t = constructor.newInstance(message);
            t.setStackTrace(root.getStackTrace());
        } catch (Exception e)
        {
            // log.log(Level.WARNING, "Could not convert " +
            // root.getClass().getName() + "(\"" + root.getMessage() + "\") to "
            // + clazz.getName() + "(\"" + root.getMessage() + "\").");
            try
            {
                t = clazz.newInstance();
            } catch (Exception e1)
            {
                // log.log(Level.SEVERE, "Could not convert " +
                // root.getClass().getName() + "(\"" + root.getMessage() +
                // "\") to " + clazz.getName() + "(\"" + root.getMessage() +
                // "\").");
                throw new RuntimeException("Could not convert " + root.getClass().getName() + "(\"" + root.getMessage() + "\") to " + clazz.getName() + "(\"" + root.getMessage() + "\").", e);
            }
        }
        
        return t;
    }
}
