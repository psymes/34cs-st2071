package org.smpte._2071._2012.mdcd.naming;

import java.io.Closeable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.ServiceName;

/**
 * The Naming interface abstracts the underlying naming system, such as DNS or mDNS.
 * 
 * @author Steve Posick
 */
public interface NamingService extends Closeable
{
    /**
     * The Naming Listener allows a client to be notified when a new NamingService is discovered or removed.
     * 
     * @author Steve Posick
     */
    public interface NamingListener
    {
        /**
         * Called when a new NamingService is Found.
         * 
         * @param namingService The NamingService
         */
        public void namingServiceAdded(ServiceInstance namingService, InetAddress localAddress);
        
        
        /**
         * Called when a NamingService is Lost or Removed.
         * 
         * @param namingService The NamingService
         */
        public void namingServiceRemoved(ServiceInstance namingService, InetAddress localAddress);


        /**
         * Called when a domain is associated to a NamingService.
         * 
         * @param namingService The NamingService
         * @param domain The domain associated to the NamingService
         */
        public void domainAssociatedToNamingService(NamingService namingService, String domain);


        /**
         * Called when a domain is unassociated from a NamingService.
         * 
         * @param namingService The NamingService
         * @param domain The domain unassociated from the NamingService
         */
        public void domainUnassociatedToNamingService(NamingService namingService, String domain);
    }
    
    
    /**
     * Available Name Resolution Types.
     * 
     * @author Steve Posick
     */
    public enum NAMING_TYPE
    {
        UNICAST_DNS, MULTICAST_DNS
    };
    
    
    /**
     * Sets the local Internet Address for the Naming service
     * 
     * @param localAddress The Internet Address of the local interface the naming service is associated with (NIC to send out requests).
     * @throws NamingException
     */
    public void setLocalAddress(InetAddress localAddress)
    throws NamingException;
    
    
    /**
     * Returns the local Internet Address for the Naming service.
     * 
     * @return the local Internet Address for the Naming service.
     */
    public InetAddress getLocalAddress();
    
    
    /**
     * Returns the complete set of Internet Socket Addresses, after the addition of the new addresses.
     * 
     * @return The complete set of Internet Socket Addresses, after the addition of the new addresses.
     */
    public Set<InetSocketAddress> getServerAddresses();
    
    
    /**
     * Returns the set of domains associated to this NamingService.
     * 
     * @return the set of domains associated to this NamingService.
     */
    public Set<String> getDomains();
    
    
    /**
     * Returns true if the domain is associated to this NamingService.
     * 
     * @return true if the domain is associated to this NamingService.
     */
    public boolean isAssociatedToDomain(String domain);
    
    
    /**
     * Associates a domain to the NamingService.
     * 
     * @param domain The domain name.
     * @return true if the domain was not already associated to the NamingService and was successfully associated.
     * @throws NamingException If the NamingService cannot be associated to the domain.
     */
    public boolean associateToDomain(String domain)
    throws NamingException;
    
    
    /**
     * Unassociates a domain from the NamingService.
     * 
     * @param domain
     * @return true if the domain was was previously associated to the NamingService, but is now unassociated.
     * @throws NamingException
     */
    public boolean unassociateToDomain(String domain)
    throws NamingException;
    
    
    /**
     * Adds new remote Naming Server's Socket Addresses to this NamingService.
     * 
     * @param serverAddress The set of Internet Socket Addresses of the remote Naming Servers.
     * @return true if one or more of the server address were added.
     * @throws NamingException
     */
    public boolean addServerAddresses(Set<InetSocketAddress> serverAddresses)
    throws NamingException;
    
    
    /**
     * Sets new remote Naming Server's Socket Addresses to this NamingService, clearing any old addresses.
     * 
     * @param serverAddress The set of Internet Socket Addresses of the remote Naming Servers.
     * @throws NamingException
     */
    public void setServerAddresses(Set<InetSocketAddress> serverAddresses)
    throws NamingException;
    
    
    /**
     * Performs a reverse DNS Lookup to acquire the host names assigned to an address.
     * 
     * @param address The Internet Address
     * @return The host names associated to the address.
     * @throws NamingException
     */
    public List<NameRecord> reverseQuery(InetAddress address)
    throws NamingException;
    
    
    /**
     * Lists the name records that match the name or type within the domain.
     *  
     * Ex.
     *     b._dns-sd._udp, b._dns-sd._udp.local, _domain._udp, _domain._udp.local
     * 
     * @param serviceName The name of the records to list. Can be an Instance, subtype or application specification.  If no domain is specified, all domains for the NamingService are searched
     * @return An list of naming records.
     * @throws NamingException
     */
    public List<NameRecord> query(ServiceName serviceName)
    throws NamingException;
    
    
    /**
     * Searches for the service records that match the application type or instance name within the domain.
     * 
     * Ex.
     *     _domain._udp, _domain._udp.local, My DNS Server_domain._udp, My DNS Server_domain._udp.local
     * 
     * @param serviceName The name of the services to list. Can be an Instance, subtype or application specification.  If no domain is specified, all domains for the NamingService are searched.
     * @return An list of Service Records matching the type or instance name in the domain.
     * @throws NamingException
     */
    public List<ServiceInstance> serviceDiscovery(ServiceName serviceName)
    throws NamingException;
    
    
    /**
     * Add a Naming Listener
     * 
     * @param listener The Naming Listener
     */
    public void addNamingListener(NamingListener listener);
    
    
    /**
     * Removes a Naming Listener
     * 
     * @param listener The Naming Listener
     */
    public void removeNamingListener(NamingListener listener);


    /**
     * Returns the Naming Type of the Naming Service
     * 
     * @return The Naming Type of the Naming Service
     */
    public NAMING_TYPE getNamingType();
}
