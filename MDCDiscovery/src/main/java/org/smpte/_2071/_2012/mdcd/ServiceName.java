package org.smpte._2071._2012.mdcd;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.naming.NamingServices;
import org.xbill.DNS.Name;

public class ServiceName implements Serializable
{
    private static final long serialVersionUID = 201210241240L;
    
    protected static final Pattern pattern = Pattern.compile("(([^_][^(\\._)]*\\.)*)?(((_[^\\.]*)\\.(_sub)\\.)?((_[^\\.]*)\\.(_(udp|tcp))))(\\.([^\\.]*(\\.[^\\.]*)*))?");
    
    private String instance;
    
    private String fullSubType;
    
    private String subType;
    
    private String fullType;
    
    private String type;
    
    private String domain;
    
    private String protocol;
    
    private String application;
    
    private String fullName;
    
    private String nameWithoutDomain;
    
    
    public ServiceName(String name)
    {
        this(name, null);
    }
    
    
    public ServiceName(String name, String domain)
    {
        if (name != null)
        {
            Matcher m = pattern.matcher(name);
            if (m.matches())
            {
                // The name is a DNS-SD Service Name.
                this.instance = m.group(1);
                if (this.instance != null && this.instance.length() > 0 && this.instance.charAt(this.instance.length() - 1) == '.')
                {
                    // Remove trailing '.'
                    this.instance = this.instance.substring(0, instance.length() - 1);
                }
                this.fullType = m.group(3);
                this.fullSubType = m.group(4);
                this.subType = m.group(5);
                this.type = m.group(7);
                this.application = m.group(8);
                this.protocol = m.group(9);
                this.domain = domain != null ? domain : m.group(12);
            } else
            {
                // The name is DNS host name.
                if (domain == null || domain.length() == 0)
                {
                    try
                    {
                        this.domain = determineParentDomain(name);
                    } catch (Exception e)
                    {
                        IllegalArgumentException iae = new IllegalArgumentException("Error determining parent domain from \"" + name + "\" - " + e.getMessage());
                        iae.setStackTrace(e.getStackTrace());
                        throw iae;
                    }
                } else
                {
                    this.domain = domain;
                }
                this.instance = trimDomain(name, domain);
            }
        } else
        {
            throw new IllegalArgumentException("Name cannot be null!");
        }
        
        if (this.domain != null && this.domain.charAt(this.domain.length() - 1) != '.')
        {
            this.domain += ".";
        }
    }
    
    
    private String determineParentDomain(String domainName)
    {
        try
        {
            StringBuilder builder = new StringBuilder(domainName.length());
            Name name = Name.fromString(domainName);
            int labels = name.labels();
            if (labels > 1)
            {
                for (int index = 1; index < labels; index++)
                {
                    String labelString = name.getLabelString(index);
                    // Skip Service Name Type components
                    if (labelString.charAt(0) != '_')
                    {
                        continue;
                    } else
                    {
                        builder.append(labelString);
                        if (index < labels - 1)
                        {
                            builder.append(".");
                        }
                    }
                }
                return builder.toString();
            } else
            {
                return null;
            }
        } catch (Exception e)
        {
            return null;
        }
    }


    private String trimDomain(String name, String domain)
    {
        if (name != null && name.length() > 0)
        {
            if (domain != null && domain.length() > 0)
            {
                // Trim domain from Name if it is present.
                String tempName;
                String tempDomain;
                
                if (domain.charAt(domain.length() - 1) == '.')
                {
                    tempDomain = domain.substring(0, domain.length() - 2);
                } else
                {
                    tempDomain = domain;
                }
                
                if (name.charAt(name.length() - 1) != '.')
                {
                    tempName = name.substring(0, name.length() - 2);
                } else
                {
                    tempName = name;
                }
                
                tempName = tempName.endsWith(tempDomain) ? tempName.substring(0, tempName.length() - tempDomain.length()) : tempName;
                return tempName != null && tempName.length() > 0 ? tempName : null;
            } else
            {
                int pos = name.indexOf('.');
                if (pos > 0)
                {
                    return name.substring(0, pos);
                }
            }
        }
        
        return null;
    }


    public ServiceName(String instance, String fullSubType, String subType, String fullType, String type, String application, String protocol, String domain)
    {
        super();
        this.instance = instance;
        this.fullSubType = fullSubType;
        this.subType = subType;
        this.fullType = fullType;
        this.type = type;
        this.domain = domain;
        this.protocol = protocol;
        this.application = application;
    }



    public String getInstance()
    {
        return instance;
    }
    
    
    public String getFullSubType()
    {
        return fullSubType;
    }
    
    
    public String getSubType()
    {
        return subType;
    }
    
    
    public String getFullType()
    {
        return fullType;
    }
    
    
    public String getType()
    {
        return type;
    }
    
    
    public String getDomain()
    {
        return domain;
    }
    
    
    public String getProtocol()
    {
        return protocol;
    }
    
    
    public String getApplication()
    {
        return application;
    }
    
    
    public String getNameWithoutDomain()
    {
        if (nameWithoutDomain == null)
        {
            generateNames();
        }
        return nameWithoutDomain;
    }
    
    /*
    public ServiceName changeDomain(String domain)
    {
        this.nameWithoutDomain = fullName = null;
        this.domain = domain;
        generateNames();
        return this;
    }
    */
    
    
    public String toString()
    {
        if (fullName == null)
        {
            generateNames();
        }
        return fullName;
    }

    
    /*
    public static ServiceName newInstance(String name)
    {
        if (name != null)
        {
            Matcher m = pattern.matcher(name);
            if (m.matches())
            {
                String instance = m.group(1);
                // Remove trailing '.'
                if (instance != null && instance.length() > 1)
                {
                    instance = instance.substring(0, instance.length() - 1);
                }
                String fullType = m.group(3);
                String fullSubtype = m.group(4);
                String subtype = m.group(5);
                String type = m.group(7);
                String application = m.group(8);
                String protocol = m.group(9);
                String domain = m.group(12);
                
                ServiceName serviceName = new ServiceName(instance, fullSubtype, subtype, fullType, type, application, protocol, domain);
                
                if (!serviceName.toString().matches(name + "\\."))
                {
                    throw new IllegalArgumentException("Something went wrong here!  Input \"" + name + "\" does not match output \"" + serviceName + "\"!");
                }
                
                return serviceName;
            }
            
            throw new IllegalArgumentException("Name \"" + name + "\" is not a DNS Service Name!");
        } else
        {
            throw new IllegalArgumentException("Name cannot be null!");
        }
    }
    */
    
    
    private final void generateNames()
    {
        StringBuilder name = new StringBuilder();
        append(name, instance);
        append(name, fullType);
        this.nameWithoutDomain = name.toString();
        
        append(name, domain);
        this.fullName = name.toString();
    }
    
    
    private final static void append(StringBuilder builder, String namepart)
    {
        if (namepart != null)
        {
            if (builder.length() > 0)
            {
                if (builder.charAt(builder.length() - 1) == '.' || (namepart.length() > 0 && namepart.charAt(0) == '.'))
                {
                    builder.append(namepart);
                } else
                {
                    builder.append('.').append(namepart);
                }
            } else
            {
                builder.append(namepart);
            }
        }
    }
}
