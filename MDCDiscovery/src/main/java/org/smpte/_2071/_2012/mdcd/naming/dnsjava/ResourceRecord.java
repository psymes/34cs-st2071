package org.smpte._2071._2012.mdcd.naming.dnsjava;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.xbill.DNS.Name;

class ResourceRecord implements Serializable
{
    private static final long serialVersionUID = 201210181454L;
    
    private Name name;
    
    private int type;
    
    private Name host;
    
    private Name target;
    
    private List<InetAddress> addresses = new ArrayList<InetAddress>();;
    
    private int priority;
    
    private int weight;
    
    private int port;
    
    private List<String> textRecords = new ArrayList<String>();

    private String flags;

    private String service;

    private Name replacement;

    private String regexp;
    
    
    ResourceRecord()
    {
        super();
    }
    
    
    public ResourceRecord(int type, Name name)
    {
        super();
        this.type = type;
        this.name = name;
    }
    
    
    ResourceRecord(int type, Name name, int priority, int weight, int port, Name host, List<InetAddress> addresses, Name target, Iterable<String> textRecords)
    {
        super();
        this.type = type;
        this.name = name;
        this.host = host;
        this.target = target;
        this.addresses = addresses;
        this.priority = priority;
        this.weight = weight;
        this.port = port;
        setTextRecords(textRecords);
    }


    public Name getName()
    {
        return name;
    }


    public Name getHost()
    {
        return host;
    }


    public Name getTarget()
    {
        return target;
    }
    
    
    public void setHost(Name name)
    {
        this.host = name;
    }
    
    
    public void setTarget(Name target)
    {
        this.target = target;
    }
    
    
    public List<InetAddress> getAddresses()
    {
        return addresses;
    }
    
    
    public void setAddresses(List<InetAddress> addresses)
    {
        this.addresses = addresses;
    }
    
    
    public int getPriority()
    {
        return priority;
    }
    
    
    public void setPriority(int priority)
    {
        this.priority = priority;
    }
    
    
    public int getWeight()
    {
        return weight;
    }
    
    
    public void setWeight(int weight)
    {
        this.weight = weight;
    }
    
    
    public int getPort()
    {
        return port;
    }
    
    
    public void setPort(int port)
    {
        this.port = port;
    }
    
    
    public Iterable<String> getTextRecords()
    {
        return textRecords;
    }
    
    
    public void setTextRecords(Iterable<String> records)
    {
        if (this.textRecords != null)
        {
            this.textRecords.clear();
        } else
        {
            this.textRecords = new ArrayList<String>();
        }
        
        addTextRecords(records);
    }


    public void addTextRecords(Iterable<String> records)
    {
        if (records != null)
        {
            for (String textRecord : records)
            {
                this.textRecords.add(textRecord);
            }
        }
    }


    public void addAddress(InetAddress address)
    {
        addresses.add(address);
    }


    public int getType()
    {
        return type;
    }


    public void setFlags(String flags)
    {
        this.flags = flags;
    }


    public String getFlags()
    {
        return this.flags;
    }


    public void setOrder(int order)
    {
        this.weight = order;
    }


    public int getOrder(int order)
    {
        return this.weight;
    }


    public void setPreference(int preference)
    {
        this.priority = preference;
    }


    public int getPreference(int preference)
    {
        return this.priority;
    }


    public void setRegexp(String regexp)
    {
        this.regexp = regexp;
    }


    public String getRegexp()
    {
        return this.regexp;
    }


    public void setReplacement(Name replacement)
    {
        this.replacement = replacement;
    }


    public Name getReplacement()
    {
        return this.replacement;
    }


    public void setService(String service)
    {
        this.service = service;
    }


    public String getService()
    {
        return this.service;
    }
}
