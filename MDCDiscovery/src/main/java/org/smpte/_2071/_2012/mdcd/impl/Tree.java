package org.smpte._2071._2012.mdcd.impl;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Info object used to represent resources in a tree structure. This
 * implementation is not thread safe. T = type, V = value
 * 
 * @author Steve Posick
 */
@XmlRootElement(name = "Tree")
@XmlType(name="Tree", propOrder = {"value"})
@XmlAccessorType(XmlAccessType.NONE)
public class Tree<T extends AbstractTree<T, V>, V> extends AbstractTree<T, V>
{
    private static final long serialVersionUID = 8926221418600464310L;
    
    
    @XmlElement
    private V value;
    

    /**
     * Creates a new Tree node with the specified value.
     */
    public Tree()
    {
        super();
    }


    /**
     * Creates a new Tree built with the specified structure.
     * 
     * @param ancestors The parent hierchy
     * @param value The value
     */
    public Tree(Collection<V> ancestors, V value)
    {
        super(ancestors, value);
    }


    /**
     * Creates a new Tree node with the specified value and automatically
     * adds it as a child to the specified node.
     * 
     * @param parent The parent node
     * @param value The value
     */
    public Tree(T parent, V value)
    {
        super(parent, value);
    }


    /**
     * Creates a new Tree node with the specified value.
     * 
     * @param value The value
     */
    public Tree(V value)
    {
        super(value);
    }


    /**
     * Gets the value for this node in the Tree.
     * 
     * @return The value for this node in the Tree
     */
    public V getValue()
    {
        return value;
    }
    

    /**
     * Sets the value for this node in the Tree.
     * 
     * @param value The value for this node in the Tree
     */
    public void setValue(V value)
    {
        this.value = value;
    }
}