package org.smpte._2071._2012.mdcd;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ServiceInstance implements Serializable
{
    private static final long serialVersionUID = 201210181454L;

    private ServiceName name;

    private String host;
    
    private String target;
    
    private List<InetAddress> addresses = new ArrayList<InetAddress>();
    
    private int priority;
    
    private int weight;
    
    private int port;
    
    private Map<String, String> textRecord = new LinkedHashMap<String, String>();
    
    private String niceText;
    
    
    public ServiceInstance(ServiceName name)
    {
        this.name = name;
    }
    
    
    public ServiceInstance(ServiceName name, int priority, int weight, int port, String host, List<InetAddress> addresses, String target, Iterable<String> textRecords)
    {
        this(name, priority, weight, port, host, addresses, target, parseTextRecords(textRecords));
    }
    
    
    public ServiceInstance(ServiceName name, int priority, int weight, int port, String host, List<InetAddress> addresses, String target, Map<String, String> textRecord)
    {
        super();
        this.name = name;
        if (target != null && (name.getInstance() == null || name.getInstance().length() == 0))
        {
            try
            {
                ServiceName targetName = new ServiceName(target);
                String targetFullName = targetName.toString();
                if (targetName.getInstance() != null && targetFullName != null)
                {
                    String nameFullName = name.toString();
                    if (targetFullName.endsWith(nameFullName))
                    {
                        this.name = targetName;
                    }
                }
            } catch (Exception e)
            {
                // ignore
            }
        }
        
        this.host = host;
        this.target = target;
        this.priority = priority;
        this.weight = weight;
        this.port = port;
        
        if (addresses != null)
        {
            this.addresses.addAll(addresses);
        }
        
        if (textRecord != null)
        {
            this.textRecord.putAll(textRecord);
        }
    }
    

    public ServiceName getName()
    {
        return name;
    }
    
    
    public String getHost()
    {
        return host;
    }
    
    
    public String getTarget()
    {
        return target;
    }


    public List<InetAddress> getAddresses()
    {
        return addresses;
    }


    public int getPriority()
    {
        return priority;
    }


    public int getWeight()
    {
        return weight;
    }


    public int getPort()
    {
        return port;
    }


    public Map<String, String> getTextRecord()
    {
        return textRecord;
    }


    public String getNiceText()
    {
        return niceText;
    }


    public void setHost(String host)
    {
        this.host = host;
    }


    public void setTarget(String target)
    {
        this.target = target;
    }


    public void setAddresses(List<InetAddress> addresses)
    {
        this.addresses.clear();
        if (addresses != null)
        {
            this.addresses.addAll(addresses);
        }
    }


    public void setPriority(int priority)
    {
        this.priority = priority;
    }


    public void setWeight(int weight)
    {
        this.weight = weight;
    }


    public void setPort(int port)
    {
        this.port = port;
    }


    public void addTextRecords(Iterable<String> textRecords)
    {
        Map<String, String> newTextRecords = parseTextRecords(textRecords);
        if (newTextRecords != null)
        {
            this.textRecord.putAll(newTextRecords);
        }
    }


    public void setNiceText(String niceText)
    {
        this.niceText = niceText;
    }


    private static Map<String, String> parseTextRecords(Iterable<String> rawTextRecords)
    {
        Map<String, String> textRecord = new LinkedHashMap<String, String>();
        
        if (textRecord != null)
        {
            for (String rawTextRecord : rawTextRecords)
            {
                List<String> pairs = split(rawTextRecord);
                for (String pair : pairs)
                {
                    if (pair != null && pair.length() > 0)
                    {
                        String key = "";
                        String value = "";
                        
                        int index = pair.indexOf('=');
                        if (index >= 0)
                        {
                            key = pair.substring(0, index);
                            index++;
                            if (index <= pair.length())
                            {
                                value = pair.substring(index);
                            }
                        } else
                        {
                            key = pair;
                        }
                        
                        textRecord.put(key, value);
                    }
                }
            }
        }
        
        return textRecord;
    }
    
    
    private static List<String>  split(String text) 
    {
        ArrayList<String> list = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        
        boolean inQuote = false;
        boolean escape = false;
        char[] chars = (text + '\n').toCharArray();
        
        for (int index = 0; index < chars.length; index++)
        {
            if (!Character.isWhitespace(chars[index]))
            {
                switch (chars[index])
                {
                    case '\\':
                        escape = true;
                        break;
                    case '\"':
                        if (!escape)
                        {
                            inQuote = !inQuote;
                            break;
                        } else
                        {
                            builder.append(chars[index]);
                        }
                        break;
                    default:
                        builder.append(chars[index]);
                        if (escape)
                        {
                            escape = false;
                        }
                        break;
                }
            } else
            {
                list.add(builder.toString());
                builder.setLength(0);
            }
        }
        
        return list;
    }
    
    
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Service (\"").append(this.name).append("\"");
        
        if (target != null)
        {
            builder.append(" \"").append(target).append("\"");
        }
        
        if (host != null)
        {
            builder.append(" can be reached at \"").append(host).append("\" ").append(getAddresses());
        }
        
        if (port > 0)
        {
            builder.append(" on port ").append(getPort());
        }
        
        StringBuilder textBuilder = new StringBuilder();
        if (textRecord != null && textRecord.size() > 0)
        {
            for (Map.Entry<String, String> entry : textRecord.entrySet())
            {
                if (textBuilder.length() == 0)
                {
                    builder.append("\n\tTXT: ");
                }
                
                textBuilder.append(entry.getKey());
                String value = entry.getValue();
                if (value != null)
                {
                    textBuilder.append("=\"").append(value).append("\"");
                }
                textBuilder.append(", ");
                
                if (textBuilder.length() > 100)
                {
                    textBuilder.setLength(builder.length() - 2); // Trim trailing comma
                    builder.append(textBuilder);
                    textBuilder.setLength(0);
                }
            }
            
            if (textBuilder.length() > 0)
            {
                textBuilder.setLength(builder.length() - 2); // Trim trailing comma
                builder.append(textBuilder);
                textBuilder.setLength(0);
            }
        }
        
        builder.append(")");
        return builder.toString();
    }
}
