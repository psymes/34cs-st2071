package org.smpte._2071._2012.mdcd.naming;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NameRecord implements Serializable
{
    private static final long serialVersionUID = 201210181454L;
    
    private String target;
    
    private List<InetAddress> addresses;
    
    
    public NameRecord(String target, InetAddress address)
    {
        super();
        this.target = target;
        
        ArrayList<InetAddress> newAddresses = new ArrayList<InetAddress>();
        newAddresses.add(address);
        
        this.addresses = Collections.unmodifiableList(newAddresses);
    }
    
    
    public NameRecord(String target, List<InetAddress> addresses)
    {
        super();
        this.target = target;
        
        ArrayList<InetAddress> newAddresses = new ArrayList<InetAddress>();
        newAddresses.addAll(addresses);
        
        this.addresses = Collections.unmodifiableList(newAddresses);
    }
    
    
    public String getTarget()
    {
        return target;
    }
    
    
    public List<InetAddress> getAddresses()
    {
        return addresses;
    }
}
