package org.smpte._2071._2012.mdcd.naming.jmdns;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;
import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.ServiceName;
import org.smpte._2071._2012.mdcd.impl.DiscoveryServiceImpl;
import org.smpte._2071._2012.mdcd.impl.Utils;
import org.smpte._2071._2012.mdcd.naming.AbstractNamingServiceImpl;
import org.smpte._2071._2012.mdcd.naming.NameRecord;
import org.smpte._2071._2012.mdcd.net.InetAddressUtils;

public class NamingServiceImpl extends AbstractNamingServiceImpl implements ServiceListener
{
    public static final String MDNS_GROUP = "224.0.0.251";
    
    public static final String MDNS_GROUP_IPV6 = "FF02::FB";
    
    public static final int    MDNS_PORT = Integer.parseInt(System.getProperty("net.mdns.port", "5353"));
    
    private JmDNS resolver;


    public NamingServiceImpl() 
    throws UnknownHostException
    {
        super();
        
        InetAddress serverAddress = null;
        
        if (this.localAddress instanceof Inet6Address) 
        {
            serverAddress = InetAddress.getByName(MDNS_GROUP_IPV6);
        } else 
        {
            serverAddress = InetAddress.getByName(MDNS_GROUP);
        }
        
        // Setup unchangeable Server Addresses for mDNS!
        LinkedHashSet<InetSocketAddress> serverAddresses = new LinkedHashSet<InetSocketAddress>();
        serverAddresses.add(new InetSocketAddress(serverAddress, MDNS_PORT));
        this.serverAddresses = Collections.unmodifiableSet(serverAddresses);
    }
    
    
    @Override
    public void close()
    throws IOException
    {
        if (resolver != null)
        {
            try
            {
                resolver.close();
            } catch (IOException e)
            {
                // Ignore.  Honestly, WHO cares if close() raises an exception.  Even if they did what 
                // could be done about it, NOTHING.  SO WHY BOTHER throwing IOException on close()!! 
            }
        }
    }
    
    
    @Override
    public void setLocalAddress(InetAddress localAddress)
    throws NamingException
    {
        super.setLocalAddress(localAddress);
        try
        {
            this.resolver = JmDNS.create(localAddress, null);
//            this.resolver.addServiceListener(DiscoveryServiceImpl._DOMAIN_UDP, this);
        } catch (IOException e)
        {
            throw Utils.convertThrowable(NamingException.class, e.getMessage(), e);
        }
    }


    @Override
    public void setServerAddresses(Set<InetSocketAddress>  serverAddresses)
    throws NamingException
    {
        // NOOP - Server Addresses are unchangeable.
    }


    @Override
    public boolean addServerAddresses(Set<InetSocketAddress>  serverAddresses)
    throws NamingException
    {
        // NOOP - Server Addresses are unchangeable.
        return false;
    }
    
    
    @Override
    public List<NameRecord> query(ServiceName serviceName)
    throws NamingException
    {
        List<NameRecord> results = new ArrayList<NameRecord>();
        
        List<ServiceInfo> list = listServiceInformation(serviceName);
        if (list.size() > 0)
        {
            for (ServiceInfo service : list)
            {
                results.add(convertServiceToNameRecord(service));
            }
        }
        
        return results;
    }


    @Override
    public List<ServiceInstance> serviceDiscovery(ServiceName serviceName)
    throws NamingException
    {
        List<ServiceInstance> results = new ArrayList<ServiceInstance>();
        
        List<ServiceInfo> list = listServiceInformation(serviceName);
        if (list.size() > 0)
        {
            for (ServiceInfo service : list)
            {
                results.add(convertServiceToServiceInstance(serviceName, service));
            }
        }
        
        return results;
    }


    private List<ServiceInfo> listServiceInformation(ServiceName serviceName)
    {
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        List<String> domains = new ArrayList<String>();
        
        final String domain = serviceName.getDomain();
        final String serviceInstance = serviceName.getInstance();
        final String serviceFullType = serviceName.getFullType();
        
        if (domain != null && domain.length() > 0)
        {
            domains.add(domain);
        } else
        {
            domains.addAll(this.domains);
        }
        
        for (String tempDomain : domains)
        {
            String qualifiedName = serviceName.getNameWithoutDomain() + "." + tempDomain;
            
            if (serviceInstance != null && serviceInstance.length() > 0)
            {
                ServiceInfo[] services = this.resolver.list(serviceFullType + "." + tempDomain); // getServiceInfo(serviceFullType + "." + tempDomain, serviceInstance, true);
//                ServiceInfo[] services1 = new ServiceInfo[] {this.resolver.getServiceInfo(serviceFullType + "." + tempDomain, serviceInstance, true)};
                if (services != null && services.length > 0)
                {
                    list.addAll(Arrays.asList(services));
                }
            } else
            {
                list.addAll(Arrays.asList(this.resolver.list(qualifiedName)));
            }
        }
        
        return list;
    }
    
    
    private NameRecord convertServiceToNameRecord(ServiceInfo service)
    {
        String qualifiedName = service.getQualifiedName();
        
        InetAddress[] addresses = service.getInetAddresses();
        return new NameRecord(qualifiedName, Arrays.asList(addresses));
    }
    
    
    private ServiceInstance convertServiceToServiceInstance(ServiceName serviceName, ServiceInfo service)
    {
        List<String> strings = new ArrayList<String>();
        
        String qualifiedName = service.getServer();
        String target = service.getQualifiedName();
        int weight = service.getWeight();
        
        int port = service.getPort();
        int priority = service.getPriority();
        
        Enumeration<String> props = service.getPropertyNames();
        while (props.hasMoreElements())
        {
            String prop = props.nextElement();
            String value = service.getPropertyString(prop);
            strings.add(prop + "=" + value);
        }
        
        InetAddress[] addresses = service.getInetAddresses();
        return new ServiceInstance(serviceName, priority, weight, port, qualifiedName, Arrays.asList(addresses), target, strings);
    }
    
    /*
    private ServiceInfo convertServiceToServiceInstance(ServiceInstance service)
    {
        List<String> strings = new ArrayList<String>();
        
        ServiceInfo info = ServiceInfo.create(type, name, subtype, port, weight, priority, persistent, text);
        
        String qualifiedName = service.getServer();
        String target = service.getQualifiedName();
        int weight = service.getWeight();
        
        int port = service.getPort();
        int priority = service.getPriority();
        
        Enumeration<String> props = service.getPropertyNames();
        while (props.hasMoreElements())
        {
            String prop = props.nextElement();
            String value = service.getPropertyString(prop);
            strings.add(prop + "=" + value);
        }
        
        InetAddress[] addresses = service.getInetAddresses();
        return new ServiceInstance(serviceName, priority, weight, port, qualifiedName, Arrays.asList(addresses), target, strings);
    }
    */


    @Override
    public void serviceAdded(ServiceEvent event)
    {
        if (DiscoveryServiceImpl._DOMAIN_UDP.equals(event.getType()))
        {
            ServiceInstance namingService = convertServiceToServiceInstance(new ServiceName(event.getName()), event.getInfo());
            namingServiceAdded(namingService, this.localAddress);
        }
    }


    @Override
    public void serviceRemoved(ServiceEvent event)
    {
        if (DiscoveryServiceImpl._DOMAIN_UDP.equals(event.getType()))
        {
            ServiceInstance namingService = convertServiceToServiceInstance(new ServiceName(event.getName()), event.getInfo());
            namingServiceRemoved(namingService, this.localAddress);
        }
    }


    @Override
    public void serviceResolved(ServiceEvent event)
    {
        // Ignore
    }


    @Override
    public List<NameRecord> reverseQuery(InetAddress address)
    throws NamingException
    {
        ArrayList<NameRecord> results = new ArrayList<NameRecord>();
        ServiceInfo[] infos = this.resolver.list(InetAddressUtils.reverseMapAddress(address));
        if (infos != null && infos.length > 0)
        {
            for (ServiceInfo info : infos)
            {
                String name = info.getQualifiedName();
                if (name != null)
                {
                    results.add(new NameRecord(name, address));
                }
            }
        }
        
        return results;
    }
    
    
    @Override
    public NAMING_TYPE getNamingType()
    {
        return NAMING_TYPE.MULTICAST_DNS;
    }
}
