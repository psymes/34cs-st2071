package org.smpte._2071._2012.mdcd.naming.jndi;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.ServiceName;
import org.smpte._2071._2012.mdcd.impl.Utils;
import org.smpte._2071._2012.mdcd.naming.AbstractNamingServiceImpl;
import org.smpte._2071._2012.mdcd.naming.NameRecord;
import org.smpte._2071._2012.mdcd.net.InetAddressUtils;

public class NamingServiceImpl extends AbstractNamingServiceImpl
{
    private static final String[] QUERY_TYPES = new String[] {"A", "PTR", "SRV"};
    
    private static final String[] PTR_TYPES = new String[] {"PTR"};
    
    private static final String[] SRV_TYPES = new String[] {"PTR", "SRV"};
    
    private static final String[] TXT_TYPES = new String[] {"TXT"}; 
    
    private static final String[] ADDRESS_TYPES = new String[] {"A", "AAAA"};
    
    private Hashtable<String, String> env = new Hashtable<String, String>();
    
    private DirContext context;
    
    
    public NamingServiceImpl()
    throws NamingException
    {
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
        context = new InitialDirContext(env);
    }
    
    
    @Override
    public List<NameRecord> reverseQuery(InetAddress address)
    throws NamingException
    {
        ArrayList<NameRecord> list = new ArrayList<NameRecord>();
        
        Attributes attrs = context.getAttributes(InetAddressUtils.reverseMapAddress(address), PTR_TYPES);
        
        NamingEnumeration<? extends Attribute> enumerator = attrs.getAll();
        while (enumerator.hasMore())
        {
            Attribute attr = enumerator.next();
            String type = attr.getID();
            
            if ("PTR".equals(type))
            {
                NamingEnumeration<?> values = attr.getAll();
                while (values.hasMore())
                {
                    Object value = values.next();
                    if (value != null)
                    {
                        list.add(new NameRecord(value.toString(), address));
                    }
                }
            }
        }
        
        return list;
    }
    
    
    @Override
    public List<NameRecord> query(ServiceName serviceName)
    throws NamingException
    {
        ArrayList<NameRecord> list = new ArrayList<NameRecord>();
        
        try
        {
            Attributes attrs = context.getAttributes(serviceName.toString(), QUERY_TYPES);
            
            NamingEnumeration<? extends Attribute> enumerator = attrs.getAll();
            while (enumerator.hasMore())
            {
                Attribute attr = enumerator.next();
                String type = attr.getID();
                
                if ("PTR".equals(type))
                {
                    NamingEnumeration<?> values = attr.getAll();
                    while (values.hasMore())
                    {
                        Object value = values.next();
                        if (value != null)
                        {
                            list.add(new NameRecord(value.toString(), (InetAddress) null));
                        }
                    }
                } else if ("SRV".equals(type))
                {
                    NamingEnumeration<?> values = attr.getAll();
                    while (values.hasMore())
                    {
                        Object value = values.next();
                        if (value != null)
                        {
                            list.add(new NameRecord(parseHostFromSRV(value.toString()), (InetAddress) null));
                        }
                    }
                } else if ("A".equals(type))
                {
                    NamingEnumeration<?> values = attr.getAll();
                    while (values.hasMore())
                    {
                        Object value = values.next();
                        if (value != null)
                        {
                            try
                            {
                                list.add(new NameRecord(serviceName.toString(), InetAddress.getByName(value.toString())));
                            } catch (UnknownHostException e)
                            {
                                Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                            }
                        }
                    }
                }
            }
        } catch (NamingException e)
        {
            Utils.log(log, Level.WARNING, "Issue querying " + Arrays.toString(QUERY_TYPES) + " for \"" + serviceName + "\" " + e.getMessage() + " - " + e.getExplanation(), Level.FINE, e);
        }
        
        return list;
    }
    
    
    private String parseHostFromSRV(String srv)
    {
        if (srv != null)
        {
            String string = srv.trim();
            
            int pos = 0;
            for (int index = 0; index < 3 && pos != -1; index++)
            {
                pos = string.indexOf(' ', pos + 1);
            }
            
            if (pos > 0)
            {
                pos++;
                if (pos < string.length())
                {
                    return string.substring(pos);
                } else
                {
                    return "";
                }
            } else
            {
                return null;
            }
        } else
        {
            return null;
        }
    }


    @Override
    public List<ServiceInstance> serviceDiscovery(ServiceName serviceName)
    throws NamingException
    {
        ArrayList<ServiceInstance> services = new ArrayList<ServiceInstance>();
        Attributes attrs = null;
        try
        {
            attrs = context.getAttributes(serviceName.toString(), SRV_TYPES);
        } catch (NamingException e)
        {
            Utils.log(log, Level.WARNING, "Issue querying " + Arrays.toString(SRV_TYPES) + " \"" + serviceName + "\"" + e.getMessage() + " - " + e.getExplanation(), Level.FINE, e);
        }
        
        if (attrs == null)
        {
            return services;
        }
        
        NamingEnumeration<? extends Attribute> enumerator = attrs.getAll();
        while (enumerator.hasMore())
        {
            Attribute attr = enumerator.next();
            String type = attr.getID();
            
            if ("PTR".equals(type))
            {
                NamingEnumeration<?> values = attr.getAll();
                while (values.hasMore())
                {
                    Object value = values.next();
                    if (value != null)
                    {
                        try
                        {
                            services.addAll(serviceDiscovery(new ServiceName(value.toString())));
                        } catch (NamingException e)
                        {
                            // Already Logged
                        }
                    }
                }
            } else if ("SRV".equals(type))
            {
                NamingEnumeration<?> values = attr.getAll();
                while (values.hasMore())
                {
                    Object value = values.next();
                    if (value != null)
                    {
                        ServiceInstance service = parseSRVRecord(serviceName, value.toString());
                        services.add(service);
                        
                        try
                        {
                            ArrayList<String> textRecords = new ArrayList<String>();
                            Attributes txtAttrs = context.getAttributes(serviceName.toString(), TXT_TYPES);
                            NamingEnumeration<? extends Attribute> txtEnumerator = txtAttrs.getAll();
                            while (txtEnumerator.hasMore())
                            {
                                Attribute txtAttr = txtEnumerator.next();
                                NamingEnumeration<?> txtValues = txtAttr.getAll();
                                while (txtValues.hasMore())
                                {
                                    Object txtValue = txtValues.next();
                                    if (txtValue != null)
                                    {
                                        textRecords.add(txtValue.toString());
                                    }
                                }
                            }
                            service.addTextRecords(textRecords);
                        } catch (NamingException e)
                        {
                            Utils.log(log, Level.WARNING, "Issue querying " + TXT_TYPES + " Record for \"" + serviceName + "\" " + e.getMessage() + " - " + e.getExplanation(), Level.FINE, e);
                            throw new NamingException("DNS-SD Error: Could not find " + Arrays.toString(TXT_TYPES) + " record for \"" + serviceName + "\" - " + e.getMessage());
                        }
                        
                        ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();
                        try
                        {
                            Attributes aAttrs = context.getAttributes(service.getHost(), ADDRESS_TYPES);
                            NamingEnumeration<? extends Attribute> aEnumerator = aAttrs.getAll();
                            while (aEnumerator.hasMore())
                            {
                                Attribute aAttr = aEnumerator.next();
                                Object aValue = aAttr.get();
                                if (aValue != null)
                                {
                                    try
                                    {
                                        addresses.add(InetAddress.getByName(aValue.toString()));
                                    } catch (Exception e)
                                    {
                                        Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                                    }
                                }
                            }
                            service.setAddresses(addresses);
                        } catch (NamingException e)
                        {
                            Utils.log(log, Level.WARNING, "Issue querying " + Arrays.toString(ADDRESS_TYPES) + " \"" + service.getHost() + "\"" + e.getMessage() + " - " + e.getExplanation(), Level.FINE, e);
                        }
                    }
                }
            }
        }
        
        return services;
    }

    
    private ServiceInstance parseSRVRecord(ServiceName serviceName, String string)
    {
        if (string == null)
        {
            return null;
        }
        
        int priority = -1;
        int weight = -1;
        int port = -1;
        String host = null;
        String target = null;
        
        int column = 0;
        int length = string.length();
        StringBuilder buffer = new StringBuilder();
        for (int index = 0; index <= length; index++)
        {
            char c;
            if (index == length)
            {
                c = ' ';
            } else
            {
                c = string.charAt(index);
            }
            
            if (Character.isWhitespace(c))
            {
                switch (column++)
                {
                    case 0 :
                        try
                        {
                            priority = Integer.parseInt(buffer.toString());
                        } catch (Exception e)
                        {
                            Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                        }
                        break;
                    case 1 :
                        try
                        {
                            weight = Integer.parseInt(buffer.toString());
                        } catch (Exception e)
                        {
                            Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                        }
                        break;
                    case 2 :
                        try
                        {
                            port = Integer.parseInt(buffer.toString());
                        } catch (Exception e)
                        {
                            Utils.log(log, Level.WARNING, e.getMessage(), Level.FINE, e);
                        }
                        break;
                    case 3 :
                        target = host = buffer.toString();
                        break;
                }
                buffer.setLength(0);
            } else
            {
                buffer.append(c);
            }
        }
        
        return new ServiceInstance(serviceName, priority, weight, port, host, (List<InetAddress>) null, target, (Map<String, String>) null);
    }


    @Override
    public void close()
    throws IOException
    {
        try
        {
            context.close();
        } catch (NamingException e)
        {
            Utils.convertThrowable(IOException.class, "Error closing JNDI Naming Service - " + e.getMessage(), e);
        }
    }
    
    
    public static void main(String[] args)
    throws Exception
    {
//        org.smpte._2071._2012.mdcd.naming.dnsjava.NamingServiceImpl namingService = new org.smpte._2071._2012.mdcd.naming.dnsjava.NamingServiceImpl();
        NamingServiceImpl namingService = new NamingServiceImpl();
        List<ServiceInstance> records = namingService.serviceDiscovery(new ServiceName("_mdc._tcp.mdc.espn.pvt"));
        System.out.println(records);
        List<NameRecord> names = namingService.query(new ServiceName("Root._device_v1._sub._mdc._tcp.mdc.espn.pvt"));
        System.out.println(names);
        namingService.close();
    }
}
