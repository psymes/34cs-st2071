package org.smpte._2071._2012.mdcd.naming;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import org.smpte._2071._2012.mdcd.ServiceInstance;
import org.smpte._2071._2012.mdcd.ServiceName;
import org.smpte._2071._2012.mdcd.impl.Utils;

public class NamingFacade implements NamingService
{
    protected final Logger log = Logger.getLogger(getClass().getName());
    
    protected List<NamingService> namingServices = new CopyOnWriteArrayList<NamingService>();
    
    protected InetAddress localAddress;
    
    
    public NamingFacade(NamingService... namingServices)
    {
        this.namingServices.addAll(Arrays.asList(namingServices));
    }
    
    
    public NamingFacade(Iterable<NamingService> namingServices)
    {
        ArrayList<NamingService> services = new ArrayList<NamingService>();
        for (NamingService naming : namingServices)
        {
            services.add(naming);
        }
        this.namingServices.addAll(services);
    }
    
    
    @Override
    public void setLocalAddress(InetAddress localAddress)
    throws NamingException
    {
        this.localAddress = localAddress;
        for (NamingService naming : namingServices)
        {
            try
            {
                naming.setLocalAddress(localAddress);
            } catch (Exception e)
            {
                // ignore
            }
        }
    }


    @Override
    public Set<InetSocketAddress> getServerAddresses()
    {
        Set<InetSocketAddress> serverAddresses = new LinkedHashSet<InetSocketAddress>();
        for (NamingService naming : namingServices)
        {
            Set<InetSocketAddress> addresses = naming.getServerAddresses();
            if (addresses != null && !addresses.isEmpty())
            {
                serverAddresses.addAll(addresses);
            }
        }
        return serverAddresses;
    }
    
    
    @Override
    public List<NameRecord> query(ServiceName serviceName)
    throws NamingException
    {
        ArrayList<NameRecord> names = new ArrayList<NameRecord>();
        for (NamingService naming : namingServices)
        {
            List<NameRecord> results = naming.query(serviceName);
            if (results != null && !results.isEmpty())
            {
                for (NameRecord result : results)
                {
                    names.add(result);
                }
            }
        }
        return names;
    }


    @Override
    public List<NameRecord> reverseQuery(InetAddress address)
    throws NamingException
    {
        ArrayList<NameRecord> names = new ArrayList<NameRecord>();
        for (NamingService naming : namingServices)
        {
            List<NameRecord> results = naming.reverseQuery(address);
            if (results != null && !results.isEmpty())
            {
                for (NameRecord result : results)
                {
                    names.add(result);
                }
            }
        }
        return names;
    }
    
    
    @Override
    public List<ServiceInstance> serviceDiscovery(ServiceName serviceName)
    throws NamingException
    {
        ArrayList<ServiceInstance> names = new ArrayList<ServiceInstance>();
        for (NamingService naming : namingServices)
        {
            List<ServiceInstance> results = naming.serviceDiscovery(serviceName);
            if (results != null && !results.isEmpty())
            {
                for (ServiceInstance result : results)
                {
                    names.add(result);
                }
            }
        }
        return names;
    }
    
    
    public NamingService getNamingService(int index)
    {
        return namingServices.get(index);
    }
    
    
    public int numberOfNamingServices()
    {
        return namingServices.size();
    }


    public void addNamingService(NamingService naming)
    {
        namingServices.add(naming);
    }


    public NamingService removeNamingService(int index)
    {
        NamingService naming = namingServices.remove(index);
        if (naming != null)
        {
            try
            {
                naming.close();
            } catch (Exception e)
            {
                Utils.log(log, Level.INFO, e.getMessage(), Level.FINE, e);
            }
        }
        
        return naming;
    }


    public boolean removeNamingService(NamingService naming)
    {
        if (namingServices.remove(naming))
        {
            try
            {
                naming.close();
            } catch (Exception e)
            {
                Utils.log(log, Level.INFO, e.getMessage(), Level.FINE, e);
            }
            
            return true;
        }
        
        return false;
    }


    @Override
    public void close()
    throws IOException
    {
        ArrayList<IOException> exceptions = new ArrayList<IOException>();
        for (NamingService naming : namingServices)
        {
            try
            {
                naming.close();
            } catch (IOException e)
            {
                exceptions.add(e);
            }
        }
        
        if (exceptions.size() > 0)
        {
            throw exceptions.get(0);
        }
    }


    @Override
    public InetAddress getLocalAddress()
    {
        return this.localAddress;
    }


    @Override
    public Set<String> getDomains()
    {
        Set<String> allDomains = new LinkedHashSet<String>();
        for (NamingService naming : namingServices)
        {
            Set<String> domains = naming.getDomains();
            if (domains != null && !domains.isEmpty())
            {
                allDomains.addAll(domains);
            }
        }
        return allDomains;
    }


    @Override
    public boolean isAssociatedToDomain(String domain)
    {
        boolean isAssociated = false;
        for (NamingService naming : namingServices)
        {
            isAssociated = naming.isAssociatedToDomain(domain);
            if (isAssociated)
            {
                break;
            }
        }
        return isAssociated;
    }


    @Override
    public boolean associateToDomain(String domain)
    throws NamingException
    {
        boolean isAssociated = false;
        for (NamingService naming : namingServices)
        {
            boolean temp = naming.associateToDomain(domain);
            if (temp)
            {
                isAssociated = true;
            }
        }
        return isAssociated;
    }


    @Override
    public boolean unassociateToDomain(String domain)
    throws NamingException
    {
        boolean isAssociated = false;
        for (NamingService naming : namingServices)
        {
            boolean temp = naming.unassociateToDomain(domain);
            if (temp)
            {
                isAssociated = true;
            }
        }
        return isAssociated;
    }

    
    @Override
    public boolean addServerAddresses(Set<InetSocketAddress>  serverAddresses)
    throws NamingException
    {
        boolean isAssociated = false;
        for (NamingService naming : namingServices)
        {
            boolean temp = naming.addServerAddresses(serverAddresses);
            if (temp)
            {
                isAssociated = true;
            }
        }
        return isAssociated;
    }
    
    
    @Override
    public void setServerAddresses(Set<InetSocketAddress> serverAddresses)
    throws NamingException
    {
        for (NamingService naming : namingServices)
        {
            naming.setServerAddresses(serverAddresses);
        }
    }


    @Override
    public void addNamingListener(NamingListener listener)
    {
        for (NamingService naming : namingServices)
        {
            naming.addNamingListener(listener);
        }
    }


    @Override
    public void removeNamingListener(NamingListener listener)
    {
        for (NamingService naming : namingServices)
        {
            naming.removeNamingListener(listener);
        }
    }


    @Override
    public NAMING_TYPE getNamingType()
    {
        NAMING_TYPE type = null;
        for (NamingService naming : namingServices)
        {
            NAMING_TYPE temp = naming.getNamingType();
            if (type == null)
            {
                type = temp;
            } else if (temp != type)
            {
                type = NAMING_TYPE.MULTICAST_DNS;
            }
        }
        return type;
    }
}
